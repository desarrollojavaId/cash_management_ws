package ar.com.gestionit.cashmanagement;

import java.util.List;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
public class FormateadorAs400Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/* NUEVO
		String query = "SELECT * from( SELECT ROW_NUMBER() OVER(ORDER BY fecha DESC) AS rownumber, result.* FROM ((SELECT mtlqop as MTLQOP, mtlqso as MTLQSO, mtlfpa as MTLFPA, mtlqrc as MTLQRC, CASE WHEN mtpnom = '' THEN (SELECT mthnom FROM mtproord WHERE ( mtlqop = mthqop AND mtlqso = mthqso )) ELSE mtpnom END AS PROVEENOM, (select sckder from SCTABLAS where SCKQTA=1 and SCKKOD=MTLKDO and scksko = 0) as sckder, mtlcui as MTLCUI, mtljpa as MTLJPA, mtlkme as MTLKME, '0' as submedio, MTLQIP, mtovto AS MTOVTO, mtlkme as statusNumber, mtlkes as statusSubnumber, (SELECT mtkdes FROM mttablas WHERE mtkqta = 88 AND mtkkod = mtlkme AND mtksko = mtlkes) as KES, mtlfuc AS FECHA, CASE WHEN (SELECT COUNT(*) FROM MTRELSUC WHERE MTAATPE=1 AND MTAASOR = MTOSUC) > 0 THEN (SELECT MTAASDE FROM MTRELSUC WHERE MTAATPE=1 AND MTAASOR = MTOSUC) ELSE MTOSUC END as codSuc, CASE WHEN (SELECT COUNT(*) FROM MTRELSUC WHERE MTAATPE=1 AND MTAASOR = MTOSUC) > 0 THEN ( SELECT MTXDEN FROM MTEHSUCU WHERE mtxkme=3 AND mtxids = (SELECT MTAASDE FROM MTRELSUC WHERE MTAATPE=1 AND MTAASOR = MTOSUC)) ELSE ( (SELECT MTXDEN FROM MTEHSUCU WHERE mtxkme=3 AND mtxids = mtosuc)) END as descripcionSuc, CBU.x999jemod AS modulo, CBU.x999jesuc AS sucursal, CBU.x999jemda AS moneda, CBU.x999jecta AS cuenta, CBU.x999jesbo AS suboperacion, (SELECT mtkdes FROM mttablas WHERE mtkqta = 11 AND mtkkod = mtlkfp AND mtksko = 0) AS KFP, mtlapb, (SELECT mtkdes FROM mttablas WHERE mtkqta = 13 AND mtkkod = mtlapb AND mtksko = 0) AS DBE FROM mtorpago, mtprovee, mtopcheq, x999je AS CBU WHERE ( mtlqpv = mtpqpv ) AND CBU.x999jecb1 = Substr(Digits(mtlqct), 1, 8) AND CBU.x999jecb2 = Substr(Digits(mtlqct), 9, 22) AND ( mtlqop = mtoqop AND mtlqso = mtoqso ) and ( (MTLKME = ? and MTLKES = ?) or ( MTLKME = 5 and MTLKES = ?) or ( MTLKME = 7 and MTLKES = ?) or ( MTLKME = 9 and MTLKES = ?) or (MTLKME = ? and MTLKES = ?) or (MTLKME = ? and MTLKES = ?) or (MTLKME = ? and MTLKES = ?) or (MTLKME = ? and MTLKES = ?) or (MTLKME = ? and MTLKES = ?) ) and ( (MTLKME = ?) or (MTLKME = ?) or (MTLKME = ?) or (MTLKME = ?) or (MTLKME = ?) or (MTLKME = ?) or (MTLKME = ?) ) ) UNION ALL (SELECT mtlqop as MTLQOP, mtlqso as MTLQSO, mtlfpa as MTLFPA, mtlqrc as MTLQRC, CASE WHEN mtpnom = '' THEN (SELECT mthnom FROM mtproord WHERE ( mtlqop = mthqop AND mtlqso = mthqso )) ELSE mtpnom END AS PROVEENOM, (select sckder from SCTABLAS where SCKQTA=1 and SCKKOD=MTLKDO and scksko = 0) as sckder, mtlcui as MTLCUI, mtljpa as MTLJPA, mtlkme as MTLKME, '0' submedio, MTLQIP, '0001-01-01' AS MTOVTO, mtlkme as statusNumber, mtlkes as statusSubnumber, (SELECT mtkdes FROM mttablas WHERE mtkqta = 8 AND mtkkod = mtlkes AND mtksko = 0) as KES, mtlfuc AS FECHA, mtgsuc as codSuc, CASE WHEN mtgsuc = '999' THEN 'Multisucursal' ELSE (SELECT mtxden FROM mtehsucu WHERE mtxkme = 3 AND mtxids = mtgsuc) END as descripcionSuc, CBU.x999jemod AS modulo, CBU.x999jesuc AS sucursal, CBU.x999jemda AS moneda, CBU.x999jecta AS cuenta, CBU.x999jesbo AS suboperacion, mtlkfp AS KFP, mtlapb, (SELECT mtkdes FROM mttablas WHERE mtkqta = 13 AND mtkkod = mtlapb AND mtksko = 0) AS DBE FROM mtorpago, mtprovee, mtopgiro, x999je AS CBU WHERE ( mtlqpv = mtpqpv ) AND CBU.x999jecb1 = Substr(Digits(mtlqct), 1, 8) AND CBU.x999jecb2 = Substr(Digits(mtlqct), 9, 22) AND ( mtlqop = mtgqop AND mtlqso = mtgqso ) and ( (MTLKME = ? and MTLKES = ?) or ( MTLKME = 5 and MTLKES = ?) or ( MTLKME = 7 and MTLKES = ?) or ( MTLKME = 9 and MTLKES = ?) or (MTLKME = ? and MTLKES = ?) or (MTLKME = ? and MTLKES = ?) or (MTLKME = ? and MTLKES = ?) or (MTLKME = ? and MTLKES = ?) or (MTLKME = ? and MTLKES = ?) ) and ( (MTLKME = ?) or (MTLKME = ?) or (MTLKME = ?) or (MTLKME = ?) or (MTLKME = ?) or (MTLKME = ?) or (MTLKME = ?) ) ) UNION ALL (SELECT mtlqop as MTLQOP, mtlqso as MTLQSO, mtlfpa as MTLFPA, mtlqrc as MTLQRC, CASE WHEN mtpnom = '' THEN (SELECT mthnom FROM mtproord WHERE ( mtlqop = mthqop AND mtlqso = mthqso )) ELSE mtpnom END AS PROVEENOM, (select sckder from SCTABLAS where SCKQTA=1 and SCKKOD=MTLKDO and scksko = 0) as sckder, mtlcui as MTLCUI, mtljpa as MTLJPA, mtlkme as MTLKME, '0' as submedio, MTLQIP, '0001-01-01' AS MTOVTO, mtlkme as statusNumber, mtlkes as statusSubnumber, (SELECT mtkdes FROM mttablas WHERE mtkqta = 8 AND mtkkod = mtlkes AND mtksko = 0) as KES, mtlfuc AS FECHA, 'Codigo Suc' as codSuc, 'Desc Sucursal' as descripcionSuc, CBU.x999jemod AS modulo, CBU.x999jesuc AS sucursal, CBU.x999jemda AS moneda, CBU.x999jecta AS cuenta, CBU.x999jesbo AS suboperacion, mtlkfp AS KFP, mtlapb, (SELECT mtkdes FROM mttablas WHERE mtkqta = 13 AND mtkkod = mtlapb AND mtksko = 0) AS DBE FROM mtorpago, mtprovee, mtopelec, x999je AS CBU WHERE ( mtlqpv = mtpqpv ) AND CBU.x999jecb1 = Substr(Digits(mtmcbu), 1, 8) AND CBU.x999jecb2 = Substr(Digits(mtmcbu), 9, 22) AND ( mtlqop = mtmqop AND mtlqso = mtmqso ) and ( (MTLKME = ? and MTLKES = ?) or ( MTLKME = 5 and MTLKES = ?) or ( MTLKME = 7 and MTLKES = ?) or ( MTLKME = 9 and MTLKES = ?) or (MTLKME = ? and MTLKES = ?) or (MTLKME = ? and MTLKES = ?) or (MTLKME = ? and MTLKES = ?) or (MTLKME = ? and MTLKES = ?) or (MTLKME = ? and MTLKES = ?) ) and ( (MTLKME = ?) or (MTLKME = ?) or (MTLKME = ?) or (MTLKME = ?) or (MTLKME = ?) or (MTLKME = ?) or (MTLKME = ?) ) ) )AS result )as total WHERE total.rownumber >= ? AND total.rownumber <= ? ";
		//342(String), 342(String), 0(String), 0(String), 0(String), 0(String), 0(String), 0(String), -1(String), -1(String), -1(String), -1(String), 04/06/15(String), 04/06/15(String), 04/06/18(String), 04/06/18(String), %%(String), %%(String), %%(String), 0(String), 0(String), 0(String), 1(String), 0(String), 0(String), 0(String), 0(String), 6(String), 0(String), 3(String), 0(String), 8(String), 0(String), 5(String), 0(String), 7(String), 0(String), 1(String), 2(String), 3(String), 4(String), 5(String), 6(String), 8(String), 1(Integer), 6(Integer)
		String query2 = "abc?qwer?yui?bnm?asd?654";
		String[] val = new String[]{"1", "0", "0", "0", "0", "6", "0", "3", "0", "8", "0", "5", "0", "7", "0", "1", "2", "3", "4", "5", "6", "8", "1", "0", "0", "0", "0", "6", "0", "3", "0", "8", "0", "5", "0", "7", "0", "1", "2", "3", "4", "5", "6", "8", "1", "0", "0", "0", "0", "6", "0", "3", "0", "8", "0", "5", "0", "7", "0", "1", "2", "3", "4", "5", "6", "8", "1", "6"};
		*/
		//viejo
		//String query = "select result.rownumber, result.cuenta, result.PROVEENOM, result.MTLKME, result.MTLQSO, result.MTLQOP, result.MTLFRH, result.MTLACO, result.MTLCIN, result.MTLRRP, result.MTLQIP, result.MTLCUI, result.MTLJPA, result.MTLFPA, result.MTLQRC, result.KME, result.KFP, result.KES, result.statusNumber, result.statusSubnumber, result.MTOSUC, result.MTGSUC, result.OPPRONOM, result.DEC, result.DBE, result.MTOVTO, result.MTOFEM, result.fecha, result.SCKDER, result.modulo, result.sucursal, result.moneda, result.suboperacion, result.submedio, result.cbu from ( select MTPROVEE.MTPNOM AS PROVEENOM, MTORPAGO.MTLKME as MTLKME, MTTKME.MTKSKO as submedio, MTORPAGO.MTLQSO as MTLQSO, MTORPAGO.MTLQOP as MTLQOP, MTORPAGO.MTLFRH as MTLFRH, MTORPAGO.MTLACO as MTLACO, MTORPAGO.MTLCIN as MTLCIN, MTORPAGO.MTLRRP as MTLRRP, MTORPAGO.MTLQIP as MTLQIP, MTORPAGO.MTLCUI as MTLCUI, MTORPAGO.MTLJPA as MTLJPA, MTORPAGO.MTLFPA as MTLFPA, MTORPAGO.MTLQRC as MTLQRC, MTTKME.MTKDES as KME, MTTKFP.MTKDES as KFP, MTTKES.MTKDES as KES, MTTKES.MTKSKO as statusSubnumber, MTTKES.MTKKOD as statusNumber, MTOPCHEQ.MTOSUC as MTOSUC, MTOPGIRO.MTGSUC as MTGSUC, MTPROORD.MTHNOM AS OPPRONOM, MEDIOS.MTKDES AS DEC, AVISO.MTKDES as DBE, MTOPCHEQ.MTOVTO as MTOVTO, MTOPCHEQ.MTOFEM as MTOFEM, MTORPAGO.MTLFUC AS fecha , SCKDER as SCKDER, ROW_NUMBER() OVER(ORDER BY MTLFPA DESC) AS rownumber, CBU.X999JEMOD as modulo, CBU.X999JESUC as sucursal, CBU.X999JEMDA as moneda, CBU.X999JECTA as cuenta, CBU.X999JESBO as suboperacion, case WHEN MTORPAGO.MTLKME not in('2','3','8') then MTOPELEC.MTMCBU else MTORPAGO.MTLQCT end as cbu from MTTABLAS as MTTKME, MTTABLAS as MTTKFP, MTTABLAS as MTTKES, TBMONEDA, MTORPAGO left outer join SCTABLAS on (SCKQTA=1 and SCKKOD=MTLKDO) left outer join MTOPELEC on (MTORPAGO.MTLQOP=MTMQOP AND MTORPAGO.MTLQSO=MTMQSO) left outer join MTOPCHEQ ON (MTORPAGO.MTLQOP=MTOQOP AND MTORPAGO.MTLQSO=MTOQSO) left outer join MTOPGIRO ON (MTORPAGO.MTLQOP=MTGQOP and MTORPAGO.MTLQSO=MTGQSO) left outer join MTTABLAS AS AVISO ON (AVISO.MTKQTA=13 and AVISO.MTKKOD=MTLAPB) left outer join MTTABLAS AS MEDIOS ON (MEDIOS.MTKQTA=14 and MEDIOS.MTKKOD=MTLCAE) left outer join MTPROORD ON (MTORPAGO.MTLQOP=MTPROORD.MTHQOP and MTORPAGO.MTLQSO=MTPROORD.MTHQSO) LEFT OUTER JOIN MTPROVEE ON (MTORPAGO.MTLQPV=MTPROVEE.MTPQPV) left outer join X999JE as CBU on (CBU.X999JECB1 = substr(lpad(ltrim(rtrim(MTORPAGO.MTLQCT)), 22 ,'0'), 1, 8) and CBU.X999JECB2 = substr(lpad(ltrim(rtrim(MTORPAGO.MTLQCT)), 22 ,'0'), 9, 22) ) where MTTKME.MTKKOD=MTORPAGO.MTLKME and MTTKME.MTKQTA=3 and MTTKFP.MTKKOD=MTLKFP and MTTKFP.MTKQTA=11 and MTTKES.MTKQTA=88 and MTTKES.MTKKOD=MTORPAGO.MTLKME and MTTKES.MTKSKO=MTORPAGO.MTLKES and TBKMDA=MTLKMD and (MTORPAGO.MTLQCA=?) and (MTPROVEE.MTPQCA=?) and (MTLQRC <= ? or '0'=?) and (MTLQRC >= ? or '0'=?) and (MTLQRC LIKE(?) or '0'=?) and (MTLJPA <= ? or '-1'=?) and (MTLJPA >= ? or '-1'=?) and (MTLFPA >= ? or '0'=?) and (MTLFPA <= ? or '0'=?) and (MTPROORD.MTHNOM LIKE(?) or MTPROVEE.MTPNOM LIKE(?) or '0'=?) and (digits(mtorpago.mtlcui) LIKE(?) or mtorpago.mtlcui=? or ?='0' ) and ( (MTTKME.MTKKOD = ? and MTTKME.MTKSKO = ?) or (MTTKME.MTKKOD = 5 and MTTKME.MTKSKO = ?) or (MTTKME.MTKKOD = 7 and MTTKME.MTKSKO = ?) or (MTTKME.MTKKOD = 9 and MTTKME.MTKSKO = ?) or (MTTKME.MTKKOD = ? and MTTKME.MTKSKO = ?) or (MTTKME.MTKKOD = ? and MTTKME.MTKSKO = ?) or (MTTKME.MTKKOD = ? and MTTKME.MTKSKO = ?) or (MTTKME.MTKKOD = ? and MTTKME.MTKSKO = ?) or (MTTKME.MTKKOD = ? and MTTKME.MTKSKO = ?) ) and ( (MTTKES.MTKSKO = ?) or (MTTKES.MTKSKO = ?) or (MTTKES.MTKSKO = ?) or (MTTKES.MTKSKO = ?) or (MTTKES.MTKSKO = ?) or (MTTKES.MTKSKO = ?) or (MTTKES.MTKSKO = ?) ) ) result  ";
		//String[] val = new String[]{"342", "342", "0", "0", "0", "0", "0", "0", "-1", "-1", "-1", "-1", "04/06/15", "04/06/15", "04/06/18", "04/06/18", "%%", "%%", "%%", "0", "0", "0", "1", "0", "0", "0", "0", "6", "0", "3", "0", "8", "0", "5", "0", "7", "0", "1", "2", "3", "4", "5", "6", "8", };
		
		//Stored
		//String query = "CALL SSCONORP ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";
		String query = "SELECT result.numero, result.numrecaudacion, result.ctagirada, result.estado, result.banco, result.codbanco, result.sucursal, result.importe, result.doclibrador, result.codpos, result.fechaacredit, result.fecharecaud, result.ref1deudor, result.ref2deudor, result.ref1comp, result.ref2comp, result.ref3comp, result.ref4comp, result.ref5comp, result.rownumber, result.tipodoclibrador, result.formacobro, result.moneda, result.fechacheque FROM ( SELECT numero, numrecaudacion, ctagirada, estado, banco, codbanco, sucursal, importe, doclibrador, codpos, fechaacredit, fecharecaud, ref1deudor, ref2deudor, ref1comp, ref2comp, ref3comp, ref4comp, ref5comp, tipodoclibrador, formacobro, moneda, fechacheque, ROWNUMBER() OVER(ORDER BY fecharecaud ASC) AS rownumber FROM( SELECT 'Al d AS VENC, MRZNCH AS NUMERO, MRLQTR AS NUMRECAUDACION, MRZNCG AS CTAGIRADA, MTKDES AS ESTADO, SCZNOM as BANCO, MRZBCH as CODBANCO, MRZSCN as SUCURSAL, MRZITE AS IMPORTE, MRZKDL AS TIPODOCLIBRADOR, MRZQDL AS DOCLIBRADOR, MRZCOP AS CODPOS, CASE when MRZFAR = '0001-01-01' then MRZFEA else MRZFAR end as FECHAACREDIT, MRLFDE AS FECHARECAUD, '0' AS QTR, '0' AS KFC, MRLRD1 AS REF1DEUDOR, MRLRD2 AS REF2DEUDOR, MRLRE1 AS REF1COMP, MRLRE2 AS REF2COMP, MRLRE3 AS REF3COMP, MRLRE4 AS REF4COMP, MRLRE5 AS REF5COMP, MRZKFC AS FORMACOBRO, TBMONEDA.TBDNOM AS MONEDA, MRRECHAD.MRZFAR AS fechacheque FROM MRCOBRAN CROSS JOIN MRRECHAD CROSS JOIN MTTABLAS LEFT OUTER JOIN SCPERCOM ON (SCZKTA=27 AND MRZBCH=SCZQAP) LEFT OUTER JOIN TBMONEDA ON (TBMONEDA.TBKMDA=MRCOBRAN.MRLMCT) WHERE MTKQTA=138 AND MRZKES = MTKKOD AND MRLQTR = MRZQTR AND (MRLFDE >= ? OR ?='0') AND (MRLFDE <= ? OR ?='0') AND MRLFAN = '0001-01-01' AND MRLCNV = ? AND MRLTBO = ? AND (MRZFEA >= ? OR ?='0') AND (MRZFEA <= ? OR ?='0') AND (MRLRD1 = ? OR ?='0') AND (MRLRD2 = ? OR ?='0') AND (MRLRE1 = ? OR ?='0') AND (MRLRE2 = ? OR ?='0') AND (MRLRE3 = ? OR ?='0') AND (MRLRE4 = ? OR ?='0') AND (MRLRE5 = ? OR ?='0') AND (MRZNCH = ? OR ?='0') AND (MRLQTR = ? OR ?='0') UNION SELECT 'A Futuro' as VENC, MRPNCH AS NUMERO, MRLQTR AS NUMRECAUDACION, MRPNCG AS CTAGIRADA, MTKDES AS ESTADO, SCZNOM as BANCO, MRPBCH as CODBANCO, MRPSCN as SUCURSAL, MRPITE AS IMPORTE, MRPKDL AS TIPODOCLIBRADOR, MRPQDL AS DOCLIBRADOR, MRPCOP AS CODPOS, CASE when MRPFAR = '0001-01-01' then MRPFEA else MRPFAR end as FECHAACREDIT, MRLFDE AS FECHARECAUD, MRPQTR AS QTR, MRPKFC AS KFC, MRLRD1 AS REF1DEUDOR, MRLRD2 AS REF2DEUDOR, MRLRE1 AS REF1COMP, MRLRE2 AS REF2COMP, MRLRE3 AS REF3COMP, MRLRE4 AS REF4COMP, MRLRE5 AS REF5COMP, MRPKFC AS FORMACOBRO, TBMONEDA.TBDNOM AS MONEDA, MRRECFPD.MRPFAR AS fechacheque FROM MRCOBRAN CROSS JOIN MRRECFPD CROSS JOIN MTTABLAS LEFT OUTER JOIN SCPERCOM ON (SCZKTA=27 AND MRPBCH=SCZQAP) LEFT OUTER JOIN TBMONEDA ON (TBMONEDA.TBKMDA=MRCOBRAN.MRLMCT) WHERE MTKQTA=138 AND MRPKES=MTKKOD AND MRLQTR = MRPQTR AND (MRLFDE >= ? OR ?='0') AND (MRLFDE <= ? OR ?='0') AND MRLFAN = '0001-01-01' AND MRLCNV = ? AND MRLTBO = ? AND (MRPFEA >= ? OR ?='0') AND (MRPFEA <= ? OR ?='0') AND (MRLRD1 = ? OR ?='0') AND (MRLRD2 = ? OR ?='0') AND (MRLRE1 = ? OR ?='0') AND (MRLRE2 = ? OR ?='0') AND (MRLRE3 = ? OR ?='0') AND (MRLRE4 = ? OR ?='0') AND (MRLRE5 = ? OR ?='0') AND (MRPNCH = ? OR ?='0') AND (MRLQTR = ? OR ?='0') ) result2 ) result ";
		String[] val = new String[]{"03/10/19", "03/10/19", "03/10/19", "03/10/19", "1472", "1", "01/06/10", "01/06/10", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "03/10/19", "03/10/19", "03/10/19", "03/10/19", "1472", "1", "01/06/10", "01/06/10", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"};
		//Resultado: valor_ CALL SSCONORP ( '0', 'ES', '99999', '342', '0', '04062015', '04062018', '', '', '', '0', '0', '0', '0', '1', '9', '6', '3', '8', '5', '7', '1', '2', '3', '4', '5', '6', '8' )
		String[] lst = query.split("\\?");
		String queryCompleto = "";
		int indice = 0;
		int cantidadSubs = 67;
		int cantInic = 0;
		//System.out.println(val.length);
		for(String a: lst){
			if(indice < lst.length -1)
				queryCompleto += a + "'" + val[indice] + "'";  
			else if(indice == lst.length -1)
				queryCompleto += a;
			//System.out.println(queryCompleto);
			indice ++;
			
		}
		System.out.println("valor_ " + queryCompleto);
		String queryAs400 = "";
		
		
		int inicioLinea = 0;
		int tmpLinea = 0;
		indice = 0;
		for(int i = 0;i<queryCompleto.length(); i++){
			//System.out.println(i);
			if(indice %16 == 0){
				//queryAs400 += "\\n";
				//System.out.println("");
			}
			if(i>0){
				if(i%67 == 0){
					String a = queryCompleto.charAt(i) + ""; 
					//queryAs400 += queryCompleto.substring(inicioLinea, a.lastIndexOf(" "));
					//inicioLinea = a.lastIndexOf(" ");
					tmpLinea = queryCompleto.substring(0, i).lastIndexOf(" ");
					queryAs400 += queryCompleto.substring(inicioLinea,tmpLinea) + "\n";
					//System.out.println(queryAs400);
					inicioLinea = tmpLinea;
					indice ++;
				}
			}
				
				
		}
		//System.out.println(queryAs400);
		guardarArchivo(queryAs400);
		/*
		String ejemplo = queryCompleto.substring(0,67);
		ejemplo.lastIndexOf(" ");
		System.out.println(ejemplo.lastIndexOf(" "));
		System.out.println(queryAs400);
		*/
	}
	
	private static void guardarArchivo(String contenido){
		try{
			String ruta = "d:\\archivoAs400.txt";
	        File archivo = new File(ruta);
	        BufferedWriter bw;
	        if(archivo.exists()) {
	            bw = new BufferedWriter(new FileWriter(archivo));
	            bw.write("El fichero de texto ya estaba creado.");
	        } else {
	            bw = new BufferedWriter(new FileWriter(archivo));
	            bw.write(contenido);
	        }
	        bw.close();
	    }catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private static void metodo(){

		// TODO Auto-generated method stub

		String query = "select result.rownumber, result.cuenta, result.PROVEENOM, result.MTLKME, result.MTLQSO, result.MTLQOP, result.MTLFRH, result.MTLACO, result.MTLCIN, result.MTLRRP, result.MTLQIP, result.MTLCUI, result.MTLJPA, result.MTLFPA, result.MTLQRC, result.KME, result.KFP, result.KES, result.statusNumber, result.statusSubnumber, result.MTOSUC, result.MTGSUC, result.OPPRONOM, result.DEC, result.DBE, result.MTOVTO, result.MTOFEM, result.fecha, result.SCKDER, result.modulo, result.sucursal, result.moneda, result.suboperacion, result.submedio, result.cbu from ( select MTPROVEE.MTPNOM AS PROVEENOM, MTORPAGO.MTLKME as MTLKME, MTTKME.MTKSKO as submedio, MTORPAGO.MTLQSO as MTLQSO, MTORPAGO.MTLQOP as MTLQOP, MTORPAGO.MTLFRH as MTLFRH, MTORPAGO.MTLACO as MTLACO, MTORPAGO.MTLCIN as MTLCIN, MTORPAGO.MTLRRP as MTLRRP, MTORPAGO.MTLQIP as MTLQIP, MTORPAGO.MTLCUI as MTLCUI, MTORPAGO.MTLJPA as MTLJPA, MTORPAGO.MTLFPA as MTLFPA, MTORPAGO.MTLQRC as MTLQRC, MTTKME.MTKDES as KME, MTTKFP.MTKDES as KFP, MTTKES.MTKDES as KES, MTTKES.MTKSKO as statusSubnumber, MTTKES.MTKKOD as statusNumber, MTOPCHEQ.MTOSUC as MTOSUC, MTOPGIRO.MTGSUC as MTGSUC, MTPROORD.MTHNOM AS OPPRONOM, MEDIOS.MTKDES AS DEC, AVISO.MTKDES as DBE, MTOPCHEQ.MTOVTO as MTOVTO, MTOPCHEQ.MTOFEM as MTOFEM, MTORPAGO.MTLFUC AS fecha , SCKDER as SCKDER, ROW_NUMBER() OVER(ORDER BY MTLFPA DESC) AS rownumber, CBU.X999JEMOD as modulo, CBU.X999JESUC as sucursal, CBU.X999JEMDA as moneda, CBU.X999JECTA as cuenta, CBU.X999JESBO as suboperacion, case WHEN MTORPAGO.MTLKME not in('2','3','8') then MTOPELEC.MTMCBU else MTORPAGO.MTLQCT end as cbu from MTTABLAS as MTTKME, MTTABLAS as MTTKFP, MTTABLAS as MTTKES, TBMONEDA, MTORPAGO left outer join SCTABLAS on (SCKQTA=1 and SCKKOD=MTLKDO) left outer join MTOPELEC on (MTORPAGO.MTLQOP=MTMQOP AND MTORPAGO.MTLQSO=MTMQSO) left outer join MTOPCHEQ ON (MTORPAGO.MTLQOP=MTOQOP AND MTORPAGO.MTLQSO=MTOQSO) left outer join MTOPGIRO ON (MTORPAGO.MTLQOP=MTGQOP and MTORPAGO.MTLQSO=MTGQSO) left outer join MTTABLAS AS AVISO ON (AVISO.MTKQTA=13 and AVISO.MTKKOD=MTLAPB) left outer join MTTABLAS AS MEDIOS ON (MEDIOS.MTKQTA=14 and MEDIOS.MTKKOD=MTLCAE) left outer join MTPROORD ON (MTORPAGO.MTLQOP=MTPROORD.MTHQOP and MTORPAGO.MTLQSO=MTPROORD.MTHQSO) LEFT OUTER JOIN MTPROVEE ON (MTORPAGO.MTLQPV=MTPROVEE.MTPQPV) left outer join X999JE as CBU on (CBU.X999JECB1 = substr(lpad(ltrim(rtrim(MTORPAGO.MTLQCT)), 22 ,'0'), 1, 8) and CBU.X999JECB2 = substr(lpad(ltrim(rtrim(MTORPAGO.MTLQCT)), 22 ,'0'), 9, 22) ) where MTTKME.MTKKOD=MTORPAGO.MTLKME and MTTKME.MTKQTA=3 and MTTKFP.MTKKOD=MTLKFP and MTTKFP.MTKQTA=11 and MTTKES.MTKQTA=88 and MTTKES.MTKKOD=MTORPAGO.MTLKME and MTTKES.MTKSKO=MTORPAGO.MTLKES and TBKMDA=MTLKMD and (MTORPAGO.MTLQCA=?) and (MTPROVEE.MTPQCA=?) and (MTLQRC <= ? or '0'=?) and (MTLQRC >= ? or '0'=?) and (MTLQRC LIKE(?) or '0'=?) and (MTLJPA <= ? or '-1'=?) and (MTLJPA >= ? or '-1'=?) and (MTLFPA >= ? or '0'=?) and (MTLFPA <= ? or '0'=?) and (MTPROORD.MTHNOM LIKE(?) or MTPROVEE.MTPNOM LIKE(?) or '0'=?) and (digits(mtorpago.mtlcui) LIKE(?) or mtorpago.mtlcui=? or ?='0' ) and ( (MTTKME.MTKKOD = ? and MTTKME.MTKSKO = ?) or (MTTKME.MTKKOD = 5 and MTTKME.MTKSKO = ?) or (MTTKME.MTKKOD = 7 and MTTKME.MTKSKO = ?) or (MTTKME.MTKKOD = 9 and MTTKME.MTKSKO = ?) or (MTTKME.MTKKOD = ? and MTTKME.MTKSKO = ?) or (MTTKME.MTKKOD = ? and MTTKME.MTKSKO = ?) or (MTTKME.MTKKOD = ? and MTTKME.MTKSKO = ?) or (MTTKME.MTKKOD = ? and MTTKME.MTKSKO = ?) or (MTTKME.MTKKOD = ? and MTTKME.MTKSKO = ?) ) and ( (MTTKES.MTKSKO = ?) or (MTTKES.MTKSKO = ?) or (MTTKES.MTKSKO = ?) or (MTTKES.MTKSKO = ?) or (MTTKES.MTKSKO = ?) or (MTTKES.MTKSKO = ?) or (MTTKES.MTKSKO = ?) ) ) result WHERE result.rownumber >= ? AND result.rownumber <= ? ";
		//342(String), 342(String), 0(String), 0(String), 0(String), 0(String), 0(String), 0(String), -1(String), -1(String), -1(String), -1(String), 04/06/15(String), 04/06/15(String), 04/06/18(String), 04/06/18(String), %%(String), %%(String), %%(String), 0(String), 0(String), 0(String), 1(String), 0(String), 0(String), 0(String), 0(String), 6(String), 0(String), 3(String), 0(String), 8(String), 0(String), 5(String), 0(String), 7(String), 0(String), 1(String), 2(String), 3(String), 4(String), 5(String), 6(String), 8(String), 1(Integer), 6(Integer)
		String query2 = "abc?qwer?yui?bnm?asd?654";
		String[] val = new String[]{"342","342","0","0","0","0","0","0","-1","-1","-1","-1","04/06/15","04/06/15","04/06/18","04/06/18","%%","%%","%%","0","0","0","1","0","0","0","0","6","0","3","0","8","0","5","0","7","0","1","2","3","4","5","6","8","1","6"};
		String[] lst = query.split("\\?");
		String queryCompleto = "";
		int indice = 0;
		int cantidadSubs = 67;
		int cantInic = 0;
		//System.out.println(val.length);
		for(String a: lst){
			if(indice < 46)
				queryCompleto += a + "'" + val[indice] + "'";  
			//System.out.println(queryCompleto);
			indice ++;
			
		}
		
		String queryAs400 = "";
		
		int inicioLinea = 0;
		int tmpLinea = 0;
		indice = 0;
		
		try{
			String ruta = "d:\\archivoAs400.txt";
	        File archivo = new File(ruta);
	        BufferedWriter bw;
	        if(archivo.exists()) {
	            bw = new BufferedWriter(new FileWriter(archivo));
	            bw.write("El fichero de texto ya estaba creado.");
	        } else {    
					for(int i = 0;i<queryCompleto.length(); i++){
						//System.out.println(i);
						if(indice %16 == 0){
							//queryAs400 += "\\n";
							System.out.println("");
						}
						if(i>0){
							if(i%67 == 0){
								String a = queryCompleto.charAt(i) + ""; 
								//queryAs400 += queryCompleto.substring(inicioLinea, a.lastIndexOf(" "));
								//inicioLinea = a.lastIndexOf(" ");
								tmpLinea = queryCompleto.substring(0, i).lastIndexOf(" ");
								queryAs400 += queryCompleto.substring(inicioLinea,tmpLinea) + "\n";
								//System.out.println(queryAs400);
								inicioLinea = tmpLinea;
								indice ++;
							}
						}
							
							
					}
	        }
	       // bw.close();
	    }catch(Exception e){
			e.printStackTrace();
		}
		System.out.println(queryAs400);
		//guardarArchivo(queryAs400);
		/*
		String ejemplo = queryCompleto.substring(0,67);
		ejemplo.lastIndexOf(" ");
		System.out.println(ejemplo.lastIndexOf(" "));
		System.out.println(queryAs400);
		*/
	}
}
