package ar.com.gestionit.cashmanagement;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;

import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CashManagementWsApplication.class)
@WebAppConfiguration
public class ChashManagementWsApplicationTests {

	@Test
	public void contextLoads() {
	}

}
