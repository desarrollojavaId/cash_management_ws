package ar.com.gestionit.cashmanagement.exporter;

import java.util.ArrayList;
import java.util.List;

import ar.com.gestionit.cashmanagement.ServiceExporterLabel;
import ar.com.gestionit.cashmanagement.persistence.dto.CardListItemDTO;
import junit.framework.TestCase;

public class ExcelExporterTester extends TestCase {
	
	public void testExporter() {
		try {
			ServiceExporterLabel.loadPorperties();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		List<CardListItemDTO> list = new ArrayList<CardListItemDTO>();
		
		CardListItemDTO item = new CardListItemDTO();
		item.setCardNumber("123456");
		item.setCardStatus("estado");
		item.setDateLastMov("12/12/2015");
		item.setDateStart("12/12/2015");
		item.setDeuName("Javier Digruttola");
		item.setPageFrom(2);
		item.setPageTo(5);
		item.setRefComp1("comprobante 1");
		item.setRefComp2("comprobante 2");
		item.setRefComp3("comprobante 3");
		item.setRefComp4("comprobante 4");
		item.setRefComp5("comprobante 5");
		item.setRefDeu1("deudor 1");
		item.setRefDeu2("deudor 2");
		item.setRownumber(5);
		list.add(item);
		
		item = new CardListItemDTO();
		item.setCardNumber("123457");
		item.setCardStatus("estado2");
		item.setDateLastMov("12/12/2016");
		item.setDateStart("12/12/2016");
		item.setDeuName("Pedro Aznar");
		item.setPageFrom(3);
		item.setPageTo(5);
		item.setRefComp1("comprobante 12");
		item.setRefComp2("comprobante 22");
		item.setRefComp3("comprobante 32");
		item.setRefComp4("comprobante 42");
		item.setRefComp5("comprobante 52");
		item.setRefDeu1("deudor 12");
		item.setRefDeu2("deudor 22");
		item.setRownumber(6);
		list.add(item);
		
//		ExcelExporter exporter = new ExcelExporter(list);
//		try {
//			exporter.export();
//		} catch (ServiceException e) {
//			
//			e.printStackTrace();
//		}
	}
}
