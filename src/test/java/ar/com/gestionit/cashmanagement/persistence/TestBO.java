package ar.com.gestionit.cashmanagement.persistence;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;

public class TestBO {
	public List<TestDTO> find(TestDTO dto) throws ServiceException {
		SqlSession session = CashManagementWsApplication.sqlSessionFactory.openSession();
		TestMapper mapper = session.getMapper(TestMapper.class);
		List<TestDTO> result = mapper.find(dto);
		session.close();
		return result;
	}
	
	public List<TestDTO> attack(TestDTO dto) throws ServiceException {
		SqlSession session = CashManagementWsApplication.sqlSessionFactory.openSession();
		TestMapper mapper = session.getMapper(TestMapper.class);
		List<TestDTO> result = mapper.attack(dto);
		session.close();
		return result;
	}
}