package ar.com.gestionit.cashmanagement.persistence;

import java.util.List;

import ar.com.gestionit.cashmanagement.service.filemanager.FileManagerDTO;

public interface TestMapper {
	public List<TestDTO> find(TestDTO dto);
	public List<TestDTO> attack(TestDTO dto);
	public int insertSeba(FileManagerDTO dto);
}