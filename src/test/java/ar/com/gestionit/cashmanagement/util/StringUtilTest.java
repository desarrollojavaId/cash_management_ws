package ar.com.gestionit.cashmanagement.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import junit.framework.TestCase;

public class StringUtilTest extends TestCase {

	public void testReplaceNewline() {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader("C:\\javi\\test.txt"));
			String line = br.readLine();
			StringBuffer result = new StringBuffer();
			while(line != null) {
				result.append(line);
				line = br.readLine();
			}
			br.close();
			
			
			line = result.toString();
			
			System.out.println(line);
			System.out.println(line.replace("\r", " + CHAR(13)"));
			System.out.println(line.replace("\n", " + CHAR(10)"));
			System.out.println(line.replace("\r\n", " + CHAR(13) + CHAR(10)"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
