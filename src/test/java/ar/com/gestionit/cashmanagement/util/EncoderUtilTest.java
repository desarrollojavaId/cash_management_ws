package ar.com.gestionit.cashmanagement.util;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import junit.framework.TestCase;

public class EncoderUtilTest extends TestCase {

	@SuppressWarnings("unused")
	private static final String FILEPATH = "C:/javi/PO103.txt";
	@SuppressWarnings("unused")
	private static final String FILEPATH_OUTPUT = "C:/javi/test.txt";
	private static final String FILEPATH_INPUT_EXCEL = "C:/javi/input.txt";
	private static final String FILEPATH_OUTPUT_EXCEL = "C:/javi/output.xls";

//	/**
//	 * This test case encodes a file and shows it by console
//	 */
//	public void testEncodeFile() {
//		StringBuilder fileContent = new StringBuilder();
//		try {
//			//Load the file in memory
//			BufferedReader br = new BufferedReader(new FileReader(FILEPATH));
//			String line = br.readLine();
//
//			while (line != null) {
//				fileContent.append(line);
//				fileContent.append(System.lineSeparator());
//				line = br.readLine();
//			}
//			br.close();
//		} catch(IOException e) {
//			e.printStackTrace();
//		}
//
//		//Convert the file content to 64 base
//		String source = null;
//		try {
//			source = EncoderUtil.stringToBase64(fileContent.toString());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		//Persist result in a file
//		try {
//			BufferedWriter bw = new BufferedWriter(new FileWriter(FILEPATH_OUTPUT));
//			bw.write(source);
//			bw.flush();
//			bw.close();
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
//	}
	
	public void testFileContent() {
		StringBuilder fileContent = new StringBuilder();
		try {
			//Load the file in memory
			BufferedReader br = new BufferedReader(new FileReader(FILEPATH_INPUT_EXCEL));
			String line = br.readLine();

			while (line != null) {
				fileContent.append(line);
				fileContent.append(System.lineSeparator());
				line = br.readLine();
			}
			br.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		//Persist result in a file
		try {
			FileOutputStream out = new FileOutputStream(FILEPATH_OUTPUT_EXCEL);
			out.write(fileContent.toString().getBytes());
			out.flush();
			out.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}