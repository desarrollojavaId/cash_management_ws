package ar.com.gestionit.cashmanagement.util;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.EscrowListDTO;
import junit.framework.TestCase;

public class XMLUtilTest extends TestCase {

	public void testXmlToDto() {

		for(int i = 0; i < 100000; i++) {
			StringBuffer source = new StringBuffer("<FIDEICOMISOS>");
			source.append("<FIDEICOMISO>");
			source.append("<ID>1</ID>");
			source.append("<CUENTA>123456</CUENTA>");
			source.append("<NOMBRE>Javier</NOMBRE>");
			source.append("<SERIE>Javier</SERIE>");
			source.append("<GRUPO>Javier</GRUPO>");
			source.append("<FECHACORTE>Javier</FECHACORTE>");
			source.append("<CAPITALCORTE>Javier</CAPITALCORTE>");
			source.append("<ESTADO>Javier</ESTADO>");
			source.append("<VUELCACLI>Javier</VUELCACLI>");
			source.append("<HISTOPER>Javier</HISTOPER>");
			source.append("<CONDESC>Javier</CONDESC>");
			source.append("<INTSEGUROS>Javier</INTSEGUROS>");
			source.append("<INTREVOLVING>Javier</INTREVOLVING>");
			source.append("<INTCOBRANZA>Javier</INTCOBRANZA>");
			source.append("<INTVUELCOINICIAL>Javier</INTVUELCOINICIAL>");
			source.append("<HISTOPER>Javier</HISTOPER>");
			source.append("</FIDEICOMISO>");
			source.append("</FIDEICOMISOS>");


			try {
				EscrowListDTO dto = (EscrowListDTO) XMLUtil.xmlToDto(source.toString(), EscrowListDTO.class);
				if(dto == null) {
					System.out.println("Error  " + i);
				}
				if(i % 1000 == 0) {
					System.out.println("Iteration " + i);
				}
			} catch (ServiceException e) {
				e.printStackTrace();
				System.out.println("Termino con errores");
				break;
			}
		}
		System.out.println("Termino");
	}
}
