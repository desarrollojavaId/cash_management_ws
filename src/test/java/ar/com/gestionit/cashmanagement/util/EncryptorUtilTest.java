package ar.com.gestionit.cashmanagement.util;

import ar.com.gestionit.cashmanagement.util.EncryptorUtil.EncryptionException;
import junit.framework.TestCase;

public class EncryptorUtilTest extends TestCase {
	
	private static final String PASS01 = "CMARTINS2";
	private static final String PASS02 = "HsDFGzI1Q8";
	private static final String PASS04 = "nsbtte";
	private static final String PASS05 = "6nG5uGNrzJ";
	
	private static final String PASS03 = "jqGofQDvgxNXtL4tQ7YEaw==";
	
	public void testEncrypt() {
		try {
			EncryptorUtil encryptor = new EncryptorUtil();
			System.out.println("Pass 01: " + encryptor.encrypt(PASS01));
			System.out.println("Pass 02: " + encryptor.encrypt(PASS02));
			System.out.println("Pass 04: " + encryptor.encrypt(PASS04));
			System.out.println("Pass 05: " + encryptor.encrypt(PASS05));
		} catch (EncryptionException e) {
			e.printStackTrace();
		}
	}
	
	public void testDecrypt() {
		try {
			EncryptorUtil encryptor = new EncryptorUtil();
			System.out.println("Pass 01: " + encryptor.decrypt(PASS03));
		} catch (EncryptionException e) {
			e.printStackTrace();
		}
	}
}