package ar.com.gestionit.cashmanagement.util;

import junit.framework.TestCase;

public class NumberUtilTest extends TestCase {
	
	public void testIsNumber() {
		String n = "123456.789";
		String result = NumberUtil.isNumeric(n) ? "Si" : "No";
		System.out.println(n + ": " + result);
		
		n = "123456.78";
		result = NumberUtil.isNumeric(n) ? "Si" : "No";
		System.out.println(n + ": " + result);
		
		n = "-123456.78";
		result = NumberUtil.isNumeric(n) ? "Si" : "No";
		System.out.println(n + ": " + result);
		
		n = "123456";
		result = NumberUtil.isNumeric(n) ? "Si" : "No";
		System.out.println(n + ": " + result);
		
		n = "-123456";
		result = NumberUtil.isNumeric(n) ? "Si" : "No";
		System.out.println(n + ": " + result);
		
		n = "123456a";
		result = NumberUtil.isNumeric(n) ? "Si" : "No";
		System.out.println(n + ": " + result);
		
		n = "a123456";
		result = NumberUtil.isNumeric(n) ? "Si" : "No";
		System.out.println(n + ": " + result);
		
		n = "1234a56";
		result = NumberUtil.isNumeric(n) ? "Si" : "No";
		System.out.println(n + ": " + result);
		
		n = "123456,21";
		result = NumberUtil.isNumeric(n) ? "Si" : "No";
		System.out.println(n + ": " + result);
		
		n = "123456,123";
		result = NumberUtil.isNumeric(n) ? "Si" : "No";
		System.out.println(n + ": " + result);
		
		n = "12.3456";
		result = NumberUtil.isNumeric(n) ? "Si" : "No";
		System.out.println(n + ": " + result);
		
		n = "12.345";
		result = NumberUtil.isNumeric(n) ? "Si" : "No";
		System.out.println(n + ": " + result);
		
		n = "12.345,12";
		result = NumberUtil.isNumeric(n) ? "Si" : "No";
		System.out.println(n + ": " + result);
		
		n = "12,3456.12";
		result = NumberUtil.isNumeric(n) ? "Si" : "No";
		System.out.println(n + ": " + result);
		
		n = "12,345.12";
		result = NumberUtil.isNumeric(n) ? "Si" : "No";
		System.out.println(n + ": " + result);
	}
	
	public void testLongToString() {
		long a = 12345678;
		long b = 1;
		long c = 0;
		long d = 000000000;
		
		System.out.println("a: " + Long.toString(a));
		System.out.println("b: " + Long.toString(b));
		System.out.println("c: " + Long.toString(c));
		System.out.println("d: " + Long.toString(d));
	}
	
	public void testAmountFormat() {
		float t1 = 0;
		float t2 = 1.5F;
		float t3 = -1;
		float t4 = -1.5F;
		float t5 = 12345567;
		float t6 = 1234567.8F;
		System.out.println("T11: " + NumberUtil.amountFormat(t1));
		System.out.println("T11: " + NumberUtil.amountFormat(t2));
		System.out.println("T11: " + NumberUtil.amountFormat(t3));
		System.out.println("T11: " + NumberUtil.amountFormat(t4));
		System.out.println("T11: " + NumberUtil.amountFormat(t5));
		System.out.println("T11: " + NumberUtil.amountFormat(t6));
		
		double d1 = 0;
		double d2 = 1.5;
		double d3 = -1;
		double d4 = -1.5;
		double d5 = 12345567;
		double d6 = 1234567.8;
		System.out.println("T12: " + NumberUtil.amountFormat(d1));
		System.out.println("T12: " + NumberUtil.amountFormat(d2));
		System.out.println("T12: " + NumberUtil.amountFormat(d3));
		System.out.println("T12: " + NumberUtil.amountFormat(d4));
		System.out.println("T12: " + NumberUtil.amountFormat(d5));
		System.out.println("T12: " + NumberUtil.amountFormat(d6));
	}
	
	public void testAmountFormatAsString() {
		String t1 = "1.5";
		String t2 = "1,5";
		String t3 = "1.500,2";
		String t4 = "1,500.2";
		System.out.println("T31: " + NumberUtil.amountFormat(t1));
		System.out.println("T31: " + NumberUtil.amountFormat(t2));
		System.out.println("T31: " + NumberUtil.amountFormat(t3));
		System.out.println("T31: " + NumberUtil.amountFormat(t4));
		
		System.out.println("T32: " + NumberUtil.amountFormatExporter(t1));
		System.out.println("T32: " + NumberUtil.amountFormatExporter(t2));
		System.out.println("T32: " + NumberUtil.amountFormatExporter(t3));
		System.out.println("T32: " + NumberUtil.amountFormatExporter(t4));
	}
	
	public void testAmountFormatExporter() {
		float t1 = 0;
		float t2 = 1.5F;
		float t3 = -1;
		float t4 = -1.5F;
		float t5 = 12345567;
		float t6 = 1234567.8F;
		System.out.println("T21: " + NumberUtil.amountFormatExporter(t1));
		System.out.println("T21: " + NumberUtil.amountFormatExporter(t2));
		System.out.println("T21: " + NumberUtil.amountFormatExporter(t3));
		System.out.println("T21: " + NumberUtil.amountFormatExporter(t4));
		System.out.println("T21: " + NumberUtil.amountFormatExporter(t5));
		System.out.println("T21: " + NumberUtil.amountFormatExporter(t6));
	}
	
	public void testPlus() {
		float result = 1092.00F + 112000.00F + 60705.00F + 10803.00F + 9514.85F + 100000.00F + 3000.00F + 1002567.38F + 9972.00F + 447.22F;
		System.out.println("Resultado: " + result);
	}
}
