package ar.com.gestionit.cashmanagement;

import java.util.Properties;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.BeansException;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import ar.com.gestionit.cashmanagement.exception.FatalException;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.EncryptorUtil;
import ar.com.gestionit.cashmanagement.util.EncryptorUtil.EncryptionException;
import ar.com.gestionit.cashmanagement.util.FactoryUtil;

/**
 * - SpringBootApplication annotation is equal to EnableAutoConfiguration annotation
 * in this case.
 * 
 * - ApplicationContextAware obligates us to implement setApplicationContext method
 * to get a reference of the Spring application context
 * 
 * - Although ServleInitializes exists and it is executed, Spring Booter searches
 * SpringBootApplication annotation 
 * 
 * @author jdigruttola
 *
 */
@SpringBootApplication
public class CashManagementWsApplication implements ApplicationContextAware {

	private static ApplicationContext appContext = null;

	/**
	 * This static variable contains all the global properties
	 * mapped in the configuration files
	 */
	public static Properties globalProperties = null;

	/**
	 * This constant contains the properties file name
	 */
	public static final String PROPERTIES_FILE = "application.properties";

	/**
	 * Mybatis SQL Session factory for the application
	 */
	public static SqlSessionFactory sqlSessionFactory = null;

	/**
	 * Mybatis SQL Session factory for the file manager
	 */
	public static SqlSessionFactory fileManagerSessionFactory = null;

	/**
	 * This method loads the application properties in main memory
	 * @throws Exception
	 */
	public static void loadPorperties() throws Exception {
		if(globalProperties != null) {
			DefinedLogger.CONTEXT.warn("Las propiedades ya estan cargadas en la aplicacion");
		} else {
			globalProperties = FactoryUtil.loadProperties(PROPERTIES_FILE);
			decodeEncryptedProperties();
		}
	}

	/**
	 * This method gets a property of the global properties
	 * of the application according to the key given by argument
	 * @param key
	 * @return
	 */
	public static String getProperty(String key) {
		if(globalProperties == null)
			return null;
		return globalProperties.getProperty(key);
	}

	/**
	 * This method gets a property of the global properties
	 * of the application according to the key given by argument
	 * as integer
	 * @param key
	 * @return
	 */
	public static int getPropertyAsInteger(String key) {
		return Integer.parseInt(getProperty(key));
	}
	
	/**
	 * This method gets a property of the global properties
	 * of the application according to the key given by argument
	 * as long
	 * @param key
	 * @return
	 */
	public static long getPropertyAsLong(String key) {
		return Long.parseLong(getProperty(key));
	}

	/**
	 * This method gets a property of the global properties
	 * of the application according to the key given by argument
	 * as boolean
	 * @param key
	 * @return
	 */
	public static boolean getPropertyAsBoolean(String key) {
		String value = getProperty(key);
		return value != null && value.trim().equals(String.valueOf(ServiceConstant.TRUE)) ? true : false;
	}

	/**
	 * This method is used to get the application context in our application
	 * The initializer invokes this method because we implement ApplicationContextAware
	 * interface
	 */
	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		appContext = context;
	}

	/**
	 * This method returns a bean according to bean name specified by argument
	 * @param beanName
	 * @return
	 */
	public static Object getBean(Class<?> beanClass) {
		return appContext.getBean(beanClass);
	}

	/**
	 * This method decodes the encrypted properties hosted in the global properties
	 * @throws FatalException
	 */
	private static void decodeEncryptedProperties() throws FatalException {
		EncryptorUtil encryptor;
		try {
			encryptor = new EncryptorUtil();
			globalProperties.setProperty(ServiceConstant.PROP_DATASOURCE_APP_PASS, 
					encryptor.decrypt(globalProperties.getProperty(ServiceConstant.PROP_DATASOURCE_APP_PASS)));
			globalProperties.setProperty(ServiceConstant.PROP_DATASOURCE_FM_PASS, 
					encryptor.decrypt(globalProperties.getProperty(ServiceConstant.PROP_DATASOURCE_FM_PASS)));

		} catch (EncryptionException e) {
			DefinedLogger.CONTEXT.error("No se pudieron desencriptar las propiedades cifradas");
			throw new FatalException(e);
		}
	}

	public static void main(String[] args) {
	}
}