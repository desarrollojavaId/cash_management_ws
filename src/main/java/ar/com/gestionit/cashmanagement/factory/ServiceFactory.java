package ar.com.gestionit.cashmanagement.factory;

import java.util.Properties;

import ar.com.gestionit.cashmanagement.exception.FatalException;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.message.ExecuteRequest;
import ar.com.gestionit.cashmanagement.service.IService;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.FactoryUtil;
import ar.com.gestionit.cashmanagement.util.StringUtil;

/**
 * This factory creates an specified service instance according to a request
 * @author jdigruttola
 */
public class ServiceFactory {
	
	/**
	 * This constant specifies the Service property file name to be called
	 */
	final static protected String PROPERTIES_FILE = "service.properties"; 
	
	/**
	 * This constant specifies the header service ID
	 */
	final static protected String SERVICE_ID = "service.id.";
	
	/**
	 * This variable contains the enabled services
	 */
	static protected Properties services = null;
	
	/**
	 * This method loads the services mapped to the application runtime
	 */
	public static void load() throws FatalException {
		// Validate if the services already were loaded
		if(services != null) {
			DefinedLogger.CONTEXT.warn("The services already is loaded in main memory");
			return;
		}
		
		try {
			services = FactoryUtil.loadProperties(PROPERTIES_FILE);
		} catch (Exception e) {
			throw new FatalException(e);
		}
	}
	
	/**
	 * This method returns a new instance of some service according to the specified request
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static IService build(ExecuteRequest request) throws ServiceException {
		//Validate request status
		if(request == null) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTREQUEST);
		}
		
		//Generate the possible key
		StringBuffer key = new StringBuffer(SERVICE_ID);
		key.append(StringUtil.autocomplete(String.valueOf(request.getAcnco()), 3, '0'));
		key.append(StringUtil.autocomplete(String.valueOf(request.getBopco()), 5, '0'));
		key.append(StringUtil.autocomplete(String.valueOf(request.getCvart()), 3, '0'));
		
		DefinedLogger.SERVICE.debug("El ID de servicio es " + key.toString());
		
		//Get specific service
		String serviceName = services.getProperty(key.toString());
		if(serviceName == null || serviceName.trim().equals("")) {
			throw new ServiceException("The service with key " + key + " is not mapped");
		}
		
		Class<IService> service = null;
		try {
			service = (Class<IService>)ServiceFactory.class.getClassLoader().loadClass(serviceName);
		} catch (ClassNotFoundException e) {
			DefinedLogger.SERVICE.error("No se encontro el servicio para la clave " + serviceName);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTSERVICE, e);
		}
		
		//Return the a service instance
		try {
			return service.newInstance();
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTSERVICE, e);
		}
	}
}