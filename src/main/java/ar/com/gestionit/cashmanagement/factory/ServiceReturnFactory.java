package ar.com.gestionit.cashmanagement.factory;

import java.util.Properties;

import ar.com.gestionit.cashmanagement.exception.FatalException;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.service.util.ServiceReturn;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.FactoryUtil;

/**
 * This factory creates an specified service return according
 * to the specified key given by argument
 * @author jdigruttola
 */
public class ServiceReturnFactory {
	/**
	 * This constant specifies the Service property file name to be called
	 */
	final static protected String PROPERTIES_FILE = "service.return.properties"; 
	
	/**
	 * Complete the key for the code
	 */
	final static protected String CODE = ".code";
	
	/**
	 * Complete the key for the description
	 */
	final static protected String DESCRIPTION = ".description";
	
	/**
	 * This variable contains the possible returns
	 */
	static protected Properties properties = null;
	
	/**
	 * This method loads the service return list mapped to the application context
	 */
	public static void load() throws FatalException {
		// Validate if the services already were loaded
		if(properties != null) {
			DefinedLogger.CONTEXT.warn("The service returns already is loaded in main memory");
			return;
		}
		
		try {
			properties = FactoryUtil.loadProperties(PROPERTIES_FILE);
		} catch(Exception e) {
			throw new FatalException(e);
		}
	}
	
	/**
	 * This method returns an specified service return according
	 * to the key given by argument
	 * @param key
	 * @return ServiceReturn
	 */
	public static ServiceReturn build(String key) throws ServiceException {
		DefinedLogger.CONTEXT.trace("The service ID is " + key.toString());
		ServiceReturn r = new ServiceReturn();
		r.setCode(properties.getProperty(key + CODE));
		r.setDescription(properties.getProperty(key + DESCRIPTION));
		return r;
	}
}