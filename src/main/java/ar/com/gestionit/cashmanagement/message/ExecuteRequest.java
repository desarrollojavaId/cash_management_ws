//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2015.07.29 a las 10:41:55 AM ART 
//


package ar.com.gestionit.cashmanagement.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import ar.com.gestionit.cashmanagement.message.entity.ArrayOfstring20;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Acnco" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Dfecl" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="Dhocl" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Drqcl" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Bopco" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Cvart" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Drqus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Drqws" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Ddpgc" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Ddsuc" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Ddmod" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Ddmda" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Ddpap" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Ddcta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Ddope" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Ddsbo" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Ddtop" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Dcpgc" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Dcsuc" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Dcmod" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Dcmda" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Dcpap" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Dccta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Dcope" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Dcsbo" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Dctop" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Dimpo" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="Dimp2" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="Dmdao" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Dcotz" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="Edatos" type="{http://spring.io/guides/gs-producing-web-service}ArrayOfstring_20"/>
 *         &lt;element name="Etdats" type="{http://spring.io/guides/gs-producing-web-service}ArrayOfstring_20"/>
 *         &lt;element name="Evalcs" type="{http://spring.io/guides/gs-producing-web-service}ArrayOfstring_20"/>
 *         &lt;element name="Evalls" type="{http://spring.io/guides/gs-producing-web-service}ArrayOfstring_20"/>
 *         &lt;element name="Ecantlin" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Hashreq" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Drpgc" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Drsuc" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Drmod" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Drtrn" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Drrel" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "acnco",
    "dfecl",
    "dhocl",
    "drqcl",
    "bopco",
    "cvart",
    "drqus",
    "drqws",
    "ddpgc",
    "ddsuc",
    "ddmod",
    "ddmda",
    "ddpap",
    "ddcta",
    "ddope",
    "ddsbo",
    "ddtop",
    "dcpgc",
    "dcsuc",
    "dcmod",
    "dcmda",
    "dcpap",
    "dccta",
    "dcope",
    "dcsbo",
    "dctop",
    "dimpo",
    "dimp2",
    "dmdao",
    "dcotz",
    "edatos",
    "etdats",
    "evalcs",
    "evalls",
    "ecantlin",
    "hashreq",
    "drpgc",
    "drsuc",
    "drmod",
    "drtrn",
    "drrel"
})
@XmlRootElement(name = "executeRequest")
public class ExecuteRequest {

    @XmlElement(name = "Acnco")
    protected short acnco;
    @XmlElement(name = "Dfecl", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dfecl;
    @XmlElement(name = "Dhocl", required = true)
    protected String dhocl;
    @XmlElement(name = "Drqcl", required = true)
    protected String drqcl;
    @XmlElement(name = "Bopco")
    protected short bopco;
    @XmlElement(name = "Cvart")
    protected short cvart;
    @XmlElement(name = "Drqus", required = true)
    protected String drqus;
    @XmlElement(name = "Drqws", required = true)
    protected String drqws;
    @XmlElement(name = "Ddpgc")
    protected short ddpgc;
    @XmlElement(name = "Ddsuc")
    protected short ddsuc;
    @XmlElement(name = "Ddmod")
    protected short ddmod;
    @XmlElement(name = "Ddmda")
    protected short ddmda;
    @XmlElement(name = "Ddpap")
    protected short ddpap;
    @XmlElement(name = "Ddcta")
    protected int ddcta;
    @XmlElement(name = "Ddope")
    protected int ddope;
    @XmlElement(name = "Ddsbo")
    protected short ddsbo;
    @XmlElement(name = "Ddtop")
    protected short ddtop;
    @XmlElement(name = "Dcpgc")
    protected short dcpgc;
    @XmlElement(name = "Dcsuc")
    protected short dcsuc;
    @XmlElement(name = "Dcmod")
    protected short dcmod;
    @XmlElement(name = "Dcmda")
    protected short dcmda;
    @XmlElement(name = "Dcpap")
    protected short dcpap;
    @XmlElement(name = "Dccta")
    protected int dccta;
    @XmlElement(name = "Dcope")
    protected int dcope;
    @XmlElement(name = "Dcsbo")
    protected short dcsbo;
    @XmlElement(name = "Dctop")
    protected short dctop;
    @XmlElement(name = "Dimpo")
    protected double dimpo;
    @XmlElement(name = "Dimp2")
    protected double dimp2;
    @XmlElement(name = "Dmdao")
    protected short dmdao;
    @XmlElement(name = "Dcotz")
    protected double dcotz;
    @XmlElement(name = "Edatos", required = true)
    protected ArrayOfstring20 edatos;
    @XmlElement(name = "Etdats", required = true)
    protected ArrayOfstring20 etdats;
    @XmlElement(name = "Evalcs", required = true)
    protected ArrayOfstring20 evalcs;
    @XmlElement(name = "Evalls", required = true)
    protected ArrayOfstring20 evalls;
    @XmlElement(name = "Ecantlin")
    protected short ecantlin;
    @XmlElement(name = "Hashreq", required = true)
    protected String hashreq;
    @XmlElement(name = "Drpgc")
    protected short drpgc;
    @XmlElement(name = "Drsuc")
    protected short drsuc;
    @XmlElement(name = "Drmod")
    protected short drmod;
    @XmlElement(name = "Drtrn")
    protected short drtrn;
    @XmlElement(name = "Drrel")
    protected short drrel;

    /**
     * Obtiene el valor de la propiedad acnco.
     * 
     */
    public short getAcnco() {
        return acnco;
    }

    /**
     * Define el valor de la propiedad acnco.
     * 
     */
    public void setAcnco(short value) {
        this.acnco = value;
    }

    /**
     * Obtiene el valor de la propiedad dfecl.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDfecl() {
        return dfecl;
    }

    /**
     * Define el valor de la propiedad dfecl.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDfecl(XMLGregorianCalendar value) {
        this.dfecl = value;
    }

    /**
     * Obtiene el valor de la propiedad dhocl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDhocl() {
        return dhocl;
    }

    /**
     * Define el valor de la propiedad dhocl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDhocl(String value) {
        this.dhocl = value;
    }

    /**
     * Obtiene el valor de la propiedad drqcl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrqcl() {
        return drqcl;
    }

    /**
     * Define el valor de la propiedad drqcl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrqcl(String value) {
        this.drqcl = value;
    }

    /**
     * Obtiene el valor de la propiedad bopco.
     * 
     */
    public short getBopco() {
        return bopco;
    }

    /**
     * Define el valor de la propiedad bopco.
     * 
     */
    public void setBopco(short value) {
        this.bopco = value;
    }

    /**
     * Obtiene el valor de la propiedad cvart.
     * 
     */
    public short getCvart() {
        return cvart;
    }

    /**
     * Define el valor de la propiedad cvart.
     * 
     */
    public void setCvart(short value) {
        this.cvart = value;
    }

    /**
     * Obtiene el valor de la propiedad drqus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrqus() {
        return drqus;
    }

    /**
     * Define el valor de la propiedad drqus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrqus(String value) {
        this.drqus = value;
    }

    /**
     * Obtiene el valor de la propiedad drqws.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrqws() {
        return drqws;
    }

    /**
     * Define el valor de la propiedad drqws.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrqws(String value) {
        this.drqws = value;
    }

    /**
     * Obtiene el valor de la propiedad ddpgc.
     * 
     */
    public short getDdpgc() {
        return ddpgc;
    }

    /**
     * Define el valor de la propiedad ddpgc.
     * 
     */
    public void setDdpgc(short value) {
        this.ddpgc = value;
    }

    /**
     * Obtiene el valor de la propiedad ddsuc.
     * 
     */
    public short getDdsuc() {
        return ddsuc;
    }

    /**
     * Define el valor de la propiedad ddsuc.
     * 
     */
    public void setDdsuc(short value) {
        this.ddsuc = value;
    }

    /**
     * Obtiene el valor de la propiedad ddmod.
     * 
     */
    public short getDdmod() {
        return ddmod;
    }

    /**
     * Define el valor de la propiedad ddmod.
     * 
     */
    public void setDdmod(short value) {
        this.ddmod = value;
    }

    /**
     * Obtiene el valor de la propiedad ddmda.
     * 
     */
    public short getDdmda() {
        return ddmda;
    }

    /**
     * Define el valor de la propiedad ddmda.
     * 
     */
    public void setDdmda(short value) {
        this.ddmda = value;
    }

    /**
     * Obtiene el valor de la propiedad ddpap.
     * 
     */
    public short getDdpap() {
        return ddpap;
    }

    /**
     * Define el valor de la propiedad ddpap.
     * 
     */
    public void setDdpap(short value) {
        this.ddpap = value;
    }

    /**
     * Obtiene el valor de la propiedad ddcta.
     * 
     */
    public int getDdcta() {
        return ddcta;
    }

    /**
     * Define el valor de la propiedad ddcta.
     * 
     */
    public void setDdcta(int value) {
        this.ddcta = value;
    }

    /**
     * Obtiene el valor de la propiedad ddope.
     * 
     */
    public int getDdope() {
        return ddope;
    }

    /**
     * Define el valor de la propiedad ddope.
     * 
     */
    public void setDdope(int value) {
        this.ddope = value;
    }

    /**
     * Obtiene el valor de la propiedad ddsbo.
     * 
     */
    public short getDdsbo() {
        return ddsbo;
    }

    /**
     * Define el valor de la propiedad ddsbo.
     * 
     */
    public void setDdsbo(short value) {
        this.ddsbo = value;
    }

    /**
     * Obtiene el valor de la propiedad ddtop.
     * 
     */
    public short getDdtop() {
        return ddtop;
    }

    /**
     * Define el valor de la propiedad ddtop.
     * 
     */
    public void setDdtop(short value) {
        this.ddtop = value;
    }

    /**
     * Obtiene el valor de la propiedad dcpgc.
     * 
     */
    public short getDcpgc() {
        return dcpgc;
    }

    /**
     * Define el valor de la propiedad dcpgc.
     * 
     */
    public void setDcpgc(short value) {
        this.dcpgc = value;
    }

    /**
     * Obtiene el valor de la propiedad dcsuc.
     * 
     */
    public short getDcsuc() {
        return dcsuc;
    }

    /**
     * Define el valor de la propiedad dcsuc.
     * 
     */
    public void setDcsuc(short value) {
        this.dcsuc = value;
    }

    /**
     * Obtiene el valor de la propiedad dcmod.
     * 
     */
    public short getDcmod() {
        return dcmod;
    }

    /**
     * Define el valor de la propiedad dcmod.
     * 
     */
    public void setDcmod(short value) {
        this.dcmod = value;
    }

    /**
     * Obtiene el valor de la propiedad dcmda.
     * 
     */
    public short getDcmda() {
        return dcmda;
    }

    /**
     * Define el valor de la propiedad dcmda.
     * 
     */
    public void setDcmda(short value) {
        this.dcmda = value;
    }

    /**
     * Obtiene el valor de la propiedad dcpap.
     * 
     */
    public short getDcpap() {
        return dcpap;
    }

    /**
     * Define el valor de la propiedad dcpap.
     * 
     */
    public void setDcpap(short value) {
        this.dcpap = value;
    }

    /**
     * Obtiene el valor de la propiedad dccta.
     * 
     */
    public int getDccta() {
        return dccta;
    }

    /**
     * Define el valor de la propiedad dccta.
     * 
     */
    public void setDccta(int value) {
        this.dccta = value;
    }

    /**
     * Obtiene el valor de la propiedad dcope.
     * 
     */
    public int getDcope() {
        return dcope;
    }

    /**
     * Define el valor de la propiedad dcope.
     * 
     */
    public void setDcope(int value) {
        this.dcope = value;
    }

    /**
     * Obtiene el valor de la propiedad dcsbo.
     * 
     */
    public short getDcsbo() {
        return dcsbo;
    }

    /**
     * Define el valor de la propiedad dcsbo.
     * 
     */
    public void setDcsbo(short value) {
        this.dcsbo = value;
    }

    /**
     * Obtiene el valor de la propiedad dctop.
     * 
     */
    public short getDctop() {
        return dctop;
    }

    /**
     * Define el valor de la propiedad dctop.
     * 
     */
    public void setDctop(short value) {
        this.dctop = value;
    }

    /**
     * Obtiene el valor de la propiedad dimpo.
     * 
     */
    public double getDimpo() {
        return dimpo;
    }

    /**
     * Define el valor de la propiedad dimpo.
     * 
     */
    public void setDimpo(double value) {
        this.dimpo = value;
    }

    /**
     * Obtiene el valor de la propiedad dimp2.
     * 
     */
    public double getDimp2() {
        return dimp2;
    }

    /**
     * Define el valor de la propiedad dimp2.
     * 
     */
    public void setDimp2(double value) {
        this.dimp2 = value;
    }

    /**
     * Obtiene el valor de la propiedad dmdao.
     * 
     */
    public short getDmdao() {
        return dmdao;
    }

    /**
     * Define el valor de la propiedad dmdao.
     * 
     */
    public void setDmdao(short value) {
        this.dmdao = value;
    }

    /**
     * Obtiene el valor de la propiedad dcotz.
     * 
     */
    public double getDcotz() {
        return dcotz;
    }

    /**
     * Define el valor de la propiedad dcotz.
     * 
     */
    public void setDcotz(double value) {
        this.dcotz = value;
    }

    /**
     * Obtiene el valor de la propiedad edatos.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfstring20 }
     *     
     */
    public ArrayOfstring20 getEdatos() {
        return edatos;
    }

    /**
     * Define el valor de la propiedad edatos.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfstring20 }
     *     
     */
    public void setEdatos(ArrayOfstring20 value) {
        this.edatos = value;
    }

    /**
     * Obtiene el valor de la propiedad etdats.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfstring20 }
     *     
     */
    public ArrayOfstring20 getEtdats() {
        return etdats;
    }

    /**
     * Define el valor de la propiedad etdats.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfstring20 }
     *     
     */
    public void setEtdats(ArrayOfstring20 value) {
        this.etdats = value;
    }

    /**
     * Obtiene el valor de la propiedad evalcs.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfstring20 }
     *     
     */
    public ArrayOfstring20 getEvalcs() {
        return evalcs;
    }

    /**
     * Define el valor de la propiedad evalcs.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfstring20 }
     *     
     */
    public void setEvalcs(ArrayOfstring20 value) {
        this.evalcs = value;
    }

    /**
     * Obtiene el valor de la propiedad evalls.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfstring20 }
     *     
     */
    public ArrayOfstring20 getEvalls() {
        return evalls;
    }

    /**
     * Define el valor de la propiedad evalls.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfstring20 }
     *     
     */
    public void setEvalls(ArrayOfstring20 value) {
        this.evalls = value;
    }

    /**
     * Obtiene el valor de la propiedad ecantlin.
     * 
     */
    public short getEcantlin() {
        return ecantlin;
    }

    /**
     * Define el valor de la propiedad ecantlin.
     * 
     */
    public void setEcantlin(short value) {
        this.ecantlin = value;
    }

    /**
     * Obtiene el valor de la propiedad hashreq.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHashreq() {
        return hashreq;
    }

    /**
     * Define el valor de la propiedad hashreq.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHashreq(String value) {
        this.hashreq = value;
    }

    /**
     * Obtiene el valor de la propiedad drpgc.
     * 
     */
    public short getDrpgc() {
        return drpgc;
    }

    /**
     * Define el valor de la propiedad drpgc.
     * 
     */
    public void setDrpgc(short value) {
        this.drpgc = value;
    }

    /**
     * Obtiene el valor de la propiedad drsuc.
     * 
     */
    public short getDrsuc() {
        return drsuc;
    }

    /**
     * Define el valor de la propiedad drsuc.
     * 
     */
    public void setDrsuc(short value) {
        this.drsuc = value;
    }

    /**
     * Obtiene el valor de la propiedad drmod.
     * 
     */
    public short getDrmod() {
        return drmod;
    }

    /**
     * Define el valor de la propiedad drmod.
     * 
     */
    public void setDrmod(short value) {
        this.drmod = value;
    }

    /**
     * Obtiene el valor de la propiedad drtrn.
     * 
     */
    public short getDrtrn() {
        return drtrn;
    }

    /**
     * Define el valor de la propiedad drtrn.
     * 
     */
    public void setDrtrn(short value) {
        this.drtrn = value;
    }

    /**
     * Obtiene el valor de la propiedad drrel.
     * 
     */
    public short getDrrel() {
        return drrel;
    }

    /**
     * Define el valor de la propiedad drrel.
     * 
     */
    public void setDrrel(short value) {
        this.drrel = value;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ExecuteRequest [acnco=" + acnco + ", dfecl=" + dfecl + ", dhocl=" + dhocl + ", drqcl=" + drqcl
				+ ", bopco=" + bopco + ", cvart=" + cvart + ", drqus=" + drqus + ", drqws=" + drqws + ", ddpgc=" + ddpgc
				+ ", ddsuc=" + ddsuc + ", ddmod=" + ddmod + ", ddmda=" + ddmda + ", ddpap=" + ddpap + ", ddcta=" + ddcta
				+ ", ddope=" + ddope + ", ddsbo=" + ddsbo + ", ddtop=" + ddtop + ", dcpgc=" + dcpgc + ", dcsuc=" + dcsuc
				+ ", dcmod=" + dcmod + ", dcmda=" + dcmda + ", dcpap=" + dcpap + ", dccta=" + dccta + ", dcope=" + dcope
				+ ", dcsbo=" + dcsbo + ", dctop=" + dctop + ", dimpo=" + dimpo + ", dimp2=" + dimp2 + ", dmdao=" + dmdao
				+ ", dcotz=" + dcotz + ", edatos=" + edatos + ", etdats=" + etdats + ", evalcs=" + evalcs + ", evalls="
				+ evalls + ", ecantlin=" + ecantlin + ", hashreq=" + hashreq + ", drpgc=" + drpgc + ", drsuc=" + drsuc
				+ ", drmod=" + drmod + ", drtrn=" + drtrn + ", drrel=" + drrel + "]";
	}
	
	public String getProcessId() {
		if(drqcl == null)
			return "";
		return drqcl.trim();
	}
}