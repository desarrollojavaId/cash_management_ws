//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2015.07.29 a las 10:41:55 AM ART 
//


package ar.com.gestionit.cashmanagement.message;

import javax.xml.bind.annotation.XmlRegistry;

import ar.com.gestionit.cashmanagement.message.entity.ArrayOfstring20;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the io.spring.guides.gs_producing_web_service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: io.spring.guides.gs_producing_web_service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ExecuteResponse }
     * 
     */
    public ExecuteResponse createExecuteResponse() {
        return new ExecuteResponse();
    }

    /**
     * Create an instance of {@link ArrayOfstring20 }
     * 
     */
    public ArrayOfstring20 createArrayOfstring20() {
        return new ArrayOfstring20();
    }

    /**
     * Create an instance of {@link ExecuteRequest }
     * 
     */
    public ExecuteRequest createExecuteRequest() {
        return new ExecuteRequest();
    }

}
