package ar.com.gestionit.cashmanagement.exporter;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.FileExporterDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.StringUtil;

public abstract class AbstractTextExporter extends AbstractExporter {
	
	protected static final String NEWLINE = "\r\n";
	
	protected static final String FIELD_SEPARATOR = ";";
	
	/**
	 * This attribute contains the file content as string
	 */
	protected StringBuffer source;
	
	public AbstractTextExporter(List<List<? extends IDTO>> list, FileExporterDTO dto) {
		super(list, dto);
		source = new StringBuffer();
	}
			
	
	public void generateGrid() throws ServiceException {
		for(int i = 0; i < list.size(); i++) {
			generateGridHeader(headerList.get(i));
			generateGridContent(list.get(i));
		}
		source.append(NEWLINE);
	}
	
	private void generateGridHeader(List<String> headers) throws ServiceException {
		if(headers == null)
			return;
		
		//Generate header row
		
		for(int i = 0; i < headers.size(); i++) {
			source.append("'").append(StringUtil.notNull(headers.get(i))).append("'" + FIELD_SEPARATOR);
		}
		source.append(NEWLINE);
	}
	
	private void generateGridContent(List<? extends IDTO> content) throws ServiceException {
		try {
			if(content == null || content.size() == 0)
				return;
			
			Field[] fields;
			Field field;
			String fieldValue;
			Method method;
			Class<? extends IDTO> c = content.get(0).getClass();

			//Generate row
			for (int i = 0; i < content.size(); i++) {
				fields = c.getDeclaredFields();

				//Generate each column in the row
				for(int j = 0; j < fields.length; j++) {
					field = fields[j];

					//Validate if the field is for output
					if(isFieldToExport(field)) {
						//Get method to execute
						method = getMethod(c, field);
						
						if(method != null
								&& isValid(field.getAnnotation(FieldToExport.class), c, field, content.get(i))) {
							source.append("'");
							
							//Execute the attribute setter
							fieldValue = StringUtil.notNull(
									String.valueOf(
											method.invoke(content.get(i))));
							
							//Validate if the value is a code to be interpreted
							fieldValue = validateMappedValue(c, field, fieldValue);
								
							source.append(fieldValue).append("'" + FIELD_SEPARATOR);
						}
					}
				}
				source.append(NEWLINE);
			}
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_EXPORTCONTENT, e);
		}
	}
	
	@Override
	protected void generateContent() throws ServiceException {
			/*
			 * Generate the file content
			 */
			generateGrid();
			fileContent = source.toString().getBytes();
			
			//Para probar persistiendo en filesystem
//			try {
//			new File("C:/Spring/Exports/").mkdirs();
//			FileOutputStream out = new FileOutputStream("C:/Spring/Exports/reporte.txt", true);
//			out.write(source.getBytes());
//			out.flush();
//			out.close();
//		}catch (Exception e) {
//			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_EXPORTCONTENT, e);
//		}
	}	
}
