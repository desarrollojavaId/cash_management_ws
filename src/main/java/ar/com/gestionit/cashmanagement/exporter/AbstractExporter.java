package ar.com.gestionit.cashmanagement.exporter;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.ServiceExporterCode;
import ar.com.gestionit.cashmanagement.ServiceExporterLabel;
import ar.com.gestionit.cashmanagement.annotation.CodeMapped;
import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.FileExporterDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.service.filemanager.FileManager;
import ar.com.gestionit.cashmanagement.service.filemanager.FileManagerDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.StringUtil;

public abstract class AbstractExporter implements IExporter {
	/**
	 * Content to export
	 */
	protected List<List<? extends IDTO>> list;

	/**
	 * Token to persist in SQL Server
	 */
	protected FileExporterDTO dto;

	/**
	 * The headers of the columns
	 */
	protected List<List<String>> headerList;

	/**
	 * This attribute contains the file content to persist in the datasource.
	 * Here, the file content has format binary
	 */
	protected byte[] fileContent;

	/**
	 * Parameterized constructor
	 * @param list
	 */
	public AbstractExporter(List<List<? extends IDTO>> list, FileExporterDTO dto) {
		this.list = list;
		this.headerList = new ArrayList<List<String>>();
		this.dto = dto;
	}

	public void export() throws ServiceException {
		loadHeaderList();
		generateContent();
		executeDownloader();
	}

	/**
	 * This method generates the content according to the type
	 * @throws ServiceException
	 */
	protected abstract void generateContent() throws ServiceException;

	/**
	 * This method generates the headers of the columns
	 * and returns them
	 * @return the headers of the columns
	 */
	private void loadHeaderList() throws ServiceException {
		//Validate if the list of the DTO list is empty
		if(list == null || list.size() == 0)
			return;

		try {
			//Define and initialize variables 
			Iterator<List<? extends IDTO>> i = list.iterator();
			List<? extends IDTO> dtoList;
			List<String> headers;
			Class<? extends IDTO> c;
			String prefix;
			Field[] fields;
			FieldToExport ann;

			//Iterate all the DTO list (each DTO list is a grid new in the exportation)
			while(i.hasNext()) {
				//Get DTO list to can get the DTO class
				dtoList = i.next();

				//Validate if the list is empty
				if(dtoList == null || dtoList.size() == 0)
					continue;

				//Initialize the header list to add the headers of the DTO
				headers = new ArrayList<String>();

				/*
				 * Get the name of the DTO class (it is used to build the key to get the header)
				 * and the declared attributes (it is used to get the XmlElement annotation
				 * where part of the key is hosted) of the DTO by reflection
				 */
				c = dtoList.get(0).getClass();
				prefix = c.getSimpleName();
				fields = c.getDeclaredFields();

				/*
				 * Iterate to get each header
				 * NOTE: we only must export the fields which have FieldToExport annotation 
				 */
				for(Field f : fields) {
					ann = f.getAnnotation(FieldToExport.class);
					if(ann != null) {
						//Validate if the header is dynamic or is parameterized (mapped in the properties file)
						if(ann.hasDynamicTitle()) {
							//So, the header is dynamic

							//Get the header (fieldName + 'Title')
							String dynamicHeader = getDynamicTitle(c, f, dtoList.get(0));

							//Validate if the dynamic header exists
							if(!StringUtil.isEmpty(dynamicHeader) && !dynamicHeader.trim().equalsIgnoreCase("null")) {
								//Add dynamic header
								headers.add(dynamicHeader);
							} else if(ann.required()) {
								//If the column is mandatory and we could not the dynamic title, so we load the parameterized title
								headers.add(
										StringUtil.notNull(
												ServiceExporterLabel.getProperty(
														new StringBuffer(prefix)
														.append(".")
														.append(ann.name()).toString()
														)));
							}
						} else {
							//So, the header is parameterized

							//Get the header according to the built key
							headers.add(
									StringUtil.notNull(
											ServiceExporterLabel.getProperty(
													new StringBuffer(prefix)
													.append(".")
													.append(ann.name()).toString()
													)));
						}
					}
				}

				//Add the header list to list root
				headerList.add(headers);
			}
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_EXPORTHEADERS, e);
		}
	}

	/**
	 * This method persists the file contents in the datasource used
	 * as interface (MS SQL Server implementation)
	 * @throws ServiceException
	 */
	protected void executeDownloader()  throws ServiceException{
		FileManagerDTO dtoInput = new FileManagerDTO();
		dtoInput.setFileContent(fileContent);
		dtoInput.setTokenId(dto.getToken());
		dtoInput.setFileName(dto.getFileName());
		FileManager manager = new FileManager();
		manager.updateFile(dtoInput);
		if (manager.updateFile(dtoInput) == 0)
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTPERSISTS);
	}

	/**
	 * This method creates the code according to the parameters given by argument.
	 * NOTE: if the code is not mapped, the method returns an empty string ("")
	 * @param code
	 * @return
	 * @throws ServiceException
	 */
	private String getMappedValueByCode(Class<? extends IDTO> c, Field field, String code)
			throws ServiceException {
		return StringUtil.notNull(
				ServiceExporterCode.getProperty(
						new StringBuffer(c.getSimpleName())
						.append(".")
						.append(field.getName())
						.append(".")
						.append(code).toString()
						));
	}

	/**
	 * This method validates if the field is marked to be exported
	 * @param field
	 * @return
	 * @throws ServiceException
	 */
	protected boolean isFieldToExport(Field field)
			throws ServiceException {
		return field != null && field.getAnnotation(FieldToExport.class) != null;
	}

	/**
	 * This method validates if the obtained value is a real value or a code to get
	 * the real value (it depends on to know if CodeMapped annotation exists or not)
	 * @param c
	 * @param field
	 * @param value
	 * @return
	 * @throws ServiceException
	 */
	protected String validateMappedValue(Class<? extends IDTO> c, Field field, String value)
			throws ServiceException {
		return field.getAnnotation(CodeMapped.class) != null
				? getMappedValueByCode(c, field, value)
						: value;
	}

	/**
	 * This method validates if the obtained value is a real value or a code to get
	 * the real value (it depends on to know if CodeMapped annotation exists or not)
	 * Shortcut
	 * @param c
	 * @param field
	 * @return
	 * @throws ServiceException
	 */
	protected Method getMethod(Class<? extends IDTO> c, Field field)
			throws ServiceException {
		return getMethod(c, field.getName());
	}

	/**
	 * This method validates if the obtained value is a real value or a code to get
	 * the real value (it depends on to know if CodeMapped annotation exists or not)
	 * @param c
	 * @param fieldName
	 * @return
	 * @throws ServiceException
	 */
	protected Method getMethod(Class<? extends IDTO> c, String fieldName)
			throws ServiceException {
		String methodName = fieldName;
		methodName = new StringBuffer("get")
				.append(methodName.substring(0, 1).toUpperCase())
				.append(methodName.substring(1)).toString();
		try {
			return c.getDeclaredMethod(methodName);
		} catch (NoSuchMethodException e) {
			DefinedLogger.SERVICE.error("File Exporter Service: No se pudo hallar el metodo buscado " + methodName, e);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT, e);
		} catch (SecurityException e) {
			DefinedLogger.SERVICE.error("File Exporter Service: No se pudo acceder al metodo buscado " + methodName, e);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT, e);
		}
	}

	/**
	 * This method returns the dynamic title if this exists
	 * @param c
	 * @param field
	 * @param instance
	 * @return
	 * @throws ServiceException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	protected String getDynamicTitle(Class<? extends IDTO> c, Field field, Object instance)
			throws ServiceException,
			IllegalAccessException,
			IllegalArgumentException,
			InvocationTargetException {
		if(field == null)
			return null;
		Method method = getMethod(c, field.getName() + DYNAMIC_TITLE_SUFIX);
		if(method == null)
			return null;
		return String.valueOf(method.invoke(instance));
	}

	protected boolean isValid(FieldToExport ann, Class<? extends IDTO> c, Field field, Object instance)
			throws IllegalAccessException,
			IllegalArgumentException,
			InvocationTargetException,
			ServiceException {
		if(ann == null)
			return true;

		/*
		 * VALIDATION 1:
		 * This validates if the dynamic title exists AND if the field is required
		 */
		if(ann.hasDynamicTitle() && !ann.required()) {
			//Validate if the dynamic title exists
			String dynamicTitle = getDynamicTitle(c, field, instance);
			if(StringUtil.isEmpty(dynamicTitle) || dynamicTitle.trim().equalsIgnoreCase("null")) {
				return false;
			}
		}

		return true;
	}
}