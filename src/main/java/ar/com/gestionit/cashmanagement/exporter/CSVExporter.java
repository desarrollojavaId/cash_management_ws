package ar.com.gestionit.cashmanagement.exporter;

import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.dto.FileExporterDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

public class CSVExporter extends AbstractTextExporter {
	public CSVExporter(List<List<? extends IDTO>> list, FileExporterDTO dto) {
		super(list, dto);
	}
}
