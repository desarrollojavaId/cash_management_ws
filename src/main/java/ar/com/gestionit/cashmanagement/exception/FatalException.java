package ar.com.gestionit.cashmanagement.exception;

/**
 * This exception was created to notify that the application must be killed
 * @author jdigruttola
 */
public class FatalException extends Exception {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor by default
	 */
	public FatalException() {
		super();
	}
	
	/**
	 * Parameterized constructor
	 * @param cause
	 */
	public FatalException(Throwable cause) {
		super(cause);
	}
	
	/**
	 * Parameterized constructor
	 * @param message
	 */
	public FatalException(String message) {
		super(message);
	}
	
	/**
	 * Parameterized constructor
	 * @param message
	 * @param cause
	 */
	public FatalException(String message, Throwable cause) {
		super(message, cause);
	}
}