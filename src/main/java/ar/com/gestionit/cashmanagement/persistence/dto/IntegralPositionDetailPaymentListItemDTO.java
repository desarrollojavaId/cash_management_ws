package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"descMedPag",
		"importe",
		"porcentaje",
		"cantidad",
		"moneda"
})
@XmlRootElement(name = ServiceConstant.DTO_INTEGRALPOSITION_MEDIOSPAGO)
public class IntegralPositionDetailPaymentListItemDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7804039559615385994L;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_DESCMEDPAG)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_DESCMEDPAG, defaultValue="")
	private String descMedPag;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_IMPORTE, defaultValue="")
	private String importe;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_PORCENTAJE)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_PORCENTAJE, defaultValue="")
	private String porcentaje;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_CANTIDAD)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_CANTIDAD, defaultValue="")
	private long cantidad;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_MONEDA)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_MONEDA, defaultValue="")
	private String moneda;
	@XmlTransient
	private double internalAmount;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_IMPORTE)
	@XmlTransient
	private String amountForExporter;
	
	public String getDescMedPag() {
		return descMedPag;
	}
	public void setDescMedPag(String descMedPag) {
		this.descMedPag = sanitateString(descMedPag);
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = sanitateString(importe);
	}
	public String getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(String porcentaje) {
		this.porcentaje = sanitateString(porcentaje);
	}
	public long getCantidad() {
		return cantidad;
	}
	public void setCantidad(long cantidad) {
		this.cantidad = cantidad;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public double getInternalAmount() {
		return internalAmount;
	}
	public void setInternalAmount(double internalAmount) {
		this.internalAmount = internalAmount;
	}
	public String getAmountForExporter() {
		return amountForExporter;
	}
	public void setAmountForExporter(String amountForExporter) {
		this.amountForExporter = amountForExporter;
	}
}