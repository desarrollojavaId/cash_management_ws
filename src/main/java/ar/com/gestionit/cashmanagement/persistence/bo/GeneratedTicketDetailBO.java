package ar.com.gestionit.cashmanagement.persistence.bo;


import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.GeneratedTicketDetailMapper;

public class GeneratedTicketDetailBO extends AbstractBO<IDTO, GeneratedTicketDetailMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = GeneratedTicketDetailMapper.class;
	}
}