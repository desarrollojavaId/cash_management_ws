package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"typesId",
		})
@XmlRootElement(name = ServiceConstant.DTO_FILERECEPTION_TYPESID)
public class FileReceptionTypesDTO extends AbstractDTO{

	
	private static final long serialVersionUID = 6047174703770579286L;
	
@XmlElement(name=ServiceConstant.DTO_FILERECEPTION_ID, defaultValue="")
private List<String> typesId;

public List<String> getTypesId() {
	return typesId;
}

public void setTypesId(List<String> typesId) {
	this.typesId = typesId;
}



	
}