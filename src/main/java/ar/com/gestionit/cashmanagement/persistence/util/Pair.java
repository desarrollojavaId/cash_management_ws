package ar.com.gestionit.cashmanagement.persistence.util;

/**
 * This class is used to work with Mybatis to do iterations
 * with a pair of values 
 * @author jdigruttola
 */
public class Pair {
	public String number;
	public String subnumber;
}