package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name = ServiceConstant.DTO_POFILE_ROOT)
public class POFileListDTO extends AbstractDTO{

	private static final long serialVersionUID = -4991324892118658096L;
	
	//Output
	@XmlElement(name=ServiceConstant.DTO_POFILE, defaultValue="")
	private List<POFileListItemDTO> list;

	/**
	 * @return the list
	 */
	public List<POFileListItemDTO> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<POFileListItemDTO> list) {
		this.list = list;
	}
	
	/**
	 * This method adds a new item in the list
	 * @param item
	 */
	public void addItem(POFileListItemDTO item) {
		if(list == null)
			list = new ArrayList<POFileListItemDTO>();
		list.add(item);
	}
}