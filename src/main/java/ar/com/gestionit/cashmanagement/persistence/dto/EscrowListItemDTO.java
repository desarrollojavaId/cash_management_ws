package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.CodeMapped;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={})

@XmlRootElement(name = ServiceConstant.DTO_ESCROWS)
public class EscrowListItemDTO extends AbstractDTO {
	
	private static final long serialVersionUID = -1215271026638111504L;
	
	//Output 
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_ID, defaultValue="")
	private String ssgcod;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_CUENTA, defaultValue="")
	private String ssncta;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_NOMBRE, defaultValue="")
	private String ssnfid;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_SERIE, defaultValue="")
	private String ssqser;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_GRUPO, defaultValue="")
	private String sskfid;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_FECHACORTE, defaultValue="")
	private String ssfcor;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_CAPITALCORTE, defaultValue="")
	private String ssjcor;
	@CodeMapped
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_ESTADO, defaultValue="")
	private String sskest;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_VUELCACLI, defaultValue="")
	private String sskcli;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_HISTOPER, defaultValue="")
	private String sskhis;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_CONDESC, defaultValue="")
	private String sskdto;
	
	//Interfaces
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_INTSEGUROS, defaultValue="")
	private String iSeguros = ServiceConstant.NO;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_INTREVOLVING, defaultValue="")
	private String iRevolving = ServiceConstant.NO;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_INTCOBRANZA, defaultValue="")
	private String iCobranza = ServiceConstant.NO;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_INTVUELCOINICIAL, defaultValue="")
	private String iVuelcoInicial = ServiceConstant.YES;

	//Fields to determine the enabled interfaces
	@XmlTransient
	private String sskestr;
	@XmlTransient
	private String sskests;
	@XmlTransient
	private String sskseg;
	@XmlTransient
	private String sskrev;
	
	
	public String getSsgcod() {
		return ssgcod;
	}

	public void setSsgcod(String ssgcod) {
		this.ssgcod = sanitateString(ssgcod);
	}

	public String getSsncta() {
		return ssncta;
	}

	public void setSsncta(String ssncta) {
		this.ssncta = sanitateString(ssncta);
	}

	public String getSsnfid() {
		return ssnfid;
	}

	public void setSsnfid(String ssnfid) {
		this.ssnfid = sanitateString(ssnfid);
	}

	public String getSsqser() {
		return ssqser;
	}

	public void setSsqser(String ssqser) {
		this.ssqser = sanitateString(ssqser);
	}

	public String getSskfid() {
		return sskfid;
	}

	public void setSskfid(String sskfid) {
		this.sskfid = sanitateString(sskfid);
	}

	public String getSsfcor() {
		return ssfcor;
	}

	public void setSsfcor(String ssfcor) {
		this.ssfcor = sanitateString(ssfcor);
	}

	public String getSsjcor() {
		return ssjcor;
	}

	public void setSsjcor(String ssjcor) {
		this.ssjcor = sanitateString(ssjcor);
	}

	public String getSskest() {
		return sskest;
	}

	public void setSskest(String sskest) {
		this.sskest = sanitateString(sskest);
	}

	public String getSskcli() {
		return sskcli;
	}

	public void setSskcli(String sskcli) {
		this.sskcli = sskcli;
	}

	public String getSskhis() {
		return sskhis;
	}

	public void setSskhis(String sskhis) {
		this.sskhis = sskhis;
	}

	public String getSskdto() {
		return sskdto;
	}

	public void setSskdto(String sskdto) {
		this.sskdto = sskdto;
	}

	/**
	 * @return the iSeguros
	 */
	public String getiSeguros() {
		return iSeguros;
	}

	/**
	 * @param iSeguros the iSeguros to set
	 */
	public void setiSeguros(String iSeguros) {
		this.iSeguros = iSeguros;
	}

	/**
	 * @return the iRevolving
	 */
	public String getiRevolving() {
		return iRevolving;
	}

	/**
	 * @param iRevolving the iRevolving to set
	 */
	public void setiRevolving(String iRevolving) {
		this.iRevolving = iRevolving;
	}

	/**
	 * @return the iCobranza
	 */
	public String getiCobranza() {
		return iCobranza;
	}

	/**
	 * @param iCobranza the iCobranza to set
	 */
	public void setiCobranza(String iCobranza) {
		this.iCobranza = iCobranza;
	}

	/**
	 * @return the iVuelcoInicial
	 */
	public String getiVuelcoInicial() {
		return iVuelcoInicial;
	}

	/**
	 * @param iVuelcoInicial the iVuelcoInicial to set
	 */
	public void setiVuelcoInicial(String iVuelcoInicial) {
		this.iVuelcoInicial = iVuelcoInicial;
	}

	public String getSskestr() {
		return sskestr;
	}

	public void setSskestr(String sskestr) {
		this.sskestr = sskestr;
	}

	public String getSskests() {
		return sskests;
	}

	public void setSskests(String sskests) {
		this.sskests = sskests;
	}

	public String getSskseg() {
		return sskseg;
	}

	public void setSskseg(String sskseg) {
		this.sskseg = sskseg;
	}

	public String getSskrev() {
		return sskrev;
	}

	public void setSskrev(String sskrev) {
		this.sskrev = sskrev;
	}
}