package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.CardListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.CardSummaryListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.CardListMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

public class CardListBO extends AbstractBO<IDTO, CardListMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = CardListMapper.class;
	}

	public List<CardSummaryListItemDTO> findTotals(CardListDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			//Get result list
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			CardListMapper mapper = session.getMapper(CardListMapper.class);
			List<CardSummaryListItemDTO> result = mapper.findTotals(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}