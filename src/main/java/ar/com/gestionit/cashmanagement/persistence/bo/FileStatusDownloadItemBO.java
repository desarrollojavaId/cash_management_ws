package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.FileStatusDownloadItemDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.FileStatusDownloadItemMapper;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

public class FileStatusDownloadItemBO extends AbstractBO<FileStatusDownloadItemDTO, FileStatusDownloadItemMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = FileStatusDownloadItemMapper.class;
	}
	
	
	/**
	 * This method returns a sucursal according to its ID
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	// revisar
	public FileStatusDownloadItemDTO find(FileStatusDownloadItemDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			//Load paging data
			super.loadPagingData(dto);
			
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			FileStatusDownloadItemMapper mapper = session.getMapper(FileStatusDownloadItemMapper.class);
			FileStatusDownloadItemDTO result = mapper.find(dto);
			queryCounter++;
			
			
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
}