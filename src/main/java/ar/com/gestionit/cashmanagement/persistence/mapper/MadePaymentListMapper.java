package ar.com.gestionit.cashmanagement.persistence.mapper;

import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.MadePaymentListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StoredProcedureParametersDTO;

public interface MadePaymentListMapper extends IMapper<IDTO> {
	public void generateReport(StoredProcedureParametersDTO dto);
	public List<String> findChannels(MadePaymentListDTO dto);
}