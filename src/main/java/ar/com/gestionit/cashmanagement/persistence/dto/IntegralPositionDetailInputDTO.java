package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"nroConsulta",
		"fechaCobroDesde",
		"fechaCobroHasta",
		"tipoRec"
})
@XmlRootElement(name = ServiceConstant.DTO_INTEGRALPOSITION_INPUT)
public class IntegralPositionDetailInputDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1035602698334575423L;
	
		//Input
		@XmlTransient
		private String convenio;
		@XmlTransient
		private String subconvenio;
		
		
		@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_NROCONSULTA, defaultValue="")
		private String nroConsulta;
		@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_FECHACOBRODESDE, defaultValue="")
		private String fechaCobroDesde;
		@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_FECHACOBROHASTA, defaultValue="")
		private String fechaCobroHasta;
		@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_TIPOREC, defaultValue="")
		private String tipoRec;
		
		public String getNroConsulta() {
			return nroConsulta;
		}
		public void setNroConsulta(String nroConsulta) {
			this.nroConsulta = sanitateString(nroConsulta);
		}
		
		public String getConvenio() {
			return convenio;
		}
		public void setConvenio(String convenio) {
			this.convenio = sanitateString(convenio);
		}
		public String getSubconvenio() {
			return subconvenio;
		}
		public void setSubconvenio(String subconvenio) {
			this.subconvenio = sanitateString(subconvenio);
		}
		public String getFechaCobroDesde() {
			return fechaCobroDesde;
		}
		public void setFechaCobroDesde(String fechaCobroDesde) {
			this.fechaCobroDesde = sanitateString(fechaCobroDesde);
		}
		public String getFechaCobroHasta() {
			return fechaCobroHasta;
		}
		public void setFechaCobroHasta(String fechaCobroHasta) {
			this.fechaCobroHasta = sanitateString(fechaCobroHasta);
		}
		public String getTipoRec() {
			return tipoRec;
		}
		public void setTipoRec(String tipoRec) {
			this.tipoRec = sanitateString(tipoRec);
		}
		
}