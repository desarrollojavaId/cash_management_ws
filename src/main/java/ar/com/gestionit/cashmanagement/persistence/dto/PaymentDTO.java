package ar.com.gestionit.cashmanagement.persistence.dto;

public class PaymentDTO extends AbstractDTO {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -7798867926488859811L;

	//Inputs
	private String paymentNumber;
	private String paymentSubnumber;
	private String adherent;

	//Outputs
	private String mtmcbu;
	private String kme;
	private String kfp;
	private String kes;
	private String tbdabr;
	private String proveenom;
	private String oppronom;
	private String dec;
	private String dbe;
	private String mtovto;
	private String mtofem;
	private String mtlqca;
	private String mtlqrc;
	private String mtlcin;
	private String mail;
	private String mtlaco;
	private String mtlrrp;
	private String mtlinv;
	private String mtafim; //Printed date of proof
	private String mtafec; //Sent date of proof
	
	/**
	 * @param paymentNumber the paymentNumber to set
	 */
	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = sanitateString(paymentNumber);
	}
	
	/**
	 * @param paymentSubnumber the paymentSubnumber to set
	 */
	public void setPaymentSubnumber(String paymentSubnumber) {
		this.paymentSubnumber = sanitateString(paymentSubnumber);
	}

	/**
	 * @param mtmcbu the mtmcbu to set
	 */
	public void setMtmcbu(String mtmcbu) {
		this.mtmcbu = sanitateString(mtmcbu);
	}


	/**
	 * @param kme the kme to set
	 */
	public void setKme(String kme) {
		this.kme = sanitateString(kme);
	}

	/**
	 * @param kfp the kfp to set
	 */
	public void setKfp(String kfp) {
		this.kfp = sanitateString(kfp);
	}

	/**
	 * @param kes the kes to set
	 */
	public void setKes(String kes) {
		this.kes = sanitateString(kes);
	}

	/**
	 * @param tbdabr the tbdabr to set
	 */
	public void setTbdabr(String tbdabr) {
		this.tbdabr = sanitateString(tbdabr);
	}

	/**
	 * @param proveenom the proveenom to set
	 */
	public void setProveenom(String proveenom) {
		this.proveenom = sanitateString(proveenom);
	}

	/**
	 * @param oppronom the oppronom to set
	 */
	public void setOppronom(String oppronom) {
		this.oppronom = sanitateString(oppronom);
	}

	/**
	 * @param dec the dec to set
	 */
	public void setDec(String dec) {
		this.dec = sanitateString(dec);
	}

	/**
	 * @param dbe the dbe to set
	 */
	public void setDbe(String dbe) {
		this.dbe = sanitateString(dbe);
	}

	/**
	 * @param mtovto the mtovto to set
	 */
	public void setMtovto(String mtovto) {
		this.mtovto = sanitateString(mtovto);
	}

	/**
	 * @param mtofem the mtofem to set
	 */
	public void setMtofem(String mtofem) {
		this.mtofem = sanitateString(mtofem);
	}

	/**
	 * @return the paymentNumber
	 */
	public String getPaymentNumber() {
		return paymentNumber;
	}

	/**
	 * @return the paymentSubnumber
	 */
	public String getPaymentSubnumber() {
		return paymentSubnumber;
	}

	/**
	 * @return the mtmcbu
	 */
	public String getMtmcbu() {
		return mtmcbu;
	}

	/**
	 * @return the kme
	 */
	public String getKme() {
		return kme;
	}

	/**
	 * @return the kfp
	 */
	public String getKfp() {
		return kfp;
	}

	/**
	 * @return the kes
	 */
	public String getKes() {
		return kes;
	}

	/**
	 * @return the tbdabr
	 */
	public String getTbdabr() {
		return tbdabr;
	}

	/**
	 * @return the proveenom
	 */
	public String getProveenom() {
		return proveenom;
	}

	/**
	 * @return the oppronom
	 */
	public String getOppronom() {
		return oppronom;
	}

	/**
	 * @return the dec
	 */
	public String getDec() {
		return dec;
	}

	/**
	 * @return the dbe
	 */
	public String getDbe() {
		return dbe;
	}

	/**
	 * @return the mtovto
	 */
	public String getMtovto() {
		return mtovto;
	}

	/**
	 * @return the mtofem
	 */
	public String getMtofem() {
		return mtofem;
	}

	/**
	 * @return the mtlqca
	 */
	public String getMtlqca() {
		return mtlqca;
	}

	/**
	 * @param mtlqca the mtlqca to set
	 */
	public void setMtlqca(String mtlqca) {
		this.mtlqca = sanitateString(mtlqca);
	}

	/**
	 * @return the mtlqrc
	 */
	public String getMtlqrc() {
		return mtlqrc;
	}

	/**
	 * @param mtlqrc the mtlqrc to set
	 */
	public void setMtlqrc(String mtlqrc) {
		this.mtlqrc = sanitateString(mtlqrc);
	}

	/**
	 * @return the mtlcin
	 */
	public String getMtlcin() {
		return mtlcin;
	}

	/**
	 * @param mtlcin the mtlcin to set
	 */
	public void setMtlcin(String mtlcin) {
		this.mtlcin = sanitateString(mtlcin);
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail) {
		this.mail = sanitateString(mail);
	}

	/**
	 * @return the mtlaco
	 */
	public String getMtlaco() {
		return mtlaco;
	}

	/**
	 * @param mtlaco the mtlaco to set
	 */
	public void setMtlaco(String mtlaco) {
		this.mtlaco = sanitateString(mtlaco);
	}

	/**
	 * @return the mtlrrp
	 */
	public String getMtlrrp() {
		return mtlrrp;
	}

	/**
	 * @param mtlrrp the mtlrrp to set
	 */
	public void setMtlrrp(String mtlrrp) {
		this.mtlrrp = sanitateString(mtlrrp);
	}

	/**
	 * @return the mtlinv
	 */
	public String getMtlinv() {
		return mtlinv;
	}

	/**
	 * @param mtlinv the mtlinv to set
	 */
	public void setMtlinv(String mtlinv) {
		this.mtlinv = sanitateString(mtlinv);
	}

	/**
	 * @return the mtafim
	 */
	public String getMtafim() {
		return mtafim;
	}

	/**
	 * @param mtafim the mtafim to set
	 */
	public void setMtafim(String mtafim) {
		this.mtafim = sanitateString(mtafim);
	}

	/**
	 * @return the mtafec
	 */
	public String getMtafec() {
		return mtafec;
	}

	/**
	 * @param mtafec the mtafec to set
	 */
	public void setMtafec(String mtafec) {
		this.mtafec = sanitateString(mtafec);
	}

	public String getAdherent() {
		return adherent;
	}

	public void setAdherent(String adherent) {
		this.adherent = adherent;
	}
	
}