package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.Date;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.AnnulmentOpDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.AnnulmentOpParamDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.AnnulmentOpMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

public class AnnulmentOpBO extends AbstractBO<IDTO, AnnulmentOpMapper>{
	
	/**
	 * Anullment Op mapper
	 */
	AnnulmentOpMapper mapper;

	
	@Override
	protected void specifyMapper() {
		mapperInterface = AnnulmentOpMapper.class;
	}

	/**
	 * This method deletes the file list transactionally
	 * @param input
	 * @throws ServiceException
	 */
	public void annulmentEcheq(AnnulmentOpDTO input) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(false);
			mapper = session.getMapper(AnnulmentOpMapper.class);

			/*
			 * File list to be deleted
			 * ------------------------------------------------------------------------
			 * query execution...
			 */
			int updatedNumber = 0;
			/*
			 * find state of payment order
			 */
			AnnulmentOpParamDTO dto;
			try {
				dto = (AnnulmentOpParamDTO) mapper.find(input);
			} catch( Exception e) {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
			}
			
			input.setTime(ServiceConstant.SDF_HOUR.format(new Date()));
			
			/*
			 * Insert annulment movement 
			 */
			
//			try {
//				if(dto.getMtlkes() == 2)
//					mapper.insert(new AnnulmentOpParamDTO(5,8,Integer.valueOf(input.getPaymentNumber()),Integer.valueOf(input.getPaymentSubnumber()), ServiceConstant.SDF_DATE.format(new Date()), ServiceConstant.SDF_HOUR.format(new Date()), null, null, input.getUser(), "WEB" ));
//				else
//					mapper.insert(new AnnulmentOpParamDTO(5,2,Integer.valueOf(input.getPaymentNumber()),Integer.valueOf(input.getPaymentSubnumber()), null, null, null, null, input.getUser(), "WEB" ));
//			} catch( Exception e) {
//				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ANNULMENTOPMOVEMENTERROR, e);
//			}
				
			
			/*
			 * Updating check 
			 */
			
//			try {
				mapper.updateEcheq(input);
//			} catch( Exception e) {
//				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ANNULMENTOPUPDATECHECKRROR, e);
//			}

			/*
			 * Updating payment order  
			 */
//			try {
				mapper.updateOp(input);
//			} catch( Exception e) {
//				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ANNULMENTOPUPDATEOPRROR, e);
//			}
//			

			session.commit();
		} catch(ServiceException e) {
			if(session != null)
				session.rollback();
			throw e;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	
}