package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"statuses"})
@XmlRootElement(name = ServiceConstant.TAG_STATUS_ROOT)
public class StatusInputListDTO extends AbstractDTO {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -7798867926488859811L;

	@XmlElement(name=ServiceConstant.TAG_STATUS)
	private List<StatusInputListItemDTO> statuses;

	/**
	 * @return the statuses
	 */
	public List<StatusInputListItemDTO> getStatuses() {
		return statuses;
	}

	/**
	 * @param statuses the statuses to set
	 */
	public void setStatuses(List<StatusInputListItemDTO> statuses) {
		this.statuses = statuses;
	}
}