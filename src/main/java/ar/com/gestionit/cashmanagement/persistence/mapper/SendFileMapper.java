package ar.com.gestionit.cashmanagement.persistence.mapper;

import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.dto.ErrorListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileAccountListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.SendFileDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StoredProcedureParametersDTO;

public interface SendFileMapper extends IMapper<SendFileDTO> {
	
	public void loadAccount(StoredProcedureParametersDTO dto);
	public String findDefaultCBU(String adherent);
	public int insertAccount(FileAccountListItemDTO dto);
	public void getLibraryAndTable(StoredProcedureParametersDTO dto);
	public int registerForSAT(SendFileDTO dto);
	public int insertFileContent(SendFileDTO dto);
	public String getFileStatus(SendFileDTO dto);
	public SendFileDTO findFileData(SendFileDTO dto);
	public List<ErrorListItemDTO> findFileErrors(SendFileDTO dto);
	public SendFileDTO findChecksumData(String adherent);
	public String verifyAdherenteForPD(SendFileDTO dto);
}