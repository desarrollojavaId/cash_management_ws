package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={
		"dateRecFrom",
		"dateRecTo" })
@XmlRootElement(name=ServiceConstant.DTO_INTEGRALPOSITION_FILTROS)
public class IntegralPositionListInputDTO extends AbstractDTO {
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 4969943919872788248L;
	
	//Inputs
	@XmlTransient
	private String requestNumber = "0";
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_FECHARECDESDE, defaultValue="")
	private String dateRecFrom;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_FECHARECHASTA, defaultValue="")
	private String dateRecTo;
	
	/**
	 * @return the requestNumber
	 */
	public String getRequestNumber() {
		return requestNumber;
	}
	/**
	 * @param requestNumber the requestNumber to set
	 */
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}
	/**
	 * @return the dateRecFrom
	 */
	public String getDateRecFrom() {
		return dateRecFrom;
	}
	/**
	 * @param dateRecFrom the dateRecFrom to set
	 */
	public void setDateRecFrom(String dateRecFrom) {
		this.dateRecFrom = dateRecFrom;
	}
	/**
	 * @return the dateRecTo
	 */
	public String getDateRecTo() {
		return dateRecTo;
	}
	/**
	 * @param dateRecTo the dateRecTo to set
	 */
	public void setDateRecTo(String dateRecTo) {
		this.dateRecTo = dateRecTo;
	}
}
