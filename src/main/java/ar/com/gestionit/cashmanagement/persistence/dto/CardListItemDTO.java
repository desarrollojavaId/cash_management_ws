package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.CodeMapped;
import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"cardNumber",
		"refDeu1",
		"refDeu2",
		"refComp1",
		"refComp2",
		"refComp3",
		"refComp4",
		"refComp5",
		"deuName",
		"cardStatus",
		"dateStart",
		"dateLastMov"
})
@XmlRootElement(name = ServiceConstant.DTO_CARD)
public class CardListItemDTO extends AbstractDTO {
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -1600726894333083824L;
	
	@FieldToExport(name=ServiceConstant.DTO_CARD_NROTAR)
	@XmlElement(name=ServiceConstant.DTO_CARD_NROTAR, defaultValue="")
	private String cardNumber;
	@FieldToExport(name=ServiceConstant.DTO_CARD_REF1DEUDOR)
	@XmlElement(name=ServiceConstant.DTO_CARD_REF1DEUDOR, defaultValue="")
	private String refDeu1;
	@FieldToExport(name=ServiceConstant.DTO_CARD_REF2DEUDOR)
	@XmlElement(name=ServiceConstant.DTO_CARD_REF2DEUDOR, defaultValue="")
	private String refDeu2;
	@FieldToExport(name=ServiceConstant.DTO_CARD_REF1COMPROBANTE)
	@XmlElement(name=ServiceConstant.DTO_CARD_REF1COMPROBANTE, defaultValue="")
	private String refComp1;
	@FieldToExport(name=ServiceConstant.DTO_CARD_REF2COMPROBANTE)
	@XmlElement(name=ServiceConstant.DTO_CARD_REF2COMPROBANTE, defaultValue="")
	private String refComp2;
	@FieldToExport(name=ServiceConstant.DTO_CARD_REF3COMPROBANTE)
	@XmlElement(name=ServiceConstant.DTO_CARD_REF3COMPROBANTE, defaultValue="")
	private String refComp3;
	@FieldToExport(name=ServiceConstant.DTO_CARD_REF4COMPROBANTE)
	@XmlElement(name=ServiceConstant.DTO_CARD_REF4COMPROBANTE, defaultValue="")
	private String refComp4;
	@FieldToExport(name=ServiceConstant.DTO_CARD_REF5COMPROBANTE)
	@XmlElement(name=ServiceConstant.DTO_CARD_REF5COMPROBANTE, defaultValue="")
	private String refComp5;
	@FieldToExport(name=ServiceConstant.DTO_CARD_RAZSOCDEUD)
	@XmlElement(name=ServiceConstant.DTO_CARD_RAZSOCDEUD, defaultValue="")
	private String deuName;
	@CodeMapped
	@FieldToExport(name=ServiceConstant.DTO_CARD_ESTTARFEC)
	@XmlElement(name=ServiceConstant.DTO_CARD_ESTTARFEC, defaultValue="")
	private String cardStatus;
	@FieldToExport(name=ServiceConstant.DTO_CARD_FECALTA)
	@XmlElement(name=ServiceConstant.DTO_CARD_FECALTA, defaultValue="")
	private String dateStart;
	@FieldToExport(name=ServiceConstant.DTO_CARD_FECULTMOV)
	@XmlElement(name=ServiceConstant.DTO_CARD_FECULTMOV, defaultValue="")
	private String dateLastMov;
	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}
	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = sanitateString(cardNumber);
	}
	/**
	 * @return the refDeu1
	 */
	public String getRefDeu1() {
		return refDeu1;
	}
	/**
	 * @param refDeu1 the refDeu1 to set
	 */
	public void setRefDeu1(String refDeu1) {
		this.refDeu1 = sanitateString(refDeu1);
	}
	/**
	 * @return the refDeu2
	 */
	public String getRefDeu2() {
		return refDeu2;
	}
	/**
	 * @param refDeu2 the refDeu2 to set
	 */
	public void setRefDeu2(String refDeu2) {
		this.refDeu2 = sanitateString(refDeu2);
	}
	/**
	 * @return the refComp1
	 */
	public String getRefComp1() {
		return refComp1;
	}
	/**
	 * @param refComp1 the refComp1 to set
	 */
	public void setRefComp1(String refComp1) {
		this.refComp1 = sanitateString(refComp1);
	}
	/**
	 * @return the refComp2
	 */
	public String getRefComp2() {
		return refComp2;
	}
	/**
	 * @param refComp2 the refComp2 to set
	 */
	public void setRefComp2(String refComp2) {
		this.refComp2 = sanitateString(refComp2);
	}
	/**
	 * @return the refComp3
	 */
	public String getRefComp3() {
		return refComp3;
	}
	/**
	 * @param refComp3 the refComp3 to set
	 */
	public void setRefComp3(String refComp3) {
		this.refComp3 = sanitateString(refComp3);
	}
	/**
	 * @return the refComp4
	 */
	public String getRefComp4() {
		return refComp4;
	}
	/**
	 * @param refComp4 the refComp4 to set
	 */
	public void setRefComp4(String refComp4) {
		this.refComp4 = sanitateString(refComp4);
	}
	/**
	 * @return the refComp5
	 */
	public String getRefComp5() {
		return refComp5;
	}
	/**
	 * @param refComp5 the refComp5 to set
	 */
	public void setRefComp5(String refComp5) {
		this.refComp5 = sanitateString(refComp5);
	}
	/**
	 * @return the deuName
	 */
	public String getDeuName() {
		return deuName;
	}
	/**
	 * @param deuName the deuName to set
	 */
	public void setDeuName(String deuName) {
		this.deuName = sanitateString(deuName);
	}
	/**
	 * @return the cardStatus
	 */
	public String getCardStatus() {
		return cardStatus;
	}
	/**
	 * @param cardStatus the cardStatus to set
	 */
	public void setCardStatus(String cardStatus) {
		this.cardStatus = sanitateString(cardStatus);
	}
	/**
	 * @return the dateStart
	 */
	public String getDateStart() {
		return dateStart;
	}
	/**
	 * @param dateStart the dateStart to set
	 */
	public void setDateStart(String dateStart) {
		this.dateStart = sanitateString(dateStart);
	}
	/**
	 * @return the dateLastMov
	 */
	public String getDateLastMov() {
		return dateLastMov;
	}
	/**
	 * @param dateLastMov the dateLastMov to set
	 */
	public void setDateLastMov(String dateLastMov) {
		this.dateLastMov = sanitateString(dateLastMov);
	}
}