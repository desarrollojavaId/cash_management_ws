package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"descImp",
		"importe",
		"moneda",
		"porcentaje",
		"cantidad"
})
@XmlRootElement(name = ServiceConstant.DTO_INTEGRALPOSITION_RESUME)
public class IntegralPositionDetailResumeListItemDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7804039559615385994L;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_DESCIMP)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_DESCIMP, defaultValue="")
	private String descImp;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_IMPORTE)
	@XmlTransient
	private String importeExportador;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_IMPORTE, defaultValue="")
	private String importe;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_MONEDA)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_MONEDA, defaultValue="")
	private String moneda;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_PORCENTAJE)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_PORCENTAJE, defaultValue="")
	private String porcentaje;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_CANTIDAD)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_CANTIDAD, defaultValue="")
	private int cantidad;
	@XmlTransient
	private double amount;
	
	public String getDescImp() {
		return descImp;
	}
	public void setDescImp(String descImp) {
		this.descImp = sanitateString(descImp);
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = sanitateString(importe);
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = sanitateString(moneda);
	}
	public String getImporteExportador() {
		return importeExportador;
	}
	public void setImporteExportador(String importeExportador) {
		this.importeExportador = importeExportador;
	}
	public String getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(String porcentaje) {
		this.porcentaje = sanitateString(porcentaje);
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
}