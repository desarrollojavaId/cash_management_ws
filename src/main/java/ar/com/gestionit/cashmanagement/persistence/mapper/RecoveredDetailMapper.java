package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

public interface RecoveredDetailMapper extends IMapper<IDTO> {
}