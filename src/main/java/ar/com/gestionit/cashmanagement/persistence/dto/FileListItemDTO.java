package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"stnars"})
@XmlRootElement(name = ServiceConstant.DTO_FILE)
public class FileListItemDTO extends AbstractDTO {
	
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -7798867926488859811L;
	
	//Input -----------------------------------------------------------
	@XmlElement(name=ServiceConstant.DTO_FILE_ID, defaultValue="")
	public String stnars; //File ID
	
	//Inputs - Outputs ----------------------------------------------------------
	@XmlTransient
	private String stkest; //Current file statusç
	@XmlTransient
	private String adherent; 
	
	/**
	 * @return the stkest
	 */
	public String getStkest() {
		return stkest;
	}
	
	/**
	 * @param stkest the stkest to set
	 */
	public void setStkest(String stkest) {
		this.stkest = sanitateString(stkest);
	}

	/**
	 * @return the stnars
	 */
	public String getStnars() {
		return stnars;
	}

	/**
	 * @param stnars the stnars to set
	 */
	public void setStnars(String stnars) {
		this.stnars = stnars;
	}

	public String getAdherent() {
		return adherent;
	}

	public void setAdherent(String adherent) {
		this.adherent = adherent;
	}
	
}