package ar.com.gestionit.cashmanagement.persistence.mapper;

import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.dto.CheckStatusCollectingAccountDTO;


public interface CheckStatusCollectingAccountMapper extends IMapper<CheckStatusCollectingAccountDTO> {

	/**
	 * This method is used to get files 
	 * @param dto
	 * @return
	 */
	
	public List<CheckStatusCollectingAccountDTO> findAccount(CheckStatusCollectingAccountDTO dto);
	public Integer findOperateChannel(CheckStatusCollectingAccountDTO dto);
	
	

}