package ar.com.gestionit.cashmanagement.persistence.bo;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.EscrowFileTypeMapper;

public class EscrowFileTypeBO extends AbstractBO<IDTO, EscrowFileTypeMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = EscrowFileTypeMapper.class;
	}
}