package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"cbu"})
@XmlRootElement(name = ServiceConstant.DTO_CASHACCOUNTSLIST_ROOT)
public class PaymentCbuInputListDTO extends AbstractDTO {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -7798867926488859811L;

	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_CBU)
	private List<String> cbu;

	/**
	 * @return the list
	 */
	public List<String> getCbu() {
		return cbu;
	}


	/**
	 * @param list the list to set
	 */
	public void setCbu(List<String> cbu) {
		this.cbu = cbu;
	}
}