package ar.com.gestionit.cashmanagement.persistence.bo;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.EscrowSendFileDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StoredProcedureParametersDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.EscrowSendFileMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;

public class EscrowSendFileBO extends AbstractBO<EscrowSendFileDTO, EscrowSendFileMapper>{

	@Override
	protected void specifyMapper() {
		mapperInterface = EscrowSendFileMapper.class;
	}

	/**
	 * This method gets the ending date for the escrow
	 * @return
	 * @throws ServiceException
	 */
	public String findEndingDate() throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			EscrowSendFileMapper mapper = session.getMapper(EscrowSendFileMapper.class);
			String result = mapper.findEndingDate();
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	/**
	 * This method validates if the escrow is available to send file 
	 * @param dto
	 * @throws ServiceException
	 */
	public void validateHash(EscrowSendFileDTO dto) throws ServiceException {
		if(dto == null) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT);
		}
		String result = null;
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			EscrowSendFileMapper mapper = session.getMapper(EscrowSendFileMapper.class);
			result = mapper.validateHash(dto);
			queryCounter++;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
		if(result == null || !result.trim().equals("1")) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ESCROWNOTENABLED);
		}
	}

	/**
	 * This method gets data to evaluate if we must update the escrow status or not
	 * @return
	 * @throws ServiceException
	 */
	public EscrowSendFileDTO findDataForStatus(EscrowSendFileDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			EscrowSendFileMapper mapper = session.getMapper(EscrowSendFileMapper.class);
			dto = mapper.findDataForStatus(dto);
			queryCounter++;
			return dto;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	/**
	 * This method updates the escrow status
	 * @return
	 * @throws ServiceException
	 */
	public int updateStatus(EscrowSendFileDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			EscrowSendFileMapper mapper = session.getMapper(EscrowSendFileMapper.class);
			int result = mapper.updateStatus(dto);
			session.commit();
			queryCounter++;
			return result;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	public void generateMember(EscrowSendFileDTO dto) throws ServiceException {
		StoredProcedureParametersDTO d = new StoredProcedureParametersDTO();
		SqlSession session = null;
		try {
			//Define and initialize variables
			d.setParam1(dto.getLibrary());
			d.setParam2(dto.getFileName() + "X");
			d.setParam3(dto.getMember() + dto.getEscrowNumber());
			d.setParam4(dto.getMember() + dto.getEscrowNumber());
			d.setParam5(" ");

			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			EscrowSendFileMapper mapper = session.getMapper(EscrowSendFileMapper.class);
			mapper.generateMember(d);
			session.commit();
			queryCounter++;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}

		DefinedLogger.SERVICE.info("EscrowSenFileService BO - SS0022P result: " + d.getParam5());

		if(d.getParam5() == null || d.getParam5().trim().equalsIgnoreCase("3")) {
			DefinedLogger.SERVICE.error("Error. No se pudo generar el miembro: " + dto.getMember());
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTGENERATEDMEMBER);
		}
	}

	/**
	 * This method gets the table name according to the escrow number, the file name and the file type
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public String findTableName(EscrowSendFileDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			EscrowSendFileMapper mapper = session.getMapper(EscrowSendFileMapper.class);
			String result = mapper.findTableName(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	public int insertFileContent(EscrowSendFileDTO dto) throws ServiceException {
		if(dto == null || dto.getFileLines() == null || dto.getFileLines().size() == 0){
			return 0;
		}
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			EscrowSendFileMapper mapper = session.getMapper(EscrowSendFileMapper.class);
			int result = mapper.insertFileContent(dto);
			queryCounter++;
			session.commit();
			session.close();
			return result;
		} catch(Exception e) {
			if (session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		}finally{
			if (session != null)
				session.close();
		}
	}

	public void insertByBatch(EscrowSendFileDTO dto) throws ServiceException {
		DefinedLogger.SERVICE.info("Starting insert01...");
		if(dto == null || dto.getFileLines() == null || dto.getFileLines().size() == 0){
			return;
		}
		SqlSession session = null;
		try {
			int maxLinesPerBatch = 20000;
			int persistedLines = 0;
			int maxAttemptNumber = 50;
			int attemptNumber = 0;
			int timeToSleep = 5000;

			List<String> lines = dto.getFileLines();

			String query = "INSERT INTO " + dto.getLibrary() + "/" + dto.getTable() + " VALUES(?)";
			DefinedLogger.SERVICE.info(DefinedLogger.TABULATOR + "Query to insert: " + query);

			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			PreparedStatement ps = session.getConnection().prepareStatement(query);

			while (persistedLines < lines.size()) {
				for (int i = 0; persistedLines < lines.size() && i < maxLinesPerBatch; i++) {
					ps.setString(1, lines.get(persistedLines));
					ps.addBatch();

					persistedLines++;
				}

				try {
					//Persisting...
					ps.executeBatch();
				} catch (SQLException e) {
					//In this case, we try to persist this batch for N attempts
					DefinedLogger.SERVICE.error("Failed insert statement to persist the escrow file. Attempt number: " + attemptNumber, e);

					// Wait for five seconds
					Thread.sleep(timeToSleep);
					do {
						try {
							ps.executeBatch();
							break;
						} catch (SQLException e2) {
							DefinedLogger.SERVICE.error("nro de intento " + attemptNumber, e2);
							//	esperar 5 segundos
							Thread.sleep(timeToSleep);
							attemptNumber++;
						}
					} while (attemptNumber < maxAttemptNumber);
				}

				//Validate if the batch could be inserted successfully
				if(attemptNumber >= maxAttemptNumber) {
					throw new ServiceException("");
				}

				attemptNumber = 0;
				ps.clearBatch();
			}					


			session.commit();
		} catch(Exception e) {
			if (session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		}finally{
			if (session != null)
				session.close();
			DefinedLogger.SERVICE.info("insert01 end");
		}
	}

	public String findResult() throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			EscrowSendFileMapper mapper = session.getMapper(EscrowSendFileMapper.class);
			String result = mapper.findResult();
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	public int updateDate(EscrowSendFileDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			EscrowSendFileMapper mapper = session.getMapper(EscrowSendFileMapper.class);
			int result = mapper.updateDate(dto);
			session.commit();
			queryCounter++;
			return result;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	public String findFileName(EscrowSendFileDTO dto) throws ServiceException {
		if(dto == null)
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT);

		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			EscrowSendFileMapper mapper = session.getMapper(EscrowSendFileMapper.class);
			String result = mapper.findFileName(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}