package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.CodeMapped;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"",
		"",
		""
})

@XmlRootElement(name = ServiceConstant.DTO_ESCROWS)
public class EscrowForSendItemDTO extends AbstractDTO {
	
	private static final long serialVersionUID = -1215271026638111504L;
	
	//Output 
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_ID, defaultValue="")
	private String ssgcod;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_CUENTA, defaultValue="")
	private String ssncta;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_NOMBRE, defaultValue="")
	private String ssnfid;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_SERIE, defaultValue="")
	private String ssqser;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_GRUPO, defaultValue="")
	private String sskfid;
	@CodeMapped
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_ESTADO, defaultValue="")
	private String sskest;
	
	@XmlTransient
	private String sskestr;
	@XmlTransient
	private String sskests;
	@XmlTransient
	private String fiarch1;
	@XmlTransient
	private String srqlng;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_ARCHTIPO, defaultValue="")
	private String srkarc;
	@XmlTransient
	private String fimie11;
	@XmlTransient
	private String fimie12;
	
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_SEGUROS, defaultValue="")
	private String sskseg;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_REVOLVING, defaultValue="")
	private String sskrev;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_INTSEGUROS, defaultValue="")
	private String iSeguros = ServiceConstant.NO;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_INTREVOLVING, defaultValue="")
	private String iRevolving = ServiceConstant.NO;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_INTCOBRANZA, defaultValue="")
	private String iCobranza = ServiceConstant.NO;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_INTVUELCOINICIAL, defaultValue="")
	private String iVuelcoInicial = ServiceConstant.YES;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_HISTOPER, defaultValue="")
	private String sskhis;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_VUELCACLI, defaultValue="")
	private String sskcli;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_MIEMBRO, defaultValue="")
	private String member;
	

	public String getSsgcod() {
		return ssgcod;
	}

	public void setSsgcod(String ssgcod) {
		this.ssgcod = sanitateString(ssgcod);
	}

	public String getSsncta() {
		return ssncta;
	}

	public void setSsncta(String ssncta) {
		this.ssncta = sanitateString(ssncta);
	}

	public String getSsnfid() {
		return ssnfid;
	}

	public void setSsnfid(String ssnfid) {
		this.ssnfid = sanitateString(ssnfid);
	}

	public String getSsqser() {
		return ssqser;
	}

	public void setSsqser(String ssqser) {
		this.ssqser = sanitateString(ssqser);
	}

	public String getSskfid() {
		return sskfid;
	}

	public void setSskfid(String sskfid) {
		this.sskfid = sanitateString(sskfid);
	}

	public String getSskest() {
		return sskest;
	}

	public void setSskest(String sskest) {
		this.sskest = sanitateString(sskest);
	}

	/**
	 * @return the sskhis
	 */
	public String getSskhis() {
		return sskhis;
	}

	/**
	 * @param sskhis the sskhis to set
	 */
	public void setSskhis(String sskhis) {
		this.sskhis = sskhis;
	}

	/**
	 * @return the sskcli
	 */
	public String getSskcli() {
		return sskcli;
	}

	/**
	 * @param sskcli the sskcli to set
	 */
	public void setSskcli(String sskcli) {
		this.sskcli = sskcli;
	}

	/**
	 * @return the sskestr
	 */
	public String getSskestr() {
		return sskestr;
	}

	/**
	 * @param sskestr the sskestr to set
	 */
	public void setSskestr(String sskestr) {
		this.sskestr = sskestr;
	}

	/**
	 * @return the sskests
	 */
	public String getSskests() {
		return sskests;
	}

	/**
	 * @param sskests the sskests to set
	 */
	public void setSskests(String sskests) {
		this.sskests = sskests;
	}

	/**
	 * @return the sskseg
	 */
	public String getSskseg() {
		return sskseg;
	}

	/**
	 * @param sskseg the sskseg to set
	 */
	public void setSskseg(String sskseg) {
		this.sskseg = sskseg;
	}

	/**
	 * @return the fiarch1
	 */
	public String getFiarch1() {
		return fiarch1;
	}

	/**
	 * @param fiarch1 the fiarch1 to set
	 */
	public void setFiarch1(String fiarch1) {
		this.fiarch1 = fiarch1;
	}

	/**
	 * @return the srqlng
	 */
	public String getSrqlng() {
		return srqlng;
	}

	/**
	 * @param srqlng the srqlng to set
	 */
	public void setSrqlng(String srqlng) {
		this.srqlng = srqlng;
	}

	/**
	 * @return the srkarc
	 */
	public String getSrkarc() {
		return srkarc;
	}

	/**
	 * @param srkarc the srkarc to set
	 */
	public void setSrkarc(String srkarc) {
		this.srkarc = srkarc;
	}

	/**
	 * @return the fimie11
	 */
	public String getFimie11() {
		return fimie11;
	}

	/**
	 * @param fimie11 the fimie11 to set
	 */
	public void setFimie11(String fimie11) {
		this.fimie11 = fimie11;
	}

	/**
	 * @return the fimie12
	 */
	public String getFimie12() {
		return fimie12;
	}

	/**
	 * @param fimie12 the fimie12 to set
	 */
	public void setFimie12(String fimie12) {
		this.fimie12 = fimie12;
	}

	/**
	 * @return the member
	 */
	public String getMember() {
		return member;
	}

	/**
	 * @param member the member to set
	 */
	public void setMember(String member) {
		this.member = member;
	}

	/**
	 * @return the iSeguros
	 */
	public String getiSeguros() {
		return iSeguros;
	}

	/**
	 * @param iSeguros the iSeguros to set
	 */
	public void setiSeguros(String iSeguros) {
		this.iSeguros = iSeguros;
	}

	/**
	 * @return the iRevolving
	 */
	public String getiRevolving() {
		return iRevolving;
	}

	/**
	 * @param iRevolving the iRevolving to set
	 */
	public void setiRevolving(String iRevolving) {
		this.iRevolving = iRevolving;
	}

	/**
	 * @return the sskrev
	 */
	public String getSskrev() {
		return sskrev;
	}

	/**
	 * @param sskrev the sskrev to set
	 */
	public void setSskrev(String sskrev) {
		this.sskrev = sskrev;
	}

	/**
	 * @return the iCobranza
	 */
	public String getiCobranza() {
		return iCobranza;
	}

	/**
	 * @param iCobranza the iCobranza to set
	 */
	public void setiCobranza(String iCobranza) {
		this.iCobranza = iCobranza;
	}

	/**
	 * @return the iVuelcoInicial
	 */
	public String getiVuelcoInicial() {
		return iVuelcoInicial;
	}

	/**
	 * @param iVuelcoInicial the iVuelcoInicial to set
	 */
	public void setiVuelcoInicial(String iVuelcoInicial) {
		this.iVuelcoInicial = iVuelcoInicial;
	}
}