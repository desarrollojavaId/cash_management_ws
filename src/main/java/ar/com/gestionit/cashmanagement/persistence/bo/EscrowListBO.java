package ar.com.gestionit.cashmanagement.persistence.bo;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.EscrowListMapper;

public class EscrowListBO extends AbstractBO<IDTO, EscrowListMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = EscrowListMapper.class;
	}
}