package ar.com.gestionit.cashmanagement.persistence.mapper;

import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.dto.CardListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.CardSummaryListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

public interface CardListMapper extends IMapper<IDTO> {
	public List<CardSummaryListItemDTO> findTotals(CardListDTO dto);
}