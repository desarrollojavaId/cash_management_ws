package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"fechaVenc",
		"empresaDesc",
		"moneda",
		"importe",
		"pagosRealizados",
		"tipoComprobante",
		"ref1",
		"ref2",
		"ref3",
		"ref4",
		"ref5"
})
@XmlRootElement(name = ServiceConstant.DTO_DEBTLIST)
public class DebtListItemDTO extends AbstractDTO{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -386047412517604852L;
	
	@XmlElement(name=ServiceConstant.DTO_DEBTLIST_FECHAVENC, defaultValue="")
	private String fechaVenc;
	@XmlElement(name=ServiceConstant.DTO_DEBTLIST_EMPRESADESC, defaultValue="")
	private String empresaDesc;
	@XmlElement(name=ServiceConstant.DTO_DEBTLIST_MONEDA, defaultValue="")
	private String moneda;
	@XmlElement(name=ServiceConstant.DTO_DEBTLIST_IMPORTE, defaultValue="")
	private String importe;
	@XmlElement(name=ServiceConstant.DTO_DEBTLIST_PAGOSREALIZADOS, defaultValue="")
	private String pagosRealizados;
	@XmlElement(name=ServiceConstant.DTO_DEBTLIST_TIPOCOMPROBANTE, defaultValue="")
	private String tipoComprobante;
	@XmlElement(name=ServiceConstant.DTO_DEBTLIST_REF1, defaultValue="")
	private String ref1;
	@XmlElement(name=ServiceConstant.DTO_DEBTLIST_REF2, defaultValue="")
	private String ref2;
	@XmlElement(name=ServiceConstant.DTO_DEBTLIST_REF3, defaultValue="")
	private String ref3;
	@XmlElement(name=ServiceConstant.DTO_DEBTLIST_REF4, defaultValue="")
	private String ref4;
	@XmlElement(name=ServiceConstant.DTO_DEBTLIST_REF5, defaultValue="")
	private String ref5;
	
	public String getFechaVenc() {
		return fechaVenc;
	}
	public void setFechaVenc(String fechaVenc) {
		this.fechaVenc = sanitateString(fechaVenc);
	}
	public String getEmpresaDesc() {
		return empresaDesc;
	}
	public void setEmpresaDesc(String empresaDesc) {
		this.empresaDesc = sanitateString(empresaDesc);
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = sanitateString(moneda);
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = sanitateString(importe);
	}
	public String getPagosRealizados() {
		return pagosRealizados;
	}
	public void setPagosRealizados(String pagosRealizados) {
		this.pagosRealizados = sanitateString(pagosRealizados);
	}
	public String getTipoComprobante() {
		return tipoComprobante;
	}
	public void setTipoComprobante(String tipoComprobante) {
		this.tipoComprobante = sanitateString(tipoComprobante);
	}
	public String getRef1() {
		return ref1;
	}
	public void setRef1(String ref1) {
		this.ref1 = sanitateString(ref1);
	}
	public String getRef2() {
		return ref2;
	}
	public void setRef2(String ref2) {
		this.ref2 = sanitateString(ref2);
	}
	public String getRef3() {
		return ref3;
	}
	public void setRef3(String ref3) {
		this.ref3 = sanitateString(ref3);
	}
	public String getRef4() {
		return ref4;
	}
	public void setRef4(String ref4) {
		this.ref4 = sanitateString(ref4);
	}
	public String getRef5() {
		return ref5;
	}
	public void setRef5(String ref5) {
		this.ref5 = sanitateString(ref5);
	}

	
	
	
}
