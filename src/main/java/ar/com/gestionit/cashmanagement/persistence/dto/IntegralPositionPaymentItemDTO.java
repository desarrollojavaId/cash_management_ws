package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"formaCobro",
		"importe",
		"porcentaje",
		"cantChq",
		"moneda"
})
@XmlRootElement(name = ServiceConstant.DTO_INTEGRALPOSITION_MEDIOSPAGO)
public class IntegralPositionPaymentItemDTO extends AbstractDTO{

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 903370156266010645L;
	
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_DESCMEDPAG, defaultValue="")
	private String formaCobro;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_IMPORTE, defaultValue="")
	private String importe;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_PORCENTAJE, defaultValue="")	
	private String porcentaje;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_CANTIDAD, defaultValue="")
	private String cantChq;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_MONEDA, defaultValue="")
	private String moneda;
	

	public String getFormaCobro() {
		return formaCobro;
	}
	public void setFormaCobro(String formaCobro) {
		this.formaCobro = sanitateString(formaCobro);
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = sanitateString(importe);
	}
	public String getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(String porcentaje) {
		this.porcentaje = sanitateString(porcentaje);
	}
	public String getCantChq() {
		return cantChq;
	}
	public void setCantChq(String cantChq) {
		this.cantChq = sanitateString(cantChq);
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = sanitateString(moneda);
	}
}