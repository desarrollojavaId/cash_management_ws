package ar.com.gestionit.cashmanagement.persistence.bo;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.FileListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.DeleteFileListMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

public class DeleteFileListBO extends AbstractBO<IDTO, DeleteFileListMapper>{

	/**
	 * Delete File List mapper
	 */
	DeleteFileListMapper mapper;

	@Override
	protected void specifyMapper() {
		mapperInterface = DeleteFileListMapper.class;
	}

	/**
	 * This method deletes the file list transactionally
	 * @param input
	 * @throws ServiceException
	 */
	public void deleteFileList(FileListDTO input) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(false);
			mapper = session.getMapper(DeleteFileListMapper.class);

			/*
			 * File list to be deleted
			 * ------------------------------------------------------------------------
			 * query execution...
			 */
			int updatedNumber = 0;
			try {
				updatedNumber = mapper.update(input);
				queryCounter++;
			} catch(Exception e) {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
			}

			/*	
			 * processing information....
			 */
			//Validate if all the files could be deleted
			if (updatedNumber < input.getList().size()) {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTDELETEFILES);
			}

			/*
			 * File list to be deleted in SAT
			 * ------------------------------------------------------------------------
			 * query execution...
			 */
			try {
				updatedNumber = mapper.updateSATByList(input);
				queryCounter++;
			} catch(Exception e) {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
			}

			/*	
			 * processing information....
			 */
			//Validate if all the files could be deleted
			if (updatedNumber < input.getList().size()){
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTDELETEFILESSAT);
			}

			session.commit();
		} catch(ServiceException e) {
			if(session != null)
				session.rollback();
			throw e;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}