package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={
		"token",
		"format",
		"channelNumber",
		"operationNumber",
		"variantNumber"
		})
@XmlRootElement(name = ServiceConstant.DTO_EXPORT)
public class FileExporterDTO extends AbstractDTO {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -7798867926488859811L;

	//Input
	@XmlElement(name=ServiceConstant.DTO_EXPORT_FORMAT, defaultValue="")
	private String format;
	@XmlElement(name=ServiceConstant.TAG_ID, defaultValue="")
	private String token;
	@XmlElement(name=ServiceConstant.DTO_EXPORT_CHANNEL, defaultValue="")
	private String channelNumber;
	@XmlElement(name=ServiceConstant.DTO_EXPORT_OPERATION, defaultValue="")
	private String operationNumber;
	@XmlElement(name=ServiceConstant.DTO_EXPORT_VARIANT, defaultValue="")
	private String variantNumber;
	
	@XmlTransient
	private String fileName;
	
	/**
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}
	/**
	 * @param format the format to set
	 */
	public void setFormat(String format) {
		this.format = format;
	}
	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}
	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}
	/**
	 * @return the channelNumber
	 */
	public String getChannelNumber() {
		return channelNumber;
	}
	/**
	 * @param channelNumber the channelNumber to set
	 */
	public void setChannelNumber(String channelNumber) {
		this.channelNumber = channelNumber;
	}
	/**
	 * @return the operationNumber
	 */
	public String getOperationNumber() {
		return operationNumber;
	}
	/**
	 * @param operationNumber the operationNumber to set
	 */
	public void setOperationNumber(String operationNumber) {
		this.operationNumber = operationNumber;
	}
	/**
	 * @return the variantNumber
	 */
	public String getVariantNumber() {
		return variantNumber;
	}
	/**
	 * @param variantNumber the variantNumber to set
	 */
	public void setVariantNumber(String variantNumber) {
		this.variantNumber = variantNumber;
	}
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}