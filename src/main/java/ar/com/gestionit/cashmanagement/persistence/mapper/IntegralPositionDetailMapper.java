package ar.com.gestionit.cashmanagement.persistence.mapper;


import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionDetailDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionDetailListItemDTO;

public interface IntegralPositionDetailMapper extends IMapper<IDTO> {
	
	public IntegralPositionDetailListItemDTO findList(IntegralPositionDetailListItemDTO dto);
	public List <IntegralPositionDetailListItemDTO> findTotals(IntegralPositionDetailDTO dto);
}