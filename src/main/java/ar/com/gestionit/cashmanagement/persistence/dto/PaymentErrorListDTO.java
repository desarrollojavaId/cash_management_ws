package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name = ServiceConstant.DTO_ERRORSLIST_ROOT)
public class PaymentErrorListDTO extends AbstractDTO{

	private static final long serialVersionUID = -4422316280008537113L;
	
	@XmlElement(name=ServiceConstant.DTO_ERRORSLIST)
	private List<PaymentErrorListItemDTO> list;

	/**
	 * @return the list
	 */
	public List<PaymentErrorListItemDTO> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<PaymentErrorListItemDTO> list) {
		this.list = list;
	}
	
	public void setError(PaymentErrorListItemDTO dto) {
		if(list == null)
			list = new ArrayList<PaymentErrorListItemDTO>();
		list.add(dto);
	}
}