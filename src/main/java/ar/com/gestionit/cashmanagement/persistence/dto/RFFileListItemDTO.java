package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "debtorRef1", "debtorRef2", "debtorName", "proofRef1", "proofRef2", "emisionDate",
		"effectiveDate", "expirationDate1", "expirationAmount1", "expirationDate2", "expirationAmount2", "minAmount" })
@XmlRootElement(name = ServiceConstant.DTO_RFFILE)
public class RFFileListItemDTO extends AbstractDTO {

	private static final long serialVersionUID = -4991324892118658096L;

	// Output
	@XmlTransient
	private String debtorRef1Title;
	@XmlTransient
	private String debtorRef2Title;
	@FieldToExport(name = ServiceConstant.DTO_RFFILE_DEBTORNAME)
	@XmlElement(name = ServiceConstant.DTO_RFFILE_DEBTORNAME, defaultValue = "")
	private String debtorName;
	@FieldToExport(name = ServiceConstant.DTO_RFFILE_DEBTORREF1, hasDynamicTitle = true, required = true)
	@XmlElement(name = ServiceConstant.DTO_RFFILE_DEBTORREF1, defaultValue = "")
	private String debtorRef1;
	@FieldToExport(name = ServiceConstant.DTO_RFFILE_PROOFREF1, hasDynamicTitle = true, required = true)
	@XmlElement(name = ServiceConstant.DTO_RFFILE_PROOFREF1, defaultValue = "")
	private String proofRef1;
	@XmlTransient
	private String proofRef1Title;
	@XmlTransient
	private String proofRef2Title;
	@FieldToExport(name = ServiceConstant.DTO_RFFILE_EXPAMOUNT1)
	@XmlElement(name = ServiceConstant.DTO_RFFILE_EXPAMOUNT1, defaultValue = "")
	private String expirationAmount1;
	@FieldToExport(name = ServiceConstant.DTO_RFFILE_EXPDATE1)
	@XmlElement(name = ServiceConstant.DTO_RFFILE_EXPDATE1, defaultValue = "")
	private String expirationDate1;
//	@FieldToExport(name = ServiceConstant.DTO_RFFILE_EXPAMOUNT2)
	@XmlElement(name = ServiceConstant.DTO_RFFILE_EXPAMOUNT2, defaultValue = "")
	private String expirationAmount2;
//	@FieldToExport(name = ServiceConstant.DTO_RFFILE_EXPDATE2)
	@XmlElement(name = ServiceConstant.DTO_RFFILE_EXPDATE2, defaultValue = "")
	private String expirationDate2;
//	@FieldToExport(name = ServiceConstant.DTO_RFFILE_MINAMOUNT)
	@XmlElement(name = ServiceConstant.DTO_RFFILE_MINAMOUNT, defaultValue = "")
	private String minAmount;
	@FieldToExport(name = ServiceConstant.DTO_RFFILE_EMISIONDATE)
	@XmlElement(name = ServiceConstant.DTO_RFFILE_EMISIONDATE, defaultValue = "")
	private String emisionDate;
	@FieldToExport(name = ServiceConstant.DTO_RFFILE_EFFECTIVEDATE)
	@XmlElement(name = ServiceConstant.DTO_RFFILE_EFFECTIVEDATE, defaultValue = "")
	private String effectiveDate;
//	@FieldToExport(name = ServiceConstant.DTO_RFFILE_DEBTORREF2, hasDynamicTitle = true, required = true)
	@XmlElement(name = ServiceConstant.DTO_RFFILE_DEBTORREF2, defaultValue = "")
	private String debtorRef2;
//	@FieldToExport(name = ServiceConstant.DTO_RFFILE_PROOFREF2, hasDynamicTitle = true, required = true)
	@XmlElement(name = ServiceConstant.DTO_RFFILE_PROOFREF2, defaultValue = "")
	private String proofRef2;

	/**
	 * @return the debtorRef1
	 */
	public String getDebtorRef1() {
		return debtorRef1;
	}

	/**
	 * @param debtorRef1
	 *            the debtorRef1 to set
	 */
	public void setDebtorRef1(String debtorRef1) {
		this.debtorRef1 = sanitateString(debtorRef1);
	}

	/**
	 * @return the debtorRef2
	 */
	public String getDebtorRef2() {
		return debtorRef2;
	}

	/**
	 * @param debtorRef2
	 *            the debtorRef2 to set
	 */
	public void setDebtorRef2(String debtorRef2) {
		this.debtorRef2 = sanitateString(debtorRef2);
	}

	/**
	 * @return the debtorName
	 */
	public String getDebtorName() {
		return debtorName;
	}

	/**
	 * @param debtorName
	 *            the debtorName to set
	 */
	public void setDebtorName(String debtorName) {
		this.debtorName = sanitateString(debtorName);
	}

	/**
	 * @return the proofRef1
	 */
	public String getProofRef1() {
		return proofRef1;
	}

	/**
	 * @param proofRef1
	 *            the proofRef1 to set
	 */
	public void setProofRef1(String proofRef1) {
		this.proofRef1 = sanitateString(proofRef1);
	}

	/**
	 * @return the proofRef2
	 */
	public String getProofRef2() {
		return proofRef2;
	}

	/**
	 * @param proofRef2
	 *            the proofRef2 to set
	 */
	public void setProofRef2(String proofRef2) {
		this.proofRef2 = sanitateString(proofRef2);
	}

	/**
	 * @return the emisionDate
	 */
	public String getEmisionDate() {
		return emisionDate;
	}

	/**
	 * @param emisionDate
	 *            the emisionDate to set
	 */
	public void setEmisionDate(String emisionDate) {
		this.emisionDate = formatDateYYYYMMDD(sanitateDate(sanitateString(emisionDate)));
	}

	/**
	 * @return the effectiveDate
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate
	 *            the effectiveDate to set
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = formatDateYYYYMMDD(sanitateDate(sanitateString(effectiveDate)));
	}

	/**
	 * @return the expirationDate1
	 */
	public String getExpirationDate1() {
		return expirationDate1;
	}

	/**
	 * @param expirationDate1
	 *            the expirationDate1 to set
	 */
	public void setExpirationDate1(String expirationDate1) {
		this.expirationDate1 = formatDateYYYYMMDD(sanitateDate(sanitateString(expirationDate1)));
	}

	/**
	 * @return the expirationAmount1
	 */
	public String getExpirationAmount1() {
		return expirationAmount1;
	}

	/**
	 * @param expirationAmount1
	 *            the expirationAmount1 to set
	 */
	public void setExpirationAmount1(String expirationAmount1) {
		this.expirationAmount1 = sanitateString(expirationAmount1);
	}

	/**
	 * @return the expirationDate2
	 */
	public String getExpirationDate2() {
		return expirationDate2;
	}

	/**
	 * @param expirationDate2
	 *            the expirationDate2 to set
	 */
	public void setExpirationDate2(String expirationDate2) {
		this.expirationDate2 = formatDateYYYYMMDD(sanitateDate(sanitateString(expirationDate2)));
	}

	/**
	 * @return the expirationAmount2
	 */
	public String getExpirationAmount2() {
		return expirationAmount2;
	}

	/**
	 * @param expirationAmount2
	 *            the expirationAmount2 to set
	 */
	public void setExpirationAmount2(String expirationAmount2) {
		this.expirationAmount2 = sanitateString(expirationAmount2);
	}

	/**
	 * @return the minAmount
	 */
	public String getMinAmount() {
		return minAmount;
	}

	/**
	 * @param minAmount
	 *            the minAmount to set
	 */
	public void setMinAmount(String minAmount) {
		this.minAmount = sanitateString(minAmount);
	}

	public String getDebtorRef1Title() {
		return debtorRef1Title;
	}

	public void setDebtorRef1Title(String debtorRef1Title) {
		this.debtorRef1Title = sanitateString(debtorRef1Title);
	}

	public String getDebtorRef2Title() {
		return debtorRef2Title;
	}

	public void setDebtorRef2Title(String debtorRef2Title) {
		this.debtorRef2Title = sanitateString(debtorRef2Title);
	}

	public String getProofRef1Title() {
		return proofRef1Title;
	}

	public void setProofRef1Title(String proofRef1Title) {
		this.proofRef1Title = sanitateString(proofRef1Title);
	}

	public String getProofRef2Title() {
		return proofRef2Title;
	}

	public void setProofRef2Title(String proofRef2Title) {
		this.proofRef2Title = sanitateString(proofRef2Title);
	}
}