package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DateUtil;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"mtqska",
		"mtkdes",
		"mtqfva",
		"mtqhva"
})
@XmlRootElement(name = ServiceConstant.DTO_ERRORSLIST)
public class PaymentErrorListItemDTO extends AbstractDTO{

	private static final long serialVersionUID = -4422316280008537113L;
	
	//Input
	@XmlTransient
	private String mtksko; 
	@XmlTransient
	private String nomArch;
	@XmlTransient
	private int pageFrom;
	@XmlTransient
	private int pageTo;

	
	//Output  - new
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_CODERROR)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_CODERROR, defaultValue="")
	private String mtqska;
	
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_DESCERROR)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_DESCERROR, defaultValue="")
	private String mtkdes;
	
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_FECHAERROR)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_FECHAERROR, defaultValue="")
	private String mtqfva;
	
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_HORAERROR)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_HORAERROR)
	private String mtqhva;

	public String getMtksko() {
		return mtksko;
	}

	public void setMtksko(String mtksko) {
		this.mtksko = sanitateString(mtksko);
	}

	public String getNomArch() {
		return nomArch;
	}

	public void setNomArch(String nomArch) {
		this.nomArch = sanitateString(nomArch);
	}

	public int getPageFrom() {
		return pageFrom;
	}

	public void setPageFrom(int pageFrom) {
		this.pageFrom = pageFrom;
	}

	public int getPageTo() {
		return pageTo;
	}

	public void setPageTo(int pageTo) {
		this.pageTo = pageTo;
	}

	public String getMtqska() {
		return mtqska;
	}

	public void setMtqska(String mtqska) {
		this.mtqska = sanitateString(mtqska);
	}

	public String getMtkdes() {
		return mtkdes;
	}

	public void setMtkdes(String mtkdes) {
		this.mtkdes = sanitateString(mtkdes);
	}

	public String getMtqfva() {
		return mtqfva;
	}

	public void setMtqfva(String mtqfva) {
		if(!"".equals(sanitateString(mtqfva))){
			this.mtqfva = DateUtil.normalizeDateFormat(sanitateString(mtqfva));
		}
		else{
			this.mtqfva = sanitateString(mtqfva);
		}
	}

	public String getMtqhva() {
		return mtqhva;
	}

	public void setMtqhva(String mtqhva) {
		this.mtqhva = sanitateString(mtqhva);
	}

	
	
	
	
}