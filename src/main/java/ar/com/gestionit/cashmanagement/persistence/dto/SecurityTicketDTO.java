package ar.com.gestionit.cashmanagement.persistence.dto;

public class SecurityTicketDTO extends AbstractDTO{

	private static final long serialVersionUID = 3012511007755070184L;

	//Inputs
	private String cuitBeneficiario;
	private String cuit;
	private String nroOp;
	private String subNroOp;
	
	//Input-Outps
	private String mtlkme;
	private String mtosuc;
	private String mtgsuc;
	
	//Outputs
	private String mtlcos;
	private String mtlfer;
	private String mtljpa;
	private String mtlqop;
	private String mtlqrc;
	
	private String mtxdom;
	private String mtxhoa;
	
	public String getCuitBeneficiario() {
		return cuitBeneficiario;
	}
	public void setCuitBeneficiario(String cuitBeneficiario) {
		this.cuitBeneficiario = sanitateString(cuitBeneficiario);
	}
	public String getCuit() {
		return cuit;
	}
	public void setCuit(String cuit) {
		this.cuit = sanitateString(cuit);
	}
	public String getNroOp() {
		return nroOp;
	}
	public void setNroOp(String nroOp) {
		this.nroOp = sanitateString(nroOp);
	}
	public String getSubNroOp() {
		return subNroOp;
	}
	public void setSubNroOp(String subNroOp) {
		this.subNroOp = sanitateString(subNroOp);
	}
	public String getMtlkme() {
		return mtlkme;
	}
	public void setMtlkme(String mtlkme) {
		this.mtlkme = sanitateString(mtlkme);
	}
	public String getMtosuc() {
		return mtosuc;
	}
	public void setMtosuc(String mtosuc) {
		this.mtosuc = sanitateString(mtosuc);
	}
	public String getMtgsuc() {
		return mtgsuc;
	}
	public void setMtgsuc(String mtgsuc) {
		this.mtgsuc = sanitateString(mtgsuc);
	}
	public String getMtlcos() {
		return mtlcos;
	}
	public void setMtlcos(String mtlcos) {
		this.mtlcos = sanitateString(mtlcos);
	}
	public String getMtlfer() {
		return mtlfer;
	}
	public void setMtlfer(String mtlfer) {
		this.mtlfer = sanitateString(mtlfer);
	}
	public String getMtljpa() {
		return mtljpa;
	}
	public void setMtljpa(String mtljpa) {
		this.mtljpa = sanitateString(mtljpa);
	}
	
	public String getMtxdom() {
		return mtxdom;
	}
	public void setMtxdom(String mtxdom) {
		this.mtxdom = sanitateString(mtxdom);
	}
	public String getMtxhoa() {
		return mtxhoa;
	}
	public void setMtxhoa(String mtxhoa) {
		this.mtxhoa = sanitateString(mtxhoa);
	}
	public String getMtlqop() {
		return mtlqop;
	}
	public void setMtlqop(String mtlqop) {
		this.mtlqop = sanitateString(mtlqop);
	}
	public String getMtlqrc() {
		return mtlqrc;
	}
	public void setMtlqrc(String mtlqrc) {
		this.mtlqrc = sanitateString(mtlqrc);
	}
	
		
}
