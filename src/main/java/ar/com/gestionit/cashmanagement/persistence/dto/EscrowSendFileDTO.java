package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

public class EscrowSendFileDTO extends AbstractDTO {
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 4969943919872788248L;
	
	private List<String> fileLines;
	private String fileStatus;
	
	private String endingDate;
	private String today;
	private String escrowNumber;
	private String fileName;
	private String table;
	private String library;
	private String member;
	private String fileType;
	
	private String escrowStatus;
	private String dateOp;
	private String datePg;
	private String dateCl;
	private String dateCu;
	private String dateSg;
	private String group;
	private String isNecessaryCl;
	private String isNecessaryPg;
	private String isNecessarySg;
	private String safeStatus;
	
	private String sskestr;
	private String sskests;
	private String sskest;
	
	/**
	 * @return the fileLines
	 */
	public List<String> getFileLines() {
		return fileLines;
	}
	/**
	 * @param fileLines the fileLines to set
	 */
	public void setFileLines(List<String> fileLines) {
		this.fileLines = fileLines;
	}
	/**
	 * @return the fileStatus
	 */
	public String getFileStatus() {
		return fileStatus;
	}
	/**
	 * @param fileStatus the fileStatus to set
	 */
	public void setFileStatus(String fileStatus) {
		this.fileStatus = fileStatus;
	}
	/**
	 * @return the endingDate
	 */
	public String getEndingDate() {
		return endingDate;
	}
	/**
	 * @param endingDate the endingDate to set
	 */
	public void setEndingDate(String endingDate) {
		this.endingDate = endingDate;
	}
	/**
	 * @return the today
	 */
	public String getToday() {
		return today;
	}
	/**
	 * @param today the today to set
	 */
	public void setToday(String today) {
		this.today = today;
	}
	/**
	 * @return the escrowNumber
	 */
	public String getEscrowNumber() {
		return escrowNumber;
	}
	/**
	 * @param escrowNumber the escrowNumber to set
	 */
	public void setEscrowNumber(String escrowNumber) {
		this.escrowNumber = escrowNumber;
	}
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the table
	 */
	public String getTable() {
		return table;
	}
	/**
	 * @param table the table to set
	 */
	public void setTable(String table) {
		this.table = table;
	}
	/**
	 * @return the library
	 */
	public String getLibrary() {
		return library;
	}
	/**
	 * @param library the library to set
	 */
	public void setLibrary(String library) {
		this.library = library;
	}
	/**
	 * @return the member
	 */
	public String getMember() {
		return member;
	}
	/**
	 * @param member the member to set
	 */
	public void setMember(String member) {
		this.member = member;
	}
	/**
	 * @return the fileType
	 */
	public String getFileType() {
		return fileType;
	}
	/**
	 * @param fileType the fileType to set
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	/**
	 * @return the escrowStatus
	 */
	public String getEscrowStatus() {
		return escrowStatus;
	}
	/**
	 * @param escrowStatus the escrowStatus to set
	 */
	public void setEscrowStatus(String escrowStatus) {
		this.escrowStatus = escrowStatus;
	}
	/**
	 * @return the dateOp
	 */
	public String getDateOp() {
		return dateOp;
	}
	/**
	 * @param dateOp the dateOp to set
	 */
	public void setDateOp(String dateOp) {
		this.dateOp = dateOp;
	}
	/**
	 * @return the datePg
	 */
	public String getDatePg() {
		return datePg;
	}
	/**
	 * @param datePg the datePg to set
	 */
	public void setDatePg(String datePg) {
		this.datePg = datePg;
	}
	/**
	 * @return the dateCl
	 */
	public String getDateCl() {
		return dateCl;
	}
	/**
	 * @param dateCl the dateCl to set
	 */
	public void setDateCl(String dateCl) {
		this.dateCl = dateCl;
	}
	/**
	 * @return the dateCu
	 */
	public String getDateCu() {
		return dateCu;
	}
	/**
	 * @param dateCu the dateCu to set
	 */
	public void setDateCu(String dateCu) {
		this.dateCu = dateCu;
	}
	/**
	 * @return the dateSg
	 */
	public String getDateSg() {
		return dateSg;
	}
	/**
	 * @param dateSg the dateSg to set
	 */
	public void setDateSg(String dateSg) {
		this.dateSg = dateSg;
	}
	/**
	 * @return the group
	 */
	public String getGroup() {
		return group;
	}
	/**
	 * @param group the group to set
	 */
	public void setGroup(String group) {
		this.group = group;
	}
	/**
	 * @return the isNecessaryCl
	 */
	public String getIsNecessaryCl() {
		return isNecessaryCl;
	}
	/**
	 * @param isNecessaryCl the isNecessaryCl to set
	 */
	public void setIsNecessaryCl(String isNecessaryCl) {
		this.isNecessaryCl = isNecessaryCl;
	}
	/**
	 * @return the isNecessaryPg
	 */
	public String getIsNecessaryPg() {
		return isNecessaryPg;
	}
	/**
	 * @param isNecessaryPg the isNecessaryPg to set
	 */
	public void setIsNecessaryPg(String isNecessaryPg) {
		this.isNecessaryPg = isNecessaryPg;
	}
	/**
	 * @return the isNecessarySg
	 */
	public String getIsNecessarySg() {
		return isNecessarySg;
	}
	/**
	 * @param isNecessarySg the isNecessarySg to set
	 */
	public void setIsNecessarySg(String isNecessarySg) {
		this.isNecessarySg = isNecessarySg;
	}
	/**
	 * @return the safeStatus
	 */
	public String getSafeStatus() {
		return safeStatus;
	}
	/**
	 * @param safeStatus the safeStatus to set
	 */
	public void setSafeStatus(String safeStatus) {
		this.safeStatus = safeStatus;
	}
	public String getSskestr() {
		return sskestr;
	}
	public void setSskestr(String sskestr) {
		this.sskestr = sskestr;
	}
	public String getSskests() {
		return sskests;
	}
	public void setSskests(String sskests) {
		this.sskests = sskests;
	}
	public String getSskest() {
		return sskest;
	}
	public void setSskest(String sskest) {
		this.sskest = sskest;
	}
}