package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"paymentType",
			"oldAmount",
			"day1Date",
			"day1",
			"day2Date",
			"day2",
			"day3Date",
			"day3",
			"day4Date",
			"day4",
			"day5Date",
			"day5",
			"nextAmount"})
public class EgressOutflowItemDTO extends AbstractDTO {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -7798867926488859811L;

	//Ouput
	@FieldToExport(name=ServiceConstant.DTO_MEDIOPAGODESC)
	@XmlElement(name=ServiceConstant.DTO_MEDIOPAGODESC, defaultValue="")
	private String paymentType;
	
	@XmlElement(name=ServiceConstant.DTO_IMPORTESANTERIORES, defaultValue="")
	private String oldAmount;
	@XmlElement(name=ServiceConstant.DTO_IMPORTEHOYFECHA, defaultValue="")
	private String day1Date;
	@XmlElement(name=ServiceConstant.DTO_IMPORTEHOY, defaultValue="")
	private String day1;
	@XmlElement(name=ServiceConstant.DTO_IMPORTEHOYMAS1FECHA, defaultValue="")
	private String day2Date;
	@XmlElement(name=ServiceConstant.DTO_IMPORTEHOYMAS1, defaultValue="")
	private String day2;
	@XmlElement(name=ServiceConstant.DTO_IMPORTEHOYMAS2FECHA, defaultValue="")
	private String day3Date;
	@XmlElement(name=ServiceConstant.DTO_IMPORTEHOYMAS2, defaultValue="")
	private String day3;
	@XmlElement(name=ServiceConstant.DTO_IMPORTEHOYMAS3FECHA, defaultValue="")
	private String day4Date;
	@XmlElement(name=ServiceConstant.DTO_IMPORTEHOYMAS3, defaultValue="")
	private String day4;
	@XmlElement(name=ServiceConstant.DTO_IMPORTEHOYMAS4FECHA, defaultValue="")
	private String day5Date;
	@XmlElement(name=ServiceConstant.DTO_IMPORTEHOYMAS4, defaultValue="")
	private String day5;
	@XmlElement(name=ServiceConstant.DTO_IMPORTEMAY5, defaultValue="")
	private String nextAmount;
	
	@XmlTransient
	private String day1ExporterTitle;
	@XmlTransient
	private String day2ExporterTitle;
	@XmlTransient
	private String day3ExporterTitle;
	@XmlTransient
	private String day4ExporterTitle;
	@XmlTransient
	private String day5ExporterTitle;
	
	@FieldToExport(name=ServiceConstant.DTO_IMPORTEHOY, hasDynamicTitle=true, required=false)
	@XmlTransient
	private String day1Exporter;
	@FieldToExport(name=ServiceConstant.DTO_IMPORTEHOYMAS1, hasDynamicTitle=true, required=false)
	@XmlTransient
	private String day2Exporter;
	@FieldToExport(name=ServiceConstant.DTO_IMPORTEHOYMAS2, hasDynamicTitle=true, required=false)
	@XmlTransient
	private String day3Exporter;
	@FieldToExport(name=ServiceConstant.DTO_IMPORTEHOYMAS3, hasDynamicTitle=true, required=false)
	@XmlTransient
	private String day4Exporter;
	@FieldToExport(name=ServiceConstant.DTO_IMPORTEHOYMAS4, hasDynamicTitle=true, required=false)
	@XmlTransient
	private String day5Exporter;
	@FieldToExport(name=ServiceConstant.DTO_IMPORTEMAY5)
	@XmlTransient
	private String nextAmountExporter;
	@FieldToExport(name=ServiceConstant.DTO_IMPORTESANTERIORES)
	@XmlTransient
	private String oldAmountExporter;
	
	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}
	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = sanitateString(paymentType);
	}
	/**
	 * @return the oldAmount
	 */
	public String getOldAmount() {
		return oldAmount;
	}
	/**
	 * @param oldAmount the oldAmount to set
	 */
	public void setOldAmount(String oldAmount) {
		this.oldAmount = oldAmount;
	}
	/**
	 * @return the day1
	 */
	public String getDay1() {
		return day1;
	}
	/**
	 * @param day1 the day1 to set
	 */
	public void setDay1(String day1) {
		this.day1 = day1;
	}
	/**
	 * @return the day2
	 */
	public String getDay2() {
		return day2;
	}
	/**
	 * @param day2 the day2 to set
	 */
	public void setDay2(String day2) {
		this.day2 = day2;
	}
	/**
	 * @return the day3
	 */
	public String getDay3() {
		return day3;
	}
	/**
	 * @param day3 the day3 to set
	 */
	public void setDay3(String day3) {
		this.day3 = day3;
	}
	/**
	 * @return the day4
	 */
	public String getDay4() {
		return day4;
	}
	/**
	 * @param day4 the day4 to set
	 */
	public void setDay4(String day4) {
		this.day4 = day4;
	}
	/**
	 * @return the day5
	 */
	public String getDay5() {
		return day5;
	}
	/**
	 * @param day5 the day5 to set
	 */
	public void setDay5(String day5) {
		this.day5 = day5;
	}
	/**
	 * @return the nextAmount
	 */
	public String getNextAmount() {
		return nextAmount;
	}
	/**
	 * @param nextAmount the nextAmount to set
	 */
	
	public void setNextAmount(String nextAmount) {
		this.nextAmount = nextAmount;
	}
	public String getDay1Date() {
		return day1Date;
	}
	public void setDay1Date(String day1Date) {
		this.day1Date = day1Date;
	}
	public String getDay2Date() {
		return day2Date;
	}
	public void setDay2Date(String day2Date) {
		this.day2Date = day2Date;
	}
	public String getDay3Date() {
		return day3Date;
	}
	public void setDay3Date(String day3Date) {
		this.day3Date = day3Date;
	}
	public String getDay4Date() {
		return day4Date;
	}
	public void setDay4Date(String day4Date) {
		this.day4Date = day4Date;
	}
	public String getDay5Date() {
		return day5Date;
	}
	public void setDay5Date(String day5Date) {
		this.day5Date = day5Date;
	}
	public String getDay1ExporterTitle() {
		return day1ExporterTitle;
	}
	public void setDay1ExporterTitle(String day1ExporterTitle) {
		this.day1ExporterTitle = day1ExporterTitle;
	}
	public String getDay2ExporterTitle() {
		return day2ExporterTitle;
	}
	public void setDay2ExporterTitle(String day2ExporterTitle) {
		this.day2ExporterTitle = day2ExporterTitle;
	}
	public String getDay3ExporterTitle() {
		return day3ExporterTitle;
	}
	public void setDay3ExporterTitle(String day3ExporterTitle) {
		this.day3ExporterTitle = day3ExporterTitle;
	}
	public String getDay4ExporterTitle() {
		return day4ExporterTitle;
	}
	public void setDay4ExporterTitle(String day4ExporterTitle) {
		this.day4ExporterTitle = day4ExporterTitle;
	}
	public String getDay5ExporterTitle() {
		return day5ExporterTitle;
	}
	public void setDay5ExporterTitle(String day5ExporterTitle) {
		this.day5ExporterTitle = day5ExporterTitle;
	}
	public String getDay1Exporter() {
		return day1Exporter;
	}
	public void setDay1Exporter(String day1Exporter) {
		this.day1Exporter = sanitateString(day1Exporter);
	}
	public String getDay2Exporter() {
		return day2Exporter;
	}
	public void setDay2Exporter(String day2Exporter) {
		this.day2Exporter = sanitateString(day2Exporter);
	}
	public String getDay3Exporter() {
		return day3Exporter;
	}
	public void setDay3Exporter(String day3Exporter) {
		this.day3Exporter = sanitateString(day3Exporter);
	}
	public String getDay4Exporter() {
		return day4Exporter;
	}
	public void setDay4Exporter(String day4Exporter) {
		this.day4Exporter = sanitateString(day4Exporter);
	}
	public String getDay5Exporter() {
		return day5Exporter;
	}
	public void setDay5Exporter(String day5Exporter) {
		this.day5Exporter = sanitateString(day5Exporter);
	}
	public String getNextAmountExporter() {
		return nextAmountExporter;
	}
	public void setNextAmountExporter(String nextAmountExporter) {
		this.nextAmountExporter = sanitateString(nextAmountExporter);
	}
	public String getOldAmountExporter() {
		return oldAmountExporter;
	}
	public void setOldAmountExporter(String oldAmountExporter) {
		this.oldAmountExporter = sanitateString(oldAmountExporter);
	}		
}