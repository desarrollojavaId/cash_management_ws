package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.CheckStatusCollectingAccountDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.CheckStatusCollectingAccountMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

public class CheckStatusCollectingAccountBO extends AbstractBO<CheckStatusCollectingAccountDTO, CheckStatusCollectingAccountMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = CheckStatusCollectingAccountMapper.class;
	}
	
	
	public List<CheckStatusCollectingAccountDTO> findAccount(CheckStatusCollectingAccountDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			//Load paging data
			super.loadPagingData(dto);
			
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			CheckStatusCollectingAccountMapper mapper = session.getMapper(CheckStatusCollectingAccountMapper.class);
			List<CheckStatusCollectingAccountDTO> result = mapper.findAccount(dto);
			queryCounter++;
			
			
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	public Integer findOperateChannel(CheckStatusCollectingAccountDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			//Load paging data
			super.loadPagingData(dto);
			
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			CheckStatusCollectingAccountMapper mapper = session.getMapper(CheckStatusCollectingAccountMapper.class);
			Integer result = mapper.findOperateChannel(dto);
			queryCounter++;
			
			
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
}