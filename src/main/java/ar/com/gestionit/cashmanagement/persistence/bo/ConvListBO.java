package ar.com.gestionit.cashmanagement.persistence.bo;



import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.ConvListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.ConvListMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

public class ConvListBO extends AbstractBO<IDTO, ConvListMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = ConvListMapper.class;
	}
	
	public ConvListItemDTO findStatus(ConvListItemDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			ConvListMapper mapper = session.getMapper(mapperInterface);
			ConvListItemDTO result = mapper.findList(dto);
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}