package ar.com.gestionit.cashmanagement.persistence.bo;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredCheckCrudDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.RecoveredCheckCRUDMapper;
import ar.com.gestionit.cashmanagement.service.RecoveredCheckCRUDService;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.StringUtil;

public class RecoveredCheckCRUDBO extends AbstractBO<RecoveredCheckCrudDTO, RecoveredCheckCRUDMapper>{
	
	private static final String CHECK_EXISTS = "1";
	
	@Override
	protected void specifyMapper() {
		mapperInterface = RecoveredCheckCRUDMapper.class;
	}
	
	/**
	 * This method adds a check to recover
	 * @param dto
	 * @throws ServiceException
	 */
	public int create(RecoveredCheckCrudDTO dto) throws ServiceException {
		if(dto == null)
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NULLDTO); 
		
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(false);
			RecoveredCheckCRUDMapper mapper = session.getMapper(RecoveredCheckCRUDMapper.class);
			
			//Validate if the check to register already exists
			String result = mapper.findCheck(dto.getCheckId());
			queryCounter++;
			if(!StringUtil.isEmpty(result) && result.trim().equals(CHECK_EXISTS)) {
				session.rollback();
				return RecoveredCheckCRUDService.ERROR_ADDED_CHECK; 
			}
			
			mapper.insert(dto);
			queryCounter++;
			
			mapper.insertEventByCreated(dto);
			queryCounter++;
			
			session.commit();
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
 			if(session != null)
				session.close();
		}
		return RecoveredCheckCRUDService.RESULT_OK;
	}
	
	/**
	 * This method deletes from the data source a check to recover
	 * @param dto
	 * @throws ServiceException
	 */
	public int delete(RecoveredCheckCrudDTO dto) throws ServiceException {
		if(dto == null)
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NULLDTO); 
		
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(false);
			RecoveredCheckCRUDMapper mapper = session.getMapper(RecoveredCheckCRUDMapper.class);
			
			mapper.delete(dto);
			queryCounter++;
			
			mapper.insertEventByDeleted(dto);
			queryCounter++;
			
			session.commit();
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
		return RecoveredCheckCRUDService.RESULT_OK;
	}
}