package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.CodeMapped;
import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"nroChq",
		"codEstado",
		"nomBenef",
		"docBenef",
		"importe",
		"refCliente",
		"nroOrdenPago",
		"subNroOrdenPago",
		"modulo",
		"sucursal",
		"moneda",
		"cuenta",
		"suboperacion",
		"cbu",
		"serie"
})
@XmlRootElement(name = ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM)
public class CheckStatusListItemDTO extends AbstractDTO implements IAccount{

	private static final long serialVersionUID = -4991324892118658096L;
	
	//Output
	@FieldToExport(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_NROCHQ)
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_NROCHQ, defaultValue="")
	private String nroChq; //MTDNCH
	@CodeMapped
	@FieldToExport(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_CODEST)
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_CODEST, defaultValue="")
	private String codEstado; //MTKDES
	@FieldToExport(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_NOMBEN)
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_NOMBEN, defaultValue="")
	private String nomBenef; //MTHNOM o MTPNOM
	@FieldToExport(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_DOCBEN)
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_DOCBEN, defaultValue="")
	private String docBenef; //MTLCUI
	@FieldToExport(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_IMPORTE)
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_IMPORTE, defaultValue="")
	private String importe; //MTLJPA
	@FieldToExport(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_REFCLIENTE)
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_REFCLIENTE, defaultValue="")
	private String refCliente; //MTLQRC
//	@FieldToExport(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_NROCTACASH)
//	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_NROCTACASH, defaultValue="")
//	private String nroCtaCash; //MTDQCT
		
//	@XmlTransient
//	private String opAsigEmpresa;//MTLQRC
	@FieldToExport(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_OPSEGUNBANCO)
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_OPSEGUNBANCO, defaultValue="")
	private String nroOrdenPago;// MTLQOP
	@FieldToExport(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_SOPSEGUNBANCO)
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_SOPSEGUNBANCO, defaultValue="")
	private String subNroOrdenPago;//MTLQSO
	@FieldToExport(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_SERIE)
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_SERIE, defaultValue="")
	private String serie;//MTDSER
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_NROCTACASH, defaultValue="")
	private String cuenta;
	//@FieldToExport(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_SUCURSAL)
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_SUCURSAL, defaultValue="")
	private String sucursal;
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_MONEDA, defaultValue="")
	private String moneda;
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_MODULO, defaultValue="")
	private String modulo;
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_SUBOPERACION, defaultValue="")
	private String suboperacion;
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_CBU, defaultValue="")
	private String cbu;
	@XmlTransient
	private String adherent;
	@FieldToExport(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM_NROCTACASH)
	@XmlTransient
	private String exportCuenta;
	//opAsigEmpresa --  MTLQRC
	//nroOrdenPago -- MTLQOP
	//subNroOrdenPago -- MTLQSO
	//
	
	public String getNroChq() {
		return nroChq;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = sanitateString(serie);
	}
	public void setNroChq(String nroChq) {
		this.nroChq = sanitateString(nroChq);
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = sanitateString(codEstado);
	}
	public String getNomBenef() {
		return nomBenef;
	}
	public void setNomBenef(String nomBenef) {
		this.nomBenef = sanitateString(nomBenef);
	}
	public String getDocBenef() {
		return docBenef;
	}
	public void setDocBenef(String docBenef) {
		this.docBenef = sanitateString(docBenef);
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = sanitateString(importe);
	}
	public String getRefCliente() {
		return refCliente;
	}
	public void setRefCliente(String refCliente) {
		this.refCliente = sanitateString(refCliente);
	}
//	public String getNroCtaCash() {
//		return nroCtaCash;
//	}
//	public void setNroCtaCash(String nroCtaCash) {
//		this.nroCtaCash = sanitateString(nroCtaCash);
//	}
//	public String getOpAsigEmpresa() {
//		return opAsigEmpresa;
//	}
//	public void setOpAsigEmpresa(String opAsigEmpresa) {
//		this.opAsigEmpresa = sanitateString(opAsigEmpresa);
//	}
	public String getNroOrdenPago() {
		return nroOrdenPago;
	}
	public void setNroOrdenPago(String nroOrdenPago) {
		this.nroOrdenPago = sanitateString(nroOrdenPago);
	}
	public String getSubNroOrdenPago() {
		return subNroOrdenPago;
	}
	public void setSubNroOrdenPago(String subNroOrdenPago) {
		this.subNroOrdenPago = sanitateString(subNroOrdenPago);
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = sanitateString(cuenta);
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sanitateString(sucursal);
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = sanitateString(moneda);
	}
	public String getModulo() {
		return modulo;
	}
	public void setModulo(String modulo) {
		this.modulo = sanitateString(modulo);
	}
	public String getSuboperacion() {
		return suboperacion;
	}
	public void setSuboperacion(String suboperacion) {
		this.suboperacion = sanitateString(suboperacion);
	}
	public String getCbu() {
		return cbu;
	}
	public void setCbu(String cbu) {
		this.cbu = sanitateString(cbu);
	}
	public String getAdherent() {
		return adherent;
	}
	public void setAdherent(String adherent) {
		this.adherent = sanitateString(adherent);
	}
	public String getExportCuenta() {
		return exportCuenta;
	}
	public void setExportCuenta(String exportCuenta) {
		this.exportCuenta = exportCuenta;
	}	
}