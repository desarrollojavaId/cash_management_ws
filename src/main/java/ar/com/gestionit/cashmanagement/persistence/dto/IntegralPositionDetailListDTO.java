package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name=ServiceConstant.DTO_INTEGRALPOSITION_ROOT)
public class IntegralPositionDetailListDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7915229072686145692L;
	
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION,  defaultValue="")
	private List <IntegralPositionDetailListItemDTO> list;

	public List<IntegralPositionDetailListItemDTO> getList() {
		return list;
	}

	public void setList(List<IntegralPositionDetailListItemDTO> list) {
		this.list = list;
	}
	
	

}