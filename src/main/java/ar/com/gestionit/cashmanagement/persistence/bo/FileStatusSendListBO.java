package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.FileContentDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileStatusSendListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileStatusSendListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.FileContentMapper;
import ar.com.gestionit.cashmanagement.persistence.mapper.FileStatusSendListMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

public class FileStatusSendListBO extends AbstractBO<FileStatusSendListItemDTO, FileStatusSendListMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = FileStatusSendListMapper.class;
	}
	
	
	/**
	 * This method returns a sucursal according to its ID
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public List<FileStatusSendListItemDTO> find(FileStatusSendListDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			//Load paging data
			super.loadPagingData(dto);
			
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			FileStatusSendListMapper mapper = session.getMapper(FileStatusSendListMapper.class);
			List<FileStatusSendListItemDTO> result = mapper.find(dto);
			queryCounter++;
			
			//Process paging
			processResultPaging(result);
			
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	public List<String> findFileContent(FileContentDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			FileStatusSendListMapper mapper = session.getMapper(FileStatusSendListMapper.class);
			List<String> result = mapper.findFileContent(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_FILENOTFOUNDINLIBRARY, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	/**
	 * This method returns a exist adherent 
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public Integer findAdherent(FileStatusSendListDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			//Load paging data
			super.loadPagingData(dto);
			
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			FileStatusSendListMapper mapper = session.getMapper(FileStatusSendListMapper.class);
			Integer result = mapper.findAdherent(dto);
			queryCounter++;
			
			//Process paging
			//processResultPaging(result);
			
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	/**
	 * This method returns a sucursal according to its ID
	 * @param dto
	 * @return
	 * @throws ServiceException
	 *//*
	public String findChkUnPrinted(FileStatusSendListDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			FileStatusSendListMapper mapper = session.getMapper(FileStatusSendListMapper.class);
			String result = mapper.findChkUnPrinted(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	public String findTotChk(FileStatusSendListDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			FileStatusSendListMapper mapper = session.getMapper(FileStatusSendListMapper.class);
			String result = mapper.findTotChk(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}*/
}