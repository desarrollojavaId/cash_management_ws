package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"fileTypeId",
		"fileTypeName",
		"member"})
@XmlRootElement(name = ServiceConstant.DTO_ESCROWS_FILE)
public class EscrowFileTypeItemDTO extends AbstractDTO{
		
	private static final long serialVersionUID = -2188135454889891925L;
	
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_MIEMBRO, defaultValue="")
	private String member;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_FILE_ID, defaultValue="")
	private String fileTypeId;
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_FILE_NAME, defaultValue="")
	private String fileTypeName;

	public String getMember() {
		return member;
	}

	public void setMember(String member) {
		this.member = member;
	}

	public String getFileTypeId() {
		return fileTypeId;
	}

	public void setFileTypeId(String fileTypeId) {
		if(fileTypeId != null)
			this.fileTypeId = fileTypeId.trim();
		else
			this.fileTypeId = fileTypeId;
	}

	public String getFileTypeName() {
		return fileTypeName;
	}

	public void setFileTypeName(String fileTypeName) {
		if(fileTypeName != null)
			this.fileTypeName = fileTypeName.trim();
		else
			this.fileTypeName = fileTypeName;
	}
}