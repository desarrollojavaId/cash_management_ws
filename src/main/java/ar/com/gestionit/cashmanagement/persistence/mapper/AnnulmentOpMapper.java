package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.AnnulmentOpDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

public interface AnnulmentOpMapper extends IMapper<IDTO> {
	public int updateEcheq(AnnulmentOpDTO input);
	public int updateOp(AnnulmentOpDTO input);
	
	
}