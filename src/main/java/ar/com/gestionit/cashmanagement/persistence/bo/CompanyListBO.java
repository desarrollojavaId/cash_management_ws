package ar.com.gestionit.cashmanagement.persistence.bo;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.CompanyListMapper;


public class CompanyListBO extends AbstractBO<IDTO, CompanyListMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = CompanyListMapper.class;
	}
}