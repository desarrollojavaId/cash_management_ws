package ar.com.gestionit.cashmanagement.persistence.bo;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.EscrowFileStatusMapper;

public class EscrowFileStatusBO extends AbstractBO<IDTO, EscrowFileStatusMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = EscrowFileStatusMapper.class;
	}
}