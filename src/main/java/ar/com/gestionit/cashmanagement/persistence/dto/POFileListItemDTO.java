package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"clientReference",
		"beneficiaryNumber",
		"beneficiaryName",
		"beneficiaryDocument",
		"amount",
		"executionModeDescription",
		"paymentDate", 
		"checkExipirationDate",
		"paymentCause",
		"cbu"		
})
@XmlRootElement(name = ServiceConstant.DTO_POFILE)
public class POFileListItemDTO extends AbstractDTO{

	private static final long serialVersionUID = -4991324892118658096L;
	
	//Output
	@FieldToExport(name=ServiceConstant.DTO_POFILE_CLIENTREFERENCE)
	@XmlElement(name=ServiceConstant.DTO_POFILE_CLIENTREFERENCE, defaultValue="")
	private String clientReference;
	@FieldToExport(name=ServiceConstant.DTO_POFILE_BENEFICIARYNAME)
	@XmlElement(name=ServiceConstant.DTO_POFILE_BENEFICIARYNAME, defaultValue="")
	private String beneficiaryName;
	@FieldToExport(name=ServiceConstant.DTO_POFILE_BENEFICIARYDOCUMENT)
	@XmlElement(name=ServiceConstant.DTO_POFILE_BENEFICIARYDOCUMENT, defaultValue="")
	private String beneficiaryDocument;
	@FieldToExport(name=ServiceConstant.DTO_POFILE_AMOUNT)
	@XmlElement(name=ServiceConstant.DTO_POFILE_AMOUNT, defaultValue="")
	private String amount;
	@FieldToExport(name=ServiceConstant.DTO_POFILE_PAYMENTDATE)
	@XmlElement(name=ServiceConstant.DTO_POFILE_PAYMENTDATE, defaultValue="")
	private String paymentDate; 
	@FieldToExport(name=ServiceConstant.DTO_POFILE_EXEMODEDESC)
	@XmlElement(name=ServiceConstant.DTO_POFILE_EXEMODEDESC, defaultValue="")
	private String executionModeDescription;
	@XmlElement(name=ServiceConstant.DTO_POFILE_CHECKEXPDATE, defaultValue="")
	private String checkExipirationDate;
	@XmlElement(name=ServiceConstant.DTO_POFILE_PAYMENTCAUSE, defaultValue="")
	private String paymentCause;
	@XmlElement(name=ServiceConstant.DTO_POFILE_CBU, defaultValue="")
	private String cbu;
	@FieldToExport(name=ServiceConstant.DTO_POFILE_BENEFICIARYNUMBER)
	@XmlElement(name=ServiceConstant.DTO_POFILE_BENEFICIARYNUMBER, defaultValue="")
	private String beneficiaryNumber;
	/**
	 * @return the beneficiaryName
	 */
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	/**
	 * @param beneficiaryName the beneficiaryName to set
	 */
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = sanitateString(beneficiaryName);
	}
	/**
	 * @return the beneficiaryNumber
	 */
	public String getBeneficiaryNumber() {
		return beneficiaryNumber;
	}
	/**
	 * @param beneficiaryNumber the beneficiaryNumber to set
	 */
	public void setBeneficiaryNumber(String beneficiaryNumber) {
		this.beneficiaryNumber = sanitateString(beneficiaryNumber);
	}
	/**
	 * @return the clientReference
	 */
	public String getClientReference() {
		return clientReference;
	}
	/**
	 * @param clientReference the clientReference to set
	 */
	public void setClientReference(String clientReference) {
		this.clientReference = sanitateString(clientReference);
	}
	/**
	 * @return the beneficiaryDocument
	 */
	public String getBeneficiaryDocument() {
		return beneficiaryDocument;
	}
	/**
	 * @param beneficiaryDocument the beneficiaryDocument to set
	 */
	public void setBeneficiaryDocument(String beneficiaryDocument) {
		this.beneficiaryDocument = sanitateString(beneficiaryDocument);
	}
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = sanitateString(amount);
	}
	/**
	 * @return the executionModeDescription
	 */
	public String getExecutionModeDescription() {
		return executionModeDescription;
	}
	/**
	 * @param executionModeDescription the executionModeDescription to set
	 */
	public void setExecutionModeDescription(String executionModeDescription) {
		this.executionModeDescription = sanitateString(executionModeDescription);
	}
	/**
	 * @return the paymentDate
	 */
	public String getPaymentDate() {
		return paymentDate;
	}
	/**
	 * @param paymentDate the paymentDate to set
	 */
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = sanitateString(paymentDate);
	}
	/**
	 * @return the checkExipirationDate
	 */
	public String getCheckExipirationDate() {
		return checkExipirationDate;
	}
	/**
	 * @param checkExipirationDate the checkExipirationDate to set
	 */
	public void setCheckExipirationDate(String checkExipirationDate) {
		this.checkExipirationDate = sanitateString(checkExipirationDate);
	}
	/**
	 * @return the paymentCause
	 */
	public String getPaymentCause() {
		return paymentCause;
	}
	/**
	 * @param paymentCause the paymentCause to set
	 */
	public void setPaymentCause(String paymentCause) {
		this.paymentCause = sanitateString(paymentCause);
	}
	/**
	 * @return the cbu
	 */
	public String getCbu() {
		return cbu;
	}
	/**
	 * @param cbu the cbu to set
	 */
	public void setCbu(String cbu) {
		this.cbu = sanitateString(cbu);
	}
}