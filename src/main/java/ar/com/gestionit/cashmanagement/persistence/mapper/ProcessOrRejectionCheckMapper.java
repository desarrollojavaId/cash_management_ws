package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.ProcessOrRejectionCheckDTO;

public interface ProcessOrRejectionCheckMapper extends IMapper<ProcessOrRejectionCheckDTO> {
	
	//Actualizo MRDRECPD en estado 2 (procesado)
	public Integer updateProMrdrecpd(ProcessOrRejectionCheckDTO dto);
	
	//Actualizo MRDRECPD en estado 5 (rechazado)
	public Integer updateRecMrdrecpd(ProcessOrRejectionCheckDTO dto);
	
	//Actualizo MRLRECPD (Fecha Actual)
	public Integer updateMrlrecpd(ProcessOrRejectionCheckDTO dto);
	
	//Actualizo el sstranse la fecha de rechazo
	public Integer updateSstrans(ProcessOrRejectionCheckDTO dto);
	
}