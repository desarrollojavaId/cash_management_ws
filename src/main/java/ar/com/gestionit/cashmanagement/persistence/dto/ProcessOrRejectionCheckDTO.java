package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.Date;

public class ProcessOrRejectionCheckDTO extends AbstractDTO {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 5665153105942209910L;

	//input
	private String nroAdh;
	private String idLote;
	private String accion;
	private Date fechaActual;
	private String nroConv;
	private String nroSubConv;
	private String date;
	private String hour;
	private String user;
	
	
	public String getNroAdh() {
		return nroAdh;
	}
	public void setNroAdh(String nroAdh) {
		this.nroAdh = nroAdh;
	}
	public String getIdLote() {
		return idLote;
	}
	public void setIdLote(String idLote) {
		this.idLote = idLote;
	}
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}
	public Date getFechaActual() {
		return fechaActual;
	}
	public void setFechaActual(Date fechaActual) {
		this.fechaActual = fechaActual;
	}
	
	public String getNroConv() {
		return nroConv;
	}
	public void setNroConv(String nroConv) {
		this.nroConv = nroConv;
	}
	public String getNroSubConv() {
		return nroSubConv;
	}
	public void setNroSubConv(String nroSubConv) {
		this.nroSubConv = nroSubConv;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getHour() {
		return hour;
	}
	public void setHour(String hour) {
		this.hour = hour;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	
	
	
	
}