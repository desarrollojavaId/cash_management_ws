package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name = ServiceConstant.DTO_RECEIVEDCHECK_ROOT)
public class ReceivedChecksListDTO extends AbstractDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7971901187181882192L;

		//Outputs
		@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK, defaultValue="")
		private List<ReceivedChecksListItemDTO> list;
		
		public List<ReceivedChecksListItemDTO> getList() {
			return list;
		}

		public void setList(List<ReceivedChecksListItemDTO> list) {
			this.list = list;
		}
		
		
}
