package ar.com.gestionit.cashmanagement.persistence.dto;

public class AnnulmentOpDTO extends AbstractDTO {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -7798867926488859811L;

	//Inputs
	private String paymentNumber;
	private String paymentSubnumber;
	private String adherent;
	private String user;
	private String time;

	//Outputs
	

	/**
	 * @return the paymentNumber
	 */
	public String getPaymentNumber() {
		return paymentNumber;
	}

	/**
	 * @param paymentNumber the paymentNumber to set
	 */
	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = sanitateString(paymentNumber);
	}
	
	/**
	 * @return the paymentSubnumber
	 */
	public String getPaymentSubnumber() {
		return paymentSubnumber;
	}
	
	/**
	 * @param paymentSubnumber the paymentSubnumber to set
	 */
	public void setPaymentSubnumber(String paymentSubnumber) {
		this.paymentSubnumber = sanitateString(paymentSubnumber);
	}

	public String getAdherent() {
		return adherent;
	}

	public void setAdherent(String adherent) {
		this.adherent = adherent;
	}

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
	
}