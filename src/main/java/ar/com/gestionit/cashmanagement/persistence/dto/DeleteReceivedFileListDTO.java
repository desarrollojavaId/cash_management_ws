package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import java.util.List;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"idArchivo"
})
@XmlRootElement(name = ServiceConstant.DTO_FILE_ROOT)
public class DeleteReceivedFileListDTO extends AbstractDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2015606268947547783L;
	
	@XmlElement(name=ServiceConstant.TAG_ID)
	private List<String> idArchivo;

	public List<String> getIdArchivo() {
		return idArchivo;
	}

	public void setIdArchivo(List<String> idArchivo) {
		this.idArchivo = idArchivo;
	}
}