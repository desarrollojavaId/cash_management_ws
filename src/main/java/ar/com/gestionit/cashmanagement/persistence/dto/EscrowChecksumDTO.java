package ar.com.gestionit.cashmanagement.persistence.dto;

public class EscrowChecksumDTO extends AbstractDTO{
	
	private static final long serialVersionUID = -1215271026638111504L;
	
	private String pgcod;
	private String account;
	private String serie;
	private String group;
	private String altSum;
	private String cuoSum;
	private String pagSum;
	private String cliSum;
	private String segSum;
	private String altSumDate;
	private String cuoSumDate;
	private String pagSumDate;
	private String cliSumDate;
	private String segSumDate;
	
	/**
	 * @return the pgcod
	 */
	public String getPgcod() {
		return pgcod;
	}
	/**
	 * @param pgcod the pgcod to set
	 */
	public void setPgcod(String pgcod) {
		this.pgcod = pgcod;
	}
	/**
	 * @return the account
	 */
	public String getAccount() {
		return account;
	}
	/**
	 * @param account the account to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}
	/**
	 * @return the serie
	 */
	public String getSerie() {
		return serie;
	}
	/**
	 * @param serie the serie to set
	 */
	public void setSerie(String serie) {
		this.serie = serie;
	}
	/**
	 * @return the group
	 */
	public String getGroup() {
		return group;
	}
	/**
	 * @param group the group to set
	 */
	public void setGroup(String group) {
		this.group = group;
	}
	/**
	 * @return the altSum
	 */
	public String getAltSum() {
		return altSum;
	}
	/**
	 * @param altSum the altSum to set
	 */
	public void setAltSum(String altSum) {
		this.altSum = altSum;
	}
	/**
	 * @return the cuoSum
	 */
	public String getCuoSum() {
		return cuoSum;
	}
	/**
	 * @param cuoSum the cuoSum to set
	 */
	public void setCuoSum(String cuoSum) {
		this.cuoSum = cuoSum;
	}
	/**
	 * @return the pagSum
	 */
	public String getPagSum() {
		return pagSum;
	}
	/**
	 * @param pagSum the pagSum to set
	 */
	public void setPagSum(String pagSum) {
		this.pagSum = pagSum;
	}
	/**
	 * @return the cliSum
	 */
	public String getCliSum() {
		return cliSum;
	}
	/**
	 * @param cliSum the cliSum to set
	 */
	public void setCliSum(String cliSum) {
		this.cliSum = cliSum;
	}
	/**
	 * @return the segSum
	 */
	public String getSegSum() {
		return segSum;
	}
	/**
	 * @param segSum the segSum to set
	 */
	public void setSegSum(String segSum) {
		this.segSum = segSum;
	}
	/**
	 * @return the altSumDate
	 */
	public String getAltSumDate() {
		return altSumDate;
	}
	/**
	 * @param altSumDate the altSumDate to set
	 */
	public void setAltSumDate(String altSumDate) {
		this.altSumDate = altSumDate;
	}
	/**
	 * @return the cuoSumDate
	 */
	public String getCuoSumDate() {
		return cuoSumDate;
	}
	/**
	 * @param cuoSumDate the cuoSumDate to set
	 */
	public void setCuoSumDate(String cuoSumDate) {
		this.cuoSumDate = cuoSumDate;
	}
	/**
	 * @return the pagSumDate
	 */
	public String getPagSumDate() {
		return pagSumDate;
	}
	/**
	 * @param pagSumDate the pagSumDate to set
	 */
	public void setPagSumDate(String pagSumDate) {
		this.pagSumDate = pagSumDate;
	}
	/**
	 * @return the cliSumDate
	 */
	public String getCliSumDate() {
		return cliSumDate;
	}
	/**
	 * @param cliSumDate the cliSumDate to set
	 */
	public void setCliSumDate(String cliSumDate) {
		this.cliSumDate = cliSumDate;
	}
	/**
	 * @return the segSumDate
	 */
	public String getSegSumDate() {
		return segSumDate;
	}
	/**
	 * @param segSumDate the segSumDate to set
	 */
	public void setSegSumDate(String segSumDate) {
		this.segSumDate = segSumDate;
	}
}