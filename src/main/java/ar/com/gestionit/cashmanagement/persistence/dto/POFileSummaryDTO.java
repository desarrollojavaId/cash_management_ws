package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"registerNumber",
	"totalAmount"		
})
@XmlRootElement(name = ServiceConstant.DTO_POFILESUMMARY)
public class POFileSummaryDTO extends AbstractDTO{

	private static final long serialVersionUID = -4991324892118658096L;
	
	//Output
	@FieldToExport(name=ServiceConstant.DTO_POFILESUMMARY_AMOUNT)
	@XmlElement(name=ServiceConstant.DTO_POFILESUMMARY_AMOUNT, defaultValue="")
	private String totalAmount;
	@FieldToExport(name=ServiceConstant.DTO_POFILESUMMARY_REGNUMBER)
	@XmlElement(name=ServiceConstant.DTO_POFILESUMMARY_REGNUMBER, defaultValue="")
	private String registerNumber;
	/**
	 * @return the totalAmount
	 */
	public String getTotalAmount() {
		return totalAmount;
	}
	/**
	 * @param totalAmount the totalAmount to set
	 */
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	/**
	 * @return the registerNumber
	 */
	public String getRegisterNumber() {
		return registerNumber;
	}
	/**
	 * @param registerNumber the registerNumber to set
	 */
	public void setRegisterNumber(String registerNumber) {
		this.registerNumber = registerNumber;
	}
}