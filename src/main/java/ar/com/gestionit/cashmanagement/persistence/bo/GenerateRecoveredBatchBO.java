package ar.com.gestionit.cashmanagement.persistence.bo;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredBatchDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.GenerateRecoveredBatchMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;

public class GenerateRecoveredBatchBO extends AbstractBO<RecoveredBatchDTO, GenerateRecoveredBatchMapper> {

	@Override
	protected void specifyMapper() {
		mapperInterface = GenerateRecoveredBatchMapper.class;
	}

	public void executeTransaction(RecoveredBatchDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			GeneralRecoveredBO generalBo = new GeneralRecoveredBO();

			session = CashManagementWsApplication.sqlSessionFactory.openSession(false);
			GenerateRecoveredBatchMapper mapper = session.getMapper(GenerateRecoveredBatchMapper.class);

			//Actualizo la tabla de lote de recupero con la fecha de generacion
			mapper.update(dto);

			//Borro los cheques que ya no sirven para recupero
				int checksForRecovered = generalBo.deleteNonRecuperableChecks(dto.getRequestNumber(),
					dto.getAgreementNumber(),
					dto.getAgreementSubnumber(),
					session,
					false);
			//STNARC
			
				//Valido si quedan cheques para recuerpo
			if (checksForRecovered <= 0) {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOCHECKSFORRECOVERED);
			}

			//Inserto el evento 5 – 4 Pedido anulado automáticamente desde Web
			generalBo.addEvent(dto.getRequestNumber(), "5", "4", session, false);

			// Actualizar el detalle del pedido con la sucursal de envio
			mapper.updateBatchDetail(dto);

			// Inserto el evento 1 – 3 Pedido generado
			generalBo.addEvent(dto.getRequestNumber(), "1", "3", session, false);
			// Inserto el evento 2 – 1 Pedido Confirmado desde Web
			generalBo.addEvent(dto.getRequestNumber(), "2", "1", session, false);

			session.commit();
			
			/*
			 * Esta linea es para contabilizar la cantidad de queries que se corrieron en total
			 * ya que el servicio despues lo solicitara
			 */
			queryCounter += generalBo.getQueryCounter();
			
			
		} catch (ServiceException e) {
			if(session != null)
				session.rollback();
			throw e;
		} catch (Exception e) {
			if(session != null)
				session.rollback();
			DefinedLogger.SERVICE.error(e.getMessage(), e);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT);
		} finally {
			if(session != null)
				session.close();
		}
	}
}