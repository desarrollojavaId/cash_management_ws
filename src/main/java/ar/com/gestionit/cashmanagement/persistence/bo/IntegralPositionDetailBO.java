package ar.com.gestionit.cashmanagement.persistence.bo;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionDetailDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionDetailListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.IntegralPositionDetailMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;


public class IntegralPositionDetailBO extends AbstractBO<IDTO, IntegralPositionDetailMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = IntegralPositionDetailMapper.class;
	}
	
	public List<IntegralPositionDetailListItemDTO> findTotals(IntegralPositionDetailDTO input) throws ServiceException {
		SqlSession session = null;
		try {
			
			//Get result list
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			IntegralPositionDetailMapper mapper = session.getMapper(IntegralPositionDetailMapper.class);
			List <IntegralPositionDetailListItemDTO> result = mapper.findTotals(input);
			queryCounter++;
			session.close();

			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}
