package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlTransient;




public class IntegralPositionDetailDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5058157788174705249L;
	
	@XmlTransient
	private IntegralPositionDetailInputDTO inputList;
	@XmlTransient
	private IntegralPositionDetailReferencesDTO referencesList;
	@XmlTransient
	private IntegralPositionDetailFormasCobroDTO formasCobroList;

	
	
	public IntegralPositionDetailInputDTO getInputList() {
		return inputList;
	}
	public void setInputList(IntegralPositionDetailInputDTO inputList) {
		this.inputList = inputList;
	}
	public IntegralPositionDetailReferencesDTO getReferencesList() {
		return referencesList;
	}
	public void setReferencesList(IntegralPositionDetailReferencesDTO referencesList) {
		this.referencesList = referencesList;
	}
	public IntegralPositionDetailFormasCobroDTO getFormasCobroList() {
		return formasCobroList;
	}
	public void setFormasCobroList(IntegralPositionDetailFormasCobroDTO formasCobroList) {
		this.formasCobroList = formasCobroList;
	}
	
	
	

}