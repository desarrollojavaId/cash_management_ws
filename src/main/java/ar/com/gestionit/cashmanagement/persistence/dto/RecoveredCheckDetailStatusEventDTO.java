package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"fechaEvento",
		"hs",
		"descEvento"})
@XmlRootElement(name = ServiceConstant.TAG_EVENT)
public class RecoveredCheckDetailStatusEventDTO extends AbstractDTO {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -7798867926488859811L;

	//Ouput
	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_FECHA, defaultValue="")
	private String fechaEvento; 
	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_HS, defaultValue="")
	private String hs; 
	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_DESCRIPEVENT, defaultValue="")
	private String descEvento;
	
	
	public String getFechaEvento() {
		return fechaEvento;
	}
	public void setFechaEvento(String fechaEvento) {
		this.fechaEvento = sanitateString(fechaEvento);
	}
	public String getHs() {
		return hs;
	}
	public void setHs(String hs) {
		this.hs = sanitateString(hs);
	}
	public String getDescEvento() {
		return descEvento;
	}
	public void setDescEvento(String descEvento) {
		this.descEvento = sanitateString(descEvento);
	} 
	
	
	
}