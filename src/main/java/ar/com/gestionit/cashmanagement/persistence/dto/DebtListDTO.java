package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name=ServiceConstant.DTO_DEBTLIST_ROOT)
public class DebtListDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5514831796591604623L;
	
	//Input
	@XmlTransient
	private String cuit;
	@XmlTransient 
	private String empresa;
	@XmlTransient
	private String convenio;
	@XmlTransient
	private String subConvenio;	
	@XmlTransient
	private String vencimientoDesde;
	@XmlTransient
	private String vencimientoHasta;
	
	//Output
	@XmlElement(name=ServiceConstant.DTO_DEBTLIST, defaultValue="")
	private List<DebtListItemDTO> list;

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = sanitateString(cuit);
	}
		
	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = sanitateString(empresa);
	}

	public String getVencimientoDesde() {
		return vencimientoDesde;
	}

	public void setVencimientoDesde(String vencimientoDesde) {
		this.vencimientoDesde = sanitateString(vencimientoDesde);
	}

	public String getVencimientoHasta() {
		return vencimientoHasta;
	}

	public void setVencimientoHasta(String vencimientoHasta) {
		this.vencimientoHasta = sanitateString(vencimientoHasta);
	}
	
	public String getConvenio() {
		return convenio;
	}

	public void setConvenio(String convenio) {
		this.convenio = sanitateString(convenio);
	}

	public String getSubConvenio() {
		return subConvenio;
	}

	public void setSubConvenio(String subConvenio) {
		this.subConvenio = sanitateString(subConvenio);
	}

	public List<DebtListItemDTO> getList() {
		return list;
	}

	public void setList(List<DebtListItemDTO> list) {
		this.list = list;
	}
	
	

}
