package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name = ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_CHEQ)
public class RecoveredCheckDetailListDTO extends AbstractDTO {

		/**
	 * 
	 */
	private static final long serialVersionUID = -8372651563727619674L;
	
	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_CHEQ)
	private List<RecoveredCheckDetailListItemDTO> list;

	/**
	 * @return the statuses
	 */
	public List<RecoveredCheckDetailListItemDTO> getList() {
		return list;
	}

	/**
	 * @param statuses the statuses to set
	 */
	public void setList(List<RecoveredCheckDetailListItemDTO> list) {
		this.list = list;
	}
		
}