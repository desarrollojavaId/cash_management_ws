package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.FileListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

public interface DeleteFileListMapper extends IMapper<IDTO> {
	public int updateSATByList(FileListDTO list);
}