package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.factory.ServiceReturnFactory;
import ar.com.gestionit.cashmanagement.persistence.dto.FileContentDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.DeleteFileListMapper;
import ar.com.gestionit.cashmanagement.persistence.mapper.FileContentMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.StringUtil;

public class FileContentBO extends AbstractBO<IDTO, FileContentMapper>{
	
	/**
	 * Delete File List mapper
	 */
	DeleteFileListMapper mapper;
	
	@Override
	protected void specifyMapper() {
		mapperInterface = FileContentMapper.class;
	}
	
	public FileContentDTO findLibrary(FileContentDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			FileContentMapper mapper = session.getMapper(FileContentMapper.class);
			FileContentDTO result = mapper.findLibrary(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	public FileContentDTO findLibraryReception(FileContentDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			FileContentMapper mapper = session.getMapper(FileContentMapper.class);
			FileContentDTO result = mapper.findLibraryReception(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	public FileContentDTO findAuxiliaryTable(FileContentDTO dto) throws ServiceException {
		String result = null;
		
		/*
		 * Access datasource
		 */
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			FileContentMapper mapper = session.getMapper(FileContentMapper.class);
			result = mapper.findAuxiliaryTable(dto.getFileId().substring(0, 2));
			queryCounter++;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
		
		/*
		 * Validate if the auxiliary table was found.
		 * In other case, we must interrupt the execution because we already
		 * tried to get the library previously. So, we throw an exception 
		 */
		if(StringUtil.isEmpty(result)) {
			DefinedLogger.SERVICE.warn(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_NOTFOUNDAUXILIARYTABLE));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFOUNDAUXILIARYTABLE);
		}
		
		/*
		 * Evaluate the result
		 */
		if(result.trim().equalsIgnoreCase("P")){
			dto.setAuxiliaryTable("MTX0001D");
		}else{
			dto.setAuxiliaryTable("MRX0001D");
		}
		
		return dto;
	}
	
	public List<FileContentDTO> findBeneficiaryData(FileContentDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			FileContentMapper mapper = session.getMapper(FileContentMapper.class);
			List<FileContentDTO> result = mapper.findBeneficiaryData(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	public List<String> findFile(FileContentDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			FileContentMapper mapper = session.getMapper(FileContentMapper.class);
			List<String> result = mapper.findFile(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_FILENOTFOUNDINLIBRARY, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	public List<String> findFileByAuxiliaryTable(FileContentDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			FileContentMapper mapper = session.getMapper(FileContentMapper.class);
			//dto = findLibraryReception(dto);
			FileContentDTO dtoAux = findAuxiliaryTable(dto);			
			dtoAux.setMessageNumber(dto.getMessageNumber());
			List<String> result = mapper.findFileByAuxiliaryTable(dtoAux);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_FILENOTFOUNDINLIBRARY, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	public String findExecutionMode(String executionModeCode) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			FileContentMapper mapper = session.getMapper(FileContentMapper.class);
			String result = mapper.findExecutionMode(executionModeCode);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	/**
	 * This method updates the file status to downloaded ("Bajado")
	 * @param dto
	 * @return
	 * @throws ServiceException 
	 */
	public int updateFileStatus(FileContentDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			FileContentMapper mapper = session.getMapper(FileContentMapper.class);
			int result = mapper.updateFileStatus(dto);
			queryCounter++;
			session.commit();
			return result;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	/**
	 * This method updates the file status in SSTRANS table
	 * @param dto
	 * @return
	 * @throws ServiceException 
	 */
	public int updateFileSstrans(FileContentDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			FileContentMapper mapper = session.getMapper(FileContentMapper.class);
			int result = mapper.updateSstrans(dto);
			queryCounter++;
			session.commit();
			return result;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}