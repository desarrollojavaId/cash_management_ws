package ar.com.gestionit.cashmanagement.persistence.mapper;

import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.dto.PaymentListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StoredProcedureParametersDTO;

public interface PaymentListMapper extends IMapper<PaymentListItemDTO> {
	
	/**
	 * This method is used to get the "sucursal"
	 * description according to the "sucursal" ID
	 * @param dto
	 * @return
	 */
	public PaymentListItemDTO findSuc(PaymentListItemDTO dto);
	
	/**
	 * Stored Procedures to get request number
	 * @param dto
	 */
	public void executeSSCONORP(StoredProcedureParametersDTO dto);
	
	/**
	 * 
	 * @param dto
	 * @return
	 */
	public List<PaymentListItemDTO> findSSCONORP(PaymentListItemDTO dto);
	
	public PaymentListItemDTO findCountSumTotal(PaymentListItemDTO dto);
	
}