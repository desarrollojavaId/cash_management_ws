package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.EscrowFileTypeDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

public interface EscrowFileTypeMapper extends IMapper<IDTO> {
	
	public EscrowFileTypeDTO findList(EscrowFileTypeDTO dto);
}
