package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.adapter.EgressOutflowItemAdapter;
import ar.com.gestionit.cashmanagement.persistence.dto.EgressOutflowDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.EgressOutflowItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.EgressOutflowMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

public class EgressOutflowBO extends AbstractBO<IDTO, EgressOutflowMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = EgressOutflowMapper.class;
	}
	
	/**
	 * This method executes SS0005 stored procedure to load SSCOFLEG table with data.
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public void executeLoader(EgressOutflowDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			//Open the Mybatis session
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			EgressOutflowMapper mapper = session.getMapper(EgressOutflowMapper.class);
			//Execute the stored procedure to load the table where take the report
			mapper.executeLoader(dto);
			queryCounter++;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	public List<EgressOutflowItemDTO> findEgress(EgressOutflowDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			//Get the report
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			EgressOutflowMapper mapper = session.getMapper(EgressOutflowMapper.class);
			List<EgressOutflowItemAdapter> result = mapper.findEgressList(dto);
			queryCounter++;

			return EgressOutflowItemAdapter.toDTO(result);
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	public List<EgressOutflowItemDTO> findChecks(EgressOutflowDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			//Open the Mybatis session
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			EgressOutflowMapper mapper = session.getMapper(EgressOutflowMapper.class);
			List<EgressOutflowItemAdapter> result = mapper.findChecks(dto);
			queryCounter++;

			return EgressOutflowItemAdapter.toDTO(result);
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}