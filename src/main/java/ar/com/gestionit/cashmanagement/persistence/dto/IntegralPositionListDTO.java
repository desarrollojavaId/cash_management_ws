package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name=ServiceConstant.DTO_INTEGRALPOSITION_CANALES)
public class IntegralPositionListDTO extends AbstractDTO {
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 4969943919872788248L;
	
	//Inputs
	@XmlTransient
	private String adherent;
	@XmlTransient
	private String agreementNumber;
	@XmlTransient
	private String agreementSubnumber;
	@XmlTransient
	private IntegralPositionListInputDTO input;
	@XmlTransient
	private IntegralPositionListReferencesDTO references;
	
	//Outputs
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_CANAL, defaultValue="")
	private List<IntegralPositionListItemDTO> list;

	/**
	 * @return the adherent
	 */
	public String getAdherent() {
		return adherent;
	}

	/**
	 * @param adherent the adherent to set
	 */
	public void setAdherent(String adherent) {
		this.adherent = adherent;
	}

	/**
	 * @return the agreementNumber
	 */
	public String getAgreementNumber() {
		return agreementNumber;
	}

	/**
	 * @param agreementNumber the agreementNumber to set
	 */
	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}

	/**
	 * @return the agreementSubnumber
	 */
	public String getAgreementSubnumber() {
		return agreementSubnumber;
	}

	/**
	 * @param agreementSubnumber the agreementSubnumber to set
	 */
	public void setAgreementSubnumber(String agreementSubnumber) {
		this.agreementSubnumber = agreementSubnumber;
	}

	/**
	 * @return the input
	 */
	public IntegralPositionListInputDTO getInput() {
		return input;
	}

	/**
	 * @param input the input to set
	 */
	public void setInput(IntegralPositionListInputDTO input) {
		this.input = input;
	}

	/**
	 * @return the references
	 */
	public IntegralPositionListReferencesDTO getReferences() {
		return references;
	}

	/**
	 * @param references the references to set
	 */
	public void setReferences(IntegralPositionListReferencesDTO references) {
		this.references = references;
	}

	/**
	 * @return the list
	 */
	public List<IntegralPositionListItemDTO> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<IntegralPositionListItemDTO> list) {
		this.list = list;
	}
}