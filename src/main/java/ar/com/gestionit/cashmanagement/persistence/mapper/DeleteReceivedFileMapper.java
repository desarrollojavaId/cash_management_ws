package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.DeleteReceivedFileListDTO;

public interface DeleteReceivedFileMapper extends IMapper<DeleteReceivedFileListDTO> {}