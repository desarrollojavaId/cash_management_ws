package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

/**
 * This DTO is used to keep data about the list paging
 * @author jdigruttola
 */
@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"next"})
@XmlRootElement(name = ServiceConstant.TAG_PAGER)
public class PagerDTO extends AbstractDTO {
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -3312541370975643689L;
	
	/**
	 * Page from
	 */
	@XmlTransient
	private int from;
	/**
	 * Real page to
	 */
	@XmlTransient
	private int to;
	/**
	 * This indicates if there is next page
	 */
	@XmlElement(name=ServiceConstant.TAG_PAGER_NEXT, defaultValue="")
	private int next;
	
	public int getFrom() {
		return from;
	}
	public void setFrom(int from) {
		this.from = from;
	}
	public int getTo() {
		return to;
	}
	public void setTo(int to) {
		this.to = to;
	}
	/**
	 * @return the next
	 */
	public int getNext() {
		return next;
	}
	/**
	 * @param next the next to set
	 */
	public void setNext(int next) {
		this.next = next;
	}
}