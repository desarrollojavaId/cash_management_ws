package ar.com.gestionit.cashmanagement.persistence.bo;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.RecoveredCheckDetailMapper;

public class RecoveredCheckDetailBO extends AbstractBO<IDTO, RecoveredCheckDetailMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = RecoveredCheckDetailMapper.class;
	}
}