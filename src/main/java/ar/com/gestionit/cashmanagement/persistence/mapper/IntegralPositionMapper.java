package ar.com.gestionit.cashmanagement.persistence.mapper;

import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionPaymentItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StoredProcedureParametersDTO;

public interface IntegralPositionMapper extends IMapper<IDTO> {
	public void executeSSRECCON(StoredProcedureParametersDTO dto);

	public List<IntegralPositionPaymentItemDTO> findPayment(IntegralPositionListDTO dto);
}