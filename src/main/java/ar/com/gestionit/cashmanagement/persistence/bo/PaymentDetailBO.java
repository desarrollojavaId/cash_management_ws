package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentErrorListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ProofListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.PaymentDetailMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

public class PaymentDetailBO extends AbstractBO<IDTO, PaymentDetailMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = PaymentDetailMapper.class;
	}

	/**
	 * This method returns a proof list according to the DTO given by argument
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public List<ProofListItemDTO> findProof(ProofListItemDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			PaymentDetailMapper mapper = session.getMapper(PaymentDetailMapper.class);
			List<ProofListItemDTO> result = mapper.findProof(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	/**
	 * This method returns the beneficiary email
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public PaymentDTO findMail(PaymentDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			PaymentDetailMapper mapper = session.getMapper(PaymentDetailMapper.class);
			PaymentDTO result = mapper.findMail(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	/**
	 * This method is used if the beneficiary email was not found
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public PaymentDTO findNoMail(PaymentDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			PaymentDetailMapper mapper = session.getMapper(PaymentDetailMapper.class);
			PaymentDTO result = mapper.findMail(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	/**
	 * This method is used for the proof dates (sent date and printed date)
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public PaymentDTO findProofDates(PaymentDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			PaymentDetailMapper mapper = session.getMapper(PaymentDetailMapper.class);
			PaymentDTO result = mapper.findProofDates(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	public PaymentDTO findOps (PaymentDTO dto) throws ServiceException{
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			PaymentDetailMapper mapper = session.getMapper(PaymentDetailMapper.class);
			PaymentDTO result = mapper.findOps(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	/**
	 * This method gets the payment again to send as structure (the same structure of PaymentListService)
	 * in the response
	 * @param dto
	 * @return
	 * @throws ServiceException 
	 */
	public PaymentListItemDTO findPaymentStructure(PaymentDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			PaymentDetailMapper mapper = session.getMapper(PaymentDetailMapper.class);
			PaymentListItemDTO result = mapper.findPaymentStructure(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	/**
	 * This method returns errors of payments
	 * in the response
	 * @param dto
	 * @return
	 * @throws ServiceException 
	 */
	public List<PaymentErrorListItemDTO> findErrorsList(PaymentDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			PaymentDetailMapper mapper = session.getMapper(PaymentDetailMapper.class);
			List<PaymentErrorListItemDTO> result = mapper.findErrorsList(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	
}