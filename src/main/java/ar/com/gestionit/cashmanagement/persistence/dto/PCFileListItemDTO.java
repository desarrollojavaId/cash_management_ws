package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"detail"		
})
@XmlRootElement(name = ServiceConstant.DTO_PCFILE)
public class PCFileListItemDTO extends AbstractDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1562661164005697995L;
	@FieldToExport(name=ServiceConstant.DTO_PCFILE_DETAIL)
	@XmlElement(name=ServiceConstant.DTO_PCFILE_DETAIL, defaultValue="")
	private String detail;

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = sanitateString(detail);
	}
}
