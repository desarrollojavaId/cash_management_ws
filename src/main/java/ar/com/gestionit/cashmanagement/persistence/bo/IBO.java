package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

/**
 * This is the BO interface to access to the persistence level
 * @author jdigruttola
 * @param <T>
 * @param <U>
 */
public interface IBO<T extends IDTO> {
	
	/**
	 * This method finds an specified register
	 * from the data source
	 * @return
	 * @throws ServiceException
	 */
	public T find() throws ServiceException;
	
	/**
	 * This method finds all the register according the specified DTO
	 * from the data source
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public T find(T dto) throws ServiceException;
	
	/**
	 * This method finds all the register according the specified DTO
	 * from the data source
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public List<? extends T> findList(T dto) throws ServiceException;
	
	/**
	 * This method finds all the register from the data source
	 * @return
	 * @throws ServiceException
	 */
	public List<? extends T> findAll() throws ServiceException;
	
	/**
	 * This method converts an specified DTO to XML format
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public String dtoToXML(T dto) throws ServiceException;
	
	/**
	 * This method inserts a new register in the data source
	 * @param instance
	 */
	public int insert(T instance) throws ServiceException;
	
	/**
	 * This method updates a register in the data source
	 * @param instance
	 */
	public int update(T instance) throws ServiceException;
}