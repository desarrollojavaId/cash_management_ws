package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name=ServiceConstant.DTO_CARD_SUMMARY_ROOT)
public class CardSummaryListDTO extends AbstractDTO {
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 4969943919872788248L;
	
	@XmlElement(name=ServiceConstant.DTO_CARD_SUMMARY, defaultValue="")
	private List<CardSummaryListItemDTO> list;

	/**
	 * @return the list
	 */
	public List<CardSummaryListItemDTO> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<CardSummaryListItemDTO> list) {
		this.list = list;
	}
}