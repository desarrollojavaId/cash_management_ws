package ar.com.gestionit.cashmanagement.persistence.bo;

import ar.com.gestionit.cashmanagement.persistence.dto.DeleteReceivedFileListDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.DeleteReceivedFileMapper;

public class DeleteReceivedFileBO extends AbstractBO<DeleteReceivedFileListDTO, DeleteReceivedFileMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = DeleteReceivedFileMapper.class;
	}
}