package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PagerDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PagerInputDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.IMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.StringUtil;
import ar.com.gestionit.cashmanagement.util.XMLUtil;

public abstract class AbstractBO<T extends IDTO, U extends IMapper<T>> implements IBO<T> {

	int number;
	/**
	 * Specified mapper interface
	 * NOTE: this will be specified in each implementation  
	 */
	protected Class<U> mapperInterface;

	/**
	 * This attribute contains information about the paging
	 * NOTE: by default this is not used
	 */
	protected PagerDTO pager;

	/**
	 * This attribute is used to control the queries number
	 * for performance tasks
	 */
	protected int queryCounter;

	/**
	 * Constructor by default
	 */
	public AbstractBO() {
		specifyMapper();
		queryCounter = 0;
	}

	/**
	 * This method is used by each implementation to specify
	 * the mapper interface
	 */
	abstract protected void specifyMapper();

	public List<? extends T> findAll() throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			U mapper = session.getMapper(mapperInterface);
			List<T> result = mapper.findAll();
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	public T find(T dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			U mapper = session.getMapper(mapperInterface);
			T result = mapper.find(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	public List<? extends T> findList(T dto) throws ServiceException {
		SqlSession session = null;
		try {
			//Load paging data
			loadPagingData(dto);

			//Get result list
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			U mapper = session.getMapper(mapperInterface);
			List<? extends T> result = mapper.findList(dto);
			queryCounter++;

			//Process paging
			processResultPaging(result);

			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	public T find() throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			U mapper = session.getMapper(mapperInterface);
			T result = mapper.find();
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	public int insert(T instance) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			U mapper = session.getMapper(mapperInterface);
			int result = mapper.insert(instance);
			queryCounter++;
			session.commit();
			return result;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	public int update(T instance) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			U mapper = session.getMapper(mapperInterface);
			int result = mapper.update(instance);
			queryCounter++;
			session.commit();
			return result;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	/*
	 * General utility methods -------------------------------------------------------------------
	 */

	public int getQueryCounter() {
		return queryCounter;
	}

	/*
	 * JAXB API usage ----------------------------------------------------------------------------
	 */

	/**
	 * This method converts a DTO implementation to XML string.
	 * If DTO is NULL, this returns an string empty ("")
	 * @param DTO implementation
	 * @throws ServiceException
	 * @return XML string
	 */
	public String dtoToXML(IDTO dto) throws ServiceException {
		return XMLUtil.dtoToXML(dto);
	}

	/**
	 * Este método convierte de XML a DTO
	 * @param data
	 * @param cls
	 * @return Objeto partiendo del class recibido como parametro.
	 * @throws ServiceException
	 */
	public Object xmlToDto(String data, Class<? extends IDTO> cls) throws ServiceException {
		return XMLUtil.xmlToDto(data, cls);
	}

	/*
	 * Pager methods -----------------------------------------------------------------------------------------
	 */
	/**
	 * @return the pagerEnabled
	 */
	public boolean isPagerEnabled() {
		return pager != null;
	}

	/**
	 * This method enables the pager for the result list
	 * @param pagingData
	 */
	public void enablePager(String pagingData) throws ServiceException {
		/*
		 * The pager data is not mandatory, so if it is NULL or empty, I understand
		 * that the user does not want paging
		 */
		DefinedLogger.SERVICE.debug("Datos de paginado: " + pagingData);
		if(StringUtil.isEmpty(pagingData)) {
			pager = null;
			return;
		}
		
		PagerInputDTO pi = (PagerInputDTO) xmlToDto(pagingData, PagerInputDTO.class); 
		pager = new PagerDTO();
		number = pi.getPageNumber();
		int amount = pi.getAmount();
		if (number == 0 || amount == 0){
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_INVALIDPAGER);
		}
		pager.setFrom(((number - 1) * amount) + 1);
		pager.setTo(number * amount);
	}

	/**
	 * This method disables the pager for the result list
	 */
	public void disablePager() {
		pager = null;
	}

	/**
	 * This method return the pager output as XML string
	 * @return
	 * @throws ServiceException
	 */
	public String getPagerAsXML() throws ServiceException {
		if(pager == null) {
			return "";
		}
		return dtoToXML(pager);
	}

	/**
	 * This method validates if there is next page or not 
	 * @param result
	 */
	protected void processResultPaging(List<? extends T> result) {
		if(pager != null) {
			pager.setNext(ServiceConstant.FALSE);
			if(result != null && result.size() > 0) {
				//Validate if the result list has more register by paging and deletes them
				while(result.get(result.size() -1).getRownumber() > pager.getTo()) {
					pager.setNext(number+1);
					result.remove(result.size() -1);
				}
			}
		}
	}

	/**
	 * This method loads paging data in the DTO input
	 * @param dto
	 */
	protected void loadPagingData(IDTO dto) {
		if(pager != null) {
			dto.setPageFrom(pager.getFrom());
			//The query must bring a register more to validate if there is next page
			dto.setPageTo(pager.getTo() + 1);
		}
	}
}