package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name = ServiceConstant.TAG_PROOF_ROOT)
public class ProofListDTO extends AbstractDTO {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -7798867926488859811L;

	@XmlElement(name=ServiceConstant.TAG_PROOF)
	private List<ProofListItemDTO> list;

	/**
	 * @return the list
	 */
	public List<ProofListItemDTO> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<ProofListItemDTO> list) {
		this.list = list;
	}
	
	public void setProof(ProofListItemDTO dto) {
		if(list == null)
			list = new ArrayList<ProofListItemDTO>();
		list.add(dto);
	}
}