package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;


import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"modulo",
		"moneda",
		"cuentasucursal",
		"cuentabt",
		"suboper",
		"maxxtippago",
		"totxtippago",
		"tippago",
		"cuentamoneda",
		"cbu"
})

@XmlRootElement(name = ServiceConstant.DTO_CUENTA)
public class ArchiveValidationStatusAccountListItemDTO extends AbstractDTO{

 static final long serialVersionUID = 498823037250008474L;
 
 	//Input
	@XmlTransient
	private List<String> IdArchivo;
	@XmlTransient
	private String adherent;
	
 	//OutPut
 	@XmlElement(name=ServiceConstant.DTO_MODULO, defaultValue="")
 	private String modulo;
 	@XmlElement(name=ServiceConstant.DTO_MONEDA, defaultValue="")
 	private String moneda;
 	@XmlElement(name=ServiceConstant.DTO_SUCURSAL, defaultValue="")
 	private String cuentasucursal;
 	@XmlElement(name=ServiceConstant.DTO_CTA, defaultValue="")
 	private String cuentabt;
 	@XmlElement(name=ServiceConstant.DTO_SBOP, defaultValue="")
 	private String suboper;
 	@XmlElement(name=ServiceConstant.DTO_IMPMAX, defaultValue="")
 	private String maxxtippago;
 	@XmlElement(name=ServiceConstant.DTO_IMPTOT, defaultValue="")
 	private String totxtippago;
 	@XmlElement(name=ServiceConstant.DTO_FORMAPAGO, defaultValue="")
 	private String tippago;
 	@XmlElement(name=ServiceConstant.DTO_MDAIMP, defaultValue="")
 	private String cuentamoneda;
 	@XmlTransient
 	private String cbu;
	
 	public String getModulo() {
		return modulo;
	}
	public void setModulo(String modulo) {
		this.modulo = sanitateString(modulo);
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = sanitateString(moneda);
	}
	public String getCuentasucursal() {
		return cuentasucursal;
	}
	public void setCuentasucursal(String cuentasucursal) {
		this.cuentasucursal = sanitateString(cuentasucursal);
	}
	public String getCuentabt() {
		return cuentabt;
	}
	public void setCuentabt(String cuentabt) {
		this.cuentabt = sanitateString(cuentabt);
	}
	public String getSuboper() {
		return suboper;
	}
	public void setSuboper(String suboper) {
		this.suboper = sanitateString(suboper);
	}
	public String getMaxxtippago() {
		return maxxtippago;
	}
	public void setMaxxtippago(String maxxtippago) {
		this.maxxtippago = sanitateString(maxxtippago);
	}
	public String getTotxtippago() {
		return totxtippago;
	}
	public void setTotxtippago(String totxtippago) {
		this.totxtippago = sanitateString(totxtippago);
	}
	public String getTippago() {
		return tippago;
	}
	public void setTippago(String tippago) {
		this.tippago = sanitateString(tippago);
	}
	public String getCuentamoneda() {
		return cuentamoneda;
	}
	public void setCuentamoneda(String cuentamoneda) {
		this.cuentamoneda = sanitateString(cuentamoneda);
	}
	/**
	 * @return the cbu
	 */
	public String getCbu() {
		return cbu;
	}
	/**
	 * @param cbu the cbu to set
	 */
	public void setCbu(String cbu) {
		this.cbu = cbu;
	}
}