package ar.com.gestionit.cashmanagement.persistence.mapper;

import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.dto.FileContentDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileStatusSendListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileStatusSendListItemDTO;

public interface FileStatusSendListMapper extends IMapper<FileStatusSendListItemDTO> {

	/**
	 * This method is used to get files 
	 * @param dto
	 * @return
	 */
	public List<FileStatusSendListItemDTO> find(FileStatusSendListDTO dto);
	public List<String> findFileContent(FileContentDTO dto);
	public Integer findAdherent(FileStatusSendListDTO dto);
	

}