package ar.com.gestionit.cashmanagement.persistence.bo;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.ReferencesRecoveryMapper;

public class ReferencesRecoveryBO extends AbstractBO<IDTO, ReferencesRecoveryMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = ReferencesRecoveryMapper.class;
	}
}