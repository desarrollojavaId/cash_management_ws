package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"estId",
		"descEstado",
		"events"})
@XmlRootElement(name = ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ESTADO)
public class RecoveredCheckDetailStatusListItemDTO extends AbstractDTO {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -7798867926488859811L;

		//input
		@XmlTransient
		private String nroCheq;
		@XmlTransient
		private String nroLote;	
		//Falta agregar el nro de lote que aún no lo especificaron
	
		//Output
		@XmlTransient
		private String fecGenPedido;
		
		@XmlTransient
		private String sucursalEnv;
		@XmlTransient
		private String fecEntregaCli;
		@XmlTransient
		private String impCheq;
		@XmlTransient
		private String bcoCheq;
		@XmlTransient
		private String sucursalCheq;
		@XmlTransient
		private String codPostalCheq;
		@XmlTransient
		private String fecVenc;
		@XmlTransient
		private String fecEnvio;
		@XmlTransient
		private String docLibrador;
		@XmlTransient
		private String nomLibrador;
		@XmlTransient
		private String nroCtaGirada;
		@XmlTransient
		private String tipoDocLibrador;
	
		@XmlTransient
		private String tmpFechaEvento; 
		@XmlTransient
		private String tmpHs; 
		@XmlTransient
		private String tmpDescEvento;
			
		
		@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_ID, defaultValue="")
		private String estId; 
		@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_DESCRIPESTADO, defaultValue="")
		private String descEstado;
		@XmlElementWrapper(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_LISTEVENTOS)
		@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_EVENTO, defaultValue="")
		private List<RecoveredCheckDetailStatusEventDTO> events; 
		@XmlTransient
		private List<RecoveredCheckDetailListItemDTO> cheq;
	
	
		public String getNroLote() {
			return nroLote;
		}
		public void setNroLote(String nroLote) {
			this.nroLote = nroLote;
		}
		public String getNroCheq() {
			return nroCheq;
		}
		public void setNroCheq(String nroCheq) {
			this.nroCheq = nroCheq;
		}
		public String getEstId() {
			return estId;
		}
		public void setEstId(String estId) {
			this.estId = sanitateString(estId);
		}
		public String getDescEstado() {
			return descEstado;
		}
		public void setDescEstado(String descEstado) {
			this.descEstado = sanitateString(descEstado);
		}
		/**
		 * @return the events
		 */
		public List<RecoveredCheckDetailStatusEventDTO> getEvents() {
			return events;
		}
		/**
		 * @param events the events to set
		 */
		public void setEvents(List<RecoveredCheckDetailStatusEventDTO> events) {
			this.events = events;
		}
		/**
		 * This method adds a new event in the event list
		 * @param event
		 */
		public void setEvent(RecoveredCheckDetailStatusEventDTO event) {
			if(event == null)
				return;
			if(events == null)
				events = new ArrayList<RecoveredCheckDetailStatusEventDTO>();
			events.add(event);
		}
		public String getTmpFechaEvento() {
			return tmpFechaEvento;
		}
		public void setTmpFechaEvento(String tmpFechaEvento) {
			this.tmpFechaEvento = tmpFechaEvento;
		}
		public String getTmpHs() {
			return tmpHs;
		}
		public void setTmpHs(String tmpHs) {
			this.tmpHs = tmpHs;
		}
		public String getTmpDescEvento() {
			return tmpDescEvento;
		}
		public void setTmpDescEvento(String tmpDescEvento) {
			this.tmpDescEvento = tmpDescEvento;
		}
		
		public String getFecGenPedido() {
			return fecGenPedido;
		}
		public void setFecGenPedido(String fecGenPedido) {
			this.fecGenPedido = fecGenPedido;
		}
		public String getSucursalEnv() {
			return sucursalEnv;
		}
		public void setSucursalEnv(String sucursalEnv) {
			this.sucursalEnv = sucursalEnv;
		}
		public String getFecEntregaCli() {
			return fecEntregaCli;
		}
		public void setFecEntregaCli(String fecEntregaCli) {
			this.fecEntregaCli = fecEntregaCli;
		}
		public String getImpCheq() {
			return impCheq;
		}
		public void setImpCheq(String impCheq) {
			this.impCheq = impCheq;
		}
		public String getBcoCheq() {
			return bcoCheq;
		}
		public void setBcoCheq(String bcoCheq) {
			this.bcoCheq = bcoCheq;
		}
		public String getSucursalCheq() {
			return sucursalCheq;
		}
		public void setSucursalCheq(String sucursalCheq) {
			this.sucursalCheq = sucursalCheq;
		}
		public String getCodPostalCheq() {
			return codPostalCheq;
		}
		public void setCodPostalCheq(String codPostalCheq) {
			this.codPostalCheq = codPostalCheq;
		}
		public String getFecVenc() {
			return fecVenc;
		}
		public void setFecVenc(String fecVenc) {
			this.fecVenc = fecVenc;
		}
		public String getFecEnvio() {
			return fecEnvio;
		}
		public void setFecEnvio(String fecEnvio) {
			this.fecEnvio = fecEnvio;
		}
		public String getDocLibrador() {
			return docLibrador;
		}
		public void setDocLibrador(String docLibrador) {
			this.docLibrador = docLibrador;
		}
		public String getNomLibrador() {
			return nomLibrador;
		}
		public void setNomLibrador(String nomLibrador) {
			this.nomLibrador = nomLibrador;
		}
		public String getNroCtaGirada() {
			return nroCtaGirada;
		}
		public void setNroCtaGirada(String nroCtaGirada) {
			this.nroCtaGirada = nroCtaGirada;
		}
		
		public List<RecoveredCheckDetailListItemDTO> getCheq() {
			return cheq;
		}
		public void setCheq(RecoveredCheckDetailListItemDTO cheq) {
			if(cheq == null)
				return;
			if(this.cheq == null)
				this.cheq = new ArrayList<RecoveredCheckDetailListItemDTO>();
			this.cheq.add(cheq);
			
		}
		public String getTipoDocLibrador() {
			return tipoDocLibrador;
		}
		public void setTipoDocLibrador(String tipoDocLibrador) {
			this.tipoDocLibrador = sanitateString(tipoDocLibrador);
		}
		
}