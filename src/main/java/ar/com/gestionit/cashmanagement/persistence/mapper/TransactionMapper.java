package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.TransactionDTO;

public interface TransactionMapper extends IMapper<TransactionDTO> {
	public int updateStkest(TransactionDTO dto);
	public int updateStnars(TransactionDTO dto);
	public String findStkestForUpdate(String stqtrs);
	public int updateForSendFile(TransactionDTO dto);
	public String findStkest(TransactionDTO dto);
	public String findStkestByStqtrs(TransactionDTO dto);
	public String findStqtrs(TransactionDTO dto);
	public int updateFile(TransactionDTO dto);
	public int updateFilePD(TransactionDTO dto);
	public int confirmFile(TransactionDTO dto);
}