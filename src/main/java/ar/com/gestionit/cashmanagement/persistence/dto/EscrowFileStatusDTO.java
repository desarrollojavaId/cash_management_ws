package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"fileList"})
@XmlRootElement(name = ServiceConstant.DTO_ESCROWS_FILE_ROOT)
public class EscrowFileStatusDTO extends AbstractDTO{
	
	private static final long serialVersionUID = -1215271026638111504L;
	
	@XmlTransient
	private String ssgcod;
	@XmlTransient
	private String sskfid;
	@XmlTransient
	private String sskest;
	@XmlTransient
	private String sskhis;
	@XmlTransient
	private String sskcli;
	@XmlTransient
	private String sskseg;
	@XmlTransient
	private String sskests;
	@XmlTransient
	private String sskestr;
	@XmlTransient
	private String sskrev;
	@XmlTransient
	private String selectedInterface;
	@XmlTransient
	private List<String> excludedFileTypes = new ArrayList<String>();
	@XmlTransient
	private List<String> includedFileTypes = new ArrayList<String>();
	
	@XmlTransient
	private String sqlCondition;
	
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_FILE, defaultValue="")
	List<EscrowFileTypeItemDTO> fileList;
	
	public String getSsgcod() {
		return ssgcod;
	}
	public void setSsgcod(String ssgcod) {
		this.ssgcod = ssgcod;
	}
	public String getSskfid() {
		return sskfid;
	}
	public void setSskfid(String sskfid) {
		this.sskfid = sskfid;
	}
	public String getSskest() {
		return sskest;
	}
	public void setSskest(String sskest) {
		this.sskest = sskest;
	}
	public String getSskhis() {
		return sskhis;
	}
	public void setSskhis(String sskhis) {
		this.sskhis = sskhis;
	}
	public String getSskcli() {
		return sskcli;
	}
	public void setSskcli(String sskcli) {
		this.sskcli = sskcli;
	}
	public String getSskseg() {
		return sskseg;
	}
	public void setSskseg(String sskseg) {
		this.sskseg = sskseg;
	}
	public String getSskests() {
		return sskests;
	}
	public void setSskests(String sskests) {
		this.sskests = sskests;
	}
	public String getSskestr() {
		return sskestr;
	}
	public void setSskestr(String sskestr) {
		this.sskestr = sskestr;
	}
	public String getSskrev() {
		return sskrev;
	}
	public void setSskrev(String sskrev) {
		this.sskrev = sskrev;
	}
	public List<EscrowFileTypeItemDTO> getFileList() {
		if(fileList == null)
			this.fileList = new ArrayList<EscrowFileTypeItemDTO>();
		return fileList;
	}
	public void setFileList(List<EscrowFileTypeItemDTO> fileList) {
		this.fileList = fileList;
	}
	public String getSqlCondition() {
		return sqlCondition;
	}
	public void setSqlCondition(String sqlCondition) {
		this.sqlCondition = sqlCondition;
	}
	public String getSelectedInterface() {
		return selectedInterface;
	}
	public void setSelectedInterface(String selectedInterface) {
		this.selectedInterface = selectedInterface;
	}
	public List<String> getExcludedFileTypes() {
		return excludedFileTypes;
	}
	public void addExcludedFileTypes(String fileType) {
		if(this.excludedFileTypes == null)
			this.excludedFileTypes = new ArrayList<String>();
		this.excludedFileTypes.add(fileType);
	}
	public List<String> getIncludedFileTypes() {
		return includedFileTypes;
	}
	public void addIncludedFileTypes(String fileType) {
		if(this.includedFileTypes == null)
			this.includedFileTypes = new ArrayList<String>();
		this.includedFileTypes.add(fileType);
	}
	public void setExcludedFileTypes(List<String> excludedFileTypes) {
		this.excludedFileTypes = excludedFileTypes;
	}
	public void setIncludedFileTypes(List<String> includedFileTypes) {
		this.includedFileTypes = includedFileTypes;
	}
}