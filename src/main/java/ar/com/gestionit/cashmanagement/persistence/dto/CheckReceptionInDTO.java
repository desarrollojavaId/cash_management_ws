package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"nroAdherente",
		"nroConvenio",
		"nroSubConv",
		"fechaRecDesde",
		"fechaRecHasta",
		"fechaAcredDesde",
		"fechaAcredHasta",
		"tipoCheque",
		"numCheque",
		"ref1Deudor",
		"ref2Dedudor",
		"ref1Comp",
		"ref2Comp",
		"ref3Comp",
		"ref4Comp",
		"ref5Comp"})
@XmlRootElement(name = ServiceConstant.DTO_CHECKRECEPTION_IN_ROOT)
public class CheckReceptionInDTO extends AbstractDTO {
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -4433465221081082180L;

}
