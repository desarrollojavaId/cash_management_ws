package ar.com.gestionit.cashmanagement.persistence.dto;

public class FileContentDTO extends AbstractDTO {
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 4969943919872788248L;
	
	//Inputs
	private String adherent;
	private String fileId;
	private int beneficiaryNumberIni;
	private int beneficiaryNumberEnd;
	private int processId;
	
	//Input/outputs
	private String invokedFunction;
	private String status;
	private String library;
	private String auxiliaryTable;
	private String fileContent;
	private String messageNumber;
	
	private String satBeneficiaryName;
	private String satBeneficiaryNumber;
	
	/**
	 * @return the fileId
	 */
	public String getFileId() {
		return fileId;
	}

	/**
	 * @param fileId the fileId to set
	 */
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	/**
	 * @return the library
	 */
	public String getLibrary() {
		return library;
	}

	/**
	 * @param library the library to set
	 */
	public void setLibrary(String library) {
		this.library = sanitateString(library);
	}

	/**
	 * @return the invokedFunction
	 */
	public String getInvokedFunction() {
		return invokedFunction;
	}

	/**
	 * @param invokedFunction the invokedFunction to set
	 */
	public void setInvokedFunction(String invokedFunction) {
		this.invokedFunction = sanitateString(invokedFunction);
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = sanitateString(status);
	}

	/**
	 * @return the beneficiaryNumberIni
	 */
	public int getBeneficiaryNumberIni() {
		return beneficiaryNumberIni;
	}

	/**
	 * @param beneficiaryNumberIni the beneficiaryNumberIni to set
	 */
	public void setBeneficiaryNumberIni(int beneficiaryNumberIni) {
		this.beneficiaryNumberIni = beneficiaryNumberIni;
	}

	/**
	 * @return the beneficiaryNumberEnd
	 */
	public int getBeneficiaryNumberEnd() {
		return beneficiaryNumberEnd;
	}

	/**
	 * @param beneficiaryNumberEnd the beneficiaryNumberEnd to set
	 */
	public void setBeneficiaryNumberEnd(int beneficiaryNumberEnd) {
		this.beneficiaryNumberEnd = beneficiaryNumberEnd;
	}

	/**
	 * @return the fileContent
	 */
	public String getFileContent() {
		return fileContent;
	}

	/**
	 * @param fileContent the fileContent to set
	 */
	public void setFileContent(String fileContent) {
		this.fileContent = sanitateString(fileContent);
	}

	/**
	 * @return the adherent
	 */
	public String getAdherent() {
		return adherent;
	}

	/**
	 * @param adherent the adherent to set
	 */
	public void setAdherent(String adherent) {
		this.adherent = adherent;
	}

	/**
	 * @return the processId
	 */
	public int getProcessId() {
		return processId;
	}

	/**
	 * @param processId the processId to set
	 */
	public void setProcessId(int processId) {
		this.processId = processId;
	}

	/**
	 * @return the satBeneficiaryName
	 */
	public String getSatBeneficiaryName() {
		return satBeneficiaryName;
	}

	/**
	 * @param satBeneficiaryName the satBeneficiaryName to set
	 */
	public void setSatBeneficiaryName(String satBeneficiaryName) {
		this.satBeneficiaryName = satBeneficiaryName;
	}

	/**
	 * @return the satBeneficiaryNumber
	 */
	public String getSatBeneficiaryNumber() {
		return satBeneficiaryNumber;
	}

	/**
	 * @param satBeneficiaryNumber the satBeneficiaryNumber to set
	 */
	public void setSatBeneficiaryNumber(String satBeneficiaryNumber) {
		this.satBeneficiaryNumber = satBeneficiaryNumber;
	}

	/**
	 * @return the auxiliaryTable
	 */
	public String getAuxiliaryTable() {
		return auxiliaryTable;
	}

	/**
	 * @param auxiliaryTable the auxiliaryTable to set
	 */
	public void setAuxiliaryTable(String auxiliaryTable) {
		this.auxiliaryTable = auxiliaryTable;
	}

	/**
	 * @return the messageNumber
	 */
	public String getMessageNumber() {
		return messageNumber;
	}

	/**
	 * @param messageNumber the messageNumber to set
	 */
	public void setMessageNumber(String messageNumber) {
		this.messageNumber = messageNumber;
	}
}