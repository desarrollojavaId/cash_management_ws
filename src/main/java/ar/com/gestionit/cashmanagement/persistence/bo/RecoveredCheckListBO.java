package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredCheckListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredCheckListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StoredProcedureParametersDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.RecoveredCheckListMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DateUtil;

public class RecoveredCheckListBO extends AbstractBO<IDTO, RecoveredCheckListMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = RecoveredCheckListMapper.class;
	}
	
	@SuppressWarnings("unchecked")
	public RecoveredCheckListDTO findChecks(RecoveredCheckListDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			StoredProcedureParametersDTO sp = new StoredProcedureParametersDTO();
			sp.setParam2("0");
			sp.setParam3("1");
			sp.setParam5("");
			sp.setParam6("");
			
			String date = ServiceConstant.SDF_AMERICAN_DATE.format(new Date());
			if(date != null)
				date = date.replace("/", "");
			sp.setParam1(date);
			
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			RecoveredCheckListMapper mapper = session.getMapper(RecoveredCheckListMapper.class);
			
			//Get days to run the stored procedure
			sp.setParam4(mapper.findDays());
			queryCounter++;

			//Run stored procedure S0004
			mapper.executeS0004(sp);
			queryCounter++;
			
			//Format the date
			date = sp.getParam2();
			if(date != null && !date.trim().equals("") && date.length() >= 6 ) {
				date = new StringBuffer(date.substring(0, 4))
					.append("-")
					.append(date.substring(4, 6))
					.append("-")
					.append(date.substring(6))
					.toString();
			}
			date = DateUtil.changeFormat(date, "yyyy-MM-dd", "dd/MM/yy");
			dto.setCpdDateTo(date);
			
			//Load paging data
			loadPagingData(dto);
			
			//Get checks
			dto.setList((List<RecoveredCheckListItemDTO>) mapper.findList(dto));
			queryCounter++;
			
			//Process paging
			processResultPaging(dto.getList());
			
			return dto;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	/**
	 * This method validates if the check belongs to Fiserv company or not
	 * @param check DTO
	 * @return TRUE if the check belongs to Fiserv company or FALSE if not
	 * @throws ServiceException 
	 */
	public boolean belongToFiservCompany(RecoveredCheckListItemDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			RecoveredCheckListMapper mapper = session.getMapper(RecoveredCheckListMapper.class);
			String result = mapper.belongToFiservCompany(dto);
			queryCounter++;
			
			if(result != null && result.trim().equals("2"))
				return true;
			return false;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	/**
	 * This method loads the request number in the DTO given by argument
	 * @param DTO
	 * @throws ServiceException 
	 */
	public void loadRequestNumber(RecoveredCheckListDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			StoredProcedureParametersDTO sp = new StoredProcedureParametersDTO();
			sp.setParam1("1");
			sp.setParam2(dto.getAdherent());
			sp.setParam3(String.valueOf(dto.getAgreementNumber()));
			sp.setParam4(String.valueOf(dto.getAgreementSubnumber()));
			sp.setParam5("0");
			
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			RecoveredCheckListMapper mapper = session.getMapper(RecoveredCheckListMapper.class);
			mapper.executeS0003(sp);
			queryCounter++;
			dto.setBatchId(sp.getParam5());

			//Validate request number
			if(dto.getBatchId() == null || dto.getBatchId().trim().equals(""))
				dto.setBatchId("0");
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}