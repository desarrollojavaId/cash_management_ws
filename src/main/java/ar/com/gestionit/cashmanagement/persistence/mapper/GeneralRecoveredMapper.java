package ar.com.gestionit.cashmanagement.persistence.mapper;

import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StoredProcedureParametersDTO;

public interface GeneralRecoveredMapper extends IMapper<IDTO> {
	
	/**
	 * @return the days to pass by argument to the stored procedure which we must execute then
	 */
	public String findDays();
	
	/**
	 * Stored Procedures to work with dates
	 * @param dto
	 */
	public void executeSS0004(StoredProcedureParametersDTO dto);
	
	/**
	 * Stored Procedures to get request number
	 * @param dto
	 */
	public void executeSS0003(StoredProcedureParametersDTO dto);
	
	/**
	 * This method deletes all the non recovered checks
	 * @param dto
	 * @return
	 */
	public int deleteNonRecoveredChecks(StoredProcedureParametersDTO dto);
	
	/**
	 * This method persists event with automatic expirations
	 * @param dto
	 * @return
	 */
	public int addEventAutomaticExpiration(StoredProcedureParametersDTO dto);
	
	/**
	 * This method persists event
	 * @param dto
	 * @return
	 */
	public int addEvent(StoredProcedureParametersDTO dto);
	
	/**
	 * This method gets values to insert in event table.
	 * @param dto
	 * @return
	 */
	public List<StoredProcedureParametersDTO> findAutomaticEventFields(StoredProcedureParametersDTO dto);
	
	/**
	 * This method gets values to insert in event table.
	 * @param dto
	 * @return
	 */
	public List<StoredProcedureParametersDTO> findEventFields(StoredProcedureParametersDTO dto);
	
	/**
	 * This method validates if there are checks for the recovered batch
	 * @param dto
	 * @return
	 */
	public int findChecksForRecovered(StoredProcedureParametersDTO dto);
}