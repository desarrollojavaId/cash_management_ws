package ar.com.gestionit.cashmanagement.persistence.bo;


import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.DebtListMapper;


public class DebtListBO extends AbstractBO<IDTO, DebtListMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = DebtListMapper.class;
	}
}