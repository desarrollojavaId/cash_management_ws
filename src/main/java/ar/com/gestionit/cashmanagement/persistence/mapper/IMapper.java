package ar.com.gestionit.cashmanagement.persistence.mapper;

import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

/**
 * This is the Mapper interface.
 * This defines the method prototypes
 * to access to data source
 * @author jdigruttola
 * @param <T>
 */
public interface IMapper<T extends IDTO> {
	
	/**
	 * This method returns a DTO instance according to query mapped 
	 * @return
	 */
	public T find();
	
	/**
	 * This method returns a DTO instance according to query mapped
	 * and the specified DTO 
	 * @return
	 */
	public T find(T dto);
	
	/**
	 * This method gets a DTO list according to query mapped and the
	 * specified DTO
	 * @return
	 */
	public List<? extends T> findList(T dto);
	
	/**
	 * This method gets all the registers of the data source
	 * @return
	 */
	public List<T> findAll();
	
	/**
	 * This method inserts a new register in the data source
	 * @param instance
	 */
	public int insert(T instance);
	
	/**
	 * This method updates a register in the data source
	 * @param instance
	 */
	public int update(T instance);
	
	/**
	 * This method deletes registers in the data source
	 * @param instance
	 */
	public int delete(T instance);
}