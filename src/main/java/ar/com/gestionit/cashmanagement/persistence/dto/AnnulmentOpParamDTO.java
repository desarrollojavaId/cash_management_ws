package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.Date;


public class AnnulmentOpParamDTO extends AbstractDTO {
	
	/**
	 * MTLQOP, MTLQSO, MTKME, MTLKFP, MTLKES
	 */
	private static final long serialVersionUID = -3997310821125462037L;
	
		
	private Integer mtqkev; 
	private Integer mtqske;
	private Integer mtlqop;
	private Integer mtlqso; 
	private Date mtqfmv; //Date mtcontro
	private Date mtqhmv;
	private Date mtqfpr; 
	private Date mtqhpr; 
	private String mtqual; 
	private String mtqpal;
	private Integer mtkme;
	
	private Integer mtlkfp;
	private Integer mtlkes;
	
	public AnnulmentOpParamDTO (Integer mtqkev, Integer mtqske, Integer mtlqop, Integer mtlqso, Date mtqfmv, Date mtqhmv,Date mtqfpr,Date mtqhpr, String mtqual, String mtqpal){
		this.mtqkev = mtqkev; 
		this.mtqske = mtqske;
		this.mtlqop = mtlqop;
		this.mtlqso = mtlqso; 
		this.mtqfmv = mtqfmv;
		this.mtqhmv = mtqhmv;
		this.mtqfpr = mtqfpr; 
		this.mtqhpr = mtqhpr; 
		this.mtqual = mtqual; 
		this.mtqpal = mtqpal;
		this.mtkme = mtkme;	
		this.mtlkfp = mtlkfp;
		this.mtlkes = mtlkes;
	}
	
	public Integer getMtqkev() {
		return mtqkev;
	}
	public void setMtqkev(Integer mtqkev) {
		this.mtqkev = mtqkev;
	}
	public Integer getMtqske() {
		return mtqske;
	}
	public void setMtqske(Integer mtqske) {
		this.mtqske = mtqske;
	}
	public Integer getMtlqop() {
		return mtlqop;
	}
	public void setMtlqop(Integer mtlqop) {
		this.mtlqop = mtlqop;
	}
	public Integer getMtlqso() {
		return mtlqso;
	}
	public void setMtlqso(Integer mtlqso) {
		this.mtlqso = mtlqso;
	}
	public Date getMtqfmv() {
		return mtqfmv;
	}
	public void setMtqfmv(Date mtqfmv) {
		this.mtqfmv = mtqfmv;
	}
	public Date getMtqhmv() {
		return mtqhmv;
	}
	public void setMtqhmv(Date mtqhmv) {
		this.mtqhmv = mtqhmv;
	}
	public Date getMtqfpr() {
		return mtqfpr;
	}
	public void setMtqfpr(Date mtqfpr) {
		this.mtqfpr = mtqfpr;
	}
	public Date getMtqhpr() {
		return mtqhpr;
	}
	public void setMtqhpr(Date mtqhpr) {
		this.mtqhpr = mtqhpr;
	}
	public String getMtqual() {
		return mtqual;
	}
	public void setMtqual(String mtqual) {
		this.mtqual = mtqual;
	}
	public String getMtqpal() {
		return mtqpal;
	}
	public void setMtqpal(String mtqpal) {
		this.mtqpal = mtqpal;
	}
	public Integer getMtkme() {
		return mtkme;
	}
	public void setMtkme(Integer mtkme) {
		this.mtkme = mtkme;
	}
	public Integer getMtlkfp() {
		return mtlkfp;
	}
	public void setMtlkfp(Integer mtlkfp) {
		this.mtlkfp = mtlkfp;
	}
	public Integer getMtlkes() {
		return mtlkes;
	}
	public void setMtlkes(Integer mtlkes) {
		this.mtlkes = mtlkes;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
}