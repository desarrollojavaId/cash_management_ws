package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"ssncta"
		})
@XmlRootElement(name = ServiceConstant.DTO_ESCROWS_CUENTAS)
public class EscrowInputListDTO  extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 91949138285388728L;
	
	@XmlElement(name=ServiceConstant.DTO_ESCROWS_NROCUENTA, defaultValue="")
	private List<String> ssncta;

	public List<String> getSsncta() {
		return ssncta;
	}

	public void setSsncta(List<String> ssncta) {
		this.ssncta = ssncta;
	}

	

}