package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"id",
		"enabled"
})
@XmlRootElement(name = ServiceConstant.DTO_ADHERENTLISTFORMEP)
public class AdherentListForMepOutputItemDTO extends AbstractDTO{

	private static final long serialVersionUID = -1414758864739578889L;
	
	/**
	 * Constructor by default
	 */
	public AdherentListForMepOutputItemDTO() {
	}
	
	/**
	 * Parameterized constructor
	 * @param id
	 * @param enabled
	 */
	public AdherentListForMepOutputItemDTO(String id, String enabled) {
		this.id = id;
		this.enabled = enabled;
	}

	@XmlElement(name=ServiceConstant.DTO_ADHERENTLISTFORMEP_ID, defaultValue="")
	private String id;
	
	@XmlElement(name=ServiceConstant.DTO_ADHERENTLISTFORMEP_ENABLED, defaultValue="")
	private String enabled;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the enabled
	 */
	public String getEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
}