package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"companyId",
		"companyDescription",
		"amount",
		"operationNumber",
		"type",
		"date"
})
@XmlRootElement(name = ServiceConstant.DTO_MADEPAYMENT)
public class MadePaymentListItemDTO extends AbstractDTO {
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -1600726894333083824L;

	@XmlElement(name=ServiceConstant.DTO_MADEPAYMENT_COMPANYID, defaultValue="")
	private String companyId;
	@XmlElement(name=ServiceConstant.DTO_MADEPAYMENT_COMPANYDESC, defaultValue="")
	private String companyDescription;
	@XmlElement(name=ServiceConstant.DTO_MADEPAYMENT_AMOUNT, defaultValue="")
	private String amount;
	@XmlElement(name=ServiceConstant.DTO_MADEPAYMENT_OPERATIONNUMBER, defaultValue="")
	private String operationNumber;
	@XmlElement(name=ServiceConstant.DTO_MADEPAYMENT_TYPE, defaultValue="")
	private String type;
	@XmlElement(name=ServiceConstant.DTO_MADEPAYMENT_DATE, defaultValue="")
	private String date;
	/**
	 * @return the companyId
	 */
	public String getCompanyId() {
		return companyId;
	}
	/**
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	/**
	 * @return the companyDescription
	 */
	public String getCompanyDescription() {
		return companyDescription;
	}
	/**
	 * @param companyDescription the companyDescription to set
	 */
	public void setCompanyDescription(String companyDescription) {
		this.companyDescription = companyDescription;
	}
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
	/**
	 * @return the operationNumber
	 */
	public String getOperationNumber() {
		return operationNumber;
	}
	/**
	 * @param operationNumber the operationNumber to set
	 */
	public void setOperationNumber(String operationNumber) {
		this.operationNumber = operationNumber;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}
}