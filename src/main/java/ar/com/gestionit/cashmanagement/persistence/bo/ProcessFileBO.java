package ar.com.gestionit.cashmanagement.persistence.bo;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.ProcessFileDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StoredProcedureParametersDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.ProcessFileMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.StringUtil;

public class ProcessFileBO extends AbstractBO<ProcessFileDTO, ProcessFileMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = ProcessFileMapper.class;
	}
	
	public String moveFilePD(String fileId) throws ServiceException {
		if(StringUtil.isEmpty(fileId))
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTPDFILEID);

		StoredProcedureParametersDTO spDto = new StoredProcedureParametersDTO();
		spDto.setParam1(fileId);
		
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			ProcessFileMapper mapper = session.getMapper(ProcessFileMapper.class);
			mapper.moveFilePD(spDto);
			session.commit();
			queryCounter++;

			return spDto.getParam1();
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}