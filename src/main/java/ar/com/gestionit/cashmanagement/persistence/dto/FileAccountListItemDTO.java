package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"accountModule",
		"accountBranch",
		"accountCurrency",
		"accountNumber",
		"accountSuboperation",
		"strAmount",
		"strAmountTotal",
		"transactionCurrency",
		"paymentType",
		"accountPaper",
		"accountCbu",
		"accountCompany",
		"accountOperation",
		"accountOperationType"
})
@XmlRootElement(name = ServiceConstant.DTO_CASHACCOUNTSLIST)
public class FileAccountListItemDTO extends AbstractDTO{

	private static final long serialVersionUID = 6076853825083306594L;
	
	public static final String FUNCTIONALITY_BY_DEFAULT = "131";

	//Output
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_MOD, defaultValue="")
	private String accountModule;
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_SUC, defaultValue="")
	private String accountBranch;
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_MDA, defaultValue="")
	private String accountCurrency;
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_CTA, defaultValue="")
	private String accountNumber;
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_SUBCTA, defaultValue="")
	private String accountSuboperation;
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_IMPMAX, defaultValue="")
	private String strAmount;
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_IMPTOT, defaultValue="")
	private String strAmountTotal;
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_TRAMDA, defaultValue="")
	private String transactionCurrency;
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_PAGFOR, defaultValue="")
	private int paymentType;
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_PAPER, defaultValue="")
	private String accountPaper;
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_CBU, defaultValue="")
	private String accountCbu;
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_COMPANY, defaultValue="")
	private String accountCompany;
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_OPERATION, defaultValue="")
	private String accountOperation;
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_OPERATIONTYPE, defaultValue="")
	private String accountOperationType;
	
	@XmlTransient
	private long amount;
	@XmlTransient
	private long amountTotal;
	@XmlTransient
	private long key;
	@XmlTransient
	private String taskCode;
	@XmlTransient
	private String functionalityCode;
	
	/**
	 * @return the accountModule
	 */
	public String getAccountModule() {
		return accountModule;
	}
	/**
	 * @param accountModule the accountModule to set
	 */
	public void setAccountModule(String accountModule) {
		this.accountModule = accountModule;
	}
	/**
	 * @return the accountBranch
	 */
	public String getAccountBranch() {
		return accountBranch;
	}
	/**
	 * @param accountBranch the accountBranch to set
	 */
	public void setAccountBranch(String accountBranch) {
		this.accountBranch = accountBranch;
	}
	/**
	 * @return the accountCurrency
	 */
	public String getAccountCurrency() {
		return accountCurrency;
	}
	/**
	 * @param accountCurrency the accountCurrency to set
	 */
	public void setAccountCurrency(String accountCurrency) {
		this.accountCurrency = accountCurrency;
	}
	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}
	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	/**
	 * @return the accountSuboperation
	 */
	public String getAccountSuboperation() {
		return accountSuboperation;
	}
	/**
	 * @param accountSuboperation the accountSuboperation to set
	 */
	public void setAccountSuboperation(String accountSuboperation) {
		this.accountSuboperation = accountSuboperation;
	}
	/**
	 * @return the transactionCurrency
	 */
	public String getTransactionCurrency() {
		return transactionCurrency;
	}
	/**
	 * @param transactionCurrency the transactionCurrency to set
	 */
	public void setTransactionCurrency(String transactionCurrency) {
		this.transactionCurrency = transactionCurrency;
	}
	/**
	 * @return the paymentType
	 */
	public int getPaymentType() {
		return paymentType;
	}
	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(int paymentType) {
		this.paymentType = paymentType;
	}
	/**
	 * @return the accountPaper
	 */
	public String getAccountPaper() {
		return accountPaper;
	}
	/**
	 * @param accountPaper the accountPaper to set
	 */
	public void setAccountPaper(String accountPaper) {
		this.accountPaper = accountPaper;
	}
	/**
	 * @return the accountCbu
	 */
	public String getAccountCbu() {
		return accountCbu;
	}
	/**
	 * @param accountCbu the accountCbu to set
	 */
	public void setAccountCbu(String accountCbu) {
		this.accountCbu = accountCbu;
	}
	/**
	 * @return the accountCompany
	 */
	public String getAccountCompany() {
		return accountCompany;
	}
	/**
	 * @param accountCompany the accountCompany to set
	 */
	public void setAccountCompany(String accountCompany) {
		this.accountCompany = accountCompany;
	}
	/**
	 * @return the accountOperation
	 */
	public String getAccountOperation() {
		return accountOperation;
	}
	/**
	 * @param accountOperation the accountOperation to set
	 */
	public void setAccountOperation(String accountOperation) {
		this.accountOperation = accountOperation;
	}
	/**
	 * @return the accountOperationType
	 */
	public String getAccountOperationType() {
		return accountOperationType;
	}
	/**
	 * @param accountOperationType the accountOperationType to set
	 */
	public void setAccountOperationType(String accountOperationType) {
		this.accountOperationType = accountOperationType;
	}
	/**
	 * @return the amount
	 */
	public long getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(long amount) {
		this.amount = amount;
	}
	/**
	 * @return the amountTotal
	 */
	public long getAmountTotal() {
		return amountTotal;
	}
	/**
	 * @param amountTotal the amountTotal to set
	 */
	public void setAmountTotal(long amountTotal) {
		this.amountTotal = amountTotal;
	}
	/**
	 * @return the strAmount
	 */
	public String getStrAmount() {
		return strAmount;
	}
	/**
	 * @param strAmount the strAmount to set
	 */
	public void setStrAmount(String strAmount) {
		this.strAmount = strAmount;
	}
	/**
	 * @return the strAmountTotal
	 */
	public String getStrAmountTotal() {
		return strAmountTotal;
	}
	/**
	 * @param strAmountTotal the strAmountTotal to set
	 */
	public void setStrAmountTotal(String strAmountTotal) {
		this.strAmountTotal = strAmountTotal;
	}
	/**
	 * @return the key
	 */
	public long getKey() {
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(long key) {
		this.key = key;
	}
	/**
	 * @return the taskCode
	 */
	public String getTaskCode() {
		return taskCode;
	}
	/**
	 * @param taskCode the taskCode to set
	 */
	public void setTaskCode(String taskCode) {
		this.taskCode = taskCode;
	}
	/**
	 * @return the functionalityCode
	 */
	public String getFunctionalityCode() {
		return functionalityCode;
	}
	/**
	 * @param functionalityCode the functionalityCode to set
	 */
	public void setFunctionalityCode(String functionalityCode) {
		this.functionalityCode = functionalityCode;
	}
	
}