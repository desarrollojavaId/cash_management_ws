package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"stnars",
		"stkest",
		"paymentWays"
})

@XmlRootElement(name = ServiceConstant.DTO_FILE)
public class ArchiveValidationStatusListItemDTO extends AbstractDTO{

	private static final long serialVersionUID = 2102947900016849520L;

	//Input
	@XmlTransient
	private List<String> IdArchivo;
	@XmlTransient
	private String adherent;
	@XmlTransient
	private String actualIdArchivo;
	@XmlTransient
	private String stqtot;

	//Output
	@XmlElement(name=ServiceConstant.DTO_FILE_ID, defaultValue="")
	private String stnars;
	@XmlElement(name=ServiceConstant.DTO_FILERECEPTION_STATE, defaultValue="")
	private String stkest;
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_WAY_FORMASDEPAGO, defaultValue="")
	private PaymentWayListDTO paymentWays;
	
	@XmlTransient
	private String stnarc;
	
	public List<String> getIdArchivo() {
		return IdArchivo;
	}
	public void setIdArchivo(List<String> idArchivo) {
		IdArchivo = idArchivo;
	}
	public String getStnars() {
		return stnars;
	}
	public void setStnars(String stnars) {
		this.stnars = sanitateString(stnars);
	}
	public String getStkest() {
		return stkest;
	}
	public void setStkest(String stkest) {
		this.stkest = sanitateString(stkest);
	}
	public String getAdherent() {
		return adherent;
	}
	public void setAdherent(String adherent) {
		this.adherent = sanitateString(adherent);
	}
	/**
	 * @return the paymentWays
	 */
	public PaymentWayListDTO getPaymentWays() {
		return paymentWays;
	}
	/**
	 * @param paymentWays the paymentWays to set
	 */
	public void setPaymentWays(PaymentWayListDTO paymentWays) {
		this.paymentWays = paymentWays;
	}
	public String getStnarc() {
		return stnarc;
	}
	public void setStnarc(String stnarc) {
		this.stnarc = sanitateString(stnarc);
	}
	/**
	 * @return the actualIdArchivo
	 */
	public String getActualIdArchivo() {
		return actualIdArchivo;
	}
	/**
	 * @param actualIdArchivo the actualIdArchivo to set
	 */
	public void setActualIdArchivo(String actualIdArchivo) {
		this.actualIdArchivo = actualIdArchivo;
	}
	public String getStqtot() {
		return stqtot;
	}
	public void setStqtot(String stqtot) {
		this.stqtot = stqtot;
	}
	
}
