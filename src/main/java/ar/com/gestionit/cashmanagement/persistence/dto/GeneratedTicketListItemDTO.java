package ar.com.gestionit.cashmanagement.persistence.dto;



import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={
		  	"fechaVto",
			"empresa",
			"nroConv",
			"subConv"
})

@XmlRootElement(name=ServiceConstant.DTO_GENERATEDTICKET)
public class GeneratedTicketListItemDTO extends AbstractDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6634794070503717903L;
	
	@XmlElement(name=ServiceConstant.DTO_GENERATEDTICKET_NROCONVENIO, defaultValue="")
	private String nroConv;
	@XmlElement(name=ServiceConstant.DTO_GENERATEDTICKET_NROBOLETA, defaultValue="")
	private String subConv;
	@XmlElement(name=ServiceConstant.DTO_GENERATEDTICKET_EMPRESADESC, defaultValue="")
	private String empresa;
	@XmlElement(name=ServiceConstant.DTO_GENERATEDTICKET_FECHAVENC, defaultValue="")
	private String fechaVto;
	
	public String getNroConv() {
		return nroConv;
	}
	public void setNroConv(String nroConv) {
		this.nroConv = sanitateString(nroConv);
	}
	public String getSubConv() {
		return subConv;
	}
	public void setSubConv(String subConv) {
		this.subConv = sanitateString(subConv);
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = sanitateString(empresa);
	}
	public String getFechaVto() {
		return fechaVto;
	}
	public void setFechaVto(String fechaVto) {
		this.fechaVto = sanitateString(fechaVto);
	}
	
}