package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

public interface ReferencesRecoveryMapper extends IMapper<IDTO> {}