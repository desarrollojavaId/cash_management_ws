package ar.com.gestionit.cashmanagement.persistence.adapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.dto.EgressOutflowItemDTO;
import ar.com.gestionit.cashmanagement.util.DateUtil;

public class EgressOutflowItemAdapter implements Serializable {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -7798867926488859811L;

	//Ouput
	private String paymentType;
	private int columnNumber;
	private String amount;
	private String date;

	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}
	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	/**
	 * @return the columnNumber
	 */
	public int getColumnNumber() {
		return columnNumber;
	}
	/**
	 * @param columnNumber the columnNumber to set
	 */
	public void setColumnNumber(int columnNumber) {
		this.columnNumber = columnNumber;
	}
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
	/**
	 * 
	 * @return date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * 
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = DateUtil.normalizeDateFormat(date);
	}

	public static List<EgressOutflowItemDTO> toDTO(List<EgressOutflowItemAdapter> list) {
		/*
		 * Validations...
		 */
		if(list == null)
			return null;

		if(list.size() == 0)
			return new ArrayList<EgressOutflowItemDTO>();

		/*
		 * Define and initialize variables
		 */
		List<EgressOutflowItemDTO> result = new ArrayList<EgressOutflowItemDTO>();
		EgressOutflowItemDTO dto = null;
		EgressOutflowItemAdapter adapter;

		/*
		 * Processing...
		 * NOTE 1: per each DTO, we have 7 adapters because of each DTO give us
		 * only one column of the 7 (old outflows, day 1, 2, 3, 4, 5, next outflows).
		 * So, we must iterate 7 times to complete only one DTO
		 * 
		 * NOTE 2: we assume that the adapter list is sorted by payment type, i.e.,
		 * the 7 iterations pertain to the same DTO. If not, problems...  
		 */
		for(int i = 0 ; i < list.size(); i++) {
			adapter = list.get(i);
			
			/*
			 * Validate if I am work with the same DTO or I must generate one new.
			 * Each 7 adapters, a new DTO 
			 */
			switch(adapter.getColumnNumber()) {
			case 1:
				dto = new EgressOutflowItemDTO();
				dto.setPaymentType(adapter.getPaymentType());
				dto.setOldAmount(adapter.getAmount());
				break;
			case 2:
				dto.setDay1ExporterTitle(adapter.getDate());
				dto.setDay1Date(adapter.getDate());
				dto.setDay1(adapter.getAmount());
				break;
			case 3:
				dto.setDay2ExporterTitle(adapter.getDate());
				dto.setDay2Date(adapter.getDate());
				dto.setDay2(adapter.getAmount());
				break;
			case 4:
				dto.setDay3ExporterTitle(adapter.getDate());
				dto.setDay3Date(adapter.getDate());
				dto.setDay3(adapter.getAmount());
				break;
			case 5:
				dto.setDay4ExporterTitle(adapter.getDate());
				dto.setDay4Date(adapter.getDate());
				dto.setDay4(adapter.getAmount());
				break;
			case 6:
				dto.setDay5ExporterTitle(adapter.getDate());
				dto.setDay5Date(adapter.getDate());
				dto.setDay5(adapter.getAmount());
				break;
			case 7:
				dto.setNextAmount(adapter.getAmount());
				//Add DTO to the result because this already is ready
				result.add(dto);
				break;
			default:
				break;	
			}
		}
		return result;
	}
	
}