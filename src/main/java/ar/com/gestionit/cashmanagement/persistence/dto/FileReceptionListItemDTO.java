package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import java.util.List;

import ar.com.gestionit.cashmanagement.annotation.CodeMapped;
import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"sanarc",
		"safgen",
		"sahgen",
		"sakest",
		"sakarc",
		"sanaor",
		"sbdres"
})
@XmlRootElement(name = ServiceConstant.DTO_FILERECEPTION)
public class FileReceptionListItemDTO extends AbstractDTO{

	
	private static final long serialVersionUID = -1597375682931440007L;
	
	//Input
	@XmlTransient
	private String agreementNumber;
	@XmlTransient
	private String agreementSubnumber;
	@XmlTransient
	private int pageFrom;
	@XmlTransient
	private int pageTo;
	@XmlTransient
	private int rowNumber;
	@XmlTransient
	private String fechaHasta;
	@XmlTransient
	private String fechaDesde;
	@XmlTransient
	private String adherent;
	@XmlTransient
	private List<String> typesId;
	@XmlTransient
	private List<String> statusId;
	@XmlTransient
	private List<FileReceptionTypesItemDTO> types;
	@XmlTransient
	private String sbkres;
	@XmlTransient
	private String sPServ;
		
	
	//Output
	@FieldToExport(name=ServiceConstant.DTO_FILERECEPTION_FECGENEC)
	@XmlElement(name=ServiceConstant.DTO_FILERECEPTION_FECGENEC, defaultValue="")
	private String safgen;
	@FieldToExport(name=ServiceConstant.DTO_FILERECEPTION_HORGENEC)
	@XmlElement(name=ServiceConstant.DTO_FILERECEPTION_HORGENEC, defaultValue="")
	private String sahgen;
	@FieldToExport(name=ServiceConstant.DTO_FILERECEPTION_TYPE)
	@CodeMapped
	@XmlElement(name=ServiceConstant.DTO_FILERECEPTION_TYPE, defaultValue="")
	private String sakarc;
	@XmlElement(name=ServiceConstant.DTO_FILERECEPTION_STATE, defaultValue="")
	private String sakest;
	@FieldToExport(name=ServiceConstant.DTO_FILERECEPTION_STATE)
	@CodeMapped
	@XmlTransient
	private String exportSakest;
	@FieldToExport(name=ServiceConstant.DTO_FILERECEPTION_NAME)
	@XmlElement(name=ServiceConstant.DTO_FILERECEPTION_NAME, defaultValue="")
	private String sanarc;
	@FieldToExport(name=ServiceConstant.DTO_FILERECEPTION_SANAOR)
	@XmlElement(name=ServiceConstant.DTO_FILERECEPTION_SANAOR, defaultValue="")
	private String sanaor;
	@XmlElement(name=ServiceConstant.DTO_FILERECEPTION_DESCRIPTION, defaultValue="")
	private String sbdres;
	
	
	public int getPageFrom() {
		return pageFrom;
	}
	public void setPageFrom(int pageFrom) {
		this.pageFrom = pageFrom;
	}
	public int getPageTo() {
		return pageTo;
	}
	public void setPageTo(int pageTo) {
		this.pageTo = pageTo;
	}
	public int getRowNumber() {
		return rowNumber;
	}
	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}
	public String getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = sanitateString(fechaHasta);
	}
	public String getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = sanitateString(fechaDesde);
	}
	public String getAdherent() {
		return adherent;
	}
	public void setAdherent(String adherent) {
		this.adherent = sanitateString(adherent);
	}
	public List<String> getTypesId() {
		return typesId;
	}
	public void setTypesId(List<String> list) {
		this.typesId = list;
	}
	public List<FileReceptionTypesItemDTO> getTypes() {
		return types;
	}
	public void setTypes(List<FileReceptionTypesItemDTO> list) {
		this.types = list;
	}
	public String getSanarc() {
		return sanarc;
	}
	public void setSanarc(String sanarc) {
		this.sanarc = sanarc;
	}
	public String getSakest() {
		return sakest;
	}
	public void setSakest(String sakest) {
		this.sakest = sakest;
	}
	public String getSakarc() {
		return sakarc;
	}
	public void setSakarc(String sakarc) {
		this.sakarc = sakarc;
	}
	public String getSbdres() {
		return sbdres;
	}
	public void setSbdres(String sbdres) {
		this.sbdres = sanitateString(sbdres);
	}
	public String getSahgen() {
		return sahgen;
	}
	public void setSahgen(String sahgen) {
		this.sahgen = sanitateString(sahgen);
	}
	public String getSbkres() {
		return sbkres;
	}
	public void setSbkres(String sbkres) {
		this.sbkres = sanitateString(sbkres);
	}
	public String getsPServ() {
		return sPServ;
	}
	public void setsPServ(String sPServ) {
		this.sPServ = sPServ;
	}
	
	public String getSafgen() {
		return safgen;
	}
	public void setSafgen(String safgen) {
		this.safgen = sanitateString(safgen);
	}
	public String getAgreementNumber() {
		return agreementNumber;
	}
	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}
	public String getAgreementSubnumber() {
		return agreementSubnumber;
	}
	public void setAgreementSubnumber(String agreementSubnumber) {
		this.agreementSubnumber = agreementSubnumber;
	}
	public String getSanaor() {
		return sanaor;
	}
	public void setSanaor(String sanaor) {
		this.sanaor = sanitateString(sanaor);
	}
	public List<String> getStatusId() {
		return statusId;
	}
	public void setStatusId(List<String> statusId) {
		this.statusId = statusId;
	}
	public String getExportSakest() {
		return exportSakest;
	}
	public void setExportSakest(String exportSakest) {
		this.exportSakest = exportSakest;
	}
}