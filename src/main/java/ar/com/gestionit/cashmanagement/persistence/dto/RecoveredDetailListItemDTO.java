package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.CodeMapped;
import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.NumberUtil;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"batchId",
		"checkNumber",
		"satCheckNumber",
		"dateGenerated",
		"branchDescription",
		"amount",
		"bankDescription",
		"dateExpiration",
		"refStatus",
		"fechaAcreditacion",
		"tipoDocLibrador",
		"docLibrador",
		"fechaRecaud"
})
@XmlRootElement(name = ServiceConstant.DTO_RECOVCHECK)
public class RecoveredDetailListItemDTO extends AbstractDTO {
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -1600726894333083824L;

	//Inputs/Outputs ---------------------------------------------------------------
	//Estos atributos son para validar si el cheque pertenece a Fiserv
	@XmlTransient
	private String transactionNumber; //Numero de transaccion
	@XmlTransient
	private String collectionMode; //Forma de cobro


	//Ouputs -----------------------------------------------------------------------

	@XmlTransient
	private String transactionDate;	

	//TODO: Es un OUTPUT o no?
	@XmlTransient
	private String firservCompanyMark;	

	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_REQNUM)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_REQNUM, defaultValue="")
	private String batchId;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_CHENUM)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_CHENUM, defaultValue="")
	private String checkNumber;
	@CodeMapped
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_STATUS, defaultValue="")
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_STATUS)
	private String refStatus;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_DATEGENER)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_DATEGENER, defaultValue="")
	private String dateGenerated;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_BRDESC)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_BRDESC, defaultValue="")
	private String branchDescription;
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_AMOUNT, defaultValue="")
	private String amount;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_AMOUNT)
	@XmlTransient
	private String amountForExporter;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_BANKDESC)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_BANKDESC, defaultValue="")
	private String bankDescription;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_DATEEXPIR)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_DATEEXPIR, defaultValue="")
	private String dateExpiration;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_SATCHENUM)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_SATCHENUM, defaultValue="")
	private String satCheckNumber;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_FECACRED)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_FECACRED, defaultValue="")
	private String fechaAcreditacion;
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_TIPODOCLIBR, defaultValue="")
	private String tipoDocLibrador;
	@XmlTransient
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_TIPODOCLIBR)
	private String tipoDocLibradorDescripcion;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_DOCLIBRADOR)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_DOCLIBRADOR, defaultValue="")
	private String docLibrador;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_FECREC)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_FECREC, defaultValue="")
	private String fechaRecaud;
		
	/**
	 * @return the batchId
	 */
	public String getBatchId() {
		return batchId;
	}
	/**
	 * @param batchId the batchId to set
	 */
	public void setBatchId(String batchId) {
		this.batchId = sanitateString(batchId);
	}
	/**
	 * @return the checkNumber
	 */
	public String getCheckNumber() {
		return checkNumber;
	}
	/**
	 * @param checkNumber the checkNumber to set
	 */
	public void setCheckNumber(String checkNumber) {
		this.checkNumber = sanitateString(checkNumber);
	}
	/**
	 * @return the dateGenerated
	 */
	public String getDateGenerated() {
		return dateGenerated;
	}
	/**
	 * @param dateGenerated the dateGenerated to set
	 */
	public void setDateGenerated(String dateGenerated) {
		this.dateGenerated = sanitateString(dateGenerated);
	}
	/**
	 * @return the branchDescription
	 */
	public String getBranchDescription() {
		return branchDescription;
	}
	/**
	 * @param branchDescription the branchDescription to set
	 */
	public void setBranchDescription(String branchDescription) {
		this.branchDescription = branchDescription;
	}
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = sanitateString(amount);
	}
	/**
	 * @return the bankDescription
	 */
	public String getBankDescription() {
		return bankDescription;
	}
	/**
	 * @param bankDescription the bankDescription to set
	 */
	public void setBankDescription(String bankDescription) {
		this.bankDescription = sanitateString(bankDescription);
	}
	/**
	 * @return the dateExpiration
	 */
	public String getDateExpiration() {
		return dateExpiration;
	}
	/**
	 * @param dateExpiration the dateExpiration to set
	 */
	public void setDateExpiration(String dateExpiration) {
		this.dateExpiration = sanitateString(dateExpiration);
	}
	/**
	 * @return the refStatus
	 */
	public String getRefStatus() {
		return refStatus;
	}
	/**
	 * @param refStatus the refStatus to set
	 */
	public void setRefStatus(String refStatus) {
		this.refStatus = sanitateString(refStatus);
	}
	/**
	 * @return the transactionDate
	 */
	public String getTransactionDate() {
		return transactionDate;
	}
	/**
	 * @param transactionDate the transactionDate to set
	 */
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = sanitateString(transactionDate);
	}
	/**
	 * @return the transactionNumber
	 */
	public String getTransactionNumber() {
		return transactionNumber;
	}
	/**
	 * @param transactionNumber the transactionNumber to set
	 */
	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = sanitateString(transactionNumber);
	}
	/**
	 * @return the collectionMode
	 */
	public String getCollectionMode() {
		return collectionMode;
	}
	/**
	 * @param collectionMode the collectionMode to set
	 */
	public void setCollectionMode(String collectionMode) {
		this.collectionMode = sanitateString(collectionMode);
	}
	/**
	 * @return the firservCompanyMark
	 */
	public String getFirservCompanyMark() {
		return firservCompanyMark;
	}
	/**
	 * @param firservCompanyMark the firservCompanyMark to set
	 */
	public void setFirservCompanyMark(String firservCompanyMark) {
		this.firservCompanyMark = firservCompanyMark;
	}
	public String getSatCheckNumber() {
		return satCheckNumber;
	}
	public void setSatCheckNumber(String satCheckNumber) {
		this.satCheckNumber = sanitateString(satCheckNumber);
	}
	/**
	 * @return the fechaAcreditacion
	 */
	public String getFechaAcreditacion() {
		return fechaAcreditacion;
	}
	/**
	 * @param fechaAcreditacion the fechaAcreditacion to set
	 */
	public void setFechaAcreditacion(String fechaAcreditacion) {
		this.fechaAcreditacion = sanitateString(fechaAcreditacion);
	}
	/**
	 * @return the docLibrador
	 */
	public String getDocLibrador() {
		return docLibrador;
	}
	/**
	 * @param docLibrador the docLibrador to set
	 */
	public void setDocLibrador(String docLibrador) {
		this.docLibrador = docLibrador;
	}
	public String getTipoDocLibrador() {
		return tipoDocLibrador;
	}
	public void setTipoDocLibrador(String tipoDocLibrador) {
		this.tipoDocLibrador = sanitateString(tipoDocLibrador);
	}
	public String getFechaRecaud() {
		return fechaRecaud;
	}
	public void setFechaRecaud(String fechaRecaud) {
		this.fechaRecaud = sanitateString(fechaRecaud);
	}
	public String getTipoDocLibradorDescripcion() {
		return tipoDocLibradorDescripcion;
	}
	public void setTipoDocLibradorDescripcion(String tipoDocLibradorDescripcion) {
		this.tipoDocLibradorDescripcion = sanitateString(tipoDocLibradorDescripcion);
	}
	public String getAmountForExporter() {
		return amountForExporter;
	}
	public void setAmountForExporter(String amountForExporter) {
		this.amountForExporter = NumberUtil.amountFormatExporter(amountForExporter);
	}
	
}