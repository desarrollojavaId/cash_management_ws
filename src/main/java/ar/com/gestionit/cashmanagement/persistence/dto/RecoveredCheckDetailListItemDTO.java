package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"nroLote",
		"nroCheq",
		"nomLibrador",
		"fecGenPedido",
		"sucursalEnv",
		"fecEntregaCli",
		"impCheq",
		"bcoCheq",
		"sucursalCheq",
		"codPostalCheq",
		"fecVenc",
		"fecEnvio",
		"docLibrador",
		"tipoDocLibrador",
		"nroCtaGirada"		
})
@XmlRootElement(name = ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_CHEQ)
public class RecoveredCheckDetailListItemDTO extends AbstractDTO{


	/**
	 * 
	 */
	private static final long serialVersionUID = -1867846710423284583L;
	
	
	//Output
	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_NROCHEQOUT, defaultValue="")
	private String nroCheq;
	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_NROLOTEOUT, defaultValue="")
	private String nroLote;	
	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_FECGENPED, defaultValue="")
	private String fecGenPedido;
	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_SUCENV, defaultValue="")
	private String sucursalEnv;
	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_FECENTCLI, defaultValue="")
	private String fecEntregaCli;
	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_IMPCHEQUE, defaultValue="")
	private String impCheq;
	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_BANCOCHE, defaultValue="")
	private String bcoCheq;
	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_SUCCHE, defaultValue="")
	private String sucursalCheq;
	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_CODPOSCHE, defaultValue="")
	private String codPostalCheq;
	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_FECVENC, defaultValue="")
	private String fecVenc;
	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_FECENVSUCLIB, defaultValue="")
	private String fecEnvio;
	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_DOCLIBRADOR, defaultValue="")
	private String docLibrador;
	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_NOMLIBRADOR, defaultValue="")
	private String nomLibrador;
	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_NUMCTAGIRADA, defaultValue="")
	private String nroCtaGirada;
	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_TIPODOCLIBRADOR, defaultValue="")
	private String tipoDocLibrador;
	
	
	public String getFecGenPedido() {
		return fecGenPedido;
	}
	public void setFecGenPedido(String fecGenPedido) {
		this.fecGenPedido = sanitateString(fecGenPedido);
	}
	public String getSucursalEnv() {
		return sucursalEnv;
	}
	public void setSucursalEnv(String sucursalEnv) {
		this.sucursalEnv = sanitateString(sucursalEnv);
	}
	public String getFecEntregaCli() {
		return fecEntregaCli;
	}
	public void setFecEntregaCli(String fecEntregaCli) {
		this.fecEntregaCli = sanitateString(fecEntregaCli);
	}
	public String getImpCheq() {
		return impCheq;
	}
	public void setImpCheq(String impCheq) {
		this.impCheq = sanitateString(impCheq);
	}
	public String getBcoCheq() {
		return bcoCheq;
	}
	public void setBcoCheq(String bcoCheq) {
		this.bcoCheq = sanitateString(bcoCheq);
	}
	public String getSucursalCheq() {
		return sucursalCheq;
	}
	public void setSucursalCheq(String sucursalCheq) {
		this.sucursalCheq = sanitateString(sucursalCheq);
	}
	public String getCodPostalCheq() {
		return codPostalCheq;
	}
	public void setCodPostalCheq(String codPostalCheq) {
		this.codPostalCheq = sanitateString(codPostalCheq);
	}
	public String getFecVenc() {
		return fecVenc;
	}
	public void setFecVenc(String fecVenc) {
		this.fecVenc = sanitateString(fecVenc);
	}
	public String getFecEnvio() {
		return fecEnvio;
	}
	public void setFecEnvio(String fecEnvio) {
		this.fecEnvio = sanitateString(fecEnvio);
	}
	public String getDocLibrador() {
		return docLibrador;
	}
	public void setDocLibrador(String docLibrador) {
		this.docLibrador = sanitateString(docLibrador);
	}
	public String getNomLibrador() {
		return nomLibrador;
	}
	public void setNomLibrador(String nomLibrador) {
		this.nomLibrador = sanitateString(nomLibrador);
	}
	public String getNroCtaGirada() {
		return nroCtaGirada;
	}
	public void setNroCtaGirada(String nroCtaGirada) {
		this.nroCtaGirada = sanitateString(nroCtaGirada);
	}
	public String getNroCheq() {
		return nroCheq;
	}
	public void setNroCheq(String nroCheq) {
		this.nroCheq = sanitateString(nroCheq);
	}
	public String getNroLote() {
		return nroLote;
	}
	public void setNroLote(String nroLote) {
		this.nroLote = sanitateString(nroLote);
	}
	public String getTipoDocLibrador() {
		return tipoDocLibrador;
	}
	public void setTipoDocLibrador(String tipoDocLibrador) {
		this.tipoDocLibrador = sanitateString(tipoDocLibrador);
	}
	
}
