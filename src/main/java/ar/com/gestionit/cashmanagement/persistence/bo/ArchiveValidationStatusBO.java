package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.ArchiveValidationStatusListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileAccountListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.ArchiveValidationStatusMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

public class ArchiveValidationStatusBO extends AbstractBO<IDTO, ArchiveValidationStatusMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = ArchiveValidationStatusMapper.class;
	}
	
	public List<FileAccountListItemDTO> findAccounts(ArchiveValidationStatusListItemDTO input) throws ServiceException {
		SqlSession session = null;
		try {
			//Load paging data
			loadPagingData(input);

			//Get result list
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			ArchiveValidationStatusMapper mapper = session.getMapper(ArchiveValidationStatusMapper.class);
			List<FileAccountListItemDTO> result = mapper.findAccounts(input);
			queryCounter++;

			//Process paging
			processResultPaging(result);

			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}
