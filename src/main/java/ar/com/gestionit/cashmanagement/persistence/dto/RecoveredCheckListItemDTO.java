package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.CodeMapped;
import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={
		"batchId",
		"checkNumber",
		"satCheckNumber",
		"docLibradorTipo",
		"docLibrador",
		"bankCode",
		"bankDescription",
		"amount",
		"fecChe",
		"dateAcred",
		"inProcess",
		"dateExpiration",
		"refStatus",
		"status",
		"branchDescription",
		"codPostal",
		"transactionDate",
		"nroCtaGirada",
		"sucursalEnv"
})
@XmlRootElement(name = ServiceConstant.DTO_RECOVCHECK)
public class RecoveredCheckListItemDTO extends AbstractDTO {
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -1600726894333083824L;

	//Inputs/Outputs ---------------------------------------------------------------
	//Estos atributos son para validar si el cheque pertenece a Fiserv
	@XmlTransient
	private String transactionNumber; //Numero de transaccion
	@XmlTransient
	private String collectionMode; //Forma de cobro


	//Ouputs -----------------------------------------------------------------------
	//TODO: Es un OUTPUT o no?
	@XmlTransient
	private String firservCompanyMark;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_REQNUM)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_REQNUM, defaultValue="")
	private String batchId;
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_STATUSCODE, defaultValue="")
	private String refStatus;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_FECREC)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_FECREC, defaultValue="")
	private String transactionDate;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_CHENUM2)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_CHENUM2, defaultValue="")
	private String checkNumber;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_STATUS2)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_STATUS2, defaultValue="")
	private String status;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_BRDESC2)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_BRDESC2, defaultValue="")
	private String branchDescription;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_AMOUNT)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_AMOUNT, defaultValue="")
	private String amount;
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_BANCHECOD, defaultValue="")
	private String bankCode;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_BANKDESC2)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_BANKDESC2, defaultValue="")
	private String bankDescription;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_DATEEXPIR2)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_DATEEXPIR2, defaultValue="")
	private String dateExpiration;
	@CodeMapped
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_TIPODOCLIBR)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_TIPODOCLIBR, defaultValue="")
	private String docLibradorTipo;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_DOCLIBR)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_DOCLIBR, defaultValue="")
	private String docLibrador;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_FECACRE)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_FECACRE, defaultValue="")
	private String dateAcred;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_CODPOS)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_CODPOS, defaultValue="")
	private String codPostal;
	@CodeMapped
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_RECENPROC)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_RECENPROC, defaultValue="")
	private String inProcess;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_SATCHENUM)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_SATCHENUM, defaultValue="")
	private String satCheckNumber;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_FECCHE)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_FECCHE, defaultValue="")
	private String fecChe;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_NUMCTAGIRADA)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_NUMCTAGIRADA, defaultValue="")
	private String nroCtaGirada;
	@FieldToExport(name=ServiceConstant.DTO_RECOVCHECK_SUCENV)
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK_SUCENV, defaultValue="")
	private String sucursalEnv;
	/**
	 * @return the checkNumber
	 */
	public String getCheckNumber() {
		return checkNumber;
	}
	/**
	 * @param checkNumber the checkNumber to set
	 */
	public void setCheckNumber(String checkNumber) {
		this.checkNumber = sanitateString(checkNumber);
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = sanitateString(status);
	}
	/**
	 * @return the branchDescription
	 */
	public String getBranchDescription() {
		return branchDescription;
	}
	/**
	 * @param branchDescription the branchDescription to set
	 */
	public void setBranchDescription(String branchDescription) {
		this.branchDescription = sanitateString(branchDescription);
	}
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = sanitateString(amount);
	}
	/**
	 * @return the bankDescription
	 */
	public String getBankDescription() {
		return bankDescription;
	}
	/**
	 * @param bankDescription the bankDescription to set
	 */
	public void setBankDescription(String bankDescription) {
		this.bankDescription = sanitateString(bankDescription);
	}
	/**
	 * @return the dateExpiration
	 */
	public String getDateExpiration() {
		return dateExpiration;
	}
	/**
	 * @param dateExpiration the dateExpiration to set
	 */
	public void setDateExpiration(String dateExpiration) {
		this.dateExpiration = sanitateString(dateExpiration);
	}
	/**
	 * @return the refStatus
	 */
	public String getRefStatus() {
		return refStatus;
	}
	/**
	 * @param refStatus the refStatus to set
	 */
	public void setRefStatus(String refStatus) {
		this.refStatus = sanitateString(refStatus);
	}
	/**
	 * @return the transactionDate
	 */
	public String getTransactionDate() {
		return transactionDate;
	}
	/**
	 * @param transactionDate the transactionDate to set
	 */
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = sanitateString(transactionDate);
	}
	/**
	 * @return the transactionNumber
	 */
	public String getTransactionNumber() {
		return transactionNumber;
	}
	/**
	 * @param transactionNumber the transactionNumber to set
	 */
	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = sanitateString(transactionNumber);
	}
	/**
	 * @return the collectionMode
	 */
	public String getCollectionMode() {
		return collectionMode;
	}
	/**
	 * @param collectionMode the collectionMode to set
	 */
	public void setCollectionMode(String collectionMode) {
		this.collectionMode = sanitateString(collectionMode);
	}
	/**
	 * @return the firservCompanyMark
	 */
	public String getFirservCompanyMark() {
		return firservCompanyMark;
	}
	/**
	 * @param firservCompanyMark the firservCompanyMark to set
	 */
	public void setFirservCompanyMark(String firservCompanyMark) {
		this.firservCompanyMark = sanitateString(firservCompanyMark);
	}
	/**
	 * @return the docLibrador
	 */
	public String getDocLibrador() {
		return docLibrador;
	}
	/**
	 * @param docLibrador the docLibrador to set
	 */
	public void setDocLibrador(String docLibrador) {
		this.docLibrador = sanitateString(docLibrador);
	}
	/**
	 * @return the dateAcred
	 */
	public String getDateAcred() {
		return dateAcred;
	}
	/**
	 * @param dateAcred the dateAcred to set
	 */
	public void setDateAcred(String dateAcred) {
		this.dateAcred = sanitateString(dateAcred);
	}
	/**
	 * @return the codPostal
	 */
	public String getCodPostal() {
		return codPostal;
	}
	/**
	 * @param codPostal the codPostal to set
	 */
	public void setCodPostal(String codPostal) {
		this.codPostal = sanitateString(codPostal);
	}
	/**
	 * @return the inProcess
	 */
	public String getInProcess() {
		return inProcess;
	}
	/**
	 * @param inProcess the inProcess to set
	 */
	public void setInProcess(String inProcess) {
		this.inProcess = inProcess;
	}
	public String getSatCheckNumber() {
		return satCheckNumber;
	}
	public void setSatCheckNumber(String satCheckNumber) {
		this.satCheckNumber = sanitateString(satCheckNumber);
	}
	public String getFecChe() {
		return fecChe;
	}
	public void setFecChe(String fecChe) {
		this.fecChe = sanitateString(fecChe);
	}
	/**
	 * @return the docLibradorTipo
	 */
	public String getDocLibradorTipo() {
		return docLibradorTipo;
	}
	/**
	 * @param docLibradorTipo the docLibradorTipo to set
	 */
	public void setDocLibradorTipo(String docLibradorTipo) {
		this.docLibradorTipo = sanitateString(docLibradorTipo);
	}
	public String getNroCtaGirada() {
		return sanitateString(nroCtaGirada);
	}
	public void setNroCtaGirada(String nroCtaGirada) {
		this.nroCtaGirada = sanitateString(nroCtaGirada);
	}
	public String getSucursalEnv() {
		return sucursalEnv;
	}
	public void setSucursalEnv(String sucursalEnv) {
		this.sucursalEnv = sanitateString(sucursalEnv);
	}
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = sanitateString(batchId);
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = sanitateString(bankCode);
	}
}