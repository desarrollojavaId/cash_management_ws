package ar.com.gestionit.cashmanagement.persistence.dto;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name = ServiceConstant.DTO_FILERECEPTION_ROOT)

public class ArchiveValidationStatusListDTO extends AbstractDTO{
	
	private static final long serialVersionUID = 42738737724173822L;
	
	@XmlElement(name=ServiceConstant.DTO_FILE)
	private List<ArchiveValidationStatusListItemDTO> list;

	/**
	 * @return the list
	 */
	public List<ArchiveValidationStatusListItemDTO> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<ArchiveValidationStatusListItemDTO> list) {
		this.list = list;
	}
	
	public void setProof(ArchiveValidationStatusListItemDTO dto) {
		if(list == null)
			list = new ArrayList<ArchiveValidationStatusListItemDTO>();
		list.add(dto);
	}

}
