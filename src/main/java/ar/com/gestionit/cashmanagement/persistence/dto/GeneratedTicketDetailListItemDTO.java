package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"montoAdeudado",
		"moneda",
		"empresa",
		"tipoComprob",
		"monto"
})
@XmlRootElement(name = ServiceConstant.DTO_GENERATEDTICKET_DETAIL)
public class GeneratedTicketDetailListItemDTO extends AbstractDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1699107084335352249L;
	
	@XmlElement(name=ServiceConstant.DTO_GENERATEDTICKET_MONTOADEUDADO, defaultValue="")
	private String montoAdeudado;
	@XmlElement(name=ServiceConstant.DTO_GENERATEDTICKET_MONEDA, defaultValue="")
	private String moneda;
	@XmlElement(name=ServiceConstant.DTO_GENERATEDTICKET_EMPRESA, defaultValue="")
	private String empresa;
	@XmlElement(name=ServiceConstant.DTO_GENERATEDTICKET_TIPOCOMPROB, defaultValue="")
	private String tipoComprob;
	@XmlElement(name=ServiceConstant.DTO_GENERATEDTICKET_MONTO, defaultValue="")
	private String monto;
	public String getMontoAdeudado() {
		return montoAdeudado;
	}
	public void setMontoAdeudado(String montoAdeudado) {
		this.montoAdeudado = sanitateString(montoAdeudado);
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = sanitateString(moneda);
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = sanitateString(empresa);
	}
	public String getTipoComprob() {
		return tipoComprob;
	}
	public void setTipoComprob(String tipoComprob) {
		this.tipoComprob = sanitateString(tipoComprob);
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = sanitateString(monto);
	}

	
}
