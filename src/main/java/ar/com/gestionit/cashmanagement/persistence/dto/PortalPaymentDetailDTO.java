package ar.com.gestionit.cashmanagement.persistence.dto;

public class PortalPaymentDetailDTO extends AbstractDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1305889766390182689L;
	
	//Inputs 
	private String paymentNumber;
	private String paymentSubNumber;
	
	//Outputs 
	//Cuit
	private String mtlqcp;
	//Nombre esmpresa
	private String mtlnep;
	//Instrumento
	private String mtlqip;
	//Estado
	private String mtlkes;
	//Medio de ejecucion
	private String mtlkme;
	//Fma Pago
	private String mtlkfp;
	//Importe
	private String mtljpa;
	//Fch. Pago
	private String mtlfer;
	//Fch. Emision
	private String mtlfal;
	//Adh
	private String mtlqca;
	//Ref interna
	private String mtlqrc;
		
	
	public String getPaymentNumber() {
		return paymentNumber;
	}
	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = sanitateString(paymentNumber);
	}
	public String getPaymentSubNumber() {
		return paymentSubNumber;
	}
	public void setPaymentSubNumber(String paymentSubNumber) {
		this.paymentSubNumber = sanitateString(paymentSubNumber);
	}
	public String getMtlqcp() {
		return mtlqcp;
	}
	public void setMtlqcp(String mtlqcp) {
		this.mtlqcp = sanitateString(mtlqcp);
	}
	public String getMtlnep() {
		return mtlnep;
	}
	public void setMtlnep(String mtlnep) {
		this.mtlnep = sanitateString(mtlnep);
	}
	public String getMtlqip() {
		return mtlqip;
	}
	public void setMtlqip(String mtlqip) {
		this.mtlqip = sanitateString(mtlqip);
	}
	public String getMtlkes() {
		return mtlkes;
	}
	public void setMtlkes(String mtlkes) {
		this.mtlkes = sanitateString(mtlkes);
	}
	public String getMtlkfp() {
		return mtlkfp;
	}
	public void setMtlkfp(String mtlkfp) {
		this.mtlkfp = sanitateString(mtlkfp);
	}
	public String getMtljpa() {
		return mtljpa;
	}
	public void setMtljpa(String mtljpa) {
		this.mtljpa = sanitateString(mtljpa);
	}
	public String getMtlfer() {
		return mtlfer;
	}
	public void setMtlfer(String mtlfer) {
		this.mtlfer = sanitateString(mtlfer);
	}
	public String getMtlfal() {
		return mtlfal;
	}
	public void setMtlfal(String mtlfal) {
		this.mtlfal = sanitateString(mtlfal);
	}
	public String getMtlqca() {
		return mtlqca;
	}
	public void setMtlqca(String mtlqca) {
		this.mtlqca = sanitateString(mtlqca);
	}
	public String getMtlqrc() {
		return mtlqrc;
	}
	public void setMtlqrc(String mtlqrc) {
		this.mtlqrc = sanitateString(mtlqrc);
	}
	/**
	 * @return the mtlkme
	 */
	public String getMtlkme() {
		return mtlkme;
	}
	/**
	 * @param mtlkme the mtlkme to set
	 */
	public void setMtlkme(String mtlkme) {
		this.mtlkme = mtlkme;
	}
}