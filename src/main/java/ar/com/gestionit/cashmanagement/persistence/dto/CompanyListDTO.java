package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name = ServiceConstant.DTO_COMPANYLIST_ROOT)

public class CompanyListDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8151871010869313688L;
	
	@XmlTransient
	private String cuit;
	@XmlElement(name=ServiceConstant.DTO_COMPANYLIST, defaultValue="")
	private List<CompanyListItemDTO> list;

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = sanitateString(cuit);
	}

	public List<CompanyListItemDTO> getList() {
		return list;
	}

	public void setList(List<CompanyListItemDTO> list) {
		this.list = list;
	}
	
	
	

}
