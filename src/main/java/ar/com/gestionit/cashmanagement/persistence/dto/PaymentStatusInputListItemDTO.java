package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"number"
		})
@XmlRootElement(name = ServiceConstant.TAG_STATUS)
public class PaymentStatusInputListItemDTO extends AbstractDTO {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -7798867926488859811L;

	@XmlElement(name=ServiceConstant.TAG_STATUS_ID, defaultValue="")
	public String number;
	//@XmlElement(name=ServiceConstant.TAG_STATUS_SID, defaultValue="")
	@XmlTransient
	public String subnumber;
	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * @return the subnumber
	 */
	
	public String getSubnumber() {
		return subnumber;
	}
	/**
	 * @param subnumber the subnumber to set
	 */
	public void setSubnumber(String subnumber) {
		this.subnumber = subnumber;
	}
}