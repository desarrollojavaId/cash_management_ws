package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"id"
		})
@XmlRootElement(name = ServiceConstant.DTO_FILERECEPTION_STATUSID)
public class FileReceptionStatusesItemDTO extends AbstractDTO {


	/**
	 * 
	 */
	private static final long serialVersionUID = -5427771640491330390L;
	@XmlElement(name=ServiceConstant.DTO_FILERECEPTION_ID, defaultValue="")
	public String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}