package ar.com.gestionit.cashmanagement.persistence.bo;

import ar.com.gestionit.cashmanagement.persistence.dto.EscrowChecksumDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.PersistEscrowChecksumMapper;

public class PersistEscrowChecksumBO extends AbstractBO<EscrowChecksumDTO, PersistEscrowChecksumMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = PersistEscrowChecksumMapper.class;
	}
}