package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.CodeMapped;
import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"idArch",
		"nomArch",
		"fecIngArch",
		"tipoArch",
		"estado",
		"impTotal",
		"cantReg",
		"paymentWayList",
		"moneda",
		"nroConvenio"
})
@XmlRootElement(name = ServiceConstant.DTO_STATUS_SEND_FILES)
public class FileStatusSendListItemDTO extends AbstractDTO{

	private static final long serialVersionUID = -4991324892118658096L;
	
	//Output
	
	@FieldToExport(name=ServiceConstant.DTO_STATUS_SEND_FILES_IDARCH)
	@XmlElement(name=ServiceConstant.DTO_STATUS_SEND_FILES_IDARCH, defaultValue="")
	private String idArch; //
	
	@FieldToExport(name=ServiceConstant.DTO_STATUS_SEND_FILES_NOMARCH)
	@XmlElement(name=ServiceConstant.DTO_STATUS_SEND_FILES_NOMARCH, defaultValue="")
	private String nomArch; 
	
	@FieldToExport(name=ServiceConstant.DTO_STATUS_SEND_FILES_FECINGARCH)
	@XmlElement(name=ServiceConstant.DTO_STATUS_SEND_FILES_FECINGARCH, defaultValue="")
	private String fecIngArch;
	
	@FieldToExport(name=ServiceConstant.DTO_STATUS_SEND_FILES_TIPOARCH)
	@XmlElement(name=ServiceConstant.DTO_STATUS_SEND_FILES_TIPOARCH, defaultValue="")
	private String tipoArch;
	
	@FieldToExport(name=ServiceConstant.DTO_STATUS_SEND_FILES_ESTADO)
	@XmlElement(name=ServiceConstant.DTO_STATUS_SEND_FILES_ESTADO, defaultValue="")
	private String estado;
	
	@FieldToExport(name=ServiceConstant.DTO_STATUS_SEND_FILES_MONEDA)
	@XmlElement(name=ServiceConstant.DTO_STATUS_SEND_FILES_MONEDA, defaultValue="")
	private String moneda;
	
	@FieldToExport(name=ServiceConstant.DTO_STATUS_SEND_FILES_IMPTOTAL)
	@XmlElement(name=ServiceConstant.DTO_STATUS_SEND_FILES_IMPTOTAL, defaultValue="")
	private String impTotal;

	@FieldToExport(name=ServiceConstant.DTO_STATUS_SEND_FILES_CANTREG)
	@XmlElement(name=ServiceConstant.DTO_STATUS_SEND_FILES_CANTREG, defaultValue="")
	private String cantReg;
	
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_WAY_FORMASDEPAGO)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_WAY_FORMASDEPAGO, defaultValue="")
	private PaymentWayListDTO paymentWayList;
	
	@FieldToExport(name=ServiceConstant.DTO_STATUS_SEND_FILES_NROCONVENIO)
	@XmlElement(name=ServiceConstant.DTO_STATUS_SEND_FILES_NROCONVENIO, defaultValue="")
	private String nroConvenio;
	
	@XmlTransient
	private String fileId; 

	public String getIdArch() {
		return idArch;
	}

	public void setIdArch(String idArch) {
		this.idArch = idArch;
	}

	public String getNomArch() {
		return nomArch;
	}

	public void setNomArch(String nomArch) {
		this.nomArch = nomArch;
	}

	public String getFecIngArch() {
		return fecIngArch;
	}

	public void setFecIngArch(String fecIngArch) {
		this.fecIngArch = fecIngArch;
	}

	public String getTipoArch() {
		return tipoArch;
	}

	public void setTipoArch(String tipoArch) {
		this.tipoArch = tipoArch;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getImpTotal() {
		return impTotal;
	}

	public void setImpTotal(String impTotal) {
		this.impTotal = impTotal;
	}

	public String getCantReg() {
		return cantReg;
	}

	public void setCantReg(String cantReg) {
		this.cantReg = cantReg;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public PaymentWayListDTO getPaymentWayList() {
		return paymentWayList;
	}

	public void setPaymentWayList(PaymentWayListDTO paymentWayList) {
		this.paymentWayList = paymentWayList;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getNroConvenio() {
		return nroConvenio;
	}
//Se debe eliminar esta funcion ya que es a modo de prueba por pedido del banco para el ambiente de desa.
	//Cuando vuelva Sonia de vacaciones se deberá crear una nueva columna en el sstrans para indicar el convenio en los archivos enviados
	public void setNroConvenio(String nroconvenio) {
		String temp = sanitateString(nroconvenio);
		this.nroConvenio = temp.substring(temp.indexOf("-")+1, temp.length());
	}

	}