package ar.com.gestionit.cashmanagement.persistence.bo;



import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.FileListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.ErrorsQueryMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

public class ErrorsQueryBO extends AbstractBO<IDTO, ErrorsQueryMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = ErrorsQueryMapper.class;
	}
	
	public FileListItemDTO findStatus(FileListItemDTO dto2) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			ErrorsQueryMapper mapper = session.getMapper(mapperInterface);
			FileListItemDTO result = mapper.findStatus(dto2);
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}