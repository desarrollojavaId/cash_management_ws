package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"contenido"})
@XmlRootElement(name = ServiceConstant.DTO_STATUS_SEND_FILES)
public class FileStatusDownloadItemDTO extends AbstractDTO{

	private static final long serialVersionUID = -708341426856672627L;

	//input
	@XmlTransient
	private String adherent;
	
	@XmlTransient
	private String token; //
	
	@XmlTransient
	private String contenido; //
	
	
	
//	@XmlTransient
//	private String empresa;	//ssclient razon social
//	@XmlTransient
//	private String convenio; //
	
	//Outputs
	@XmlElement(name=ServiceConstant.DTO_STATUS_DOWNLOAD, defaultValue="")
	private String estado; //
	
	
	public String getAdherent() {
		return adherent;
	}

	public void setAdherent(String adherent) {
		this.adherent = adherent;
	}

		
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	

	
	
}