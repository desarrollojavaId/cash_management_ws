package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"
})
@XmlRootElement(name = ServiceConstant.DTO_PORTALPAYMENT_ROOT)
public class PortalPaymentListDTO extends AbstractDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 310545415758822419L;
	
	@XmlTransient
	private String estado; 
	@XmlTransient
	private List<StatusInputListItemDTO> lstEstados;
	@XmlTransient
	private String cuitBeneficiario;
	@XmlTransient
	private String cuit;
	@XmlTransient
	private String formaPago; 
	@XmlTransient
	private String fechaPagoDesde;
	@XmlTransient
	private String fechaPagoHasta;
	@XmlTransient
	private String fechaEmisionDesde;
	@XmlTransient
	private String fechaEmisionHasta;
	@XmlTransient
	private String impDesde;
	@XmlTransient
	private String impHasta;
	@XmlElement(name=ServiceConstant.DTO_PORTALPAYMENT, defaultValue="")
	private List<PortalPaymentListItemDTO> list;

	
	public List<PortalPaymentListItemDTO> getList() {
		return list;
	}

	public void setList(List<PortalPaymentListItemDTO> list) {
		this.list = list;
	}
	
	public List<StatusInputListItemDTO> getLstEstados() {
		return lstEstados;
	}

	public void setLstEstados(List<StatusInputListItemDTO> lstEstados) {
		this.lstEstados = lstEstados;
	}

	
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = sanitateString(estado);
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = sanitateString(cuit);
	}
	
	
	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = sanitateString(formaPago);
	}

	public String getFechaPagoDesde() {
		return fechaPagoDesde;
	}

	public void setFechaPagoDesde(String fechaPagoDesde) {
		this.fechaPagoDesde = sanitateString(fechaPagoDesde);
	}

	public String getFechaPagoHasta() {
		return fechaPagoHasta;
	}

	public void setFechaPagoHasta(String fechaPagoHasta) {
		this.fechaPagoHasta = sanitateString(fechaPagoHasta);
	}

	public String getFechaEmisionDesde() {
		return fechaEmisionDesde;
	}

	public void setFechaEmisionDesde(String fechaEmisionDesde) {
		this.fechaEmisionDesde = sanitateString(fechaEmisionDesde);
	}

	public String getFechaEmisionHasta() {
		return fechaEmisionHasta;
	}

	public void setFechaEmisionHasta(String fechaEmisionHasta) {
		this.fechaEmisionHasta = sanitateString(fechaEmisionHasta);
	}

	public String getImpDesde() {
		return impDesde;
	}

	public void setImpDesde(String impDesde) {
		this.impDesde = sanitateString(impDesde);
	}

	public String getImpHasta() {
		return impHasta;
	}

	public void setImpHasta(String impHasta) {
		this.impHasta = sanitateString(impHasta);
	}

	public String getCuitBeneficiario() {
		return cuitBeneficiario;
	}

	public void setCuitBeneficiario(String cuitBeneficiario) {
		this.cuitBeneficiario = sanitateString(cuitBeneficiario);
	}
	
}
