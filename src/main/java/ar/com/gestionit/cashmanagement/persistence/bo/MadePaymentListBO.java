package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.MadePaymentListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ReferenceDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StoredProcedureParametersDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.MadePaymentListMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.StringUtil;

public class MadePaymentListBO extends AbstractBO<IDTO, MadePaymentListMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = MadePaymentListMapper.class;
	}
	
	public String findChannelDescription(MadePaymentListDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			MadePaymentListMapper mapper = session.getMapper(MadePaymentListMapper.class);
			List<String> result = mapper.findChannels(dto);
			session.close();
			queryCounter++;
			
			return result != null && result.size() > 0 ? result.get(0) : null;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	public String generateReport(MadePaymentListDTO d) throws ServiceException {
		SqlSession session = null;
		try {
			StoredProcedureParametersDTO dto = new StoredProcedureParametersDTO();
			dto.setParam1(d.getRequestNumber());
			dto.setParam2("");
			dto.setParam3("");
			dto.setParam4("");
			dto.setParam5(d.getCompany());
			dto.setParam6(d.getAgreementNumber());
			dto.setParam7(d.getAgreementSubnumber());
			dto.setParam8(d.getChannel());
			dto.setParam9(d.getDateFrom());
			dto.setParam10(d.getDateTo());
			dto.setParam11(d.getAmountFrom());
			dto.setParam12(d.getAmountTo());
			
			if(d.getReferences() != null) {
				ReferenceDTO rd = d.getReferences();
				dto.setParam13(StringUtil.notNull(rd.getComprobante1()));
				dto.setParam14(StringUtil.notNull(rd.getComprobante2()));
				dto.setParam15(StringUtil.notNull(rd.getComprobante3()));
				dto.setParam16(StringUtil.notNull(rd.getComprobante4()));
				dto.setParam17(StringUtil.notNull(rd.getComprobante5()));
			} else {
				dto.setParam13("");
				dto.setParam14("");
				dto.setParam15("");
				dto.setParam16("");
				dto.setParam17("");
			}

			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			MadePaymentListMapper mapper = session.getMapper(MadePaymentListMapper.class);
			mapper.generateReport(dto);
			session.close();
			queryCounter++;

			return dto.getParam1();
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}