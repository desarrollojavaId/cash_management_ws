package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"
})
@XmlRootElement(name = ServiceConstant.DTO_ADHERENTLISTFORMEP_ROOT)
public class AdherentListForMepDTO extends AbstractDTO{

	private static final long serialVersionUID = -1414758864739578889L;

	@XmlElement(name=ServiceConstant.DTO_ADHERENTLISTFORMEP, defaultValue="")
	private List<String> list;

	/**
	 * @return the list
	 */
	public List<String> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<String> list) {
		this.list = list;
	}
}