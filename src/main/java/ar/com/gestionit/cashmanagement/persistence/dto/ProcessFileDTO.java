package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"signatoryList",
		})
@XmlRootElement(name=ServiceConstant.DTO_SIGNATORY_ROOT)
public class ProcessFileDTO extends AbstractDTO {
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 4969943919872788248L;
	

	@XmlTransient
	private int stqtrs;
	@XmlTransient
	private String adherent;
	@XmlTransient
	private String fileId;
	@XmlTransient
	private String newSchema;
	@XmlTransient
	private String action;
	@XmlTransient
	private String ticket;
	@XmlElement(name=ServiceConstant.DTO_SIGNATORY)
	private List<SignatoryListItemDTO> signatoryList;
	/**
	 * @return the adherent
	 */
	public String getAdherent() {
		return adherent;
	}
	/**
	 * @param adherent the adherent to set
	 */
	public void setAdherent(String adherent) {
		this.adherent = adherent;
	}
	/**
	 * @return the fileId
	 */
	public String getFileId() {
		return fileId;
	}
	/**
	 * @param fileId the fileId to set
	 */
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	/**
	 * @return the newSchema
	 */
	public String getNewSchema() {
		return newSchema;
	}
	/**
	 * @param newSchema the newSchema to set
	 */
	public void setNewSchema(String newSchema) {
		this.newSchema = newSchema;
	}
	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}
	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}
	/**
	 * @return the ticket
	 */
	public String getTicket() {
		return ticket;
	}
	/**
	 * @param ticket the ticket to set
	 */
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	/**
	 * @return the signatoryList
	 */
	public List<SignatoryListItemDTO> getSignatoryList() {
		return signatoryList;
	}
	/**
	 * @param signatoryList the signatoryList to set
	 */
	public void setSignatoryList(List<SignatoryListItemDTO> signatoryList) {
		this.signatoryList = signatoryList;
	}
	public int getStqtrs() {
		return stqtrs;
	}
	public void setStqtrs(int stqtrs) {
		this.stqtrs = stqtrs;
	}
	
	
	
}