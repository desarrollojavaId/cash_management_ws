package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

/**
 * This DTO is used to get data about the list paging
 * @author jdigruttola
 */
@XmlAccessorType(XmlAccessType.FIELD )
@XmlRootElement(name = ServiceConstant.TAG_PAGER_INPUT)
public class PagerInputDTO extends AbstractDTO {
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -3312541370975643689L;
	
	/**
	 * Page number
	 */
	@XmlElement(name=ServiceConstant.TAG_PAGER_INPUT_NUMBER, defaultValue="")
	private int pageNumber;
	/**
	 * Amount of register per page
	 */
	@XmlElement(name=ServiceConstant.TAG_PAGER_INPUT_AMOUNT, defaultValue="")
	private int amount;
	/**
	 * @return the pageNumber
	 */
	public int getPageNumber() {
		return pageNumber;
	}
	/**
	 * @param pageNumber the pageNumber to set
	 */
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	/**
	 * @return the amount
	 */
	public int getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(int amount) {
		this.amount = amount;
	}
}