package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.ErrorListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ErrorListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileAccountListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.SendFileDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StoredProcedureParametersDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.SendFileMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;

public class SendFileBO extends AbstractBO<SendFileDTO, SendFileMapper>{

	@Override
	protected void specifyMapper() {
		mapperInterface = SendFileMapper.class;
	}

	public SendFileDTO findDefaultCBU(SendFileDTO dto) throws ServiceException {
		if(dto == null)
			return dto;
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			SendFileMapper mapper = session.getMapper(SendFileMapper.class);
			dto.setCbu(mapper.findDefaultCBU(dto.getAdherent()));
			queryCounter++;
			session.close();
			return dto;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	/**
	 * This method gets the account data according to a specified CBU given by argument
	 * 
	 * Fields:
	 * ---------------------------------------------------------------
	 * CuentaBT       
	 * SucursalBPS    
	 * SubOperacion   
	 * Modulo         
	 * Moneda         
	 * PPapel         
	 * POperacion     
	 * PTipoOper      
	 * PEmpresa
	 * 
	 * @param dto
	 * @return
	 */
	public FileAccountListItemDTO loadAccount(String cbu) throws ServiceException {
		FileAccountListItemDTO result = new FileAccountListItemDTO();

		StoredProcedureParametersDTO spDto = new StoredProcedureParametersDTO();
		String aux = "00000000000000000000000" + cbu;
		spDto.setParam1(aux.substring(aux.length()-22, aux.length()));
		spDto.setParam2("0");
		spDto.setParam3("0");
		spDto.setParam4("0");
		spDto.setParam5("0");
		spDto.setParam6("0");
		spDto.setParam7("0");
		spDto.setParam8("0");
		spDto.setParam9("0");
		spDto.setParam10("0");

		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			SendFileMapper mapper = session.getMapper(SendFileMapper.class);
			mapper.loadAccount(spDto);
			queryCounter++;
			session.commit();
			session.close();
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}

		try {
			result.setAccountCbu(spDto.getParam1());
			result.setAccountNumber(spDto.getParam2());
			result.setAccountBranch(spDto.getParam3());
			result.setAccountSuboperation(spDto.getParam4());
			result.setAccountModule(spDto.getParam5());
			result.setAccountCurrency(spDto.getParam6());
			result.setAccountPaper(spDto.getParam7());
			result.setAccountOperation(spDto.getParam8());
			result.setAccountOperationType(spDto.getParam9());
			result.setAccountCompany(spDto.getParam10());

			return result;
		} catch (Throwable e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		}	
	}

	public int insertAccount(FileAccountListItemDTO dto) throws ServiceException {
		if(dto == null)
			return 0;
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			SendFileMapper mapper = session.getMapper(SendFileMapper.class);
			int result = mapper.insertAccount(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	public int registerInSAT(SendFileDTO dto) throws ServiceException {
		if(dto == null)
			return 0;
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			SendFileMapper mapper = session.getMapper(SendFileMapper.class);
			int result = mapper.registerForSAT(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	public int insertFileContent(SendFileDTO dto) throws ServiceException {
		if(dto == null){
			return 0;
		}
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			SendFileMapper mapper = session.getMapper(SendFileMapper.class);
			int result = mapper.insertFileContent(dto);
			queryCounter++;
			session.commit();
			session.close();
			return result;
		} catch(Exception e) {
			if (session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		}finally{
			if (session != null)
				session.close();
			}
		}
	

	public SendFileDTO getFileStatus(SendFileDTO dto) throws ServiceException {
		if(dto == null)
			return null;
		try {
			SqlSession session = CashManagementWsApplication.sqlSessionFactory.openSession();
			SendFileMapper mapper = session.getMapper(SendFileMapper.class);
			String result = mapper.getFileStatus(dto);
			queryCounter++;
			session.close();
			dto.setFileStatus(result);
			return dto;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		}
	}

	/**
	 * This method gets the locations in the file about the important data to report (amount total, register number in the file,...)
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public SendFileDTO findFileData(SendFileDTO dto) throws ServiceException {
		if(dto == null)
			return null;
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			SendFileMapper mapper = session.getMapper(SendFileMapper.class);
			SendFileDTO result = mapper.findFileData(dto);
			queryCounter++;
			session.close();

			if(result == null) {
				DefinedLogger.SERVICE.error("No se pudo obtener los campos SRQFIR, SRQFII de la tabla SSARCREC acorde al tipo de archivo " + dto.getFileType());
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC);
			}

			dto.setLocationAmountTotal(result.getLocationAmountTotal());
			dto.setLocationRegisterNumber(result.getLocationRegisterNumber());

			return dto;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	public SendFileDTO getLibraryAndTable(SendFileDTO dto) throws ServiceException {
		if(dto == null || dto.getFileType() == null || dto.getFileType().trim().equals(""))
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFILETYPE);

		StoredProcedureParametersDTO spDto = new StoredProcedureParametersDTO();
		spDto.setParam1(dto.getFileType());
		spDto.setParam2(" ");
		
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			SendFileMapper mapper = session.getMapper(SendFileMapper.class);
			mapper.getLibraryAndTable(spDto);
			queryCounter++;
			session.commit();

			dto.setTable(spDto.getParam1());
			dto.setLibrary(spDto.getParam2());

			return dto;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	public SendFileDTO findFileErrors(SendFileDTO dto) throws ServiceException {
		if(dto == null)
			return null;
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			SendFileMapper mapper = session.getMapper(SendFileMapper.class);
			List<ErrorListItemDTO> result = mapper.findFileErrors(dto);
			queryCounter++;

			if(result == null || result.size() == 0) {
				DefinedLogger.SERVICE.error("No se pudo hallar errores para el archivo " + dto.getTable());
			} else {
				ErrorListDTO errorList = new ErrorListDTO();
				errorList.setList(result);
				dto.setErrorList(errorList);
			}
			return dto;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	public SendFileDTO findChecksumData(SendFileDTO dto) throws ServiceException {
		if(dto == null)
			return dto;
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			SendFileMapper mapper = session.getMapper(SendFileMapper.class);
			dto = mapper.findChecksumData(dto.getAdherent());
			queryCounter++;
			return dto;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	public String verifyAdherenteForPD(SendFileDTO dto) throws ServiceException {
		if(dto == null)
			return null;
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			SendFileMapper mapper = session.getMapper(SendFileMapper.class);
			String result = mapper.verifyAdherenteForPD(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}