package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"statusId",
})
@XmlRootElement(name = ServiceConstant.DTO_FILERECEPTION_STATUSID)
public class FileReceptionStatusesDTO extends AbstractDTO{

	private static final long serialVersionUID = -1414758864739578889L;

	@XmlElement(name=ServiceConstant.DTO_FILERECEPTION_ID, defaultValue="")
	private List<String> statusId;

	public List<String> getStatusId() {
		return statusId;
	}

	public void setStatusId(List<String> statusId) {
		this.statusId = statusId;
	}
}
