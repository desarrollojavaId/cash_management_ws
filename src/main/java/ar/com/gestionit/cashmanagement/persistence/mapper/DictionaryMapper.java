package ar.com.gestionit.cashmanagement.persistence.mapper;

import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.dto.DictionaryListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

	public interface DictionaryMapper extends IMapper<IDTO> {
		public List<DictionaryListItemDTO> findDic();
		
	}