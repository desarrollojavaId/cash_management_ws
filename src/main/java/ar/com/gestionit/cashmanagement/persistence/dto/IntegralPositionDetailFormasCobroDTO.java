package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlRootElement(name = ServiceConstant.DTO_INTEGRALPOSITION_FORMASCOBRO)
@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
public class IntegralPositionDetailFormasCobroDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6349663295058167366L;
	
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_FORMASFCOB,  defaultValue="")
	private List <String> list;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}
}
