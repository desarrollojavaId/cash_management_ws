package ar.com.gestionit.cashmanagement.persistence.dto;

public class RecoveredBatchDTO extends AbstractDTO {

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 4969943919872788248L;

	//Inputs
	private String adherent;
	private String agreementNumber;
	private String agreementSubnumber;
	private String currentDate;
	private String currentHour;

	//Inputs/outputs
	private String requestNumber;
	private String branch;

	/**
	 * @return the adherent
	 */
	public String getAdherent() {
		return adherent;
	}

	/**
	 * @param adherent the adherent to set
	 */
	public void setAdherent(String adherent) {
		this.adherent = adherent;
	}

	/**
	 * @return the requestNumber
	 */
	public String getRequestNumber() {
		return requestNumber;
	}

	/**
	 * @param requestNumber the requestNumber to set
	 */
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * @return the branch
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * @param branch the branch to set
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}

	/**
	 * @return the currentDate
	 */
	public String getCurrentDate() {
		return currentDate;
	}

	/**
	 * @param currentDate the currentDate to set
	 */
	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	/**
	 * @return the currentHour
	 */
	public String getCurrentHour() {
		return currentHour;
	}

	/**
	 * @param currentHour the currentHour to set
	 */
	public void setCurrentHour(String currentHour) {
		this.currentHour = currentHour;
	}

	public String getAgreementNumber() {
		return agreementNumber;
	}

	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}

	public String getAgreementSubnumber() {
		return agreementSubnumber;
	}

	public void setAgreementSubnumber(String agreementSubnumber) {
		this.agreementSubnumber = agreementSubnumber;
	}
}