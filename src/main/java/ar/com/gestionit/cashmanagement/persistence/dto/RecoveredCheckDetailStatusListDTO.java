package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"statuses"})
@XmlRootElement(name = ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ROOT)
public class RecoveredCheckDetailStatusListDTO extends AbstractDTO {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -7798867926488859811L;

	@XmlElement(name=ServiceConstant.DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ESTADO)
	private List<RecoveredCheckDetailStatusListItemDTO> statuses;
	
	/**
	 * @return the statuses
	 */
	public List<RecoveredCheckDetailStatusListItemDTO> getStatuses() {
		return statuses;
	}

	/**
	 * @param statuses the statuses to set
	 */
	public void setStatuses(List<RecoveredCheckDetailStatusListItemDTO> statuses) {
		this.statuses = statuses;
	}
	
	public void setStatus(RecoveredCheckDetailStatusListItemDTO dto) {
		if(statuses == null)
			statuses = new ArrayList<RecoveredCheckDetailStatusListItemDTO>();
		statuses.add(dto);
	}
	
	
}