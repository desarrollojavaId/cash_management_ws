package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name = ServiceConstant.DTO_EGRESS_ROOT)
public class EgressOutflowDTO extends AbstractDTO {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -7798867926488859811L;

	//Input
	@XmlTransient
	private String adherent;
	@XmlTransient
	private String requestId;

	//Ouput
	@XmlElement(name=ServiceConstant.DTO_EGRESS, defaultValue="")
	private List<EgressOutflowItemDTO> list;

	/**
	 * @return the adherent
	 */
	public String getAdherent() {
		return adherent;
	}

	/**
	 * @param adherent the adherent to set
	 */
	public void setAdherent(String adherent) {
		this.adherent = adherent;
	}

	/**
	 * @return the list
	 */
	public List<EgressOutflowItemDTO> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<EgressOutflowItemDTO> list) {
		this.list = list;
	}

	/**
	 * @return the requestId
	 */
	public String getRequestId() {
		return requestId;
	}

	/**
	 * @param requestId the requestId to set
	 */
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
}