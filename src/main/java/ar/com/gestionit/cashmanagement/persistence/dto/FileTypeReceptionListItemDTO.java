package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={
		"sakarc",
		"skdtar"
})
@XmlRootElement(name = ServiceConstant.DTO_FILERECEPTION)
public class FileTypeReceptionListItemDTO extends AbstractDTO{

	
	private static final long serialVersionUID = -1597375682931440007L;
	
	//Input
	@XmlTransient
	private String agreementNumber;
	@XmlTransient
	private String agreementSubnumber;
	@XmlTransient
	private int pageFrom;
	@XmlTransient
	private int pageTo;
	@XmlTransient
	private int rowNumber;
	@XmlTransient
	private String fechaHasta;
	@XmlTransient
	private String fechaDesde;
	@XmlTransient
	private String adherent;
	@XmlTransient
	private List<String> typesId;
	@XmlTransient
	private List<String> statusId;
	@XmlTransient
	private List<FileReceptionTypesItemDTO> types;
	@XmlTransient
	private String sbkres;
	@XmlTransient
	private String sPServ;
		
	//Output
	@XmlElement(name=ServiceConstant.DTO_FILERECEPTION_TYPE, defaultValue="")
	private String sakarc;
	@XmlElement(name=ServiceConstant.DTO_FILERECEPTION_DESCRIPTION, defaultValue="")
	private String skdtar;

	public String getAgreementNumber() {
		return agreementNumber;
	}

	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}

	public String getAgreementSubnumber() {
		return agreementSubnumber;
	}

	public void setAgreementSubnumber(String agreementSubnumber) {
		this.agreementSubnumber = agreementSubnumber;
	}

	public int getPageFrom() {
		return pageFrom;
	}

	public void setPageFrom(int pageFrom) {
		this.pageFrom = pageFrom;
	}

	public int getPageTo() {
		return pageTo;
	}

	public void setPageTo(int pageTo) {
		this.pageTo = pageTo;
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	public String getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public String getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public String getAdherent() {
		return adherent;
	}

	public void setAdherent(String adherent) {
		this.adherent = adherent;
	}

	public List<String> getTypesId() {
		return typesId;
	}

	public void setTypesId(List<String> typesId) {
		this.typesId = typesId;
	}

	public List<String> getStatusId() {
		return statusId;
	}

	public void setStatusId(List<String> statusId) {
		this.statusId = statusId;
	}

	public List<FileReceptionTypesItemDTO> getTypes() {
		return types;
	}

	public void setTypes(List<FileReceptionTypesItemDTO> types) {
		this.types = types;
	}

	public String getSbkres() {
		return sbkres;
	}

	public void setSbkres(String sbkres) {
		this.sbkres = sbkres;
	}

	public String getsPServ() {
		return sPServ;
	}

	public void setsPServ(String sPServ) {
		this.sPServ = sPServ;
	}

	public String getSakarc() {
		return sakarc;
	}

	public void setSakarc(String sakarc) {
		this.sakarc = sakarc;
	}

	public String getSkdtar() {
		return skdtar;
	}

	public void setSkdtar(String skdtar) {
		if(skdtar != null)
			skdtar = skdtar.trim();
		this.skdtar = skdtar;
	}
}