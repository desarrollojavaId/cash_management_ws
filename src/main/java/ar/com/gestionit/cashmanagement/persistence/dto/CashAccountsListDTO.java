package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name = ServiceConstant.DTO_CASHACCOUNTSLIST_ROOT)
public class CashAccountsListDTO extends AbstractDTO{
		
	private static final long serialVersionUID = -8706359386762896251L;
	
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST)
	private List<CashAccountsListItemDTO> list;

	/**
	 * @return the list
	 */
	public List<CashAccountsListItemDTO> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<CashAccountsListItemDTO> list) {
		this.list = list;
	}
	
	public void setError(CashAccountsListItemDTO dto) {
		if(list == null)
			list = new ArrayList<CashAccountsListItemDTO>();
		list.add(dto);
	}
}
