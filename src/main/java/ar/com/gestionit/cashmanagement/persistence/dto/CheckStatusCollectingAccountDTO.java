package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"contenido"})
@XmlRootElement(name = ServiceConstant.DTO_STATUS_SEND_FILES)
public class CheckStatusCollectingAccountDTO extends AbstractDTO{

	private static final long serialVersionUID = -708341426856672627L;

	//input
	@XmlTransient
	private String adherent;
	
	@XmlTransient
	private String account; //
	
	@XmlTransient
	private String subaccount; //
	
	@XmlTransient
	private String branchOffice; //
	
	@XmlTransient
	private String currency; //
	
	@XmlTransient
	private String module; //
	
	
	//Outputs
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_COLLECTING_ACCOUNT_CASH, defaultValue="")
	private String isCash; //
	
	//Outputs
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_COLLECTING_ACCOUNT_OPERATE_CHANNEL, defaultValue="")
	private String operateChannel; //
	
	//Outputs
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_COLLECTING_ACCOUNT_NROCONVENIO, defaultValue="")
	private String nroConv; //
	
	//Outputs
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_COLLECTING_ACCOUNT_NROSUBCONV, defaultValue="")
	private String nroSubConv; //

	
	
	public String getAdherent() {
		return adherent;
	}

	public void setAdherent(String adherent) {
		this.adherent = adherent;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getSubaccount() {
		return subaccount;
	}

	public void setSubaccount(String subaccount) {
		this.subaccount = subaccount;
	}

	public String getBranchOffice() {
		return branchOffice;
	}

	public void setBranchOffice(String branchOffice) {
		this.branchOffice = branchOffice;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getIsCash() {
		return isCash;
	}

	public void setIsCash(String isCash) {
		this.isCash = isCash;
	}

	public String getOperateChannel() {
		return operateChannel;
	}

	public void setOperateChannel(String operateChannel) {
		this.operateChannel = operateChannel;
	}

	public String getNroConv() {
		return nroConv;
	}

	public void setNroConv(String nroConv) {
		this.nroConv = nroConv;
	}

	public String getNroSubConv() {
		return nroSubConv;
	}

	public void setNroSubConv(String nroSubConv) {
		this.nroSubConv = nroSubConv;
	}
	

	
	
}