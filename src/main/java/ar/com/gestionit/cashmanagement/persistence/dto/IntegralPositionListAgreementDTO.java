package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"agreement",
		"subAgreement",
		"agreementDescription",
		"amount"
})
@XmlRootElement(name = ServiceConstant.DTO_INTEGRALPOSITION_COBRO)
public class IntegralPositionListAgreementDTO extends AbstractDTO {

	private static final long serialVersionUID = -5680743257985240257L;
	
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_COBRO_CONV, defaultValue="")
	private String agreement;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_COBRO_SUBCONV, defaultValue="")
	private String subAgreement;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_COBRO_CONVDESC, defaultValue="")
	private String agreementDescription;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_COBRO_IMP, defaultValue="")
	private String amount;
	/**
	 * @return the agreement
	 */
	public String getAgreement() {
		return agreement;
	}
	/**
	 * @param agreement the agreement to set
	 */
	public void setAgreement(String agreement) {
		this.agreement = agreement;
	}
	/**
	 * @return the subAgreement
	 */
	public String getSubAgreement() {
		return subAgreement;
	}
	/**
	 * @param subAgreement the subAgreement to set
	 */
	public void setSubAgreement(String subAgreement) {
		this.subAgreement = subAgreement;
	}
	/**
	 * @return the agreementDescription
	 */
	public String getAgreementDescription() {
		return agreementDescription;
	}
	/**
	 * @param agreementDescription the agreementDescription to set
	 */
	public void setAgreementDescription(String agreementDescription) {
		this.agreementDescription = agreementDescription;
	}
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
}		