package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.EscrowSendFileDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StoredProcedureParametersDTO;

public interface EscrowSendFileMapper extends IMapper<EscrowSendFileDTO> {
	public String findEndingDate();
	public String validateHash(EscrowSendFileDTO dto);
	public EscrowSendFileDTO findDataForStatus(EscrowSendFileDTO dto);
	public int updateStatus(EscrowSendFileDTO dto);
	public void generateMember(StoredProcedureParametersDTO dto);
	public String findTableName(EscrowSendFileDTO dto);
	public int insertFileContent(EscrowSendFileDTO dto);
	public String findResult();
	public int updateDate(EscrowSendFileDTO dto);
	public String findFileName(EscrowSendFileDTO dto);
}