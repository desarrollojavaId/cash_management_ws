package ar.com.gestionit.cashmanagement.persistence.mapper;

import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.dto.FileStatusDownloadItemDTO;


public interface FileStatusDownloadItemMapper extends IMapper<FileStatusDownloadItemDTO> {

	/**
	 * This method is used to get files 
	 * @param dto
	 * @return
	 */
	
	//revisar
	public FileStatusDownloadItemDTO find(FileStatusDownloadItemDTO dto);
	
	public Integer findAdherent(FileStatusDownloadItemDTO dto);
	

}