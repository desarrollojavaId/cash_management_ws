package ar.com.gestionit.cashmanagement.persistence.mapper;

import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.dto.FileContentDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

public interface FileContentMapper extends IMapper<IDTO> {
	/**
	 * This method is used to get the library where the file is hosted
	 * @param dto
	 * @return
	 */
	public FileContentDTO findLibrary(FileContentDTO dto);
	
	/**
	 * FOR RECEPTION: This method is used to get the library where the file is hosted
	 * @param dto
	 * @return
	 */
	public FileContentDTO findLibraryReception(FileContentDTO dto);
	
	/**
	 * This method is used to get the auxiliary table if the library was not found previously
	 * @param dto
	 * @return
	 */
	public String findAuxiliaryTable(String dto);
	
	/**
	 * This method updates the file status to downloaded ("Bajado")
	 * @param dto
	 * @return
	 */
	public int updateFileStatus(FileContentDTO dto);
	
	/**
	 * This method updates the file status in SSTRANS table
	 * @param dto
	 * @return
	 */
	public int updateSstrans(FileContentDTO dto);
	
	public List<FileContentDTO> findBeneficiaryData(FileContentDTO dto);
	public List<String> findFile(FileContentDTO dto);
	public List<String> findFileByAuxiliaryTable(FileContentDTO dto);
	public String findExecutionMode(String executionModeCode);
}