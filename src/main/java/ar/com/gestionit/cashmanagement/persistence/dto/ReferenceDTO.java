package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"deudor1",
			"deudor2",
			"comprobante1",
			"comprobante2",
			"comprobante3",
			"comprobante4",
			"comprobante5"})
@XmlRootElement(name=ServiceConstant.DTO_REFERENCE)
public class ReferenceDTO extends AbstractDTO {
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 4969943919872788248L;
	
	//Inputs
	@XmlElement(name=ServiceConstant.DTO_REFERENCE_DEU1, defaultValue="")
	private String deudor1;
	@XmlElement(name=ServiceConstant.DTO_REFERENCE_DEU2, defaultValue="")
	private String deudor2;
	@XmlElement(name=ServiceConstant.DTO_REFERENCE_COMP1, defaultValue="")
	private String comprobante1;
	@XmlElement(name=ServiceConstant.DTO_REFERENCE_COMP2, defaultValue="")
	private String comprobante2;
	@XmlElement(name=ServiceConstant.DTO_REFERENCE_COMP3, defaultValue="")
	private String comprobante3;
	@XmlElement(name=ServiceConstant.DTO_REFERENCE_COMP4, defaultValue="")
	private String comprobante4;
	@XmlElement(name=ServiceConstant.DTO_REFERENCE_COMP5, defaultValue="")
	private String comprobante5;
	/**
	 * @return the deudor1
	 */
	public String getDeudor1() {
		return deudor1;
	}
	/**
	 * @param deudor1 the deudor1 to set
	 */
	public void setDeudor1(String deudor1) {
		this.deudor1 = deudor1;
	}
	/**
	 * @return the deudor2
	 */
	public String getDeudor2() {
		return deudor2;
	}
	/**
	 * @param deudor2 the deudor2 to set
	 */
	public void setDeudor2(String deudor2) {
		this.deudor2 = deudor2;
	}
	/**
	 * @return the comprobante1
	 */
	public String getComprobante1() {
		return comprobante1;
	}
	/**
	 * @param comprobante1 the comprobante1 to set
	 */
	public void setComprobante1(String comprobante1) {
		this.comprobante1 = comprobante1;
	}
	/**
	 * @return the comprobante2
	 */
	public String getComprobante2() {
		return comprobante2;
	}
	/**
	 * @param comprobante2 the comprobante2 to set
	 */
	public void setComprobante2(String comprobante2) {
		this.comprobante2 = comprobante2;
	}
	/**
	 * @return the comprobante3
	 */
	public String getComprobante3() {
		return comprobante3;
	}
	/**
	 * @param comprobante3 the comprobante3 to set
	 */
	public void setComprobante3(String comprobante3) {
		this.comprobante3 = comprobante3;
	}
	/**
	 * @return the comprobante4
	 */
	public String getComprobante4() {
		return comprobante4;
	}
	/**
	 * @param comprobante4 the comprobante4 to set
	 */
	public void setComprobante4(String comprobante4) {
		this.comprobante4 = comprobante4;
	}
	/**
	 * @return the comprobante5
	 */
	public String getComprobante5() {
		return comprobante5;
	}
	/**
	 * @param comprobante5 the comprobante5 to set
	 */
	public void setComprobante5(String comprobante5) {
		this.comprobante5 = comprobante5;
	}
}