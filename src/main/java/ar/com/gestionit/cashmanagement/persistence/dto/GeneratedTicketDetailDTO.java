package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name = ServiceConstant.DTO_GENERATEDTICKET_DETAILROOT)
public class GeneratedTicketDetailDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7621404326769358141L;

	//Input
	@XmlTransient
	private String cuit;
	@XmlTransient
	private String nroBoleta;
	
	//Output
	@XmlElement(name=ServiceConstant.DTO_GENERATEDTICKET_DETAIL, defaultValue="")
	private List<GeneratedTicketDetailListItemDTO> list;

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = sanitateString(cuit);
	}

	public String getNroBoleta() {
		return nroBoleta;
	}

	public void setNroBoleta(String nroBoleta) {
		this.nroBoleta = sanitateString(nroBoleta);
	}

	public List<GeneratedTicketDetailListItemDTO> getList() {
		return list;
	}

	public void setList(List<GeneratedTicketDetailListItemDTO> list) {
		this.list = list;
	}
	
	

}
