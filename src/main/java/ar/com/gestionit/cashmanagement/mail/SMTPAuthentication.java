package ar.com.gestionit.cashmanagement.mail;

import java.io.Serializable;

import javax.mail.PasswordAuthentication;

public class SMTPAuthentication extends javax.mail.Authenticator implements Serializable {
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 1L;

	private String user;
	private String password;

	/**
	 * Parameterized constructor
	 * @param user
	 * @param password
	 */
	public SMTPAuthentication(String user, String password){
		this.user = user;
		this.password  = password;
	}

	/**
	 * This method generate the PasswordAuthentication object the way right
	 */
	@Override
	public PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(user, password);
	}
}
