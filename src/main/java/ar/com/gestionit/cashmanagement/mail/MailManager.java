package ar.com.gestionit.cashmanagement.mail;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

/**
 * This class is used for the mail management
 * @author jdigruttola
 */
public class MailManager {

	private String from;
	private String smtp;
	private int port;
	private boolean debug;
	private boolean requiereAuthentication;
	private String user;
	private String password;
	private String recipients;

	/**
	 * Constructor by default
	 */
	public MailManager() throws ServiceException {
		try {
			//Get the necessary parameters from the global properties
			from = CashManagementWsApplication.getProperty(ServiceConstant.PROP_MAIL_FROM_ADDRESS);
			recipients = CashManagementWsApplication.getProperty(ServiceConstant.PROP_MAIL_TO_ADDRESS);
			smtp = CashManagementWsApplication.getProperty(ServiceConstant.PROP_MAIL_SMTP_HOST);
			port = CashManagementWsApplication.getPropertyAsInteger(ServiceConstant.PROP_MAIL_SMTP_PORT);
			debug = CashManagementWsApplication.getPropertyAsBoolean(ServiceConstant.PROP_MAIL_SMTP_DEBUG);
			requiereAuthentication = CashManagementWsApplication.getPropertyAsBoolean(ServiceConstant.PROP_MAIL_SMTP_AUTH);
			user = CashManagementWsApplication.getProperty(ServiceConstant.PROP_MAIL_SMTP_USER);
			password = CashManagementWsApplication.getProperty(ServiceConstant.PROP_MAIL_SMTP_PASS);

			//Validate the obtained parameters

		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_MAILPROPTROUBLES);
		}
	}

	/**
	 * This method sends mails
	 * @param subject
	 * @param message
	 * @throws ServiceException
	 */
	public void postMail(String subject, String message) throws ServiceException {
		try {
			// Create some properties and get the default Session
			Properties props = new Properties();
			props.put("mail.smtp.host", smtp);
			props.put("mail.smtp.auth", requiereAuthentication);
			props.put("mail.smtp.port",port);

			Authenticator auth = new SMTPAuthentication(user,password);

			Session session = Session.getDefaultInstance(props, auth);
			//Session session = Session.getDefaultInstance(props, null);
			session.setDebug(debug);

			// create a message
			Message msg = new MimeMessage(session);

			// Set the from and to address
			InternetAddress addressFrom = new InternetAddress(from);
			msg.setFrom(addressFrom);

			// Format the several mails as recipients
			String[] mails = recipients.split(",");
			InternetAddress[] addressTo = new InternetAddress[mails.length]; 
			for (int i = 0; i < mails.length; i++){
				addressTo[i] = new InternetAddress(mails[i]);
			}

			msg.setRecipients(Message.RecipientType.TO, addressTo);


			// Optional : You can also set your custom headers in the Email if you Want
			//msg.addHeader("MyHeaderName", "myHeaderValue");

			// Setting the Subject and Content Type
			msg.setSubject(subject);
			msg.setContent(message, "text/plain");
			Transport.send(msg);
		} catch(MessagingException e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTSENDMAIL, e);
		}
	}
}
