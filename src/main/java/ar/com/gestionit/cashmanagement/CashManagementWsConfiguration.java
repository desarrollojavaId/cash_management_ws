package ar.com.gestionit.cashmanagement;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;


/**
 * In this class, we can define beans managed by Mr. Spring
 * @author jdigruttola
 *
 */
@Configuration
public class CashManagementWsConfiguration {

	/**
	 * This bean generates new instance of TaskExecutor for service EscrowSendFileService (21.3.21).
	 * This service needs a thread pool to process the sent of files.
	 * @return
	 */
	@Bean
	public TaskExecutor getThreadPool() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(100);
		executor.setMaxPoolSize(200);
		executor.setQueueCapacity(300);
		return executor;
	}
}