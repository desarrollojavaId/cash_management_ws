package ar.com.gestionit.cashmanagement.service;

import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.DebtListBO;
import ar.com.gestionit.cashmanagement.persistence.dto.DebtListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.DebtListItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;

public class DebtListService extends AbstractService{
	
	DebtListBO bo;
	DebtListDTO dto;
	

	@Override
	protected void initialize() throws ServiceException {
		bo = new DebtListBO();
		dto = new DebtListDTO();		
	}
	
	@Override
	protected void loadInputs() throws ServiceException {
		
		String cuit = getDataFromVector(2);
		cuit = ServiceUtil.validateNumberAndLengthObligatory(cuit, 11, ServiceConstant.SERVICERETURN_KEY_INVALIDCUIT , ServiceConstant.SERVICERETURN_KEY_OBLIGATORYCUIT);
		dto.setCuit(cuit);
		
		String empresa = getDataFromVector(3);
		empresa = ServiceUtil.validateLength(empresa, 60, ServiceConstant.SERVICERETURN_KEY_INVALIDCOMPANY);
		if (empresa.equals("0"))
			empresa = ""; 		
		empresa = "%" + empresa + "%";
		dto.setEmpresa(empresa);
				
		String convenio = getDataFromVector(4);
		convenio = ServiceUtil.validateNumberAndLength(convenio, 7, ServiceConstant.SERVICERETURN_KEY_INVALIDAGREEMENT);
		dto.setConvenio(convenio);
		
		String subConvenio = getDataFromVector(5); 
		subConvenio = ServiceUtil.validateNumberAndLength(subConvenio, 7, ServiceConstant.SERVICERETURN_KEY_INVALIDSUBAGREEMENT);
		dto.setSubConvenio(subConvenio);
		
		
		String vtoDesde = getDataFromVector(6);
		String vtoHasta = getDataFromVector(7);
		
		vtoDesde = ServiceUtil.validateDate(vtoDesde, ServiceConstant.SERVICERETURN_KEY_NOTDATEFROM);
		dto.setVencimientoDesde(vtoDesde);
		
		vtoHasta = ServiceUtil.validateDate(vtoHasta, ServiceConstant.SERVICERETURN_KEY_NOTDATETO);
		dto.setVencimientoHasta(vtoHasta);
		
		ServiceUtil.validateDateInterval(dto.getVencimientoDesde(), dto.getVencimientoHasta());
		
		bo.enablePager(getDataFromVector(8));
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void execute() throws ServiceException {
		dto.setList((List<DebtListItemDTO>) bo.findList(dto));
		
	}
	
	@Override
	protected void loadOutputs() throws ServiceException {
		setDataInVector(9, bo.dtoToXML(dto));
		setDataInVector(10, bo.getPagerAsXML());
		
	}

	

}
