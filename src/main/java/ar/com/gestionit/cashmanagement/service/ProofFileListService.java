package ar.com.gestionit.cashmanagement.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.service.wsclient.Comprobante;
import ar.com.gestionit.cashmanagement.service.wsclient.GetComprobanteResponse;
import ar.com.gestionit.cashmanagement.service.wsclient.ProofFileListClient;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;

public class ProofFileListService extends AbstractService {

	private String paymentNumber;
	private String paymentSubnumber;

	/**
	 * Proof file list
	 */
	private List<String> output;

	@Override
	protected void initialize() throws ServiceException {
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		
		
		paymentNumber = getDataFromVector(2);
		paymentNumber = ServiceUtil.validateNumberAndLengthObligatory(paymentNumber, 8, ServiceConstant.SERVICERETURN_KEY_INVALIDPAYORDER, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYPAYORDER);
		
		paymentSubnumber = getDataFromVector(3);
		paymentSubnumber = ServiceUtil.validateNumberAndLengthObligatory(paymentSubnumber, 3, ServiceConstant.SERVICERETURN_KEY_INVALIDSUBPAYORDER, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYSUBPAYORDER);
	}

	@Override
	public void execute() throws ServiceException {
		DefinedLogger.SERVICE.debug("Ejecutando querys y cargando listas...");
		//Get WS client by Spring context
		ProofFileListClient client = (ProofFileListClient)
				CashManagementWsApplication.getBean(ProofFileListClient.class);
		
		//Consume WS to get the proof file list
		GetComprobanteResponse response = client.getProof(paymentNumber, paymentSubnumber);
		if(response == null
				|| response.getReturn() == null
				|| response.getReturn().size() == 0) {
			DefinedLogger.SERVICE.info("No hay archivos de comprobantes para "
					+ "la orden de pago con nro " + paymentNumber
					+ " y subnro " + paymentSubnumber);
			//Probar
			throw new ServiceException( ServiceConstant.SERVICERETURN_KEY_NOVOUCHERFORRECOVERED);
			//return;
		}
		
		//List each proof file
		Iterator<Comprobante> i = response.getReturn().iterator();
		output = new ArrayList<String>();
		while(i.hasNext()) {
			output.add(i.next().getPdf());
		}
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs");
		//Validate if there are proof files to send
		if(output == null || output.size() == 0)
			return;

		//Load each proof file
		for( int i = 0; i < output.size(); i++ ) {
			setDataInVector(i + 4, output.get(i));
		}
	}
}