package ar.com.gestionit.cashmanagement.service;

import java.util.Date;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.PersistEscrowChecksumBO;
import ar.com.gestionit.cashmanagement.persistence.dto.EscrowChecksumDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.NumberUtil;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;
import ar.com.gestionit.cashmanagement.util.StringUtil;

public class PersistEscrowChecksumService extends AbstractService{
	
	/**
	 * Business object for this service
	 */
	private PersistEscrowChecksumBO bo;
	
	/**
	 * DTO
	 */
	private EscrowChecksumDTO dto;
	
	/**
	 * This attribute contains the amount of checksums sent by the request
	 */
	int checksumAmount;
	
	@Override
	protected void validateBefore() throws ServiceException {
		//Validate if the reques is NULL
		if(request == null) {
			try {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTREQUEST);
			} catch(ServiceException e) {
				throw e;
			}
		}
	}
	@Override
	protected void initialize() throws ServiceException {
		bo = new PersistEscrowChecksumBO();
		dto = new EscrowChecksumDTO();
		checksumAmount = 0;
	}
	
	@Override
	protected void loadInputs() throws ServiceException {
		
		/*
		 * -------------------------------------------------------------------------
		 * Get data from request
		 */
		dto.setPgcod(ServiceUtil.validateNumberAndLengthObligatory(getDataFromVector(2), 3, ServiceConstant.SERVICERETURN_KEY_NOTVALIDESCROWCODE, ServiceConstant.SERVICERETURN_KEY_NOTVALIDESCROWCODE));
		dto.setAccount(ServiceUtil.validateNumberAndLengthObligatory(getDataFromVector(3), 9, ServiceConstant.SERVICERETURN_KEY_NOTVALIDESCROWACCOUNT, ServiceConstant.SERVICERETURN_KEY_NOTVALIDESCROWACCOUNT));
		dto.setSerie(ServiceUtil.validateNumberAndLengthObligatory(getDataFromVector(4), 7, ServiceConstant.SERVICERETURN_KEY_NOTVALIDESCROWSERIE, ServiceConstant.SERVICERETURN_KEY_NOTVALIDESCROWSERIE));
		dto.setGroup(getDataFromVector(5));
		dto.setAltSum(validateChecksum(getDataFromVector(6), ServiceConstant.SERVICERETURN_KEY_NOTVALIDESCROWALTSUM));
		dto.setCliSum(validateChecksum(getDataFromVector(7), ServiceConstant.SERVICERETURN_KEY_NOTVALIDESCROWCLISUM));
		dto.setCuoSum(validateChecksum(getDataFromVector(8), ServiceConstant.SERVICERETURN_KEY_NOTVALIDESCROWCUOSUM));
		dto.setPagSum(validateChecksum(getDataFromVector(9), ServiceConstant.SERVICERETURN_KEY_NOTVALIDESCROWPAGSUM));
		dto.setSegSum(validateChecksum(getDataFromVector(10), ServiceConstant.SERVICERETURN_KEY_NOTVALIDESCROWSEGSUM));
		
		/*
		 * -------------------------------------------------------------------------
		 * Validate data
		 */
		if(StringUtil.isEmpty(dto.getGroup())) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTVALIDESCROWGROUP);
		} else {
			dto.setGroup(ServiceUtil.validateLength(dto.getGroup(), 4, ServiceConstant.SERVICERETURN_KEY_NOTVALIDESCROWGROUP));
		}
		
		//Validate if there is a checksum at least
		if(checksumAmount == 0) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTESCROWCHECKSUMS);
		}
	}

	@Override
	protected void execute() throws ServiceException {
		/*
		 * Define and initialize variables
		 */
		int queryResult;
		String today = ServiceConstant.SDF_DATE_FOR_INPUT.format(new Date());
		
		/*
		 * Set dates in the DTO to update in the data source
		 */
		dto.setAltSumDate(today);
		dto.setCliSumDate(today);
		dto.setCuoSumDate(today);
		dto.setPagSumDate(today);
		dto.setSegSumDate(today);
		
		/*
		 * Update SSFIDEIC table 
		 */
		queryResult = bo.update(dto);
		if(queryResult <= 0) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTUPDATEESCROWCHECKSUM);
		}
		
		/*
		 * Generate register in SSFIDCLA
		 */
		queryResult = bo.insert(dto);
		if(queryResult <= 0) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTINSERTESCROWCHECKSUM);
		}
	}

	@Override
	protected void loadOutputs() throws ServiceException {
	}
	
	private String validateChecksum(String checksum, String keyError) throws ServiceException {
		if(!StringUtil.isEmpty(checksum)) {
			if(!NumberUtil.isNumeric(checksum))
				throw new ServiceException(keyError);
			if(NumberUtil.toNumber(checksum) <= 0)
				throw new ServiceException(keyError);
			checksumAmount++;
			return checksum;
		}
		return "0";
	}
}