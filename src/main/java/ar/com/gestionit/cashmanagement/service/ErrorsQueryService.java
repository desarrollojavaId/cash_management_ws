package ar.com.gestionit.cashmanagement.service;

import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.factory.ServiceReturnFactory;
import ar.com.gestionit.cashmanagement.persistence.bo.ErrorsQueryBO;
import ar.com.gestionit.cashmanagement.persistence.dto.ErrorListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ErrorListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileListItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;

public class ErrorsQueryService extends AbstractService {
	/**
	 * This attribute is used to load the input parameters
	 */
	private ErrorListItemDTO input;
	private FileListItemDTO dto2;

	/**
	 * Business object for this service
	 */
	private ErrorsQueryBO bo;

	/**
	 * This is the payment list result to send
	 */
	private String xmlErrorsQuery;

	/**
	 * Pager attributes
	 */
	private String xmlPager;

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		bo = new ErrorsQueryBO();
		input = new ErrorListItemDTO();
		dto2 = new FileListItemDTO();
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		dto2.setAdherent(getDataFromVector(1));
		
		String a = getDataFromVector(2);
		ServiceUtil.validateLength(a, 10, ServiceConstant.SERVICERETURN_KEY_INVALIDFILENAME);
		input.setNomArch(a);
		
		bo.enablePager(getDataFromVector(3));
		
		//Validate if the file name exists
		if(input.getNomArch() == null || input.getNomArch().trim().equals("")) {
			DefinedLogger.SERVICE.debug(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_NOTFILENAME));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFILENAME);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute() throws ServiceException {
		DefinedLogger.SERVICE.debug("Ejecutando querys y cargando listas...");
		List<ErrorListItemDTO> list = (List<ErrorListItemDTO>) bo.findList(input);
		ErrorListDTO errorList = new ErrorListDTO();
		
		DefinedLogger.SERVICE.debug("Obteniendo datos y armando xml...");
		//Si obtiene datos, arma el XML
		if(list != null && list.size() > 0) {
			contentToExport.add(list);
			Iterator<ErrorListItemDTO> i = list.iterator();
			ErrorListItemDTO dto; 		

			boolean esPD = false;
			String nomArch = input.getNomArch();
			if (nomArch.substring(0, 2).equalsIgnoreCase("PD")){
				esPD = true;
			}

			while(i.hasNext()) {
				dto = i.next();
				if(esPD) {
					dto.setDescription(dto.getDetdes());
				} else {				
					dto.setDescription(dto.getMtkdes());
				}
				dto.setRowError(dto.getDetnre());
				errorList.setError(dto);
			}
		}else {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}
		
		//Valido si la lista esta vacia
		if(errorList == null || errorList.getList() == null || errorList.getList().size() == 0) {
			FileListItemDTO fileStatus = bo.findStatus(dto2);
			
			//Validate if I get anything by the file name given
			if(fileStatus == null) {
				DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_NOTFINDFILES));
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFINDFILES);
			}

			if (fileStatus.getStkest().equalsIgnoreCase("APVI")) {
				reportSuccess(ServiceConstant.SERVICERETURN_KEY_VALPENDING);
			} else {
				reportSuccess(ServiceConstant.SERVICERETURN_KEY_WITHOUTERRORS);
			}
		}

		xmlErrorsQuery = bo.dtoToXML(errorList);
		xmlPager = bo.getPagerAsXML();
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");
		setDataInVector(4, xmlErrorsQuery);
		setDataInVector(5, xmlPager); 
        //Notify number of executed queries
        reportQueryNumber(bo.getQueryCounter());

	}
}