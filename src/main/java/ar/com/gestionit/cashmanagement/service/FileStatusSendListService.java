package ar.com.gestionit.cashmanagement.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlTransient;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.FileStatusSendListBO;
import ar.com.gestionit.cashmanagement.persistence.bo.SendFileBO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileAccountListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileContentDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FilePaymentDetailDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileStatusSendListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileStatusSendListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentWayListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentWayListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.SendFileDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StatusInputListDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.ClassUtil;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.NumberUtil;
import ar.com.gestionit.cashmanagement.util.StringUtil;

/** Servicio de Consulta situacion cheque  **/
public class FileStatusSendListService extends AbstractAccountManagerService {
	/**
	 * Constants to browse the file content
	 */
	private static final int NRO_BENEFICIARIO_INI = 575;
	private static final int NRO_BENEFICIARIO_END = 600;
	private static final int NOMBRE_BENEFICIARIO_INI = 600;
	private static final int NOMBRE_BENEFICIARIO_END = 660;
	private static final int REFERENCIA_CLIENTE_INI = 2;
	private static final int REFERENCIA_CLIENTE_END = 27;
	private static final int MOTIVO_PAGO_INI = 27;
	private static final int MOTIVO_PAGO_END = 132;
	private static final int FECHA_PAGO_INI = 132;
	private static final int FECHA_PAGO_END = 140;
	private static final int MODO_EJEC_INI = 140;
	private static final int MODO_EJEC_END = 143;
	private static final int IMPORTE_PAGO_INI = 143;
	private static final int IMPORTE_PAGO_END = 158;
	private static final int FECHA_VENC_CHPD_INI = 161;
	private static final int FECHA_VENC_CHPD_END = 169;
	private static final int NRO_DOC_BENEF_INI = 663;
	private static final int NRO_DOC_BENEF_END = 674;
	private static final int CBU_INI = 947;
	private static final int CBU_END = 969;
	private static final int CTA_ADH_INI = 1025;
	private static final int CTA_ADH_END = 1047;
	private static final int MONEDA_INI = 1049;
	private static final int MONEDA_END = 1052;

	private HashMap<String, FileAccountListItemDTO> accountMap = new HashMap<String, FileAccountListItemDTO>();
	private PaymentWayListDTO paymentWayList = new PaymentWayListDTO();
	private String strAmountTotal;
	private FileStatusSendListDTO dto;
//	private CheckStatusUnPrintedDTO uDto;
	private FileStatusSendListBO bo;
	private String[] lines;
	private SendFileBO sendBo;
	private SendFileDTO sendDto;

	/**
	 * Adherent Currency
	 */
	private String currencyAdh;
	
	@Override
	protected void execute() throws ServiceException {
		
		DefinedLogger.SERVICE.debug("Ejecutando querys y cargando listas...");
		/*
		 * Executing query....
		 * ------------------------------------------------------------------------
		 */
		//validating profiling adherent 
//		Integer exist = bo.findAdherent(dto);
//		if(exist == 0) {
//			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_FILESTATUSSENDFILE);
//		}
//		
		//Cargo y seteo la lista de cheques 
		List<FileStatusSendListItemDTO> list = bo.find(dto);
		if (list == null || list.size() == 0){
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}
		FileContentDTO fileDto = new FileContentDTO();
		fileDto.setLibrary("SATHISVAL");
		for(FileStatusSendListItemDTO dto : list) {
			//set value default
			dto.setMoneda("80");
			//if state apa1 then find payment way list
			if("APA1".equalsIgnoreCase(dto.getEstado()) && dto.getTipoArch().equalsIgnoreCase("PO")) {
				
				fileDto.setFileId(dto.getFileId());
				 List<String> lstContent = bo.findFileContent(fileDto);
				 loadReportForSendFile(lstContent, CBU_END);
				 dto.setPaymentWayList(this.paymentWayList);
			}
		}
		
		
		dto.setList(list);
	
	}
	
	/**
	 * This method gets the default account from the datasource
	 * If the default CBU is not found, the method returns NULL
	 * @return
	 * @throws ServiceException
	 */
	private FileAccountListItemDTO loadDefaultAccount() throws ServiceException {
		// Get the CBU by default
		sendDto = sendBo.findDefaultCBU(sendDto);

		// Validate if the account CBU could be obtained
		if (sendDto == null || sendDto.getCbu() == null || !NumberUtil.isNumeric(sendDto.getCbu())) {
			sendDto.setCbu("0");
			DefinedLogger.SERVICE.warn("No se pudo obtener la cuenta por defecto");
			return null;
		}
		// Get account by default according to the obtained CBU
		return sendBo.loadAccount(sendDto.getCbu());
	}
	
	/**
	 * @return the currencyAdh
	 */
	public String getCurrencyAdh() {
		return currencyAdh;
	}

	/**
	 * @param currencyAdh the currencyAdh to set
	 */
	public void setCurrencyAdh(String currencyAdh) {
		this.currencyAdh = currencyAdh;
	}
	
	public void loadReportForSendFile(List<String> lines, long serviceKey) throws ServiceException {
		
		DefinedLogger.SERVICE.debug("Armando reporte de cuentas para archivo PO...");

		// Define and initialize variables
		FilePaymentDetailDTO det = null;
		String tipoPago;
		FileAccountListItemDTO defaultAccount = null;
		FileAccountListItemDTO currentAccount;
		PaymentWayListItemDTO paymentWayDTO = null;
		HashMap<String, PaymentWayListItemDTO> paymentWayMap = new HashMap <String, PaymentWayListItemDTO>();
		long amountTotal = 0;

		// Get default account (if it is possible. If not, the method returns NULL)
		defaultAccount = loadDefaultAccount();

		//Set Adherent currency
		if(defaultAccount != null){
			DefinedLogger.SERVICE.debug("la moneda de la cuenta del adherente es:" + defaultAccount.getAccountCurrency());
			setCurrencyAdh(defaultAccount.getAccountCurrency());
		}
		else{
			DefinedLogger.SERVICE.debug("LA CUANTA DEL ADHERENTE ESTA EN NULL");	
		}
		
		// Process each register (each line)
		for (int i = 1; i < lines.size(); i++) {

			// This IF deletes the header and footer
			if (lines.get(i).length() > 1 && lines.get(i).substring(0, 2).equals("PO")) {

				// Convert the line to DTO
				det = loadRegisterDetail(lines.get(i));

				// Get current account
				currentAccount = loadCurrentAccount(det, defaultAccount);

				/*
				 * Validate if we could find an account for this register
				 */
				if (currentAccount == null) {
					DefinedLogger.SERVICE.warn("No se encontro cuenta para el registro en la linea " + i
							+ " del archivo, por ende, no se tiene en cuenta para los totalizadores");
					continue;
				}

				/*
				 * Persist payment type in the account
				 */
				currentAccount.setTaskCode("1" + det.getTipoPago());
				currentAccount.setPaymentType(Integer.parseInt(det.getTipoPago()));
				currentAccount.setTransactionCurrency(getTransactionCurrencyByCode(det.getMonedaPago()));

				tipoPago = generatePaymentTypeForReport(det.getTipoPago());

				if (paymentWayMap.containsKey(tipoPago)) {
					paymentWayDTO = paymentWayMap.get(tipoPago); 

					/*
					 * PAYMENT WAY TOTALS
					 * If it exists, we must validate is the current amount is
					 * greater than the last persisted
					 */
					if (paymentWayDTO.getAmount() < det.getImpAPagar()) {
						// Update max amount
						paymentWayDTO.setAmount(det.getImpAPagar());
					}

					// Update amount total
					paymentWayDTO.setAmountTotal(paymentWayDTO.getAmountTotal() + det.getImpAPagar());
				} else {
					//Generate a new payment type
					paymentWayDTO = new PaymentWayListItemDTO();
					paymentWayDTO.setAccountMap(new HashMap<String, FileAccountListItemDTO>());
					paymentWayDTO.setAmount(det.getImpAPagar());
					paymentWayDTO.setAmountTotal(det.getImpAPagar());
					paymentWayDTO.setIdFormasDePago(tipoPago);
					paymentWayDTO.setDescFormaPago(generatePaymentTypeDescriptionForReport(tipoPago));

					//Add new payment type
					paymentWayMap.put(tipoPago, paymentWayDTO);
					DefinedLogger.SERVICE.info("Se carga la forma de pago");
				}
				
				/*
				 * Update payment way for this account. The same account can be associated for other payment way
				 * if there are payments generate by other payments ways.
				 */
				currentAccount.setPaymentType(Integer.parseInt(tipoPago));

				/*
				 * PAYMENT WAY -> ACCOUNT TOTALS
				 * If it exists, we must validate is the current amount is
				 * greater than the last persisted
				 */
				updateAccountStatus(paymentWayDTO.getAccountMap(), det, currentAccount);

				/*
				 * ACCOUNT REPORT
				 * Calculate the total amount and max amount for the account
				 * Validate if the account exists in the report. If not, we must
				 * persist this account
				 */
				updateAccountStatus(accountMap, det, currentAccount);
			}
		}
		
		//Formating payment way list...
		for(String paymentType : paymentWayMap.keySet()) {
			//Payment way
			paymentWayDTO = paymentWayMap.get(paymentType);
			paymentWayDTO.setStrAmount(formatNumber(Long.toString(paymentWayDTO.getAmount())));
			paymentWayDTO.setStrAmountTotal(formatNumber(Long.toString(paymentWayDTO.getAmountTotal())));
			
			//Payment way - account list
			paymentWayDTO.setAccounts(new ArrayList<FileAccountListItemDTO>());
			for(String accountNumber : paymentWayDTO.getAccountMap().keySet()) {
				currentAccount = paymentWayDTO.getAccountMap().get(accountNumber);
				currentAccount.setStrAmount(formatNumber(Long.toString(currentAccount.getAmount())));
				currentAccount.setStrAmountTotal(formatNumber(Long.toString(currentAccount.getAmountTotal())));
				paymentWayDTO.getAccounts().add(currentAccount);
				
				//Persist in datasource to can get the report later (ArchiveValidationStatusService)
				currentAccount.setFunctionalityCode(FileAccountListItemDTO.FUNCTIONALITY_BY_DEFAULT);
				currentAccount.setKey(serviceKey);
				//bo.insertAccount(currentAccount);
			}
		}
		
		//Formating account list and persisting...
		for(String accountNumber : accountMap.keySet()) {
			currentAccount = accountMap.get(accountNumber);
			currentAccount.setStrAmount(formatNumber(Long.toString(currentAccount.getAmount())));
			currentAccount.setStrAmountTotal(formatNumber(Long.toString(currentAccount.getAmountTotal())));			
			amountTotal += currentAccount.getAmountTotal();
		}
		

		paymentWayList.setList(new ArrayList<PaymentWayListItemDTO>(paymentWayMap.values()));
		strAmountTotal = formatNumber(Long.toString(amountTotal));
		DefinedLogger.SERVICE.info("bindeo los datos de forma de pago de la cantidad: " + paymentWayList.getList().size());
	}
	
	
	/**
	 * This method formats a register in a line to DTO
	 * 
	 * @param l
	 * @return register in DTO
	 * @throws ServiceException 
	 */
	private FilePaymentDetailDTO loadRegisterDetail(String l) throws ServiceException {
			FilePaymentDetailDTO detail = new FilePaymentDetailDTO();
		try {
			// NumberFormat nf = NumberFormat.getInstance(local);
			detail.setRegistroId(l.substring(0, 2));
			detail.setRefCliente(l.substring(REFERENCIA_CLIENTE_INI, REFERENCIA_CLIENTE_END).trim());
			detail.setMotivoPago(l.substring(MOTIVO_PAGO_INI, MOTIVO_PAGO_END).trim());
			detail.setFechaEjeOrden(l.substring(FECHA_PAGO_INI, FECHA_PAGO_END).trim());
			detail.setTipoPago(l.substring(MODO_EJEC_INI, MODO_EJEC_END).trim()); // key
			String impPago = l.substring(IMPORTE_PAGO_INI, IMPORTE_PAGO_END).trim();
			// Integer enteroDouble = Integer.parseInt(impPago.substring(0,
			// impPago.length() - 2).trim());
			// String entero = Integer.toString(enteroDouble);
			// impPago = entero + "." + impPago.substring(impPago.length() -
			// 2).trim();;
			detail.setImpAPagar(Long.valueOf(impPago).longValue());
			detail.setMonedaPago(l.substring(158, 161)); // key
			detail.setFechaVtoCHPD(l.substring(FECHA_VENC_CHPD_INI, FECHA_VENC_CHPD_END).trim());
			detail.setRequerirRecibo(l.substring(169, 170));
			detail.setClausulaNoALaOrden(l.substring(170, 171));
			detail.setIncluFirma(l.substring(171, 172));
			detail.setAcompaniamientoCompAdjuntos(l.substring(171, 175));
			detail.setTxtRefAsoc01(l.substring(175, 255));
			detail.setTxtRefAsoc02(l.substring(255, 335));
			detail.setTxtRefAsoc02(l.substring(335, 415));
			detail.setInstrCustomServ(l.substring(415, 575));
			detail.setProvNumero(l.substring(NRO_BENEFICIARIO_INI, NRO_BENEFICIARIO_END).trim());
			detail.setProvNombre(l.substring(NOMBRE_BENEFICIARIO_INI, NOMBRE_BENEFICIARIO_END).trim());
			detail.setProvTipoDocum(l.substring(660, 663));
			detail.setProvCuitCuilCdi(l.substring(NRO_DOC_BENEF_INI, NRO_DOC_BENEF_END).trim());
			detail.setProvDomicilio(l.substring(674, 794));
	
			detail.setProvCodPostal(l.substring(794, 809));
			detail.setProvEmail(l.substring(809, 879));
			detail.setProvFax(l.substring(879, 904));
			detail.setProvMediosComunic(l.substring(904, 907));
	
			detail.setProvCbu(l.substring(CBU_INI, CBU_END).trim());
			detail.setProvSistemaCtaAAcreditar(l.substring(969, 971));
			detail.setProvMonedaCtaAAcreditar(l.substring(971, 974));
	
			if (l.length() > CTA_ADH_END) {
				detail.setCuentaCbu(l.substring(CTA_ADH_INI, CTA_ADH_END).trim());
			} else if (l.length() == CTA_ADH_END) {
				detail.setCuentaCbu(l.substring(CTA_ADH_INI).trim());
			}
	
			if (l.length() > MONEDA_END) {
				detail.setCuentaMoneda(l.substring(MONEDA_INI, MONEDA_END).trim());
			} else if (l.length() == MONEDA_END) {
				detail.setMonedaPago(l.substring(MONEDA_INI).trim());
			}
		}catch(Exception e){
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_INVALIDFILEDATA);
		}
		return detail;
	}
	
	/**
	 * This method formats the amounts
	 * 
	 * @param n
	 * @return
	 */
	public static String formatNumber(String n) {
		if (n == null)
			return "";

		if (n.length() > 2)
			return n.substring(0, n.length() - 2) + "." + n.substring(n.length() - 2).replaceAll(" ", "0");
		else
			return "0." + n;
	}
	

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		try {
			dto.setAdherent(getDataFromVector(1));
			sendDto.setAdherent(dto.getAdherent());
		} catch(Exception e) {
			DefinedLogger.SERVICE.error("No pude convertir a long el adherente para persistir transaccion", e);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTADHERENT);
		}
		
		dto.setOperador(getDataFromVector(2));
		dto.setEmpresa(getDataFromVector(3));	
		dto.setTypeFile(getDataFromVector(4));

		bo.enablePager(getDataFromVector(6));

		/*
		 * Validating...
		 * ------------------------------------------------------------------------
		 */
		DefinedLogger.SERVICE.debug("Realizando validaciones...");
		if(dto.getAdherent() == null)
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTADHERENT);
		
		//setting character hyphen and percentage to get the nroConv 
		if(dto.getOperador() != null && !dto.getOperador().equals("")) {
			dto.setOperador(dto.getOperador() + "-%");
		}
		
	}
	
	
	/**
	 * This method gets the account for the current PO register.
	 * If the register specifies CBU account, we obtain the account from the datasource
	 * If the register does not specify CBU account, we use the default account (if it exists) 
	 * @param det
	 * @param defaultAccount
	 * @return
	 * @throws ServiceException
	 */
	private FileAccountListItemDTO loadCurrentAccount(FilePaymentDetailDTO det, FileAccountListItemDTO defaultAccount) throws ServiceException {
		//Define and intialize variables
		BigInteger cbu = new BigInteger("0");
		FileAccountListItemDTO currentAccount = null;
		if(det.getCuentaCbu() == null || "".equalsIgnoreCase(det.getCuentaCbu().trim()))
			det.setCuentaCbu("0");
		 
			
		// Get account CBU
		try {
				cbu = new BigInteger(det.getCuentaCbu());
		} catch (Exception e) {
			DefinedLogger.SERVICE.error("No se pudo convertir el CBU de un registro a numero", e);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_CBUNOTVALID, e);
		}

		/*
		 * Validate CBU. - If it does not exist or is equal to the
		 * default CBU, so we use the account by default which already
		 * is in main memory. - If not, we must access to DB to get it.
		 */
		if (cbu.doubleValue() < 1 || Double.parseDouble(sendDto.getCbu()) == cbu.doubleValue()) {
			// Validate if the account by default exists
			if (defaultAccount != null) {
				det.setCuentaCbu(sendDto.getCbu());
				currentAccount = defaultAccount;
			} else {
				DefinedLogger.SERVICE.error("El registro no tiene una cuenta y no existe cuenta por defecto");
				currentAccount = null;
			}
		} else {
			// Access to data source
			currentAccount = sendBo.loadAccount(det.getCuentaCbu());
		}
		return currentAccount;
	}

	/**
	 * This method returns the currency numeric code according to the currency
	 * code given by the argument ("ARS", "USD")
	 * 
	 * @param currency
	 * @return
	 * @throws ServiceException
	 */
	private String getTransactionCurrencyByCode(String currency) throws ServiceException {
		DefinedLogger.SERVICE.debug("Moneda es: " + currency);
		return !StringUtil.isEmpty(currency) && currency.trim().equalsIgnoreCase("usd") ? "2" : "80";
	}
	
	

	/**
	 * This method generates the payment type code according to the payment type
	 * code given in PO register
	 * @param paymentType
	 * @return
	 */
	private String generatePaymentTypeForReport(String paymentType) throws ServiceException {
		String result = "";
		switch(Integer.parseInt(paymentType)) {
		case 25:
			result = CashManagementWsApplication.getProperty(ServiceConstant.PROP_SENDFILE_PAYMENTTYPE_3_CODE);
			break;
		case 1:
		case 2:
			result = CashManagementWsApplication.getProperty(ServiceConstant.PROP_SENDFILE_PAYMENTTYPE_1_CODE);
			break;
		case 3:
		case 4:
		case 7:
		case 9:
		case 20:
			result = CashManagementWsApplication.getProperty(ServiceConstant.PROP_SENDFILE_PAYMENTTYPE_2_CODE);
			break;
		case 10:
			result = CashManagementWsApplication.getProperty(ServiceConstant.PROP_SENDFILE_PAYMENTTYPE_4_CODE);
			break;
		default:
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_PONOTVALIDPAYMENTTYPE);
		}
		return result;
	}
	

	/**
	 * This method generates the payment type description
	 * according to the code specified by argument 
	 * @param paymentType
	 * @return
	 */
	private String generatePaymentTypeDescriptionForReport(String paymentType) {
		String result = "";
		switch(Integer.parseInt(paymentType)) {
		case 1003:
			result = CashManagementWsApplication.getProperty(ServiceConstant.PROP_SENDFILE_PAYMENTTYPE_3_DESCRIPTION);
			break;
		case 1001:
			result = CashManagementWsApplication.getProperty(ServiceConstant.PROP_SENDFILE_PAYMENTTYPE_1_DESCRIPTION);
			break;
		case 1002:
			result = CashManagementWsApplication.getProperty(ServiceConstant.PROP_SENDFILE_PAYMENTTYPE_2_DESCRIPTION);
			break;
		case 1004:
			result = CashManagementWsApplication.getProperty(ServiceConstant.PROP_SENDFILE_PAYMENTTYPE_4_DESCRIPTION);
			break;
		}
		return result;
	}

	
	/**
	 * This method updates the account status in the several reports (payment type report and account report)
	 * @param report
	 * @param det
	 * @param currentAccount
	 * @throws ServiceException
	 */
	private void updateAccountStatus(HashMap<String, FileAccountListItemDTO> report, FilePaymentDetailDTO det, FileAccountListItemDTO account)
			throws ServiceException {

		if(account == null || account.getAccountCbu() == null)
			return;

		try {
			if (report.containsKey(account.getAccountCbu())) {
				account = report.get(account.getAccountCbu());

				/*
				 * If it exists, we must validate is the current amount is
				 * greater than the last persisted
				 */
				if (account.getAmount() < det.getImpAPagar()) {
					// Update max amount
					account.setAmount(det.getImpAPagar());
				}

				// Update amount total
				account.setAmountTotal(account.getAmountTotal() + det.getImpAPagar());
			} else {
				FileAccountListItemDTO currentAccount = (FileAccountListItemDTO) ClassUtil.copyInstance(account);
				currentAccount.setAmount(det.getImpAPagar());
				currentAccount.setAmountTotal(det.getImpAPagar());
				report.put(currentAccount.getAccountCbu(), currentAccount);
			}
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT, e);
		}
	} 

	
	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs");
		setDataInVector(5, bo.dtoToXML(dto));
		setDataInVector(7, bo.getPagerAsXML());
		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando");
		dto = new FileStatusSendListDTO();
		bo = new FileStatusSendListBO();
		sendDto = new SendFileDTO();
		sendBo = new SendFileBO(); 
		
	}
}