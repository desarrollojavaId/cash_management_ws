package ar.com.gestionit.cashmanagement.service;

import java.util.Date;
import java.util.Iterator;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.factory.ServiceReturnFactory;
import ar.com.gestionit.cashmanagement.persistence.bo.GeneralRecoveredBO;
import ar.com.gestionit.cashmanagement.persistence.bo.RecoveredCheckCRUDBO;
import ar.com.gestionit.cashmanagement.persistence.bo.RecoveredCheckListBO;
import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredCheckCrudDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredCheckListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredCheckListItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;
import ar.com.gestionit.cashmanagement.util.StringUtil;

/**
 * Seleccionar deseleccionar cheque de recupero
 * @author jdigruttola
 */
public class RecoveredCheckCRUDService extends AbstractService {
	
	/**
	 * Constants
	 */
	private static final String ACTION_CREATE = "S";
	private static final String ACTION_DELETE = "D";
	private static final String ACTION_DELETE_ALL = "R";
	public static final int RESULT_OK = 0;
	public static final int ERROR_ADDED_CHECK = 1;

	/**
	 * This attribute is used to load the input parameters
	 */
	private RecoveredCheckCrudDTO input;

	/**
	 * Business object for this service
	 */
	private RecoveredCheckCRUDBO bo;
	
	/**
	 * General BO for CPD
	 */
	private GeneralRecoveredBO generalBo;

	/**
	 * This attribute keeps the current number of the check for this recovered
	 */
	private int checkTotal;

	/**
	 * This attribute keeps the current amount for this recovered
	 */
	private float amountTotal;


	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		bo = new RecoveredCheckCRUDBO();
		generalBo = new GeneralRecoveredBO();
		amountTotal = 0;
		checkTotal = 0;
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		input = (RecoveredCheckCrudDTO)
				bo.xmlToDto(getDataFromVector(5), RecoveredCheckCrudDTO.class);
		
		//Validate if the check list is right
		if(input == null) {
			if(getDataFromVector(4) != null && getDataFromVector(4).trim().equals(ACTION_DELETE_ALL)) {
				input = new RecoveredCheckCrudDTO();
			} else {
				DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_NOTVALIDCHECKLIST));
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTVALIDCHECKLIST);
			}
		}
		
		input.setAdherent(getDataFromVector(1));
		input.setAgreementNumber(getDataFromVector(2));
		input.setAgreementSubnumber(getDataFromVector(3));
		input.setAction(getDataFromVector(4));

		
		/*
		 * Validations...
		 * NOTE: adherent already is validated in the abstract service
		 */
		if(StringUtil.isEmpty(input.getAction())){
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_NOTFINDACTION));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFINDACTION);
		}
		if(!input.getAction().trim().equals(ACTION_CREATE)
				&& !input.getAction().trim().equals(ACTION_DELETE)
				&& !input.getAction().trim().equals(ACTION_DELETE_ALL)){
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_ACTIONNOTVALID));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ACTIONNOTVALID);
		}
		if(StringUtil.isEmpty(input.getAgreementNumber())){
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_OBLIGATORYAGREEMENT));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_OBLIGATORYAGREEMENT);
		}
		if(StringUtil.isEmpty(input.getAgreementSubnumber())){
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_OBLIGATORYSUBAGREEMENT));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_OBLIGATORYSUBAGREEMENT);
		}
		if( (input.getCheckList() == null || input.getCheckList().size() == 0)
				&& !input.getAction().trim().equals(ACTION_DELETE_ALL)){
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_VOIDCHECKLIST));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDCHECKLIST);
		}
		}

	@Override
	public void execute() throws ServiceException {
		DefinedLogger.SERVICE.debug("Ejecutando querys...");
		boolean create = false;
		int result = 0;
		
		/*
		 * Request number
		 * Executing query....
		 * ------------------------------------------------------------------------
		 */
		input.setRequestId(
				generalBo.executeSS0003(input.getAdherent(),
						input.getAgreementNumber(),
						input.getAgreementSubnumber()));
		
		/*
		 * Load general attributes
		 */
		Date d = new Date();
		input.setDate("'" + ServiceConstant.SDF_DATE_FOR_INPUT.format(d) + "'");
		input.setTimeStamp("'" + ServiceConstant.SDF_DATE_FULL.format(d) + "'");
		input.setHour("'" + ServiceConstant.SDF_HOUR.format(d) + "'");
		
		/*
		 * Process action
		 */
		//Validate if this is ACTION_DELETE_ALL. In this case, we must unmark all the selected hecks
		if(input.getAction().trim().equals(ACTION_DELETE_ALL)) {
			bo.delete(input);
		} else {
			//Process the other cases
			if(input.getAction().trim().equals(ACTION_CREATE))
				create = true;
			Iterator<String> i = input.getCheckList().iterator();
			while(i.hasNext()) {
				String a = i.next();
				a = ServiceUtil.validateNumberAndLength(a, 10, ServiceConstant.SERVICERETURN_KEY_NOTVALIDCHECKNUMBER);			
				input.setCheckId(a);
				if(create)
					result = bo.create(input);
				else
					result = bo.delete(input);
				
				//Validate if there are problems...
				if(result == ERROR_ADDED_CHECK) {
					reportError(ServiceConstant.SERVICERETURN_KEY_CHECKEXISTS);
					break;
				}
			}
		}
		
		/*
		 * Recovered check list summary
		 */
		RecoveredCheckListDTO rclDto = new RecoveredCheckListDTO();
		rclDto.setAdherent(input.getAdherent());
		rclDto.setAgreementNumber(input.getAgreementNumber());
		rclDto.setAgreementSubnumber(input.getAgreementSubnumber());
		rclDto.setAmountFrom("0");
		rclDto.setAmountTo("0");
		rclDto.setCheckNumber("0");
		rclDto.setDateRecoveredFrom("0");
		rclDto.setDateRecoveredTo("0");
		rclDto.setDateAcredFrom("0");
		rclDto.setDateAcredTo("0");
		rclDto.setDateCheckFrom("0");
		rclDto.setDateCheckTo("0");
		rclDto.setStatuses(null);
		rclDto.setBatchId(input.getRequestId());
		
		RecoveredCheckListBO rclBo = new RecoveredCheckListBO();
		rclDto = rclBo.findChecks(rclDto);
		
		if(rclDto != null && rclDto.getList() != null && rclDto.getList().size() > 0) {
			checkTotal = rclDto.getList().size();
			Iterator<RecoveredCheckListItemDTO> it = rclDto.getList().iterator();
			while(it.hasNext()) {
				try {
					amountTotal += Float.parseFloat(it.next().getAmount());
				} catch(Exception e) {}
			}
		}
		
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");
		setDataInVector(6, String.valueOf(checkTotal));
		setDataInVector(7, String.valueOf(amountTotal));

		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}
}