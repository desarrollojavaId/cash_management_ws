package ar.com.gestionit.cashmanagement.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.PortalPaymentDetailBO;
import ar.com.gestionit.cashmanagement.persistence.dto.PortalPaymentDetailDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ProofListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ProofListItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;

public class PortalPaymentDetailService extends AbstractService{

	PortalPaymentDetailBO bo;
	PortalPaymentDetailDTO dto;
	ProofListItemDTO proof;
	ProofListDTO proofList;

	@Override
	protected void initialize() throws ServiceException {
		bo = new PortalPaymentDetailBO();
		dto = new PortalPaymentDetailDTO();
		proof = new ProofListItemDTO();
		proofList = new ProofListDTO();
	}

	@Override
	protected void loadInputs() throws ServiceException {
		String paymentNumber = getDataFromVector(2);
		paymentNumber = ServiceUtil.validateNumberAndLengthObligatory(paymentNumber, 46,  ServiceConstant.SERVICERETURN_KEY_INVALIDPAYORDER, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYPAYORDER);
		dto.setPaymentNumber(paymentNumber);
		
		String paymentSubNumber = getDataFromVector(3);
		paymentSubNumber = ServiceUtil.validateNumberAndLengthObligatory(paymentSubNumber, 46,  ServiceConstant.SERVICERETURN_KEY_INVALIDSUBPAYORDER, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYSUBPAYORDER);
		dto.setPaymentSubNumber(paymentSubNumber);
		
	}
	@SuppressWarnings("unchecked")
	@Override
	protected void execute() throws ServiceException {
		dto = (PortalPaymentDetailDTO)bo.find(dto);
		Date fechaEmi=null;
		Date fechaPago=null;
		if(dto.getMtlfal()!=null){
			try{
				fechaEmi=ServiceConstant.SDF_DATE_FOR_INPUT.parse(dto.getMtlfal());
			}
			catch(ParseException e){
				e.printStackTrace();
			}
			dto.setMtlfal(ServiceConstant.SDF_DATE.format(fechaEmi));
		}
		if(dto.getMtlfer()!=null){
			try{
				fechaPago=ServiceConstant.SDF_DATE_FOR_INPUT.parse(dto.getMtlfer());
			}
			catch(ParseException e){
				e.printStackTrace();
			}
			dto.setMtlfer(ServiceConstant.SDF_DATE.format(fechaPago));
		}
		proof.setCustomerNumber(dto.getMtlqca());
		proof.setReferenceNumber(dto.getMtlqrc());
				
		proofList.setList((List<ProofListItemDTO>) bo.findList(proof));
		
	}
	
	@Override
	protected void loadOutputs() throws ServiceException {
		setDataInVector(4, dto.getMtlnep());
		setDataInVector(5, dto.getMtlqcp());
		setDataInVector(6, dto.getMtlkfp());
		setDataInVector(7, dto.getMtlqip());
		setDataInVector(8, dto.getMtljpa());
		setDataInVector(9, dto.getMtlkme() + "-" + dto.getMtlkes());
		setDataInVector(10, dto.getMtlfal());
		setDataInVector(11, dto.getMtlfer());
		setDataInVector(12, bo.dtoToXML(proofList));
	}
	
	/**
	 * I override this method to avoid the adherent validation
	 */
	@Override
	protected void validateBefore() throws ServiceException {
		//Validate if the reques is NULL
		if(request == null) {
			try {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTREQUEST);
			} catch(ServiceException e) {
				throw e;
			}
		}
	}
}