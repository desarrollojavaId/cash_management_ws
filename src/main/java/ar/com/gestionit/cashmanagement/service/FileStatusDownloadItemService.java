package ar.com.gestionit.cashmanagement.service;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.factory.ServiceReturnFactory;
import ar.com.gestionit.cashmanagement.persistence.bo.FileStatusDownloadItemBO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileStatusDownloadItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.TokenInputListDTO;
import ar.com.gestionit.cashmanagement.service.filemanager.FileManager;
import ar.com.gestionit.cashmanagement.service.filemanager.FileManagerDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;

/** Servicio de Consulta situacion cheque  **/
public class FileStatusDownloadItemService extends AbstractAccountManagerService {
	
	private FileStatusDownloadItemDTO dto;
	private FileStatusDownloadItemBO bo;
	private FileManager manager;

	/**
	 * File Manager DTO
	 */
	private FileManagerDTO fmDto;
	
	@Override
	protected void execute() throws ServiceException {
		
		DefinedLogger.SERVICE.debug("Ejecutando querys y cargando listas...");
		
		
		// Get file from File Manager datasource (MS SQL Server engine)
		fmDto = manager.getFile04(fmDto);

		//Cargo y seteo la lista de cheques 
		//dto = bo.find(dto);
		if(fmDto.getDownload()>0) {
			dto.setEstado("FINALIZADO");	
		}
		else{
			dto.setEstado("PENDIENTE");
		}
		
		
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		try {
			dto.setAdherent(getDataFromVector(1));
		} catch(Exception e) {
			DefinedLogger.SERVICE.error("No pude convertir a long el adherente para persistir transaccion", e);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTADHERENT);
		}
		
//		try {
//			dto.setAdherent(getDataFromVector(2));
//		} catch(Exception e) {
//			DefinedLogger.SERVICE.error("No pude convertir el token para persistir la transaccion", e);
//			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFINDTOKENS);
//		}
//
		
		fmDto.setTokenId(getDataFromVector(2));
				
		/*
		 * Validating...
		 * ------------------------------------------------------------------------
		 */
		DefinedLogger.SERVICE.debug("Realizando validaciones...");
		if(dto.getAdherent() == null)
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTADHERENT);
		
		// Validating...
		if (fmDto.getTokenId() == null || fmDto.getTokenId().trim().equals("")) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_TOKENNOTVALID);
		}
		
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs");
		setDataInVector(3, dto.getEstado());
		
		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando");
		dto = new FileStatusDownloadItemDTO();
		bo = new FileStatusDownloadItemBO();
		manager = new FileManager();
		fmDto = new FileManagerDTO();
	}
}