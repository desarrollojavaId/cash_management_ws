package ar.com.gestionit.cashmanagement.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.CardListBO;
import ar.com.gestionit.cashmanagement.persistence.dto.CardListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.CardListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.CardSummaryListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.CardSummaryListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ReferenceDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StatusInputList2DTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DateUtil;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;

public class CardListService extends AbstractService {

	/**
	 * Card List DTO
	 */
	private CardListDTO ioDto;
	
	public String prueba; 
	/**
	 * Card Summary List DTO
	 */
	private CardSummaryListDTO csDto;

	/**
	 * Business object for this service
	 */
	private CardListBO bo;
		
	DecimalFormat numberFormat = new DecimalFormat("#.00");
	
	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		bo = new CardListBO();
		csDto = new CardSummaryListDTO();
		ioDto = new CardListDTO();
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		ioDto.setAdherent(getDataFromVector(1));
		
		String a = getDataFromVector(2);
		a = ServiceUtil.validateNumberAndLength(a, 7, ServiceConstant.SERVICERETURN_KEY_INVALIDAGREEMENT);
		ioDto.setAgreementNumber(a);
		
		String b = getDataFromVector(3); 
		b = ServiceUtil.validateNumberAndLength(b, 7, ServiceConstant.SERVICERETURN_KEY_INVALIDSUBAGREEMENT);
		ioDto.setAgreementSubnumber(b);
		
		DefinedLogger.SERVICE.debug("Cargando referencias...");
		ReferenceDTO references = (ReferenceDTO)bo.xmlToDto(getDataFromVector(4), ReferenceDTO.class);
		
		DefinedLogger.SERVICE.debug("Cargando fechas...");
		String dateStartFrom = getDataFromVector(5);
		String dateStartTo = getDataFromVector(6);
		String dateLastMovFrom = getDataFromVector(7);
		String dateLastMovTo = getDataFromVector(8);
		
		ioDto.setDateStartFrom(ServiceUtil.validateDateForDepuration(dateStartFrom, depurationDate, ServiceConstant.SERVICERETURN_KEY_NOTVALIDDATESTARTFROM));
		ioDto.setDateStartTo(ServiceUtil.validateDate(dateStartTo, ServiceConstant.SERVICERETURN_KEY_NOTVALIDDATESTARTTO));
		ioDto.setDateLastMovFrom(ServiceUtil.validateDate(dateLastMovFrom, ServiceConstant.SERVICERETURN_KEY_NOTVALIDDATELASTMOVFROM));
		ioDto.setDateLastMovTo(ServiceUtil.validateDate(dateLastMovTo, ServiceConstant.SERVICERETURN_KEY_NOTVALIDDATELASTMOVTO));
		ServiceUtil.validateDateInterval(ioDto.getDateStartFrom(), ioDto.getDateStartTo(), ServiceConstant.SDF_DATE_FOR_INPUT);
		ServiceUtil.validateDateInterval(ioDto.getDateLastMovFrom(), ioDto.getDateLastMovTo(), ServiceConstant.SDF_DATE_FOR_INPUT);
		
		DefinedLogger.SERVICE.debug("Cargando estados...");
		StatusInputList2DTO statuses = (StatusInputList2DTO)
				bo.xmlToDto(getDataFromVector(9), StatusInputList2DTO.class);
		if(statuses != null && statuses.getStatuses().size() > 0) {
			ioDto.setStatuses(statuses.getStatuses());
		} else {
			ioDto.setStatuses(new ArrayList<String>());
		}
		
		bo.enablePager(getDataFromVector(10));
		
		/*
		 * Validating...
		 * ------------------------------------------------------------------------
		 */
		if(ioDto.getAdherent() == null
				|| ioDto.getAdherent().trim().equals(""))
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTADHERENT);
		
		//Reference validations...
		DefinedLogger.SERVICE.debug("Validando referencias...");
		if(references == null) {
			references = new ReferenceDTO();
		}
		String c = references.getDeudor1();
		ServiceUtil.validateLength(c,25,ServiceConstant.SERVICERETURN_KEY_DEBREFERENCELENGTHERROR);
		if (references.getDeudor1() == null || references.getDeudor1().trim().equals("")) {
			references.setDeudor1("0");
		}
		String d = references.getDeudor2();
		ServiceUtil.validateLength(d,25,ServiceConstant.SERVICERETURN_KEY_DEBREFERENCELENGTHERROR);
		if (references.getDeudor2() == null || references.getDeudor2().trim().equals("")) {
			references.setDeudor2("0");
		}
		String e = references.getComprobante1();
		ServiceUtil.validateLength(e, 60, ServiceConstant.SERVICERETURN_KEY_COMPREFERENCELENGTHERROR);
		if(references.getComprobante1() != null && !references.getComprobante1().equalsIgnoreCase(""))
			references.setComprobante1("%" + references.getComprobante1().toLowerCase() + "%");
		if (references.getComprobante1() == null || references.getComprobante1().trim().equals("")) {
			references.setComprobante1("");
		}
		String f = references.getComprobante2();
		ServiceUtil.validateLength(f, 19, ServiceConstant.SERVICERETURN_KEY_COMPREFERENCELENGTHERROR);
		if (references.getComprobante2() == null || references.getComprobante2().trim().equals("")) {
			references.setComprobante2("0");
		}
		DefinedLogger.SERVICE.debug("Seteando referencias...");
		ioDto.setReferences(references);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void execute() throws ServiceException {
		/*
		 * Card list
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		DefinedLogger.SERVICE.debug("Ejecutando querys...");
		ioDto.setList((List<CardListItemDTO>)bo.findList(ioDto));
		contentToExport.add(ioDto.getList());
		
		Iterator <CardListItemDTO> j = ioDto.getList().iterator();
		CardListItemDTO CLdto =null;
		while(j.hasNext()){
			CLdto = j.next();
			
			CLdto.setDateStart(DateUtil.normalizeDateFormat(CLdto.getDateStart()));
			CLdto.setDateLastMov(DateUtil.normalizeDateFormat(CLdto.getDateLastMov()));
		}
		
	
		/*
		 * Card Summary list
		 * -----------------------------------------------------------------------
		 * query execution...
		 */
		List<CardSummaryListItemDTO> summaryList = (List<CardSummaryListItemDTO>)bo.findTotals(ioDto);
		contentToExport.add(summaryList);
		
		/*
		 * Processing information....
		 * ------------------------------------------------------------------------
		 */
		//Validate if there are summaries to process
		DefinedLogger.SERVICE.debug("Validando resumenes...");
		if(summaryList != null && summaryList.size() > 0) {
			int total = 0;
			CardSummaryListItemDTO dto;
			
			//This iteration is only to get the total (I do not like to do it, but... If you have a better idea, avanti!)
			Iterator<CardSummaryListItemDTO> i = summaryList.iterator();
			while(i.hasNext()) {
				total += i.next().getAmount();
			}
			
			//This iteration is to get the percentages
			i = summaryList.iterator();
			while(i.hasNext()) {
				dto = i.next();
				dto.setPercentage(numberFormat.format(dto.getAmount() * 100 / total));
			}
		}else {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}
		csDto.setList(summaryList);
		
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");		
		setDataInVector(11, bo.dtoToXML(ioDto));
		setDataInVector(12, bo.dtoToXML(csDto));
		setDataInVector(13, bo.getPagerAsXML());
		
		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}
}