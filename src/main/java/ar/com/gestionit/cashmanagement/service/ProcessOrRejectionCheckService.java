package ar.com.gestionit.cashmanagement.service;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.ProcessOrRejectionCheckBO;
import ar.com.gestionit.cashmanagement.persistence.dto.ProcessOrRejectionCheckDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.TransactionDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;

/** Servicio de Consulta situacion cheque  **/
public class ProcessOrRejectionCheckService extends AbstractService {

	private static final String ARCH_AUTORIZADO = "Archivo_autorizado";
	private static final String ARCH_RECHAZADO = "Archivo_rechazado";

	private ProcessOrRejectionCheckDTO input;
	private ProcessOrRejectionCheckBO bo;


	@Override
	protected void execute() throws ServiceException {
		DefinedLogger.SERVICE.debug("Ejecutando transaction...");
		bo.executeTransaction(input);
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		String aux;
		
		input.setUser(request.getDrqus());

		try {
			input.setNroAdh(getDataFromVector(1));
		} catch(Exception e) {
			DefinedLogger.SERVICE.error("No pude convertir a long el adherente para persistir transaccion", e);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTADHERENT);
		}
		
		aux = getDataFromVector(4);
		ServiceUtil.validateLength(aux, 10, ServiceConstant.SERVICERETURN_KEY_INVALIDFILENAME);
		if(aux != null && !aux.equalsIgnoreCase(""))
			input.setIdLote(aux);
		else{
			DefinedLogger.SERVICE.error("No se informa el lote id");
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFILENAME);
		}
		
		//obtengo y valido la acción
		aux = getDataFromVector(5);
		if(aux.equalsIgnoreCase("P") || aux.equalsIgnoreCase("R"))
			input.setAccion(aux);
		else{
			DefinedLogger.SERVICE.error("No se indica una accion válida");
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORNOTACTION);
		}

		//Nro Convenio
		try {
			input.setNroConv(getDataFromVector(2));			
		} catch(Exception e) {
			DefinedLogger.SERVICE.error("No pude convertir a int el convenio para la anulación de cheques", e);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_INVALIDAGREEMENT);
		}
		//Nro Sub convenio
		//Cambiar el indice del vector hasta que nos informen el orden correcto
		try {
			input.setNroSubConv(getDataFromVector(3));			
		} catch(Exception e) {
			DefinedLogger.SERVICE.error("No pude convertir a int el subconvenio para la anulación de cheques", e);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_INVALIDSUBAGREEMENT);
		}

	}

	@Override
	protected void loadOutputs() throws ServiceException {
		setDataInVector(6, String.valueOf(serviceKey));
	}

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		input = new ProcessOrRejectionCheckDTO();
		bo = new ProcessOrRejectionCheckBO();	
	}


	@Override
	protected TransactionDTO buildTransactionDto() throws ServiceException {
		TransactionDTO dto = super.buildTransactionDto();
		//AUTO
		if(input.getAccion().equalsIgnoreCase("procesar")){
			dto.setStktrs(ARCH_AUTORIZADO);
		}
		//RECH
		else if (input.getAccion().equalsIgnoreCase("rechazar")){
			dto.setStktrs(ARCH_RECHAZADO);
		}
		dto.setStnars(input.getIdLote());////File ID
		dto.setStdobs("AutorizarRechazarArchivo");//Desc. Observaciones
		
		return dto;
	}
}
