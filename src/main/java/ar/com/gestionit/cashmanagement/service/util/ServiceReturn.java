package ar.com.gestionit.cashmanagement.service.util;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This POJO is used to specify a service return in the response
 * @author jdigruttola
 */
@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"code",
		"description"})
@XmlRootElement(name = ServiceConstant.SERVICERETURN)
public class ServiceReturn {
	
	/**
	 * This constants are used to report when we cannot access to the codes mapped
	 */
	public static final String ERROR_CODE = "-1";
	public static final String ERROR_DESC = "No se pudo obtener el estado de respuesta";
	
	/**
	 * Service return code
	 */
	@XmlElement(name=ServiceConstant.SERVICERETURN_FIELD_CODE, defaultValue="")
	private String code;
	
	/**
	 * Service return description 
	 */
	@XmlElement(name=ServiceConstant.SERVICERETURN_FIELD_DESC, defaultValue="")
	private String description;
	
	/**
	 * Constructor by default
	 */
	public ServiceReturn() {
		super();
	}
	
	/**
	 * Parameterized constructor
	 * @param code
	 * @param description
	 */
	public ServiceReturn(String code, String description) {
		super();
		this.code = code;
		this.description = description;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String toString() {
		return "Error code = " + code + ", Error description = " + description;
	}
}