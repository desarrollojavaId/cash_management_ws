package ar.com.gestionit.cashmanagement.service.filemanager;

import java.util.List;

public interface FileManagerMapper {
	public FileManagerDTO find(FileManagerDTO dto);
	public int insert(FileManagerDTO dto);
	public int update(FileManagerDTO dto);
	public FileManagerDTO getFile(FileManagerDTO dto);
	public FileManagerDTO getEscrowFile(FileManagerDTO dto);
	public FileManagerDTO getFileTest(FileManagerDTO dto);
	public FileManagerDTO getFile04(FileManagerDTO dto);	
}