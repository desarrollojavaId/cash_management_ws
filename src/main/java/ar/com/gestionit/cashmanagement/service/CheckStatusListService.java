package ar.com.gestionit.cashmanagement.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.CheckStatusListBO;
import ar.com.gestionit.cashmanagement.persistence.dto.CheckStatusListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.CheckStatusListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.CheckStatusUnPrintedDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StatusInputListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StatusInputListItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.ClassUtil;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.NumberUtil;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;

/** Servicio de Consulta situacion cheque  **/
public class CheckStatusListService extends AbstractAccountManagerService {
	
	private CheckStatusListDTO dto;
	private CheckStatusUnPrintedDTO uDto;
	private CheckStatusListBO bo;
	
	@Override
	protected void execute() throws ServiceException {
		
		DefinedLogger.SERVICE.debug("Ejecutando querys y cargando listas...");
		/*
		 * Executing query....
		 * ------------------------------------------------------------------------
		 */
		//Cargo y seteo la lista de cheques 
		List<CheckStatusListItemDTO> list = bo.findChk(dto);
		if (list == null || list.size() == 0){
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}
		dto.setList(list);
		
		validateResult(dto.getList(), dto.getNroAdh());
		//Cargo y seteo la cantidad de cheques sin imprimir
		String countChk = bo.findChkUnPrinted(dto);
		uDto.setCantCheSinImp(countChk);
		//Cargo y seteo la cantidad total de cheques
		String countTotChk = bo.findTotChk(dto);
		uDto.setCantTotChks(countTotChk);
		List<CheckStatusListItemDTO> exportList = new ArrayList<CheckStatusListItemDTO>();
		Iterator<CheckStatusListItemDTO> i = dto.getList().iterator();
		CheckStatusListItemDTO exportDTO = null;
		CheckStatusListItemDTO itemDTO = null;
		
		while(i.hasNext()){
			itemDTO = i.next();
			itemDTO.setExportCuenta(itemDTO.getSucursal() + "-" + itemDTO.getCuenta() + "-" + itemDTO.getSuboperacion());
			try{
				exportDTO = (CheckStatusListItemDTO) ClassUtil.copyInstance(itemDTO);
				if(exportDTO.getImporte() != null && !exportDTO.getImporte().equalsIgnoreCase(""))
					exportDTO.setImporte(NumberUtil.twoDecimalMax(Float.parseFloat(exportDTO.getImporte())));
				exportList.add(exportDTO);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		contentToExport.add(exportList);
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		try {
			dto.setNroAdh (getDataFromVector(1));
		} catch(Exception e) {
			DefinedLogger.SERVICE.error("No pude convertir a long el adherente para persistir transaccion", e);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTADHERENT);
		}
		dto.setNroCheDsd(getDataFromVector(3));
		dto.setNroCheHas(getDataFromVector(4));
		dto.setNombreBenef(getDataFromVector(5));
		dto.setDocBenef(getDataFromVector(6));
		dto.setImpDesde(getDataFromVector(7));
		dto.setImpHasta(getDataFromVector(8));
		dto.setNroCtaCash(getDataFromVector(9));
		
		DefinedLogger.SERVICE.debug("Cargando, formateando y validando estados...");
		DefinedLogger.SERVICE.debug("Los estados son: " + getDataFromVector(2));
		StatusInputListDTO statuses = (StatusInputListDTO) bo.xmlToDto(getDataFromVector(2), StatusInputListDTO.class);
		if(statuses != null && statuses.getStatuses() != null) {
			for(StatusInputListItemDTO item : statuses.getStatuses()){
				DefinedLogger.SERVICE.debug("Seteo el estado: " + item.getNumber());
			}
			dto.setLstEstados(statuses.getStatuses());
		} else {
			DefinedLogger.SERVICE.debug("Seteo la lista de estados vacías");
			dto.setLstEstados(new ArrayList<StatusInputListItemDTO>());
		}
					
		
		bo.enablePager(getDataFromVector(10));

		/*
		 * Validating...
		 * ------------------------------------------------------------------------
		 */
		DefinedLogger.SERVICE.debug("Realizando validaciones...");
		if(dto.getNroAdh() == null)
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTADHERENT);
		
		
		dto.setNroCheDsd(ServiceUtil.validateNumberAndLength(dto.getNroCheDsd(), 8, ServiceConstant.SERVICERETURN_KEY_CHECKFROM));
		dto.setNroCheHas(ServiceUtil.validateNumberAndLength(dto.getNroCheHas(), 8, ServiceConstant.SERVICERETURN_KEY_CHECKTO));
		if (!dto.getNroCheHas().equalsIgnoreCase("0")) 
			ServiceUtil.validateInterval(dto.getNroCheDsd(), dto.getNroCheHas(), ServiceConstant.SERVICERETURN_KEY_CHECKORDERRANGE);
		
		ServiceUtil.validateLength(dto.getNombreBenef(), 40, ServiceConstant.SERVICERETURN_KEY_INVALIDBENEFICIARYNAME);
		if(!dto.getNombreBenef().equalsIgnoreCase(""))
			dto.setNombreBenef("%" + dto.getNombreBenef().toLowerCase() + "%");
		dto.setDocBenef(ServiceUtil.validateNumberAndLength(dto.getDocBenef(), 11, ServiceConstant.SERVICERETURN_KEY_INVALIDBENEFICIARYDOC));
		
		dto.setImpDesde(ServiceUtil.validateNumberAndLength(dto.getImpDesde(),16, ServiceConstant.SERVICERETURN_KEY_INVALIDAMOUNTFROM));
		dto.setImpHasta(ServiceUtil.validateNumberAndLength(dto.getImpHasta(), 16, ServiceConstant.SERVICERETURN_KEY_INVALIDAMOUNTTO));
		if (!dto.getImpHasta().equalsIgnoreCase("0"))
			ServiceUtil.validateInterval(dto.getImpDesde(), dto.getImpHasta(), ServiceConstant.SERVICERETURN_KEY_AMOUNTINTERVALNOTVALID);
		
		
		dto.setNroCtaCash(ServiceUtil.validateNumberAndLength(dto.getNroCtaCash(), 22, ServiceConstant.SERVICERETURN_KEY_INVALIDACCOUNT));
		
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs");
		setDataInVector(12, bo.getPagerAsXML());
		setDataInVector(11, bo.dtoToXML(dto) + bo.dtoToXML(uDto));
		
		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando");
		dto = new CheckStatusListDTO();
		uDto = new CheckStatusUnPrintedDTO();
		bo = new CheckStatusListBO();
	}
}