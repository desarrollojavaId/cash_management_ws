package ar.com.gestionit.cashmanagement.service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.core.task.TaskExecutor;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.ServiceId;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.factory.ServiceReturnFactory;
import ar.com.gestionit.cashmanagement.persistence.bo.EscrowSendFileBO;
import ar.com.gestionit.cashmanagement.persistence.bo.FileContentBO;
import ar.com.gestionit.cashmanagement.persistence.bo.TransactionBO;
import ar.com.gestionit.cashmanagement.persistence.dto.EscrowSendFileDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileContentDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileInputListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.TokenInputListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.TransactionDTO;
import ar.com.gestionit.cashmanagement.service.filemanager.FileManagerDTO;
import ar.com.gestionit.cashmanagement.service.filemanager.FileManagerOutputDTO;
import ar.com.gestionit.cashmanagement.service.routine.DownloadFileListRoutine;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.StringUtil;
import ar.com.gestionit.cashmanagement.util.XMLUtil;

public class DownloadFileListRService extends AbstractService {
	
	/**
	 * BOs use in this service
	 */
	private EscrowSendFileBO bo;

	/**
	 * Service DTO
	 */
	private EscrowSendFileDTO dto;

	/**
	 * File Manager DTO
	 */
	private FileManagerDTO fmDto;

	/**
	 * Escrow status
	 */
	private String status = "";
	
	/**
	 * This attribute keeps the date when the service started to be executed
	 */
	private long startedDate;
	
	/**
	 * This attribute keeps the max time to execute the service
	 */
	private long timeout;
	
	/**
	 * Timeout by default. This should not be used
	 */
	private static final long DEFAULT_TIMEOUT = 85000;
	
	/**
	 * Thread pool
	 */
	private TaskExecutor executor = (TaskExecutor) CashManagementWsApplication.getBean(TaskExecutor.class);
	
	
	
	/*****************************/
	//  Atributos del servicio
	/*****************************/
	
	/**
	 * File content business object
	 */
	private FileContentBO bo1;

	/**
	 * Service DTO
	 */
	private FileManagerDTO dtoInput;

	/**
	 * This DTO is used for each file given in the list
	 */
	private FileContentDTO dtoFileContent;

	/**
	 * This attribute keeps the token for each file to process 
	 */
	private String currentToken;

	/**
	 * This attribute keeps the file name for each file to process 
	 */
	private String currentFileName;
	
	
	@Override
	protected void initialize() throws ServiceException {
		startedDate = new Date().getTime();
		try {
			timeout = CashManagementWsApplication.getPropertyAsLong(ServiceConstant.PROP_ESCROWSENDFILE_TIMEOUT);
		} catch(Exception e) {
			timeout = DEFAULT_TIMEOUT;
		}
		
		bo = new EscrowSendFileBO();
		transactionBo = new TransactionBO();
		fmDto = new FileManagerDTO();
		dto = new EscrowSendFileDTO();
		dtoInput = new FileManagerDTO();
		dtoInput.setList(new ArrayList<FileManagerOutputDTO>());
	}

	@Override
	protected void loadInputs() throws ServiceException {
		/*
		 * This is a workaround because buildTransaction() method
		 * will try to get the adherent from the position 1 of the
		 * vectors. So, we  
		 * FIXME: I think that a better way could exist
		 */
		setDataInVector(1, "0");
		
		/*****************************/
		//  validacion del servicio
		/*****************************/
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		//Get file name list
		FileInputListDTO lstFiles = (FileInputListDTO) bo.xmlToDto(getDataFromVector(3), FileInputListDTO.class);
		if(lstFiles == null || lstFiles.getFiles() == null || lstFiles.getFiles().size() == 0) {
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_NOTFINDFILES));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFINDFILES);
		}

		//Get token list
		TokenInputListDTO lstToken = (TokenInputListDTO) bo.xmlToDto(getDataFromVector(4), TokenInputListDTO.class);
		if(lstToken == null || lstToken.getTokens() == null || lstToken.getTokens().size() == 0) {
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_NOTFINDTOKENS));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFINDTOKENS);
		}

		//Validate if the size of the linked lists is different
		if(lstFiles.getFiles().size() != lstToken.getTokens().size()) {
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_FILETOKENNUMBER));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_FILETOKENNUMBER);
		}

		//Validate if any element of the both lists is empty
		for(int i = 0; i < lstFiles.getFiles().size(); i++) {
			if(StringUtil.isEmpty(lstFiles.getFiles().get(i))
					|| StringUtil.isEmpty(lstToken.getTokens().get(i))) {
				DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_LISTELEMENTEMPTY));
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_LISTELEMENTEMPTY);
			}
		}

		dtoInput.setFiles(lstFiles.getFiles());
		dtoInput.setTokens(lstToken.getTokens());
		
		
	}

	@SuppressWarnings("static-access")
	@Override
	public void execute() throws ServiceException {
		
		//Initialize thread where the routine (service business) will be executed. This is a CALLABLE implementation
		DownloadFileListRoutine routine = new DownloadFileListRoutine();
		
		try {
			//Pass necessary data for the routine will be able to run
			routine.setChannel(CHANNEL);
			routine.setProcessId(request.getProcessId());
			routine.setUser(request.getDrqus());
			routine.setDto(dto);
			routine.setFmDto(fmDto);
			routine.setDtoInput(dtoInput);
			String invokedFunction = "";
			if(getInvokedFunction() == null){
				invokedFunction = ServiceId.getProperty(this.getClass().getSimpleName());
				setInvokedFunction(invokedFunction);
			}
			//If there is not a mapped invoked function, so this will be empty...
			if(getInvokedFunction() == null)
				invokedFunction = "";
			//Validate the variable long
			if(getInvokedFunction() != null && getInvokedFunction().length() > 25) {
				invokedFunction = getInvokedFunction().substring(0, 24);
			}
			routine.setInvokedFunction(invokedFunction);
			
			routine.setServiceKey(serviceKey);
			//Execute routine (execute thread)
			executor.execute(routine);
		} catch (Exception e) {
			if(e instanceof ServiceException) {
				throw (ServiceException) e;
			}
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT, e);
		}
		
		long currentTime = new Date().getTime();
		while(!routine.isFinished() && (currentTime - startedDate) < (timeout - 5000)) {
			try {
				Thread.currentThread().sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			currentTime = new Date().getTime();
		}
		//Validate exceptionKey
		if(!"".equals(routine.getExceptionKey())){
			throw new ServiceException(routine.getExceptionKey());
		}
			
		status = routine.getStatus();
	}
	
	@Override
	protected void loadOutputs() throws ServiceException {
		
		setDataInVector(5, XMLUtil.dtoToXML(dtoInput));
		setDataInVector(6, status);
		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}
	
	/**
	 * We override this method to avoid the adherent validation
	 * and create a fictional adherent to can work
	 */
	@Override
	protected void validateBefore() throws ServiceException {
	}
	
	/**
	 * THIS IS A WORKAROUND FOR THIS SERVICE BECAUSE OF IT IS NOT ADHERENT NUMBER IN THE REQUEST
	 * This method generates a transaction DTO with the data
	 * by default already loaded
	 * @return
	 * @throws ServiceException
	 */
	@Override
	protected TransactionDTO buildTransactionDto() throws ServiceException {
		//Generate new transaction
		TransactionDTO dto = new TransactionDTO();

		//Service name --------------------------------------
		String serviceName = this.getClass().getSimpleName();
		if(serviceName != null && serviceName.length() > 60)
			dto.setServiceName(serviceName.substring(0, 59));
		else
			dto.setServiceName(serviceName);

		/*
		 * Invoked function --------------------------------------
		 * 
		 * Validate if the service implementation loads this attribute
		 * (this case should happen only if the service implementation
		 * has more than one option).
		 * If not, this attribute should be NULL always
		 */
		if(getInvokedFunction() == null)
			setInvokedFunction(ServiceId.getProperty(serviceName));
		//If there is not a mapped invoked function, so this will be empty...
		if(getInvokedFunction() == null)
			setInvokedFunction("");
		//Validate the variable long
		if(getInvokedFunction().length() > 25) {
			setInvokedFunction(getInvokedFunction().substring(0, 24));
		}
		dto.setStktrs(getInvokedFunction());
		
		//Other fields --------------------------------------
		dto.setStkcnl(CHANNEL);
		dto.setStqadh(0);
		dto.setStidpr(request.getProcessId());
		
		return dto;
	}

	@Override
	protected void reportTransaction() throws ServiceException {
		
	}
	
}