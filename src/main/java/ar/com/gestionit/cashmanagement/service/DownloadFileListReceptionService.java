package ar.com.gestionit.cashmanagement.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.factory.ServiceReturnFactory;
import ar.com.gestionit.cashmanagement.persistence.bo.FileContentBO;
import ar.com.gestionit.cashmanagement.persistence.dto.ArchiveInputListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileContentDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileInputListDTO;
import ar.com.gestionit.cashmanagement.service.filemanager.FileManager;
import ar.com.gestionit.cashmanagement.service.filemanager.FileManagerDTO;
import ar.com.gestionit.cashmanagement.service.filemanager.FileManagerOutputDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.service.util.ServiceReturn;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.StringUtil;
import ar.com.gestionit.cashmanagement.util.XMLUtil;

public class DownloadFileListReceptionService extends AbstractService {

	/**
	 * File content business object
	 */
	private FileContentBO bo;

	/**
	 * Service DTO
	 */
	private FileManagerDTO dtoInput;

	/**
	 * This DTO is used for each file given in the list
	 */
	private FileContentDTO dtoFileContent;

	/**
	 * This attribute keeps the token for each file to process 
	 */
	private String currentToken;

	/**
	 * This attribute keeps the file name for each file to process 
	 */
	private String currentFileName;

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		bo = new FileContentBO();
		dtoInput = new FileManagerDTO();
		dtoInput.setList(new ArrayList<FileManagerOutputDTO>());
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parámetros de entrada...");
		//Get file name list
		FileInputListDTO lstFiles = (FileInputListDTO) bo.xmlToDto(getDataFromVector(5), FileInputListDTO.class);
		if(lstFiles == null || lstFiles.getFiles() == null || lstFiles.getFiles().size() == 0) {
			DefinedLogger.SERVICE.debug(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_NOTFINDFILES));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFINDFILES);
		}

		//Get token list
		ArchiveInputListDTO lstToken = (ArchiveInputListDTO) bo.xmlToDto(getDataFromVector(6), ArchiveInputListDTO.class);
		if(lstToken == null || lstToken.getTokens() == null || lstToken.getTokens().size() == 0) {
			DefinedLogger.SERVICE.debug(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_NOTFINDTOKENS));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFINDTOKENS);
		}

		//Validate if the size of the linked lists is different
		if(lstFiles.getFiles().size() != lstToken.getTokens().size()) {
			DefinedLogger.SERVICE.debug(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_FILETOKENNUMBER));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_FILETOKENNUMBER);
		}

		//Validate if any element of the both lists is empty
		for(int i = 0; i < lstFiles.getFiles().size(); i++) {
			if(StringUtil.isEmpty(lstFiles.getFiles().get(i))
					|| StringUtil.isEmpty(lstToken.getTokens().get(i))) {
				DefinedLogger.SERVICE.debug(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_LISTELEMENTEMPTY));
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_LISTELEMENTEMPTY);
			}
		}

		dtoInput.setFiles(lstFiles.getFiles());
		dtoInput.setTokens(lstToken.getTokens());
	}

	@Override
	public void execute() throws ServiceException {
		/*
		 * Por cada id del archivo realizo cada una de las siguientes operaciones.
		 */
		DefinedLogger.SERVICE.debug("Descargando archivos...");
		FileManagerOutputDTO output;
		for(int i = 0; i < dtoInput.getFiles().size(); i++) {
			dtoFileContent = new FileContentDTO();			
			//Get the file name (file id) and its token
			dtoFileContent.setFileId(dtoInput.getFiles().get(i));
			currentToken = dtoInput.getTokens().get(i);
			currentFileName = dtoInput.getFiles().get(i);
			DefinedLogger.SERVICE.debug("procesando el archivo: " + currentFileName );
			
			/*
			 * File library
			 * This method obtains the library where the file is allocated according
			 * to the file ID given by the request
			 */
			executeFileLibrary();
			
			/*
			 * Get file content
			 * Validate if the library was found. In this case, we get the file content
			 * In other case, we try to get the file content by a auxiliary table 
			 */
			if(!StringUtil.isEmpty(dtoFileContent.getLibrary())) {
				//File content by library
				executeFileContentByLibrary();
			} else {
				//Auxiliary table
				executeAuxiliaryTable();
				
				//File content by auxiliary table
				executeFileContentByAuxiliaryTable();
			}
			
			/*
			 * Change file status to Downloaded 
			 */
			executeFileStatus();

			/*
			 * Persist the file for the ws client (frontend)
			 */
			DefinedLogger.SERVICE.debug("Comienza la actualización en el sql : " + currentFileName );
			executeDownloader();
			DefinedLogger.SERVICE.debug("Finaliza la actualización en el sql : " + currentFileName );
			/*
			 * Generate the list for the response
			 */
			DefinedLogger.SERVICE.debug("Generando response...");
			output = new FileManagerOutputDTO();
			output.setFileId(dtoInput.getFiles().get(i));
			output.setTokenId(dtoInput.getTokens().get(i));
			dtoInput.getList().add(output);
		}
	}

	/**
	 * This method obtains the library where the file is allocated
	 * @throws ServiceException
	 */
	private void executeFileStatus() throws ServiceException {
		DefinedLogger.SERVICE.debug("Obteniendo librerías...");
		/*
		 * File library
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		bo.updateFileStatus(dtoFileContent);

		/*
		 * File library
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		dtoFileContent.setProcessId(serviceKey);
		bo.updateFileSstrans(dtoFileContent);
	}

	/**
	 * This method obtains the library where the file is allocated
	 * @throws ServiceException
	 */
	private void executeFileLibrary() throws ServiceException {
		DefinedLogger.SERVICE.debug("Obteniendo librerías...");
		/*
		 * File library
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		FileContentDTO aux = bo.findLibraryReception(dtoFileContent);

		/*
		 * Processing information....
		 * ------------------------------------------------------------------------
		 */
		if(aux == null) {
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_NOTFINDFILES).toString() + " RESPUESTA EN NULL");
			dtoFileContent.setLibrary(null);
		} else if (StringUtil.isEmpty(aux.getLibrary())) {
			DefinedLogger.SERVICE.warn(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_NOTFINDFILES).toString());
			dtoFileContent.setLibrary(null);
			dtoFileContent.setMessageNumber(aux.getMessageNumber());
		} else {
			dtoFileContent.setLibrary(aux.getLibrary());
		}
	}
	
	/**
	 * This method obtains the auxiliary table where the file is allocated
	 * if the library was not found previously
	 * @throws ServiceException
	 */
	private void executeAuxiliaryTable() throws ServiceException {
		DefinedLogger.SERVICE.debug("Obteniendo tabla auxiliar donde se encuentra el archivo...");
		/*
		 * Auxiliary table
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		dtoFileContent = bo.findAuxiliaryTable(dtoFileContent);
	}

	/**
	 * This method obtains the file content from the data source by library
	 * and processes it.
	 * @throws ServiceException
	 */
	private void executeFileContentByLibrary() throws ServiceException {
		DefinedLogger.SERVICE.debug("Obteniendo y procesando contenido de archivo...");
		/*
		 * File content
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		DefinedLogger.SERVICE.debug("Obteniendo el archivo en el as400: " + currentFileName );
		List<String> list = bo.findFile(dtoFileContent);
		DefinedLogger.SERVICE.debug("Se obtuvo el archivo en el as400: " + currentFileName );
		/*
		 * Processing information....
		 * ------------------------------------------------------------------------
		 */
		//Define and initialize variables
		Iterator<String> i = list.iterator();
		StringBuilder fileContent = new StringBuilder();
		//Parse to string the file content
		DefinedLogger.SERVICE.debug("Se arma el archivo para enviar al sql: " + currentFileName );
		while(i.hasNext()){
			fileContent.append(i.next().trim()).append("\r\n");
		}
		DefinedLogger.SERVICE.debug("Finaliza el armado del archivo para el sql : " + currentFileName ); 
		//dtoFileContent.setFileContent(fileContent.toString().replaceAll("\n", "\r\n"));
		
		dtoFileContent.setFileContent(fileContent.toString());
		
	}
	
	/**
	 * This method obtains the file content from the data source according to an auxiliary table
	 * and processes it.
	 * 
	 * NOTE:
	 * Si el campo SANLIB está en blanco, significa que todavía no está generado. 
	 * En este caso, con el número de mensaje del campo nuevo SANMMR, 
	 * obtiene la información del MTX0001D si se trata de archivos de Pagos (QN, QO, QB, QC, QX, EC), 
	 * de MRX0001D en el caso de archivos de Cobros (SR, SC, SH, EG, SE, SD, ST, SF, SB, MA, DS, CA),
	 * donde  deja disponible un archivo para la descarga del cliente.
	 * 
	 * @throws ServiceException
	 */
	private void executeFileContentByAuxiliaryTable() throws ServiceException {
		DefinedLogger.SERVICE.debug("Obteniendo contenido de archivo desde tabla auxiliar...");
		
		/*
		 * File content
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		DefinedLogger.SERVICE.debug("Obteniendo el archivo en el as400: " + currentFileName );
		List<String> list = bo.findFileByAuxiliaryTable(dtoFileContent);
		DefinedLogger.SERVICE.debug("Se obtuvo el archivo en el as400: " + currentFileName );
		/*
		 * Processing information....
		 * ------------------------------------------------------------------------
		 */
		//Define and initialize variables
		Iterator<String> i = list.iterator();
		StringBuilder fileContent = new StringBuilder();
		DefinedLogger.SERVICE.debug("Se arma el archivo para enviar al sql: " + currentFileName );
		//Parse to string the file content
		while(i.hasNext())
			fileContent.append(i.next().trim()).append("\r\n");

		DefinedLogger.SERVICE.debug("Finaliza el armado del archivo para el sql : " + currentFileName );
		
		dtoFileContent.setFileContent(fileContent.toString());
	}

	/**
	 * This method persists the file in the appropriate data source.
	 * The WS client (frontend) will access it by this data source.
	 * @throws ServiceException
	 */
	private void executeDownloader() throws ServiceException {
		try {
			
			dtoInput.setFileContent(dtoFileContent.getFileContent().getBytes());
			dtoInput.setTokenId(currentToken);
			dtoInput.setFileName(currentFileName + ".txt");
			FileManager manager = new FileManager();
			if(manager.updateFile(dtoInput) == 0) {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFINDTOKENINDB);
			}
		} catch (ServiceException e) {
			throw e;
		} catch (Exception e) {
			DefinedLogger.SERVICE.error("Falló el seteo de campos del dto (executeDownloader...", e);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		}
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");
		setDataInVector(7, XMLUtil.dtoToXML(dtoInput));

		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}
	
	@Override
	protected void reportResponse(ServiceReturn r, int level) {
		super.reportResponse(r, level);
		if(r == null || r.getDescription() == null)
			return;
		setDataInVector(2, r.getDescription());
	}
	
	
}