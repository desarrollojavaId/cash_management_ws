package ar.com.gestionit.cashmanagement.service;


import java.util.Iterator;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.ProcessFileBO;
import ar.com.gestionit.cashmanagement.persistence.bo.TransactionBO;
import ar.com.gestionit.cashmanagement.persistence.dto.ProcessFileDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.SignatoryListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.TransactionDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.StringUtil;
import ar.com.gestionit.cashmanagement.util.XMLUtil;

public class ProcessFileService extends AbstractService {

	/**
	 * Constants
	 */
	private static final String ACTION_AUTHORIZE = "A";
	private static final String ACTION_REJECT = "R";

	private static final String FILE_TYPE_PD = "PD";
	//	private static final short CANAL = '5';
	/**
	 * BOs use in this service
	 */
	private ProcessFileBO bo;
	private TransactionBO transactionBo;

	/**
	 * This keeps the values loaded during the execution to persist
	 * the transaction in the ending of execution
	 */
	private TransactionDTO transactionDto;

	/**
	 * Service DTO
	 */
	private ProcessFileDTO dto;

	/**
	 * This attribute contains the file type for validations
	 */
	private String fileType;

	@Override
	protected void initialize() throws ServiceException {
		bo = new ProcessFileBO();
		transactionBo = new TransactionBO();

		//Initialize with the values by default
		transactionDto = super.buildTransactionDto();
	}

	@Override
	protected void loadInputs() throws ServiceException {
		//Initialize DTO by this way because this contains embedded the signatory list (I avoid creating a new DTO)
		DefinedLogger.SERVICE.info("Info - Lista de Adherentes: " + getDataFromVector(5));
		DefinedLogger.SERVICE.debug("Debug - Lista de Adherentes: " + getDataFromVector(5));
		DefinedLogger.SERVICE.error("Error - Lista de Adherentes: " + getDataFromVector(5));
		dto = (ProcessFileDTO) XMLUtil.xmlToDto(getDataFromVector(5), ProcessFileDTO.class);

		/*
		 * If the authorization does not pertain to the new schema of signatory,
		 * the signatories are not mandatory. So, this could be empty 
		 */
		if(dto == null)
			dto = new ProcessFileDTO();

		dto.setAdherent(getDataFromVector(1));
		dto.setFileId(getDataFromVector(4));
		dto.setNewSchema(getDataFromVector(6));
		dto.setAction(getDataFromVector(7));

		if (dto.getSignatoryList() != null && dto.getSignatoryList().size() > 0){
			Iterator<SignatoryListItemDTO> i = dto.getSignatoryList().iterator();
			SignatoryListItemDTO item = null;
			int j;
			while(i.hasNext()) {
				for (j = 1; j <= dto.getSignatoryList().size(); j++){
					item = i.next();
					item.setSecuencia(String.valueOf(j));
				}
			}
		}

		if(StringUtil.isEmpty(dto.getFileId())) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFILEID);
		}
		if(StringUtil.isEmpty(dto.getNewSchema())
				|| (!dto.getNewSchema().trim().equals(String.valueOf(ServiceConstant.TRUE)))
				&& !dto.getNewSchema().trim().equals(String.valueOf(ServiceConstant.FALSE))) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTNEWSCHEMA);
		}
		if(StringUtil.isEmpty(dto.getAction())
				|| (!dto.getAction().trim().equalsIgnoreCase(ACTION_AUTHORIZE))
				&& !dto.getAction().trim().equalsIgnoreCase(ACTION_REJECT)) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTACTION);
		}
		if(StringUtil.isEmpty(dto.getAdherent())) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTADHERENT);
		}

		/*
		 * If the authorization pertains to the new schema of signatory,
		 * so the signatory list is mandatory
		 */
		if(dto.getNewSchema().trim().equals(ServiceConstant.TRUE)
				&& (dto.getSignatoryList() == null
				|| dto.getSignatoryList().size() == 0)) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_SIGNATORIESMANDATORY);
		}
		/*
		 * If the authorization pertains to the new schema of signatory,
		 * so the signatory list is mandatory
		 */
		if(dto.getNewSchema().trim().equals(ServiceConstant.TRUE)
				&& (dto.getSignatoryList() != null
				&& dto.getSignatoryList().size() != 0)) {
			for(SignatoryListItemDTO aux : dto.getSignatoryList()){
				if (aux.getCountry() == null || aux.getDocumentNumber() == null || aux.getDocumentType() == null){
					throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_SIGNATORIESDATAMANDATORY);
				}
			}
		}

		// TODO VALIDAR LA RELACIÓN ENTRE EL BOOLEANO DE NUEVO ESQUEMA QUE HAY EN ENVIAR ARCHIVO Y EN ESTE SERVICIO

	}

	@Override
	public void execute() throws ServiceException {
		//Get file type for several validations
		fileType = getFileType(dto.getFileId());

		/*
		 * This method gets the file status to know if we can execute
		 * the action given by the request
		 */
		getFileStatus();


		//Execute a routine according to the sent action
		if(dto.getAction().trim().equalsIgnoreCase(ACTION_REJECT)) {
			DefinedLogger.SERVICE.info("El archivo " + dto.getFileId() + " sera eliminado.");
			rejectFile();
		} else {
			DefinedLogger.SERVICE.info("El archivo " + dto.getFileId() + " sera ejecutado.");
			executeFile();
		}

		if(dto.getNewSchema().trim().equals(ServiceConstant.TRUE)){
			bo.insert(dto);
		}
	}

	/**
	 * This method gets the file status to know if we can execute the action given by the request
	 * @throws ServiceException
	 */
	private void getFileStatus() throws ServiceException {
		/*
		 * Find the file status
		 */
		TransactionDTO tdto = new TransactionDTO();
		tdto.setStnars(dto.getFileId());
		tdto.setStktrs(TransactionBO.STKTRS_FILE_SENT);
		
		/*
		 * If the file type is PD, not validate status in SSTRANS.
		 * This will be always APVI because the AS processes do not validate it.
		 */
		if(FILE_TYPE_PD.equalsIgnoreCase(fileType)) {
			DefinedLogger.SERVICE.info("Como es archivo PD, no valido su estado en la SSTRANS");
		} else {

			tdto = transactionBo.findStkest(tdto);

			//Validate if the file status could be obtained
			if(tdto == null
					|| StringUtil.isEmpty(tdto.getStkest())) {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFILESTATUS);
			}

			//Validate if the file still does not validated
			if(tdto.getStkest().trim().equalsIgnoreCase(TransactionBO.STKEST_APVI)) {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_FILESTATUSAPVI);
			}

		}
		
		//Get the ticket ID
		tdto = transactionBo.findStqtrs(tdto);
		dto.setStqtrs(tdto.getStqtrs());
	}

	/**
	 * This method executes the routine to reject the file
	 * @throws ServiceException
	 */
	private void rejectFile() throws ServiceException {
		//Update SSTRANS table with the new status (reject)
		DefinedLogger.SERVICE.info(
				new StringBuffer("Persistiendo en SSTRANS el estado ")
				.append(TransactionBO.STKEST_AREC)
				.append(" para el archivo ")
				.append(dto.getFileId()).toString());
		TransactionDTO tdto = new TransactionDTO();
		tdto.setStnars(dto.getFileId());
		tdto.setStktrs(TransactionBO.STKTRS_FILE_SENT);
		tdto.setStkest(TransactionBO.STKEST_AREC);
		int result = transactionBo.updateFile(tdto);

		//Validate if the file could be deleted in SSTRANS table
		if(result == 0) {
			DefinedLogger.SERVICE.error("Error mientras se ejecutaba el update en SSTRANS para rechazar archivo "
					+ dto.getFileId() + ". Para ver detalles del query mirar el log de queries");
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTREJECTFILE);
		}

		//Load Transaction DTO to register the transaction in the ending of the execution
		DefinedLogger.SERVICE.debug("Actualizando TrasactionDTO global para luego registrar la transaccion con los valores adecuados");

		transactionDto.setStktrs(TransactionBO.SKTRS_FILE_REJECTED);
		transactionDto.setStnars(dto.getFileId());
		transactionDto.setStdobs(TransactionBO.SKTRS_FILE_REJECTED);
		transactionDto.setStqtrs(serviceKey);
		
		//Update the file status in MTVALCAB table
		DefinedLogger.SERVICE.info("Actualizando archivo para eliminar en MTVALCAB");
		bo.update(dto);
	}

	private void executeFile() throws ServiceException {
		//Update SSTRANS table with the new status (reject)
		DefinedLogger.SERVICE.info(
				new StringBuffer("Persistiendo en SSTRANS el estado ")
				.append(TransactionBO.STKEST_AOKB)
				.append(" para el archivo ")
				.append(dto.getFileId()).toString());
		TransactionDTO tdto = new TransactionDTO();
		tdto.setStnars(dto.getFileId());
		tdto.setStktrs(TransactionBO.STKTRS_FILE_SENT);
		tdto.setStkest(TransactionBO.STKEST_AOKB);

		//Validate file type
		int result;
		if(FILE_TYPE_PD.equalsIgnoreCase(fileType)) {
			result = transactionBo.updateFilePD(tdto);
		} else {
			result = transactionBo.updateFile(tdto);
		}

		//Validate if the file could be deleted in SSTRANS table
		if(result == 0) {
			DefinedLogger.SERVICE.error("Error mientras se ejecutaba el update en SSTRANS para autorizar archivo "
					+ dto.getFileId() + ". Para ver detalles del query mirar el log de queries");
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTAUTORIZEDFILE);
		}

		//Load Transaction DTO to register the transaction in the ending of the execution
		DefinedLogger.SERVICE.debug("Actualizando TrasactionDTO global para luego registrar la transaccion con los valores adecuados");
		transactionDto.setStktrs(TransactionBO.STKTRS_FILE_AUTORIZED);
		transactionDto.setStnars(dto.getFileId());
		transactionDto.setStdobs(TransactionBO.STKTRS_FILE_AUTORIZED);
		transactionDto.setStqtrs(serviceKey);
		
		//Update the file status in MTVALCAB table
		DefinedLogger.SERVICE.info("Actualizando archivo para autorizar en MTVALCAB");
		bo.update(dto);

		//Validate if the file type is PD. In this case, we must run a stored procedure instead of to update MTVALCAB
		if(FILE_TYPE_PD.equalsIgnoreCase(fileType)) {
			bo.moveFilePD(dto.getFileId());
		}
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		//		setDataInVector(5, dto.getTable()); //The table name is the file ID (file name)
		//		setDataInVector(6, dto.getFileStatus());
		//		setDataInVector(7, XMLUtil.dtoToXML(accountList));
		setDataInVector(9, Integer.toString(dto.getStqtrs()));
		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}

	/**
	 * This method generates a transaction DTO with the data
	 * by default already loaded
	 * @return
	 * @throws ServiceException
	 */
	@Override
	protected TransactionDTO buildTransactionDto() throws ServiceException {
		return transactionDto;
	}

	/**
	 * This method gets the file type according to the file name
	 * @param fileName
	 * @return
	 */
	private String getFileType(String fileName) {
		if(StringUtil.isEmpty(fileName) || fileName.length() < 2)
			return "";
		return fileName.substring(0,2);
	}
}