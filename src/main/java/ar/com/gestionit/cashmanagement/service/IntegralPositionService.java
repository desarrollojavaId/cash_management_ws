package ar.com.gestionit.cashmanagement.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.IntegralPositionBO;
import ar.com.gestionit.cashmanagement.persistence.bo.TransactionBO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionDetailResumeListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionDetailResumeListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionListAgreementDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionListInputDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionListReferencesDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionPaymentItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionPaymentListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.TransactionDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.NumberUtil;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;
import ar.com.gestionit.cashmanagement.util.StringUtil;
import ar.com.gestionit.cashmanagement.util.XMLUtil;

public class IntegralPositionService extends AbstractService {
	
	private static final String LABEL_IMPORTERECAUDADO = "Importe Recaudado";
	private static final String YEAR = "20";
	/**
	 * Service DTO
	 */
	private IntegralPositionListDTO dto;

	/**
	 * Business object for this service
	 */
	private IntegralPositionBO bo;

	/**
	 * Transaction BO to manage SSTRANS table
	 */
	private TransactionBO transactionBo;

	/**
	 * Payment methods DTO
	 */
	private IntegralPositionPaymentListDTO paymentDto;
	
	/**
	 * DTO to show total amounts according to currency
	 */
	private IntegralPositionDetailResumeListDTO totalAmountReportDto;
	
	/**
	 * This attribute contains the total amount according to currency
	 */
	private HashMap<String, IntegralPositionDetailResumeListItemDTO> currencyMap = new HashMap<String, IntegralPositionDetailResumeListItemDTO>();

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		bo = new IntegralPositionBO();
		transactionBo = new TransactionBO();
		dto = new IntegralPositionListDTO();
		paymentDto = new IntegralPositionPaymentListDTO();
		totalAmountReportDto = new IntegralPositionDetailResumeListDTO();
		totalAmountReportDto.setList(new ArrayList<IntegralPositionDetailResumeListItemDTO>());
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		dto.setAdherent(getDataFromVector(1));
		dto.setAgreementNumber(getDataFromVector(2));
		dto.setAgreementSubnumber(getDataFromVector(3));
		dto.setInput((IntegralPositionListInputDTO) XMLUtil.xmlToDto(getDataFromVector(4), IntegralPositionListInputDTO.class));
		dto.setReferences((IntegralPositionListReferencesDTO) XMLUtil.xmlToDto(getDataFromVector(5), IntegralPositionListReferencesDTO.class));

		/*
		 * Validating --------------------------------------------------
		 */
		if(dto.getInput() == null) dto.setInput(new IntegralPositionListInputDTO());
		if(dto.getReferences() == null) dto.setReferences(new IntegralPositionListReferencesDTO());

		if(StringUtil.isEmpty(dto.getAgreementNumber())) dto.setAgreementNumber("0");
		if(StringUtil.isEmpty(dto.getAgreementSubnumber())) dto.setAgreementSubnumber("0");

		IntegralPositionListInputDTO input = dto.getInput();
		input.setDateRecFrom(ServiceUtil.validateDate(input.getDateRecFrom(), ServiceConstant.SERVICERETURN_KEY_NOTDATEFROM));
		input.setDateRecFrom(input.getDateRecFrom().replace("/", ""));
		if(input.getDateRecFrom().length() == 6){
			input.setDateRecFrom(input.getDateRecFrom().substring(0, 4) + YEAR + input.getDateRecFrom().substring(4, 6));		
		}
		input.setDateRecTo(ServiceUtil.validateDate(input.getDateRecTo(), ServiceConstant.SERVICERETURN_KEY_NOTDATETO));
		input.setDateRecTo(input.getDateRecTo().replace("/", ""));
		if(input.getDateRecTo().length() == 6){
			input.setDateRecTo(input.getDateRecTo().substring(0, 4) + YEAR + input.getDateRecTo().substring(4, 6));
		}
		dto.setInput(input);

		IntegralPositionListReferencesDTO reference = dto.getReferences();
		if(StringUtil.isEmpty(reference.getRefDebtor1())) reference.setRefDebtor1("");
		if(StringUtil.isEmpty(reference.getRefProof1())) reference.setRefProof1("");
		dto.setReferences(reference);
	}

	@Override
	public void execute() throws ServiceException {

		/*
		 * Execute stored procedure to full the data source with the report
		 * and get the request ID
		 */
		dto = bo.loadData(dto);

		/*
		 * Update register status in SSTRANS table
		 */
		updateStatus();

		/*
		 * Get the grouping list with their agreements
		 */
		executeGroupingLoader(); 

		/*
		 * Generate total reports according to payment type
		 */
		paymentDto.setList(bo.findPayment(dto));
		//Calculate total amount
		Iterator<IntegralPositionPaymentItemDTO> i = paymentDto.getList().iterator();
		IntegralPositionPaymentItemDTO payDto;
		while(i.hasNext()) {
			payDto = i.next();
			updateTotal(payDto.getMoneda(), payDto.getImporte(), LABEL_IMPORTERECAUDADO, payDto.getCantChq());
		}

		//Calculate percentages and format amounts 
		Iterator<IntegralPositionPaymentItemDTO> j = paymentDto.getList().iterator();
		IntegralPositionPaymentItemDTO payDtoPorc;
		double porcentaje;
		while(j.hasNext()) {
			payDtoPorc = j.next();
			porcentaje = (Double.parseDouble(payDtoPorc.getImporte()) * 100 / currencyMap.get(payDtoPorc.getMoneda()).getAmount());
			payDtoPorc.setPorcentaje(NumberUtil.amountFormat(porcentaje));
			payDtoPorc.setImporte(NumberUtil.amountFormat(payDtoPorc.getImporte()));
		}
		
		/*
		 * Generate total reports according to total amounts
		 */
		IntegralPositionDetailResumeListItemDTO item;
		for(String key : currencyMap.keySet()) {
			item = currencyMap.get(key);
			item.setImporte(NumberUtil.amountFormat((item.getAmount())));
			item.setImporteExportador(NumberUtil.amountFormatExporter((item.getAmount())));
			totalAmountReportDto.getList().add(item);
		}
	}

	/**
	 * This method updates the register status in SSTRANS table
	 * @throws ServiceException
	 */
	private void updateStatus() throws ServiceException {
		//Generate the Transaction DTO and load it
		TransactionDTO tdto = new TransactionDTO();
		tdto.setStkest("TROK");
		tdto.setStqtrs(serviceKey);

		//Execute update
		transactionBo.updateStkest(tdto);
	}

	/**
	 * This method finds the grouping list (tipo de recaudaciones) from the datasource
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	private void executeGroupingLoader() throws ServiceException {
		DefinedLogger.SERVICE.info("Obteniendo tipo de recaudaciones de la base de datos...");

		//Get data from the datasource
		List<IntegralPositionListItemDTO> list = (List<IntegralPositionListItemDTO>) bo.findList(dto);

		//Validations...
		if(list == null) {
			DefinedLogger.SERVICE.info("No se encontraron tipo de recaudaciones para esta busqueda...");
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}

		if(list.size() == 0) {
			DefinedLogger.SERVICE.info("No se encontraron tipo de recaudaciones para esta busqueda...");
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}

		//Define and initialize variables
		Iterator<IntegralPositionListItemDTO> i = list.iterator();
		IntegralPositionListItemDTO item, grouping = null;
		List<IntegralPositionListItemDTO> groupings = new ArrayList<IntegralPositionListItemDTO>();
		List<IntegralPositionListAgreementDTO> agreements = null;
		IntegralPositionListAgreementDTO agreement;
		String prevGroupingCode = "";
		double totalAmount = 0;

		//Processing data...
		while(i.hasNext()) {
			item = i.next();

			//Validate if this item is a new grouping
			if(!prevGroupingCode.equals(item.getGrouping())) {
				//This is a new grouping

				//Validate if previously a grouping exists
				if(grouping != null) {
					//Associate the previous agreement list to the grouping
					grouping.setAgreements(agreements);

					//Update the current total amount
					grouping.setMontoRec(NumberUtil.amountFormat(totalAmount));
					totalAmount = 0;

					//Associate the previous grouping to the global grouping list
					groupings.add(grouping);
				}

				//Take the new grouping
				grouping = item;
				
				//Format its attributes
				grouping.setGroupingDescription(grouping.getGroupingDescription().trim());
				grouping.setMontoRec(NumberUtil.amountFormat(grouping.getMontoRec()));

				//Generate a new agreement list
				agreements = new ArrayList<IntegralPositionListAgreementDTO>();
			}

			//Generate the new agreement
			agreement = new IntegralPositionListAgreementDTO();
			agreement.setAgreement(item.getAgreement());
			agreement.setAgreementDescription(item.getAgreementDescription().trim());
			agreement.setAmount(item.getMontoRec().replace(",", "."));
			agreement.setSubAgreement(item.getSubAgreement());

			//Associate the new agreement to the current agreement list
			agreements.add(agreement);

			//Update the current total amount
			totalAmount += Double.parseDouble(item.getMontoRec());
		}

		//The last grouping never is associated to the list. So, I do it here.
		if(grouping != null) {
			//Associate the previous agreement list to the grouping
			grouping.setAgreements(agreements);

			//Update the current total amount
			grouping.setMontoRec(NumberUtil.amountFormat(totalAmount));
			
			//Associate the previous grouping to the global grouping list
			groupings.add(grouping);
		}

		dto.setList(groupings);
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");		
		setDataInVector(6, bo.dtoToXML(dto));
		setDataInVector(7, bo.dtoToXML(paymentDto));
		setDataInVector(8, dto.getInput().getRequestNumber());
		setDataInVector(9, XMLUtil.dtoToXML(totalAmountReportDto));

		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}
	
	/**
	 * This method updates the total amount according to currency
	 * @param currency
	 * @param strAmount
	 * @param label
	 */
	protected void updateTotal(String currency, String strAmount, String label, String cantCheques) {
		if (strAmount != null && NumberUtil.isNumeric(strAmount) && Double.parseDouble(strAmount) > 0){
			//Define and initialize variables
			IntegralPositionDetailResumeListItemDTO item;

			//Get item according to the currency
			if(!currencyMap.containsKey(currency)) {
				item = new IntegralPositionDetailResumeListItemDTO();
				item.setAmount(0);
				item.setPorcentaje("100.00");
				item.setCantidad(0);
				item.setDescImp(label);
				item.setMoneda(currency);
				currencyMap.put(currency, item);
			} else {
				item = currencyMap.get(currency);
			}

			//Update amount
			item.setAmount(item.getAmount() + Double.parseDouble(strAmount));
			item.setCantidad(item.getCantidad() + Integer.parseInt(cantCheques));
		}
	}
}