package ar.com.gestionit.cashmanagement.service;


import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.TransactionBO;
import ar.com.gestionit.cashmanagement.persistence.dto.TransactionDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.StringUtil;

/**
 * This service only updates the SSTRANS to indicate that the files was confirmed 
 * @author jdigruttola
 */
public class ConfirmFileService extends AbstractService {
	
	/*
	 * Constants to persist in STKTRS field of the SSTRANS table
	 */
	private static final String INVOKEDTRANSACTION_RECAUD = "Envio_archivo_recaud_con";
	private static final String INVOKEDTRANSACTION_PAGO = "Envio_archivo_pago_con";
	
	/*
	 * Constants to persist in STKPEP field of the SSTRANS table
	 */
	private static final String PEP_SI = "SI";
	private static final String PEP_NO = "NO";
	
	/*
	 * File type list
	 */
	private static final String FILE_TYPE_PB = "PB";
	private static final String FILE_TYPE_PO = "PO";
	private static final String FILE_TYPE_PC = "PC";

	/**
	 * Like this method only updates SSTRANS, this does not need its own BO
	 */
	private TransactionBO transactionBo;

	/**
	 * Use this DTO to update SSTRANS
	 */
	private TransactionDTO transactionDto;

	@Override
	protected void initialize() throws ServiceException {
		transactionBo = new TransactionBO();
		transactionDto = new TransactionDTO();
	}

	@Override
	protected void loadInputs() throws ServiceException {
		transactionDto.setStqadh(Long.parseLong(getDataFromVector(1))); //Adherent
		transactionDto.setStnars(getDataFromVector(2)); //File ID
		
		//Validate if the file type is PO. In this case, the PEP condition must be evaluated
		transactionDto.setStkpep("");
		if(FILE_TYPE_PO.equalsIgnoreCase(getDataFromVector(4))) {
			//Validate the PEP condition
			if("S".equalsIgnoreCase(getDataFromVector(3))) {
				transactionDto.setStkpep(PEP_SI);
			}else{
				transactionDto.setStkpep(PEP_NO);
			}
		}

		//Validate if the request contains the file ID 
		if(StringUtil.isEmpty(transactionDto.getStnars())) {
			throw new ServiceException("");
		}
	}

	@Override
	public void execute() throws ServiceException {
		//Update SSTRANS register to indicate that the file was confirmed
		DefinedLogger.SERVICE.info("El archivo " + transactionDto.getStnars() + " sera confirmado.");

		//Validate if the file is about payments or receipts
		if(isAboutPayment()) {
			transactionDto.setStktrs(INVOKEDTRANSACTION_PAGO);
		} else {
			transactionDto.setStktrs(INVOKEDTRANSACTION_RECAUD);
		}
		
		//Update SSTRANS register with the new service
		transactionDto.setServiceName(ConfirmFileService.class.getSimpleName());

		//Execute update
		int result = transactionBo.confirmFile(transactionDto);

		//Validate if the file could be confirmed in SSTRANS table
		if(result == 0) {
			DefinedLogger.SERVICE.error("Error mientras se ejecutaba el update en SSTRANS para confirmar archivo "
					+ transactionDto.getStnars() + ". Para ver detalles del query mirar el log de queries");
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTCONFIRMFILE);
		}
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		//Notify number of executed queries
		reportQueryNumber(transactionBo.getQueryCounter());
	}
	
	/**
	 * This method validate if the file is about payments or receipts
	 * @return TRUE if the file type is about payments; FALSE if the file type is about receipts
	 */
	private boolean isAboutPayment() {
		String fileType = getFileType(transactionDto.getStnars());
		return FILE_TYPE_PB.equalsIgnoreCase(fileType) || FILE_TYPE_PO.equalsIgnoreCase(fileType) || FILE_TYPE_PC.equalsIgnoreCase(fileType); 
	}

	/**
	 * This method gets the file type according to the file name
	 * @param fileName
	 * @return
	 */
	private String getFileType(String fileName) {
		if(StringUtil.isEmpty(fileName) || fileName.length() < 2)
			return "";
		return fileName.substring(0,2);
	}
}