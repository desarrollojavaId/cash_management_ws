package ar.com.gestionit.cashmanagement.service;

import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.CheckStatusCollectingAccountBO;
import ar.com.gestionit.cashmanagement.persistence.dto.CheckStatusCollectingAccountDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;

/** Servicio de Consulta estado de cuenta recaudadora  **/
public class CheckStatusCollectingAccountService extends AbstractAccountManagerService {
	
	private CheckStatusCollectingAccountDTO dto;
	private List<CheckStatusCollectingAccountDTO> dtoRespList;
	private CheckStatusCollectingAccountBO bo;
	
	@Override
	protected void execute() throws ServiceException {
		
		DefinedLogger.SERVICE.debug("Ejecutando querys y cargando listas...");
		boolean operaCanal = false;
		Integer opera = 0;
		dtoRespList = bo.findAccount(dto);
		int i = 0;
		dto.setIsCash("false");
		dto.setOperateChannel("false");
		for(CheckStatusCollectingAccountDTO dtoResp:dtoRespList) {
			if(dtoResp != null) {
				dto.setIsCash("true");
 				dto.setNroConv(dtoResp.getNroConv());
				dto.setNroSubConv(dtoResp.getNroSubConv());
				opera = bo.findOperateChannel(dtoResp);
			}
			
			if(opera>0) {
				dto.setOperateChannel("true");
				break;
			}
			i++;
		}		
		 
		
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		/*
		 * Validating...
		 * ------------------------------------------------------------------------
		 */
		DefinedLogger.SERVICE.debug("Realizando validaciones...");
		try {
			dto.setAdherent(getDataFromVector(1));
		} catch(Exception e) {
			DefinedLogger.SERVICE.error("No pude convertir a long el adherente para persistir transaccion", e);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTADHERENT);
		}
		
		try {
			dto.setAccount(getDataFromVector(2));
		} catch(Exception e) {
			DefinedLogger.SERVICE.error("No pude convertir a long  la cuenta para persistir la transaccion", e);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_OBLIGATORYACCOUNT);
		}
		
		try {
			dto.setSubaccount(getDataFromVector(3));
		} catch(Exception e) {
			DefinedLogger.SERVICE.error("No pude convertir a long  la sub cuenta para persistir la transaccion", e);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_OBLIGATORYSUBACCOUNT);
		}
		
		try {
			dto.setBranchOffice(getDataFromVector(4));
		} catch(Exception e) {
			DefinedLogger.SERVICE.error("No pude convertir a long la sucursal para persistir la transaccion", e);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_OBLIGATORYBANCHOFFICE);
		}
		
		try {
			dto.setCurrency(getDataFromVector(5));
		} catch(Exception e) {
			DefinedLogger.SERVICE.error("No pude convertir a long  la moneda para persistir la transaccion", e);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_OBLIGATORYCURRENCY);
		}
		
		try {
			dto.setModule(getDataFromVector(6));
		} catch(Exception e) {
			DefinedLogger.SERVICE.error("No pude convertir a long  el modulo para persistir la transaccion", e);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_OBLIGATORYMODULE);
		}
				
		
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs");
		setDataInVector(7, dto.getIsCash());
		setDataInVector(8, dto.getOperateChannel());
		setDataInVector(9, dto.getNroConv());
		setDataInVector(10, dto.getNroSubConv());
		
		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando");
		dto = new CheckStatusCollectingAccountDTO();
		bo = new CheckStatusCollectingAccountBO();
		
	}
}