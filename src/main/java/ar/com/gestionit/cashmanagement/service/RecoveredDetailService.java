package ar.com.gestionit.cashmanagement.service;

import java.util.ArrayList;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.RecoveredDetailBO;
import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredDetailListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredDetailListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StatusInputList2DTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;

public class RecoveredDetailService extends AbstractService {

	/**
	 * Service DTO
	 */
	private RecoveredDetailListDTO ioDto;

	/**
	 * Business object for this service
	 */
	private RecoveredDetailBO bo;

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		bo = new RecoveredDetailBO();
		ioDto = new RecoveredDetailListDTO();
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		ioDto.setAdherent(getDataFromVector(1));
		
		String checkNumber = getDataFromVector(2);
		checkNumber = ServiceUtil.validateNumberAndLength(checkNumber, 10, ServiceConstant.SERVICERETURN_KEY_NOTVALIDCHECKNUMBER);
		ioDto.setCheckNumber(checkNumber);
		
		String a = getDataFromVector(3);
		a = ServiceUtil.validateNumberAndLengthObligatory(a, 7, ServiceConstant.SERVICERETURN_KEY_INVALIDAGREEMENT, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYAGREEMENT);
		ioDto.setAgreementNumber(a);

		String b = getDataFromVector(4);
		b = ServiceUtil.validateNumberAndLengthObligatory(b,7,ServiceConstant.SERVICERETURN_KEY_INVALIDSUBAGREEMENT, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYSUBAGREEMENT);
		ioDto.setAgreementSubnumber(b);

		ioDto.setDateRecoveredFrom(ServiceUtil.validateDateForDepuration(getDataFromVector(5), depurationDate, ServiceConstant.SERVICERETURN_KEY_NOTVALIDDATERECOVFROM));
		ioDto.setDateRecoveredTo(ServiceUtil.validateDate(getDataFromVector(6), ServiceConstant.SERVICERETURN_KEY_NOTVALIDDATERECOVTO));

		String amount = getDataFromVector(7);
		amount = ServiceUtil.validateNumberAndLength(amount, 16, ServiceConstant.SERVICERETURN_KEY_INVALIDAMOUNTFROM);
		ServiceUtil.validateTwoDecimalsMax(amount, ServiceConstant.SERVICERETURN_KEY_INVALIDAMOUNTFROM);
		ioDto.setAmountFrom(amount);

		amount = getDataFromVector(8);
		amount = ServiceUtil.validateNumberAndLength(amount, 16, ServiceConstant.SERVICERETURN_KEY_INVALIDAMOUNTTO);
		ServiceUtil.validateTwoDecimalsMax(amount, ServiceConstant.SERVICERETURN_KEY_INVALIDAMOUNTTO);
		ioDto.setAmountTo(amount);


		StatusInputList2DTO statuses = (StatusInputList2DTO)
				bo.xmlToDto(getDataFromVector(9), StatusInputList2DTO.class);
		if(statuses != null && statuses.getStatuses().size() > 0) {
			ioDto.setStatuses(statuses.getStatuses());
		} else {
			ioDto.setStatuses(new ArrayList<String>());
		}

		bo.enablePager(getDataFromVector(10));
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute() throws ServiceException {
		DefinedLogger.SERVICE.debug("Ejecutando query y cargando lista...");
		/*
		 * Check list
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		ioDto.setList((List<RecoveredDetailListItemDTO>)bo.findList(ioDto));
		
		if(ioDto == null || ioDto.getList().size() == 0){
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}
		
		contentToExport.add(ioDto.getList());
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");
		setDataInVector(11, bo.dtoToXML(ioDto));
		setDataInVector(12, bo.getPagerAsXML());
		

		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}
		
}