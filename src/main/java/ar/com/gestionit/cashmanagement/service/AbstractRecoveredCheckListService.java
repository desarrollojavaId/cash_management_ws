package ar.com.gestionit.cashmanagement.service;

import java.util.ArrayList;
//import java.security.Provider.Service; ?????????????????????????????????????????????????????????
import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.RecoveredCheckListBO;
import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredCheckListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredCheckListItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.ClassUtil;
import ar.com.gestionit.cashmanagement.util.DateUtil;
import ar.com.gestionit.cashmanagement.util.NumberUtil;

public abstract class AbstractRecoveredCheckListService extends AbstractService {
	
	/**
	 * Recovered Check List DTO
	 */
	protected RecoveredCheckListDTO ioDto;

	/**
	 * Business object for this service
	 */
	protected RecoveredCheckListBO bo;
	
	/**
	 * Amount total of the requested checks
	 */
	protected float amountTotal;
	
	/**
	 * This attribute keeps the current number of the check for this recovered
	 */
	protected int checkTotal;
	
	
	@Override
	protected void initialize() throws ServiceException {
		bo = new RecoveredCheckListBO();
		ioDto = new RecoveredCheckListDTO();
	}
	
	@Override
	public void execute() throws ServiceException {
		/*
		 * Recovered check list
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		ioDto = bo.findChecks(ioDto);

		/*
		 * Processing information....
		 * ------------------------------------------------------------------------
		 */
		//Validate if there are checks to show
		if(ioDto != null && ioDto.getList() != null && ioDto.getList().size() > 0) {
			List<RecoveredCheckListItemDTO> list = new ArrayList<RecoveredCheckListItemDTO>();
			Iterator<RecoveredCheckListItemDTO> i = ioDto.getList().iterator();
			RecoveredCheckListItemDTO dto;
			RecoveredCheckListItemDTO listDto;
			while (i.hasNext()) {
				dto = i.next();
				
				if (dto.getRefStatus() != null && dto.getRefStatus().trim().equals("1")) {
					dto.setInProcess("S");
				} else {
					dto.setInProcess("N");
				}

				//TODO: validar si se contempla este campo como OUTPUT o no
//				if (bo.belongToFiservCompany(dto)) {
//					dto.setFirservCompanyMark("S");
//				} else {
//					dto.setFirservCompanyMark("");
//				}
				
				dto.setFecChe(DateUtil.normalizeDateFormat(dto.getFecChe()));
				dto.setDateAcred(DateUtil.normalizeDateFormat(dto.getDateAcred()));
				dto.setDateExpiration(DateUtil.normalizeDateFormat(dto.getDateExpiration()));
				dto.setTransactionDate(DateUtil.normalizeDateFormat(dto.getTransactionDate()));
				if (dto.getSucursalEnv().equals("0")){
					dto.setSucursalEnv("");
				}
				//Calucate amount total of the requested checks
				try{
					listDto = (RecoveredCheckListItemDTO) ClassUtil.copyInstance(dto);
					listDto.setAmount(NumberUtil.twoDecimalMax(Float.parseFloat(listDto.getAmount())));
					list.add(listDto);
				}catch(Exception e){
					e.printStackTrace();
				}
				amountTotal += Float.parseFloat(dto.getAmount());
				checkTotal += 1;
			}
			contentToExport.add(list);
			// Obtener número de pedido: debo correr el stored procedure SS0003
//			String numeroDePedido = UtilCPD.getNumeroPedido(db.getCon(), adh, convenio, subConvenio);
		}else{
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}

	}

	@Override
	protected void loadOutputs() throws ServiceException {
		setDataInVector(15, bo.dtoToXML(ioDto));
		setDataInVector(16, bo.getPagerAsXML());
		
		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}
}