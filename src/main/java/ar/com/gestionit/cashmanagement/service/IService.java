package ar.com.gestionit.cashmanagement.service;

import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.message.ExecuteRequest;
import ar.com.gestionit.cashmanagement.message.ExecuteResponse;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

/**
 * Service interface to implement in each front end service 
 * @author jdigruttola
 */
public interface IService {
	
	/**
	 * This method processes the service routine according to the specified request
	 * and returns the response to send to the front end by the web service
	 * @param request
	 * @return
	 * @throws ServiceException
	 */
	public ExecuteResponse execute(ExecuteRequest request) throws ServiceException;
	
	/**
	 * This method returns the content to export.
	 * This is used only by Exporter service to get the content to export.
	 * @return the contentToExport
	 */
	public List<List<? extends IDTO>> getContentToExport();
	
	/**
	 * This method indicates if the service was called directly or by the file exporter
	 * @param value: TRUE is the service was called by the service; FALSE is the service
	 * was called directly
	 */
	public void setExporterIndicator(boolean value);
}