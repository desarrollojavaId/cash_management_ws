package ar.com.gestionit.cashmanagement.service;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.AnnulmentOpBO;
import ar.com.gestionit.cashmanagement.persistence.dto.AnnulmentOpDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;

public class AnnulmentOpService extends AbstractService {
	
	private static final String INVOKED_FUNCTION = "Registro_anulado";
	
	/**
	 * This attribute is used to load the input parameters
	 */
	private AnnulmentOpDTO input;

	/**
	 * Business object for this service
	 */
	private AnnulmentOpBO bo;


	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		bo = new AnnulmentOpBO();
		input = new AnnulmentOpDTO();
		
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		String adherent = getDataFromVector(1);
		adherent = ServiceUtil.validateNumberAndLengthObligatory(adherent, 7, ServiceConstant.SERVICERETURN_KEY_ADHERENTLENGTHERROR, ServiceConstant.SERVICERETURN_KEY_NOTADHERENT);
		input.setAdherent(adherent);
		
		String paymentNumber = getDataFromVector(2);
		paymentNumber = ServiceUtil.validateNumberAndLengthObligatory(paymentNumber, 8, ServiceConstant.SERVICERETURN_KEY_INVALIDPAYORDER, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYPAYORDER);
		input.setPaymentNumber(paymentNumber);
		
		String paymentSubNumber = getDataFromVector(3);
		paymentSubNumber = ServiceUtil.validateNumberAndLengthObligatory(paymentSubNumber, 3, ServiceConstant.SERVICERETURN_KEY_INVALIDSUBPAYORDER, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYSUBPAYORDER);
		input.setPaymentSubnumber(paymentSubNumber);
		
		input.setUser(request.getDrqus());
	}

	@Override
	public void execute() throws ServiceException {
		DefinedLogger.SERVICE.debug("Ejecutando querys...");
		bo.annulmentEcheq(input);
		
		
		//Set general fields for the file list
//				Date now = new Date();
//				input.setDate(ServiceConstant.SDF_DATE.format(now));
//				input.setHour(ServiceConstant.SDF_HOUR.format(now));
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando lista de outputs...");
		
		
		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
		setInvokedFunction(INVOKED_FUNCTION);
	}
	
	
}