package ar.com.gestionit.cashmanagement.service;


import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.ConvListBO;
import ar.com.gestionit.cashmanagement.persistence.dto.ConvListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ConvListItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.NumberUtil;


public class ConvListService extends AbstractService {
	/**
	 * This attribute is used to load the input parameters
	 */
	private ConvListItemDTO input;

	/**
	 * Business object for this service
	 */
	private ConvListBO bo;

	/**
	 * This is the payment list result to send
	 */
	private String xmlConvList;

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando");
		bo = new ConvListBO();
		input = new ConvListItemDTO();
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando inputs");
		input.setAdh(getDataFromVector(1));	

		//Validate if the adherent is right
		if(input.getAdh() == null
				|| input.getAdh().trim().equals("")
				|| !NumberUtil.isNumeric(input.getAdh())) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTADHERENT);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute() throws ServiceException {
		DefinedLogger.SERVICE.debug("Ejecutando querys y cargando listas");
		ConvListDTO convList = new ConvListDTO();
		convList.setList((List<ConvListItemDTO>) bo.findList(input));
		if(convList == null || convList.getList().size() == 0){
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}
		xmlConvList = bo.dtoToXML(convList);
		contentToExport.add(convList.getList());
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs");
		
		setDataInVector(2, xmlConvList);
		
		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}
}