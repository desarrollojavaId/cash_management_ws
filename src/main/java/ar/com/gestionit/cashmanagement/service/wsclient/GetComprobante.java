//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2015.08.21 a las 04:06:07 PM ART 
//


package ar.com.gestionit.cashmanagement.service.wsclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para getComprobante complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="getComprobante">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="numeroDeOrdenDePago" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="subnumeroDeOrdenDePago" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="numeroMail" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getComprobante", propOrder = {
    "numeroDeOrdenDePago",
    "subnumeroDeOrdenDePago",
    "numeroMail"
})
@XmlRootElement
public class GetComprobante {

    protected String numeroDeOrdenDePago;
    protected String subnumeroDeOrdenDePago;
    protected int numeroMail;

    /**
     * Obtiene el valor de la propiedad numeroDeOrdenDePago.
     * 
     */
    public String getNumeroDeOrdenDePago() {
        return numeroDeOrdenDePago;
    }

    /**
     * Define el valor de la propiedad numeroDeOrdenDePago.
     * 
     */
    public void setNumeroDeOrdenDePago(String value) {
        this.numeroDeOrdenDePago = value;
    }

    /**
     * Obtiene el valor de la propiedad subnumeroDeOrdenDePago.
     * 
     */
    public String getSubnumeroDeOrdenDePago() {
        return subnumeroDeOrdenDePago;
    }

    /**
     * Define el valor de la propiedad subnumeroDeOrdenDePago.
     * 
     */
    public void setSubnumeroDeOrdenDePago(String value) {
        this.subnumeroDeOrdenDePago = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroMail.
     * 
     */
    public int getNumeroMail() {
        return numeroMail;
    }

    /**
     * Define el valor de la propiedad numeroMail.
     * 
     */
    public void setNumeroMail(int value) {
        this.numeroMail = value;
    }

}
