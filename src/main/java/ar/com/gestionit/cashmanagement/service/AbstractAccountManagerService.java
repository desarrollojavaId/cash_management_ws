package ar.com.gestionit.cashmanagement.service;

import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.GeneralAccountBO;
import ar.com.gestionit.cashmanagement.persistence.dto.IAccount;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.StringUtil;


public abstract class AbstractAccountManagerService extends AbstractService {
	
	/**
	 * Under normal conditions, if an account is not in SCCLITIT table, we call QGPL/pZ051114 stored proceudre
	 * to generate the content in SCCLITIT.
	 * But, when the account module is 23, we must send only the account CBU without the internal data.
	 * ESTO ES POR DEFINICION DEL BANCO
	 */
	protected static final String ACCOUNT_MODULE_FOR_EXCEPTION = "23";
	
	GeneralAccountBO accountBO;
	
	/**
	 * This method validates if the cash account data is empty or not
	 * @param dto
	 * @return TRUE if the cash account data is not empty or FALSE in other case
	 */
	protected boolean validateCashAccount(IAccount dto) {
		if(dto == null
			|| StringUtil.isEmpty(dto.getCuenta())
			|| StringUtil.isEmpty(dto.getModulo())
			|| StringUtil.isEmpty(dto.getMoneda())
			|| StringUtil.isEmpty(dto.getSuboperacion())
			|| StringUtil.isEmpty(dto.getSucursal())
				) {
			return false;
		}
		return true;
	}
	
	/**
	 * This method validates the result given by the data source
	 * NOTE: the account data can be empty. So, we must run an stored procedure to complete the data source
	 * and access to the data source again 
	 * @param dto
	 * @throws ServiceException 
	 */
	
	protected void validateResult(List<? extends IAccount> dto, String adherent) throws ServiceException {
		//Validate if there are accounts
		if(dto == null || dto.size() == 0)
			return;
		
		//Define and initialize variables
		accountBO = new GeneralAccountBO();
		
		//Validate if the account data is empty
		Iterator<? extends IAccount> i = dto.iterator();
		IAccount item;
		IAccount aux;
		while(i.hasNext()) {
			item = i.next();
			
			//If the account data is empty, we must run the stored procedures and try again
			if(!validateCashAccount(item)) {
				aux = loadAccount(item, adherent);
				if(aux != null) {
					item.setModulo(aux.getModulo());
					item.setSucursal(aux.getSucursal());
					item.setMoneda(aux.getMoneda());
					item.setCuenta(aux.getCuenta());
					item.setSuboperacion(aux.getSuboperacion());
				}
			}
			
			/*
			 * When the account module is 23 and the account with this module is not in SCCLITIT,
			 * we must send only the account CBU without the internal data.
			 * ESTO ES POR DEFINICION DEL BANCO
			 */
			if(item != null && item.getModulo().trim().equals(ACCOUNT_MODULE_FOR_EXCEPTION)) {
				item.setModulo("0");
				item.setSucursal("0");
				item.setMoneda("0");
				item.setSuboperacion("0");
			}
		}
	}
	
	/**
	 * This method runs the stored procedure to load the data source and access
	 * to the data source again to get the account data
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	protected IAccount loadAccount(IAccount dto, String adherent)  throws ServiceException {
		//Validate if the DTO is right
		if(dto == null || StringUtil.isEmpty(dto.getCbu()))
			throw new ServiceException (ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT);
		
		//Try to get the account data from the data source again
		dto.setAdherent(adherent);
		return accountBO.findAccount(dto);
	}
}