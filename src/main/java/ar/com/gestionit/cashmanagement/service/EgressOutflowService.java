package ar.com.gestionit.cashmanagement.service;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.EgressOutflowBO;
import ar.com.gestionit.cashmanagement.persistence.dto.EgressOutflowCheckDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.EgressOutflowDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.EgressOutflowItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.NumberUtil;

/**
 * Consulta Flujo de egresos
 * @author jdigruttola
 */
public class EgressOutflowService extends AbstractService {

	/**
	 * Service DTO
	 */
	private EgressOutflowDTO dto;

	/**
	 * Business object for this service
	 */
	private EgressOutflowBO bo;

	/**
	 * This is the egress outflow list result to send
	 */
	private String xmlEgressOutflow;

	/**
	 * Pager attributes
	 */
	private String xmlCheck;


	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		bo = new EgressOutflowBO();
		dto = new EgressOutflowDTO();
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando inputs...");
		dto.setAdherent(getDataFromVector(1));
		
		/*
		 * Try to assign Service Key (own key) as request ID.
		 * This must be different to zero and must have 15 digits as maximum.
		 * As serviceKey variable is an INT, so this always is less than 15 digits
		 */
		if(serviceKey != 0) {
			dto.setRequestId(String.valueOf(serviceKey));
		} else {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTVALIDREQUESTID);
		}
	}

	@Override
	public void execute() throws ServiceException {
		DefinedLogger.SERVICE.debug("Ejecutando querys y cargando listas...");
		bo.executeLoader(dto);
		
		dto.setList(bo.findEgress(dto));
		xmlEgressOutflow = bo.dtoToXML(dto);
		
		EgressOutflowCheckDTO checkDto = new EgressOutflowCheckDTO();
		checkDto.setList(bo.findChecks(dto));
		if (checkDto == null || checkDto.getList().size() == 0){
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}
		for (EgressOutflowItemDTO idto : dto.getList()){
			idto.setDay1Exporter(NumberUtil.amountFormatExporter(idto.getDay1()));
			idto.setDay2Exporter(NumberUtil.amountFormatExporter(idto.getDay2()));
			idto.setDay3Exporter(NumberUtil.amountFormatExporter(idto.getDay3()));
			idto.setDay4Exporter(NumberUtil.amountFormatExporter(idto.getDay4()));
			idto.setDay5Exporter(NumberUtil.amountFormatExporter(idto.getDay5()));
			idto.setOldAmountExporter(NumberUtil.amountFormatExporter(idto.getOldAmount()));
			idto.setNextAmountExporter(NumberUtil.amountFormatExporter(idto.getNextAmount()));
		}	
		
		for (EgressOutflowItemDTO idto : checkDto.getList()){
			idto.setDay1Exporter(NumberUtil.amountFormatExporter(idto.getDay1()));
			idto.setDay2Exporter(NumberUtil.amountFormatExporter(idto.getDay2()));
			idto.setDay3Exporter(NumberUtil.amountFormatExporter(idto.getDay3()));
			idto.setDay4Exporter(NumberUtil.amountFormatExporter(idto.getDay4()));
			idto.setDay5Exporter(NumberUtil.amountFormatExporter(idto.getDay5()));
			idto.setOldAmountExporter(NumberUtil.amountFormatExporter(idto.getOldAmount()));
			idto.setNextAmountExporter(NumberUtil.amountFormatExporter(idto.getNextAmount()));
		}	
		
		xmlCheck = bo.dtoToXML(checkDto);
		contentToExport.add(dto.getList());
		contentToExport.add(checkDto.getList());
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");
		setDataInVector(2, xmlEgressOutflow);
		setDataInVector(3, xmlCheck);

		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}
}