package ar.com.gestionit.cashmanagement.service;

import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.EscrowForSendBO;
import ar.com.gestionit.cashmanagement.persistence.dto.EscrowForSendDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.EscrowForSendItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.EscrowInputListDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

public class EscrowForSendService extends AbstractService{

	/**
	 * Business object for this service
	 */
	private EscrowForSendBO bo;
	/**
	 * Input DTO
	 */
	private EscrowInputListDTO input;
	/**
	 * DTO
	 */
	private EscrowForSendDTO dto;

	@Override
	protected void validateBefore() throws ServiceException {
		//Validate if the reques is NULL
		if(request == null) {
			try {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTREQUEST);
			} catch(ServiceException e) {
				throw e;
			}
		}
	}
	@Override
	protected void initialize() throws ServiceException {
		bo = new EscrowForSendBO();
		input = new EscrowInputListDTO();
		dto = new EscrowForSendDTO();
	}

	@Override
	protected void loadInputs() throws ServiceException {

		EscrowInputListDTO ssncta = (EscrowInputListDTO)
				bo.xmlToDto(getDataFromVector(2), EscrowInputListDTO.class);

		if (ssncta != null && ssncta.getSsncta() != null){
			input.setSsncta(ssncta.getSsncta());
		}else{
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_OBLIGATORYACCOUNT);
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	protected void execute() throws ServiceException {
		/*
		 * Executing query to get the escrow list...
		 */
		dto.setList((List<EscrowForSendItemDTO>)bo.findList(input));
		if (dto == null || dto.getList().size() == 0){
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}

		/*
		 * Determining the enabled interfaces...
		 */
		for(EscrowForSendItemDTO item : dto.getList()) {
			//Validate if the status is Vuelco Habilitado (V)
			if(item.getSskest() != null && !item.getSskest().trim().equalsIgnoreCase(ServiceConstant.EscrowStatus.VUELCO_HABILITADO.getCode())) {
				//Disable Vuelco Inicial interface
				item.setiVuelcoInicial(ServiceConstant.NO);
				
				//Active Cobranza interface
				item.setiCobranza(ServiceConstant.YES);

				//Validate if the Seguros interface is enabled
				if(item.getSskseg() != null
						&& item.getSskseg().trim().equalsIgnoreCase(ServiceConstant.YES)
						&& item.getSskests() != null
						&& item.getSskests().trim().equalsIgnoreCase(ServiceConstant.EscrowStatus.ACTIVO.getCode())) {
					item.setiSeguros(ServiceConstant.YES);
				}

				//Validate if the Revolving interface is enabled
				if(item.getSskrev() != null
						&& item.getSskrev().trim().equalsIgnoreCase(ServiceConstant.YES)
						&& item.getSskestr() != null
						&& item.getSskestr().trim().equalsIgnoreCase(ServiceConstant.EscrowStatus.ACTIVO.getCode())) {
					item.setiRevolving(ServiceConstant.YES);
				}
			}
		}
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		setDataInVector(3, bo.dtoToXML(dto));
	}
}
