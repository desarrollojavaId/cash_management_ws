package ar.com.gestionit.cashmanagement.service;


import java.util.LinkedList;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.AdherentListForMepBO;
import ar.com.gestionit.cashmanagement.persistence.dto.AdherentListForMepDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.AdherentListForMepOutputDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.AdherentListForMepOutputItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.XMLUtil;

/**
 * Service for "Consulta de adherentes habilitados para MEP"
 * @author jdigruttola
 */
public class AdherentListForMepService extends AbstractService{
	/**
	 * Service DTOs
	 */
	private AdherentListForMepDTO input;
	private AdherentListForMepOutputDTO output;
	
	/**
	 * Business object for this service
	 */
	private AdherentListForMepBO bo;

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		output = new AdherentListForMepOutputDTO();
		bo = new AdherentListForMepBO();
	}

	@Override
	protected void loadInputs() throws ServiceException {
		/*
		 * This is a workaround because buildTransaction() method
		 * will try to get the adherent from the position 1 of the
		 * vectors. So, we  
		 * FIXME: I think that a better way could exist
		 */
		setDataInVector(1, "0");
		
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		input = (AdherentListForMepDTO) XMLUtil.xmlToDto(getDataFromVector(2), AdherentListForMepDTO.class);
		if(input == null || input.getList() == null || input.getList().size() == 0) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTADHERENTFORMEP);
		}
		
	}		

	@SuppressWarnings("unchecked")
	@Override
	protected void execute() throws ServiceException {
		
		/*
		 * Executing query....
		 * ------------------------------------------------------------------------
		 */
		
		DefinedLogger.SERVICE.debug("Ejecutando querys y cargando listas...");		
		List<AdherentListForMepOutputItemDTO> adherents = (List<AdherentListForMepOutputItemDTO>) bo.findList(input);
		
		/*
		 * Processing information....
		 * ------------------------------------------------------------------------
		 * Validate if there are adherents who are not enabled for MEP
		 * So, we must iterate all the adherents given by request and validate if they are in the result (adherents variable)
		 */
		//Iterate adherents given by request
		boolean found;
		List<AdherentListForMepOutputItemDTO> disabledAdherents = new LinkedList<AdherentListForMepOutputItemDTO>();
		for(String adherentInput : input.getList()) {
			found = false;
			//Iterate adherents given by the query execution
			if(adherents != null) {
				for(AdherentListForMepOutputItemDTO adherentOutput : adherents) {
					//Validate if the adherent given by request is in the result of query execution
					if(adherentOutput.getId().trim().equals(adherentInput)) {
						found = true;
						break;
					}
				}
			}
			
			//Validate if the adherent was not found
			if(!found) {
				//In this case, I must add in the result list as adherent who is not enabled for MEP
				disabledAdherents.add(new AdherentListForMepOutputItemDTO(adherentInput, "N"));
			}
		}
		adherents.addAll(disabledAdherents);
		output.setAdherents(adherents);
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");	
		setDataInVector(3, XMLUtil.dtoToXML(output));
	}
	
	/**
	 * We override this method to avoid the adherent validation
	 * and create a fictional adherent to can work
	 */
	@Override
	protected void validateBefore() throws ServiceException {
		//Validate if the reques is NULL
		if(request == null) {
			try {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTREQUEST);
			} catch(ServiceException e) {
				throw e;
			}
		}
	}
}