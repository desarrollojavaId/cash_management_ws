package ar.com.gestionit.cashmanagement.service;

import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.EscrowListBO;
import ar.com.gestionit.cashmanagement.persistence.dto.EscrowInputListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.EscrowListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.EscrowListItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

public class EscrowListService extends AbstractService{
	
	/**
	 * Business object for this service
	 */
	private EscrowListBO bo;
	/**
	 * Input DTO
	 */
	private EscrowInputListDTO input;
	/**
	 * DTO
	 */
	private EscrowListDTO dto;
	
	@Override
	protected void validateBefore() throws ServiceException {
		//Validate if the reques is NULL
		if(request == null) {
			try {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTREQUEST);
			} catch(ServiceException e) {
				throw e;
			}
		}
	}
	@Override
	protected void initialize() throws ServiceException {
		bo = new EscrowListBO();
		input = new EscrowInputListDTO();
		dto = new EscrowListDTO();
	}
	
	@Override
	protected void loadInputs() throws ServiceException {
		
		/*
		 * This is a workaround because buildTransaction() method
		 * will try to get the adherent from the position 1 of the
		 * vectors. So, we  
		 * FIXME: I think that a better way could exist
		 */
		setDataInVector(1, "0");
		
		EscrowInputListDTO ssncta = (EscrowInputListDTO)
				bo.xmlToDto(getDataFromVector(2), EscrowInputListDTO.class);
		
		if (ssncta != null && ssncta.getSsncta() != null){
			input.setSsncta(ssncta.getSsncta());
		}else{
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_OBLIGATORYACCOUNT);
		}
		
		//Enable pager in business object level
		bo.enablePager(getDataFromVector(4));
	}
	@SuppressWarnings("unchecked")
	@Override
	protected void execute() throws ServiceException {
		dto.setList((List<EscrowListItemDTO>)bo.findList(input));
		if (dto == null || dto.getList().size() == 0){
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}
		
		/*
		 * Determining the enabled interfaces...
		 */
		for(EscrowListItemDTO item : dto.getList()) {
			//Validate if the status is Vuelco Habilitado (V)
			if(item.getSskest() != null && !item.getSskest().trim().equalsIgnoreCase(ServiceConstant.EscrowStatus.VUELCO_HABILITADO.getCode())) {
				//Disable Vuelco Inicial interface
				item.setiVuelcoInicial(ServiceConstant.NO);
				
				//Active Cobranza interface
				item.setiCobranza(ServiceConstant.YES);

				//Validate if the Seguros interface is enabled
				if(item.getSskseg() != null
						&& item.getSskseg().trim().equalsIgnoreCase(ServiceConstant.YES)
						&& item.getSskests() != null
						&& item.getSskests().trim().equalsIgnoreCase(ServiceConstant.EscrowStatus.ACTIVO.getCode())) {
					item.setiSeguros(ServiceConstant.YES);
				}

				//Validate if the Revolving interface is enabled
				if(item.getSskrev() != null
						&& item.getSskrev().trim().equalsIgnoreCase(ServiceConstant.YES)
						&& item.getSskestr() != null
						&& item.getSskestr().trim().equalsIgnoreCase(ServiceConstant.EscrowStatus.ACTIVO.getCode())) {
					item.setiRevolving(ServiceConstant.YES);
				}
			}
		}
		
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		setDataInVector(3, bo.dtoToXML(dto));
		setDataInVector(5, bo.getPagerAsXML());
		
		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}
}
