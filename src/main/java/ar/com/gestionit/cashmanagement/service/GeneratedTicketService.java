package ar.com.gestionit.cashmanagement.service;

import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.GeneratedTicketBO;
import ar.com.gestionit.cashmanagement.persistence.dto.GeneratedTicketDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.GeneratedTicketListItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;

public class GeneratedTicketService extends AbstractService{
	
	GeneratedTicketBO bo;
	GeneratedTicketDTO dto;

	@Override
	protected void initialize() throws ServiceException {
		bo = new GeneratedTicketBO();
		dto = new GeneratedTicketDTO();
		
	}
	@Override
	protected void loadInputs() throws ServiceException {
		String cuit = getDataFromVector(2);
		cuit = ServiceUtil.validateNumberAndLengthObligatory(cuit, 11,ServiceConstant.SERVICERETURN_KEY_INVALIDCUIT , ServiceConstant.SERVICERETURN_KEY_OBLIGATORYCUIT);
		dto.setCuit(cuit);
		
		bo.enablePager(getDataFromVector(4));
	}
	@SuppressWarnings("unchecked")
	@Override
	protected void execute() throws ServiceException {
		dto.setList(((List<GeneratedTicketListItemDTO>)bo.findList(dto)));
		
	}
	@Override
	protected void loadOutputs() throws ServiceException {
		setDataInVector(3, bo.dtoToXML(dto));
		setDataInVector(4, bo.getPagerAsXML());
	}


}
