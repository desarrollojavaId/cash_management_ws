package ar.com.gestionit.cashmanagement.service.filemanager;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"fileId",
		"tokenId"
})
@XmlRootElement(name = ServiceConstant.DTO_FILE)
public class FileManagerOutputDTO implements Serializable {
	
	private static final long serialVersionUID = 4969943919872788248L;
	
	@XmlElement(name=ServiceConstant.TAG_ID, defaultValue="")
	private String fileId; 
	@XmlElement(name=ServiceConstant.TAG_TOKEN, defaultValue="")
	private String tokenId;
	/**
	 * @return the fileId
	 */
	public String getFileId() {
		return fileId;
	}
	/**
	 * @param fileId the fileId to set
	 */
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	/**
	 * @return the tokenId
	 */
	public String getTokenId() {
		return tokenId;
	}
	/**
	 * @param tokenId the tokenId to set
	 */
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
}