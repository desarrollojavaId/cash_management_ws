package ar.com.gestionit.cashmanagement.service;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.SecurityTicketBO;
import ar.com.gestionit.cashmanagement.persistence.dto.SecurityTicketDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;

public class SecurityTicketService extends AbstractService{
	
	public static final String LEYENDA = "Por motivos de seguridad, le solicitamos imprimir el presente ticket y presentarlo en el Banco al momento de retirar el pago, caso contrario el mismo no podrá ser entregado";
	SecurityTicketDTO dto;
	SecurityTicketBO bo;
	
	@Override
	protected void initialize() throws ServiceException {
		dto = new SecurityTicketDTO();
		bo = new SecurityTicketBO();
		
	}
	@Override
	protected void loadInputs() throws ServiceException {
		
		String cuitBenef = getDataFromVector(2);
		String cuit = getDataFromVector(3); 
		String nroOp = getDataFromVector(4);
		String subNroOp = getDataFromVector(5);
		
		cuit = ServiceUtil.validateNumberAndLength(cuit, 11, ServiceConstant.SERVICERETURN_KEY_INVALIDCUIT);
		dto.setCuit(cuit);
		
		cuitBenef = ServiceUtil.validateNumberAndLengthObligatory(cuitBenef, 11, ServiceConstant.SERVICERETURN_KEY_INVALIDCUIT, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYCUIT);
		dto.setCuitBeneficiario(cuitBenef);
		
		nroOp = ServiceUtil.validateNumberAndLengthObligatory(nroOp, 8, ServiceConstant.SERVICERETURN_KEY_INVALIDPAYORDER, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYPAYORDER);
		dto.setNroOp(nroOp);
		
		subNroOp = ServiceUtil.validateNumberAndLengthObligatory(subNroOp, 3, ServiceConstant.SERVICERETURN_KEY_INVALIDSUBPAYORDER, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYSUBPAYORDER);
		dto.setSubNroOp(subNroOp);
		 
	}
	@Override
	protected void execute() throws ServiceException {
		dto = bo.find(dto);
	}
	@Override
	protected void loadOutputs() throws ServiceException {
		setDataInVector(6, dto.getMtlcos());
		setDataInVector(7, dto.getMtlfer());
		setDataInVector(8, dto.getMtlqop());
		setDataInVector(9, dto.getMtlqrc());
		setDataInVector(10, dto.getMtljpa());
		setDataInVector(11, dto.getMtosuc());
		setDataInVector(12, dto.getMtxdom());
		setDataInVector(13, dto.getMtxhoa());
		setDataInVector(14, LEYENDA);
	}

	/**
	 * I override this method to avoid the adherent validation
	 */
	@Override
	protected void validateBefore() throws ServiceException {
		//Validate if the reques is NULL
		if(request == null) {
			try {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTREQUEST);
			} catch(ServiceException e) {
				throw e;
			}
		}
	}
}