package ar.com.gestionit.cashmanagement.service;

import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.DictionaryBO;
import ar.com.gestionit.cashmanagement.persistence.dto.DictionaryListItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;


public class DictionaryService extends AbstractService{
	
	/**
	 * Constants to generate the dictionary list
	 */
	private static final String FIELD_SEPARATOR = "|-|";
	private static final String REGISTER_SEPARATOR = ",-,";
	
	/**
	 * Business object for this service
	 */
	private DictionaryBO bo;	
	
	/**
	 * This attribute contains the result for the response
	 * according to the expected format
	 */
	private StringBuffer dictionaryList;
	
	@Override
	protected void initialize() throws ServiceException {
		bo = new DictionaryBO();
		dictionaryList = new StringBuffer("");
	}
	
	@Override
	protected void loadInputs() throws ServiceException {
	}

	@Override
	protected void execute() throws ServiceException {
		/*
		 * Define an initialize variables 
		 */
		DictionaryListItemDTO item;
		List<DictionaryListItemDTO> list;
		Iterator<DictionaryListItemDTO> i;
		
		/*
		 * Get dictionary
		 */
		list = bo.findDic();
		if(list == null || list.size() == 0) {
			//TODO
		}
		
		/*
		 * Format result for response
		 */
		i = list.iterator();
		while(i.hasNext()) {
			item = i.next();
			dictionaryList.append(item.getSschar().trim())
				.append(FIELD_SEPARATOR)
				.append(item.getSsvalu().trim())
				.append(REGISTER_SEPARATOR);
		}
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		setDataInVector(3, dictionaryList.toString());
	}
	
	/**
	 * This method is override to avoid the adherent validations
	 * @throws ServiceException
	 */
	@Override
	protected void validateBefore() throws ServiceException {
		//Validate if the request is NULL
		if(request == null) {
			try {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTREQUEST);
			} catch(ServiceException e) {
				throw e;
			}
		}
	}
}