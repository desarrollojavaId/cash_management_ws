package ar.com.gestionit.cashmanagement.service;

import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.factory.ServiceReturnFactory;
import ar.com.gestionit.cashmanagement.persistence.bo.FileReceptionBO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileReceptionListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileReceptionListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileReceptionStatusesDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileReceptionTypesDTO;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;

public class FileReceptionService extends AbstractService {
	/**
	 * This attribute is used to load the input parameters
	 */
	private FileReceptionListItemDTO input;
	
	/**
	 * Business object for this service
	 */
	private FileReceptionBO bo;

	/**
	 * This is the payment list result to send
	 */
	private String xmlFileReception;

	/**
	 * Pager attributes
	 */
	private String xmlPager;
	
	private static final String RECEPCIONADO = "Ya Recepcionado";
	private static final String NUNCA_RECEPCIONADO = "Nunca Recepcionado";

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		bo = new FileReceptionBO();
		input = new FileReceptionListItemDTO();
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parámetros de entrada...");
		String a = getDataFromVector(1);
		a = ServiceUtil.validateNumberAndLengthObligatory(a, 7, ServiceConstant.SERVICERETURN_KEY_NOTADHERENT, ServiceConstant.SERVICERETURN_KEY_ADHERENTLENGTHERROR);
		input.setAdherent(a);
		
		input.setAgreementNumber(getDataFromVector(2));
		
		input.setAgreementSubnumber(getDataFromVector(3));
		DefinedLogger.SERVICE.debug("Cargando tipos de archivo...");
		FileReceptionTypesDTO typesId = (FileReceptionTypesDTO) bo.xmlToDto(getDataFromVector(5), FileReceptionTypesDTO.class);
		if(typesId != null && typesId.getTypesId() != null) 
			input.setTypesId(typesId.getTypesId());
		
		DefinedLogger.SERVICE.debug("Cargando estados de archivo...");
		FileReceptionStatusesDTO statusId = (FileReceptionStatusesDTO) bo.xmlToDto(getDataFromVector(6), FileReceptionStatusesDTO.class);
		if(statusId != null && statusId.getStatusId() != null) 
			input.setStatusId(statusId.getStatusId());
		
		DefinedLogger.SERVICE.debug("Cargando fechas...");
		String dateFrom = getDataFromVector(7);
		String dateTo = getDataFromVector(8);
		input.setFechaDesde(ServiceUtil.validateDateForDepuration(dateFrom, depurationDate, ServiceConstant.SERVICERETURN_KEY_NOTDATEFROM));
		input.setFechaHasta(ServiceUtil.validateDate(dateTo, ServiceConstant.SERVICERETURN_KEY_NOTDATETO));
		ServiceUtil.validateDateInterval(input.getFechaDesde(), input.getFechaHasta());
		
		DefinedLogger.SERVICE.debug("Tratamiento de tipo de archivo...");
		input.setsPServ(getDataFromVector(11));
		if (input.getsPServ().equalsIgnoreCase("0")){
			input.setsPServ("P");
		}else if (input.getsPServ().equalsIgnoreCase("1")){
			input.setsPServ("R");
		}else{
			DefinedLogger.SERVICE.debug(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_PAYMENTORDEBT));
			throw new  ServiceException(ServiceConstant.SERVICERETURN_KEY_PAYMENTORDEBT);
		}

		bo.enablePager(getDataFromVector(10));
	}


	@SuppressWarnings("unchecked")
	@Override
	public void execute() throws ServiceException {
		DefinedLogger.SERVICE.debug("Ejecutando querys y cargnado listas...");
		FileReceptionListDTO fileReceptionList = new FileReceptionListDTO();
		
		fileReceptionList.setList((List<FileReceptionListItemDTO>) bo.findList(input));
		if (fileReceptionList == null || fileReceptionList.getList().size() == 0){
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}
		
		xmlFileReception = bo.dtoToXML(fileReceptionList);
		xmlPager = bo.getPagerAsXML();
		for (FileReceptionListItemDTO dto : fileReceptionList.getList()){
			if(!dto.getSakest().isEmpty() && dto.getSbdres().equalsIgnoreCase(RECEPCIONADO)){
				dto.setExportSakest("1");
			}if(!dto.getSakest().isEmpty() && dto.getSbdres().equalsIgnoreCase(NUNCA_RECEPCIONADO)){
				dto.setExportSakest("2");
			}
		}
		contentToExport.add(fileReceptionList.getList());

	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");
		
		setDataInVector(9, xmlFileReception);
		setDataInVector(12, xmlPager);
		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());

	}
}