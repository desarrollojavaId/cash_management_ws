package ar.com.gestionit.cashmanagement.service;

import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.MadePaymentListBO;
import ar.com.gestionit.cashmanagement.persistence.dto.MadePaymentListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.MadePaymentListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ReferenceDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DateUtil;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.NumberUtil;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;
import ar.com.gestionit.cashmanagement.util.StringUtil;
import ar.com.gestionit.cashmanagement.util.XMLUtil;

/**
 * Consulta de pagos realizados
 * @author jdigruttola
 *
 */
public class MadePaymentListService extends AbstractService {

	/**
	 * Service DTO
	 */
	private MadePaymentListDTO dto;
	
	/**
	 * Business object for this service
	 */
	private MadePaymentListBO bo;
	
	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		bo = new MadePaymentListBO();
		dto = new MadePaymentListDTO();
	}

	@Override
	protected void loadInputs() throws ServiceException {
		//Loading....
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		dto.setRequestNumber("0");
		dto.setCompany(StringUtil.valueOrZero(getDataFromVector(2)));
		dto.setAgreementNumber(StringUtil.valueOrZero(getDataFromVector(3)));
		dto.setAgreementSubnumber(StringUtil.valueOrZero(getDataFromVector(4)));
		dto.setChannel(StringUtil.valueOrZero(getDataFromVector(5)));
		dto.setAmountFrom(StringUtil.valueOrZero(getDataFromVector(6)));
		dto.setAmountTo(StringUtil.valueOrZero(getDataFromVector(7)));
		dto.setDateFrom(StringUtil.valueOrZero(getDataFromVector(8)));
		dto.setDateTo(StringUtil.valueOrZero(getDataFromVector(9)));
		dto.setReferences((ReferenceDTO)XMLUtil.xmlToDto(getDataFromVector(10), ReferenceDTO.class));
		
		//Validating...
		if(!StringUtil.isEmptyOrZero(dto.getChannel()) && !NumberUtil.isNumeric(dto.getChannel())) {
			DefinedLogger.SERVICE.error(ServiceConstant.SERVICERETURN_KEY_INVALIDCHANNEL);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_INVALIDCHANNEL);
		}
		if(!StringUtil.isEmptyOrZero(dto.getAgreementNumber()) && !NumberUtil.isNumeric(dto.getAgreementNumber())) {
			DefinedLogger.SERVICE.error(ServiceConstant.SERVICERETURN_KEY_INVALIDAGREEMENT);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_INVALIDAGREEMENT);
		}
		if(!StringUtil.isEmptyOrZero(dto.getAgreementSubnumber()) && !NumberUtil.isNumeric(dto.getAgreementSubnumber())) {
			DefinedLogger.SERVICE.error(ServiceConstant.SERVICERETURN_KEY_INVALIDSUBAGREEMENT);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_INVALIDSUBAGREEMENT);
		}
		if(!StringUtil.isEmptyOrZero(dto.getAmountFrom()) && !NumberUtil.isNumeric(dto.getAmountFrom())) {
			DefinedLogger.SERVICE.error(ServiceConstant.SERVICERETURN_KEY_INVALIDAMOUNTFROM);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_INVALIDAMOUNTFROM);
		}
		if(!StringUtil.isEmptyOrZero(dto.getAmountTo()) && !NumberUtil.isNumeric(dto.getAmountTo())) {
			DefinedLogger.SERVICE.error(ServiceConstant.SERVICERETURN_KEY_INVALIDAMOUNTTO);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_INVALIDAMOUNTTO);
		}
		if(!StringUtil.isEmptyOrZero(dto.getAmountFrom()) && !StringUtil.isEmptyOrZero(dto.getAmountTo())) {
			ServiceUtil.validateNumberInterval(dto.getAmountFrom(), dto.getAmountTo());
		}
		if(!StringUtil.isEmptyOrZero(dto.getDateFrom()) && !DateUtil.isDate(dto.getDateFrom(), ServiceConstant.SDF_DATE_FOR_INPUT)) {
			DefinedLogger.SERVICE.error(ServiceConstant.SERVICERETURN_KEY_NOTDATEFROM);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTDATEFROM);
		}
		if(!StringUtil.isEmptyOrZero(dto.getDateTo()) && !DateUtil.isDate(dto.getDateTo(), ServiceConstant.SDF_DATE_FOR_INPUT)) {
			DefinedLogger.SERVICE.error(ServiceConstant.SERVICERETURN_KEY_NOTDATETO);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTDATETO);
		}
		if(!StringUtil.isEmptyOrZero(dto.getDateFrom()) && !StringUtil.isEmptyOrZero(dto.getDateTo())) {
			ServiceUtil.validateDateInterval(dto.getDateFrom(), dto.getDateTo());
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void execute() throws ServiceException {
		/*
		 * Execute SP to generate the report
		 */
		DefinedLogger.SERVICE.debug("Ejecutando querys...");
		dto.setRequestNumber(bo.generateReport(dto));
		
		/*
		 * Get the channel description
		 */
		dto.setChannelDescription(bo.findChannelDescription(dto));
		
		/*
		 * Get the made payments 
		 */
		dto.setList((List<MadePaymentListItemDTO>) bo.findList(dto));
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");		
		setDataInVector(11, XMLUtil.dtoToXML(dto));
		
		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}
}