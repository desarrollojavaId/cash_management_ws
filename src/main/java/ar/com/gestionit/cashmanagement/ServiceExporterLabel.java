package ar.com.gestionit.cashmanagement;

import java.util.Properties;

import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.FactoryUtil;

/**
 * This class is used by Exporter service.
 * This contains the labels to generate the report in the files.
 * 
 * @author jdigruttola
 */
public class ServiceExporterLabel {
	
	/**
	 * This constant contains the properties file name
	 */
	public static final String PROPERTIES_FILE = "service.export.label.properties";
	
	/**
	 * This static variable contains all the labels possibles
	 */
	public static Properties properties = null;

	/**
	 * This method loads the application properties in main memory
	 * @throws Exception
	 */
	public static void loadPorperties() throws Exception {
		if(properties != null)
			return;

		if(properties != null) {
			DefinedLogger.CONTEXT.warn("Las propiedades ya estan cargadas en la aplicacion");
		} else {
			properties = FactoryUtil.loadProperties(PROPERTIES_FILE);
		}
	}

	/**
	 * This method gets a property of the properties
	 * of the application according to the key given by argument
	 * @param key
	 * @return
	 */
	public static String getProperty(String key) {
		if(properties == null)
			return null;
		return properties.getProperty(key);
	}
}