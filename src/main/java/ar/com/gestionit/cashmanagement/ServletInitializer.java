package ar.com.gestionit.cashmanagement;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

import ar.com.gestionit.cashmanagement.exception.FatalException;
import ar.com.gestionit.cashmanagement.factory.ServiceFactory;
import ar.com.gestionit.cashmanagement.factory.ServiceReturnFactory;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;

public class ServletInitializer extends SpringBootServletInitializer {
	
	private static final String MYBATIS_APP_CONFIG = "mybatis-config.xml";
	private static final String MYBATIS_FILEMANAGER_CONFIG = "mybatis-filemanager-config.xml";

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		try {
			//Load mapped services
			ServiceFactory.load();

			//Load mapped service return list
			ServiceReturnFactory.load();

			//Load global properties of the application
			CashManagementWsApplication.loadPorperties();
			
			//Service ID for the data source
			ServiceId.loadPorperties();
			
			//Service Exporter label for generate the files and the mapped codes for the exporter
			ServiceExporterLabel.loadPorperties();
			ServiceExporterCode.loadPorperties();
			
			// Load App Mybatis context
			CashManagementWsApplication.sqlSessionFactory = loadMybatisContext(MYBATIS_APP_CONFIG,
					CashManagementWsApplication.globalProperties.getProperty(ServiceConstant.PROP_DATASOURCE_APP_USER),
					CashManagementWsApplication.globalProperties.getProperty(ServiceConstant.PROP_DATASOURCE_APP_PASS));
			
			// Load File Manager Mybatis context
			CashManagementWsApplication.fileManagerSessionFactory = loadMybatisContext(MYBATIS_FILEMANAGER_CONFIG,
					CashManagementWsApplication.globalProperties.getProperty(ServiceConstant.PROP_DATASOURCE_FM_USER),
					CashManagementWsApplication.globalProperties.getProperty(ServiceConstant.PROP_DATASOURCE_FM_PASS));
		} catch (IOException e) {
			DefinedLogger.CONTEXT.error("No se pudo iniciar el contexto de Mybatis en la aplicacion", e);
			System.exit(-1);
		} catch (FatalException e) {
			DefinedLogger.CONTEXT.error(e.getMessage(), e);
			System.exit(-1);
		} catch (Exception e) {
			DefinedLogger.CONTEXT.error(e.getMessage(), e);
			System.exit(-1);
		}
		return application.sources(CashManagementWsApplication.class);
	}
	
	/**
	 * This method loads MyBatis contexts given by the file in the argument
	 * @param configurationFile
	 * @param username
	 * @param password
	 * @throws IOException
	 */
	private SqlSessionFactory loadMybatisContext(String configurationFile, String username, String password) throws IOException {
		DefinedLogger.CONTEXT.info("Cargando contexto de Maybatis definido en " + configurationFile);
		
		//Load the customized properties
		Properties properties = new Properties();
		properties.setProperty("username", username);
		properties.setProperty("password", password);
		
		//File context
		InputStream inputStream = Resources.getResourceAsStream(configurationFile);
		return new SqlSessionFactoryBuilder().build(inputStream, properties);
	}
}