package ar.com.gestionit.cashmanagement.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;

/**
 * This class contains static methods for general usage about classes 
 * @author jdigruttola
 *
 */
public class ClassUtil {

	/**
	 * This method generate a new instance equal to the instance given by argument
	 * @param obj
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	public static Object copyInstance(Object obj) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		if(obj == null)
			return null;
		
		Class<?> clazz = obj.getClass();
		Object result = clazz.newInstance();
		
		for(Field field : clazz.getDeclaredFields()) {
			try {
				BeanUtils.setProperty(
						result,
						field.getName(),
						BeanUtils.getProperty(obj, field.getName()));
			} catch(NoSuchMethodException e) {}
		}
		return result;
	}
}
