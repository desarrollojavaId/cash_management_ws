package ar.com.gestionit.cashmanagement.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Class methods for Factory usage
 * @author jdigruttola
 */
public class FactoryUtil {
	/**
	 * This method loads the services mapped to the application runtime
	 */
	public static Properties loadProperties(String propertyFile) throws Exception {
		Properties properties = new Properties();

		//Load the properties file and its respective validation
		InputStream is = FactoryUtil.class.getClassLoader().getResourceAsStream(propertyFile);
		if(is == null) {
			throw new Exception("The service returns properties file could not be loaded rightly");
		}

		//Load the properties in main memory and its respective validation
		try {
			properties.load(is);
		} catch (IOException e) {
			throw new Exception("The service returns properties could not be loaded in main memory", e);
		}

		try {
			is.close();
		} catch (IOException e) {
			DefinedLogger.CONTEXT.warn("Input stream instance could not be closed.");
		}
		
		return properties;
	}
}
