package ar.com.gestionit.cashmanagement.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.factory.ServiceReturnFactory;
import ar.com.gestionit.cashmanagement.persistence.dto.StatusInputListItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

public class ServiceUtil {
	
	public static void validateTwoDecimalsMax (String s, String errorKey) throws ServiceException{
		if (!NumberUtil.twoDecimalMax(s))
			throw new ServiceException(errorKey);
	}
	
	/**
	 * This static method validates if the 's'
	 * if it's numeric or not
	 * and if it has puntuctuation simbols
	 * @param from, to, errorKey
	 * @return
	 */
	public static String validateNumberWithoutPunctuation (String s, String errorKey) throws ServiceException{
		try { 
			Long.parseLong(s);
		return s;	
		
		}catch(NumberFormatException e){ 
			throw new ServiceException(errorKey);
		}
		
	}
	
	/**
	 * This static method validates if the 's'
	 * exists, is shorter than the length, and
	 * if it's numeric or not
	 * @param from, to, errorKey
	 * @return
	 */
	public static String validateNumberAndLengthObligatory(String s, long length, String errorKey, String errorKeyObligatory) throws ServiceException{
		if (s == null || s.equals(""))
			throw new ServiceException(errorKeyObligatory);

		return  validateNumberAndLength(s, length, errorKey);

	}
	
	/**
	 * This static method validates if the 's'
	 * is shorter than the length, and
	 * if it's numeric or not
	 * @param from, to, errorKey
	 * @return
	 */
	public static String validateNumberAndLength(String s, long length, String errorKey) throws ServiceException{
					
		s = NumberUtil.getNumberOrZero(s, errorKey);
		s = validateLength(s, length, errorKey);
		
		return s;
	}
	
	/**
	 * This static method validates if the 's'
	 * is shorter than the length, and
	 * if it's numeric or not. This method is used
	 * in specific cases of amounts strings.
	 * @param from, to, errorKey
	 * @return
	 */
	public static String validateNumberAndLengthForAmounts(String s, long length, String errorKey) throws ServiceException{
					
		s = NumberUtil.getNumberOrZeroForAmounts(s, errorKey);
		s = validateLengthForAmounts(s, length, errorKey);
		
		return s;
	}
	
	/**
	 * This static method validates if the value 'from'
	 * is before'to'
	 * @param from, to, errorKey
	 * @return
	 */
	public static void validateInterval (String from, String to, String errorKey) throws ServiceException {
		if(Double.parseDouble(from) > Double.parseDouble(to)){
		if(NumberUtil.isNumeric(from)
				&& NumberUtil.isNumeric(to)
				&& from.compareTo(to) > 0) {
			throw new ServiceException(errorKey);
		}
	}
	}
	/**
	 * This method is used to validate the dates given as inputs
	 * @param date
	 * @param errorKey
	 * @return
	 * @throws ServiceException
	 */
	public static String validateDate(String date, String errorKey) throws ServiceException {
		Date d;
		//Validate according to the input format
		if (StringUtil.isEmpty(date)) {
			return "0";
		} else if((d = DateUtil.getDate(date, ServiceConstant.SDF_AMERICAN_DATE)) == null) {
			throw new ServiceException(errorKey);
		}
		return ServiceConstant.SDF_DATE_FOR_INPUT.format(d);
	}
	
	/**
	 * This method is used to measure the length of the String 
	 * @param date
	 * @param errorKey
	 * @return 
	 * @return
	 * @throws ServiceException
	 */
	public static String validateLength(String variable, long length, String errorKey) throws ServiceException{
		if (variable != null && variable.length() > length){
			throw new ServiceException(errorKey);
		}
		if(variable == null || variable.isEmpty() || variable.equalsIgnoreCase("")){
			variable = "0";
		}
		return variable;
	}
	/**
	 * This method is used to measure the length of the String
	 * when the sring is an amount.
	 * @param date
	 * @param errorKey
	 * @return 
	 * @return
	 * @throws ServiceException
	 */
	public static String validateLengthForAmounts(String variable, long length, String errorKey) throws ServiceException{
		if (variable != null && variable.length() > length){
			throw new ServiceException(errorKey);
		}
		if(variable == null || variable.isEmpty() || variable.equalsIgnoreCase("")){
			variable = "-1";
		}
		return variable;
	}
	/**
	 * This method is used only for the "date from" which must greater than depuration date
	 * NOTE: it is not for all the "dates from" necessarily
	 * @param date
	 * @param errorKey
	 * @return
	 * @throws ServiceException
	 */
	public static String validateDateForDepuration(String date, Date depurationDate, String errorKey) throws ServiceException {
		if(depurationDate == null)
			return "0";
		
		Date d;
		//Validate according to the input format
		if (StringUtil.isEmpty(date) && depurationDate != null) {
				d = depurationDate;
		} else if((d = DateUtil.getDate(date, ServiceConstant.SDF_AMERICAN_DATE)) == null) {
			throw new ServiceException(errorKey);
		}

		//Validate if the date is before the depuration date 
		if(depurationDate != null && d.before(depurationDate)) {
			d = depurationDate;
		}

		return ServiceConstant.SDF_DATE_FOR_INPUT.format(d);
	}

	/**
	 * This method throws a ServiceException if the date interval is not valid
	 * @param from
	 * @param to
	 * @throws ServiceException
	 */
	public static void validateDateInterval(String from, String to) throws ServiceException {
		if(from == null || to == null)
			return;

		Date f = null;
		Date t = null;
		try {
			f = ServiceConstant.SDF_DATE_FOR_INPUT.parse(from);
			t = ServiceConstant.SDF_DATE_FOR_INPUT.parse(to);
		} catch(ParseException e) {
			return;
		}

		if(!DateUtil.validateDateInterval(f, t)) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_DATEINTERVALNOTVALID);
		}
	}
	
	/**
	 * This method throws a ServiceException if the date interval is not valid
	 * @param from
	 * @param to
	 * @throws ServiceException
	 */
	public static void validateDateInterval(String from, String to, SimpleDateFormat sdf) throws ServiceException {
		if(from == null || to == null)
			return;

		Date f = null;
		Date t = null;
		try {
			f = sdf.parse(from);
			t = sdf.parse(to);
		} catch(ParseException e) {
			return;
		}

		if(!DateUtil.validateDateInterval(f, t)) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_DATEINTERVALNOTVALID);
		}
	}

	/**
	 * This method throws a ServiceException if the number interval is not valid
	 * @param from
	 * @param to
	 * @throws ServiceException
	 */
	public static void validateNumberInterval(String from, String to) throws ServiceException {
		if(from == null || to == null || from.trim().equals("0") || to.trim().equals("0"))
			return;

		float f, t;
		try {
			f = Float.parseFloat(from);
			t = Float.parseFloat(to);
		} catch(NumberFormatException e) {
			return;
		}

		validateNumberInterval(f, t);
	}
	
	/**
	 * This method throws a ServiceException if the number interval is not valid
	 * @param from
	 * @param to
	 * @throws ServiceException
	 */
	public static void validateNumberInterval(int from, int to) throws ServiceException {
		validateNumberInterval((float)from, (float)to);
	}
	
	/**
	 * This method throws a ServiceException if the number interval is not valid
	 * @param from
	 * @param to
	 * @throws ServiceException
	 */
	public static void validateNumberInterval(long from, long to) throws ServiceException {
		validateNumberInterval((float)from, (float)to);
	}
	
	/**
	 * This method throws a ServiceException if the number interval is not valid
	 * @param from
	 * @param to
	 * @throws ServiceException
	 */
	public static void validateNumberInterval(float from, float to) throws ServiceException {
		if(!NumberUtil.validateNumberInterval(from, to)) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_AMOUNTINTERVALNOTVALID);
		}
	}
	
	/**
	 * This method is used to validate the number given as inputs
	 * @param n
	 * @param errorKey
	 * @throws ServiceException
	 */
	public static void validateNumber(String n, String errorKey) throws ServiceException {
		if (!NumberUtil.isNumeric(n))
			throw new ServiceException(errorKey);
	}
	
	public static List<StatusInputListItemDTO> getStatusInput(List<String> statuses) throws ServiceException {
		List<StatusInputListItemDTO> result = new ArrayList<StatusInputListItemDTO>();
		if(statuses != null && statuses.size() > 0) {
			Iterator<String> i = statuses.iterator();
			String item;
			int index;
			StatusInputListItemDTO status;
			while(i.hasNext()) {
				item = i.next();
				//obtenemos el indice del - y reemplazamos el contenido del atributo por el id
				index = item.indexOf(ServiceConstant.FIELD_ID_SEPARATOR);
				if (index != -1) {
				status = new StatusInputListItemDTO();
				status.setNumber(item.substring(0, index));
				if(index > 0 && index < item.length()) {
//					status.setSubnumber(item.substring(index + 1, item.length()));
				}

				if(!NumberUtil.isNumeric(status.getNumber())) {
					DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_INVALIDSTATUS));
					throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_INVALIDSTATUS);
				}
//				if(!NumberUtil.isNumeric(status.getSubnumber())) {
//					DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_INVALIDSTATUS));
//					throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_INVALIDSTATUS);
//				}
				
				result.add(status);
				}else{
					throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_INVALIDSTATUS);
				}
			}
		}
		return result;
	}
	
	/**
	 * This method is used to validate the dates given as inputs
	 * @param date
	 * @param errorKey
	 * @return
	 * @throws ServiceException
	 */
	public static String validateDateDDMMYYYY(String date, String errorKey) throws ServiceException {
		Date d;
		//Validate according to the input format
		if (StringUtil.isEmpty(date)) {
			return "0";
		} else if((d = DateUtil.getDate(date, ServiceConstant.SDF_AMERICAN_DATE)) == null) {
			throw new ServiceException(errorKey);
		}
		return ServiceConstant.SDF_DDMMYYYY.format(d);
	}
}