package ar.com.gestionit.cashmanagement.util;

import org.apache.log4j.Logger;

public class DefinedLogger {
	/**
	 * Tabulator to format the text in logging
	 */
	public static final String TABULATOR = "\t";
	
	/**
	 * This logger is used to trace all about the services
	 */
	public static final Logger SERVICE = Logger.getLogger("Service");
	
	/**
	 * This logger reports all about the web service context
	 */
	public static final Logger CONTEXT = Logger.getLogger("Context");
	
	/**
	 * This logger reports all about the application performance
	 */
	public static final Logger PERFORMANCE = Logger.getLogger("Performance");
}