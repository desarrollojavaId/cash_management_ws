package ar.com.gestionit.cashmanagement.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * 
 * @author jdigruttola
 *
 */
public class EncryptorUtil {

	public  static final String DESEDE_ENCRYPTION_SCHEME = "DESede";
	public  static final String DES_ENCRYPTION_SCHEME    = "DES";
	public  static final String DEFAULT_ENCRYPTION_KEY	 = "La cucaracha, la cucaracha, ya no puede caminar...";
	private static final String	UNICODE_FORMAT			 = "UTF8";

	private KeySpec				keySpec;
	private SecretKeyFactory	keyFactory;
	private Cipher				cipher;
	
	/**
	 * Constructor by default
	 * @throws EncryptionException
	 */
	public EncryptorUtil() throws EncryptionException {
		this(DES_ENCRYPTION_SCHEME);
	}
	
	/**
	 * Parameterized constructor
	 * @param encryptionScheme
	 * @throws EncryptionException
	 */
	public EncryptorUtil( String encryptionScheme ) throws EncryptionException	{
		this( encryptionScheme, DEFAULT_ENCRYPTION_KEY );
	}

	/**
	 * Parameterized constructor
	 * @param encryptionScheme
	 * @param encryptionKey
	 * @throws EncryptionException
	 */
	public EncryptorUtil( String encryptionScheme, String encryptionKey ) throws EncryptionException {

		if ( encryptionKey == null )
			throw new IllegalArgumentException("Clave de Encripci�n nula");
		if ( encryptionKey.trim().length() < 24 )
			throw new IllegalArgumentException("La Clave de encripci�n tiene menos de 24 caracteres");

		try	{
			byte[] keyAsBytes = encryptionKey.getBytes( UNICODE_FORMAT );

			if ( encryptionScheme.equals( DESEDE_ENCRYPTION_SCHEME) ) {
				keySpec = new DESedeKeySpec( keyAsBytes );
			} else if ( encryptionScheme.equals( DES_ENCRYPTION_SCHEME ) ) {
				keySpec = new DESKeySpec( keyAsBytes );
			} else	{
				throw new IllegalArgumentException( "Esquema de encriptaci�n no soportado: "	+ encryptionScheme );
			}

			keyFactory = SecretKeyFactory.getInstance( encryptionScheme );
			cipher     = Cipher.getInstance( encryptionScheme );

		} catch (InvalidKeyException e)	{
			throw new EncryptionException( e );
		} catch (UnsupportedEncodingException e) {
			throw new EncryptionException( e );
		} catch (NoSuchAlgorithmException e) {
			throw new EncryptionException( e );
		} catch (NoSuchPaddingException e) {
			throw new EncryptionException( e );
		}
	}
	
	/**
	 * This method encrypts the string given by argument
	 * @param unencryptedString
	 * @return
	 * @throws EncryptionException
	 */
	public String encrypt( String unencryptedString ) throws EncryptionException {
		if ( unencryptedString == null || unencryptedString.trim().length() == 0 )
			throw new IllegalArgumentException("El string a encriptar esta vacio o es nulo" );

		try	{
			SecretKey key = keyFactory.generateSecret( keySpec );
			cipher.init( Cipher.ENCRYPT_MODE, key );
			byte[] cleartext  = unencryptedString.getBytes( UNICODE_FORMAT );
			byte[] ciphertext = cipher.doFinal( cleartext );

			return new String(Base64.encodeBase64(ciphertext));
		} catch (Exception e) {
			throw new EncryptionException( e );
		}
	}
	
	/**
	 * This method decrypts the string given by argument 
	 * @param encryptedString
	 * @return
	 * @throws EncryptionException
	 */
	public String decrypt( String encryptedString ) throws EncryptionException	{
		if ( encryptedString == null || encryptedString.trim().length() <= 0 )
			throw new IllegalArgumentException( "El string encriptado est� vacio o es nulo" );

		try	{
			SecretKey key = keyFactory.generateSecret( keySpec );
			cipher.init( Cipher.DECRYPT_MODE, key );
			byte[] cleartext  = Base64.decodeBase64(encryptedString);
			byte[] ciphertext = cipher.doFinal( cleartext );

			return bytes2String( ciphertext );
		} catch (Exception e) {
			throw new EncryptionException( e );
		}
	}

	private static String bytes2String( byte[] bytes )	{
		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < bytes.length; i++)	{
			stringBuffer.append( (char) bytes[i] );
		}
		return stringBuffer.toString();
	}

	/**
	 * The exception managed in this class is embedded
	 * @author jdigruttola
	 */
	public static class EncryptionException extends Exception {
		/**
		 * Serial version
		 */
		private static final long serialVersionUID = 1L;

		public EncryptionException( Throwable t ) {
			super( t );
		}
	}
}