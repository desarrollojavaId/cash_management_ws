package ar.com.gestionit.cashmanagement.util;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

/**
 * This class is used for date management 
 * @author jdigruttola
 */
public class XMLUtil {
	
	/**
	 * These charsets are used to encode and decode XML-Object by JAXB
	 */
	private static String MARSHALLER_CHARSET = "ISO-8859-1";
	private static String UNMARSHALLER_CHARSET = "UTF-8";

	/**
	 * This method converts a DTO implementation to XML string.
	 * If DTO is NULL, this returns an string empty ("")
	 * @param DTO implementation
	 * @throws ServiceException
	 * @return XML string
	 */
	public static String dtoToXML(Object dto) throws ServiceException {
		try {
			if(dto == null)
				return "";

			StringWriter writer = new StringWriter();
			JAXBContext jaxbContext = JAXBContext.newInstance(dto.getClass());
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			/*
			 *  output pretty printed
			 */
			//La linea comentada sirve para deshabilitar el tag XML que se genera por cada conversion
			//			jaxbMarshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, MARSHALLER_CHARSET);
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(dto, writer);

			return writer.toString();
		} catch (JAXBException e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORDTOTOXML, e);
		}
	}

	/**
	 * Este método convierte de XML a DTO
	 * @param data
	 * @param cls
	 * @return Objeto partiendo del class recibido como parametro.
	 * @throws ServiceException
	 */
	public static Object xmlToDto(String data, Class<?> cls) throws ServiceException {
		if(data == null || data.trim().equals(""))
			return null;
		Object obj;
		try {
			obj = Class.forName(cls.getName()).newInstance();
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORDTOTOXML, e);
		} 
		try {
			ByteArrayInputStream bais = new ByteArrayInputStream(data.getBytes(UNMARSHALLER_CHARSET));
			JAXBContext jaxbContext = JAXBContext.newInstance(cls);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			obj = jaxbUnmarshaller.unmarshal(bais);

		} catch (JAXBException e) {
			e.printStackTrace();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORDTOTOXML, e);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORDTOTOXML, e);
		}
		return obj;
	}
	
	/**
	 * This method escapes the characters < for &lt; and > for &gt;
	 * @param s
	 * @return
	 */
	public static String replaceTags(String s) {
		return s == null ? null : s.replace("<", "&lt;").replace(">", "&gt;");
	}
}