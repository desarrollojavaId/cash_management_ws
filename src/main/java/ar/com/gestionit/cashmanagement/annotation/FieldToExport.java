package ar.com.gestionit.cashmanagement.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is used to identify which are the fields to export
 * for each service.
 * 
 * NOTE: the file which has the column name is service.export.label.properties
 * @author jdigruttola
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD) //Can use in field only.
public @interface FieldToExport {
	
	/**
	 * This attribute defines the column header name.
	 * It is mandatory
	 * @return
	 */
	public String name();
	
	/**
	 * This attribute defines if the column has dynamic title. In this case, the DTO must have
	 * other attribute with the same attribute name plus "Title" and its content must indicate the
	 * column title.
	 * 
	 * ---------------------------------------------------------------------------
	 * Example:
	 * If there is an attribute in the DTO as the following:
	 * 		@ FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_REF1DEUDOR, hasDynamicTitle=true)
	 * 		private String name;
	 * where the attribute content is name="Juan Perez"
	 * 
	 * We must generate the following attribute:
	 *  	private String nameTitle;
	 * where the attribute content is nameTitle="Nombre y apellido"
	 * 
	 * So, the exported file should generate in this mode:
	 * |Nombre y apellido|
	 * |Juan Perez|
	 * ---------------------------------------------------------------------------
	 * 
	 * @return
	 */
	public boolean hasDynamicTitle() default false;
	
	/**
	 * This attribute defines if the column is required/mandatory or not.
	 * @return
	 */
	public boolean required() default true;
}