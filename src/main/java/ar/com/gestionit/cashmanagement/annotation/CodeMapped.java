package ar.com.gestionit.cashmanagement.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is used to identify the fields to export
 * which must be replaced for a description mapped according to the code
 * 
 * NOTE: the file which has the values is service.export.codemapped.properties
 * @author jdigruttola
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD) //Can use in field only.
public @interface CodeMapped {
}